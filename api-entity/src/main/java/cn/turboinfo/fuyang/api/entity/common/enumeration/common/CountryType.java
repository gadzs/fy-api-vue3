package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public enum CountryType implements BaseEnum {
    DEFAULT(0, "未知", "", ""),
    CN(156, "中国", "China", "CHN"),
    RU(643, "俄罗斯", "Russian Federation", "RUS"),
    US(840, "美国", "United States of America", "USA"),
    GB(826, "英国", "United Kingdom of Great Britain and Northern Ireland", "GBR"),
    FR(250, "法国", "France", "FRA"),
    AF(4, "阿富汗", "Afghanistan", "AFG"),
    AX(248, "奥兰", "Åland Islands", "ALA"),
    AL(8, "阿尔巴尼亚", "Albania", "ALB"),
    DZ(12, "阿尔及利亚", "Algeria", "DZA"),
    AS(16, "美属萨摩亚", "American Samoa", "ASM"),
    AD(20, "安道尔", "Andorra", "AND"),
    AO(24, "安哥拉", "Angola", "AGO"),
    AI(660, "安圭拉", "Anguilla", "AIA"),
    AQ(10, "南极洲", "Antarctica", "ATA"),
    AG(28, "安提瓜和巴布达", "Antigua and Barbuda", "ATG"),
    AR(32, "阿根廷", "Argentina", "ARG"),
    AM(51, "亚美尼亚", "Armenia", "ARM"),
    AW(533, "阿鲁巴", "Aruba", "ABW"),
    AU(36, "澳大利亚", "Australia", "AUS"),
    AT(40, "奥地利", "Austria", "AUT"),
    AZ(31, "阿塞拜疆", "Azerbaijan", "AZE"),
    BS(44, "巴哈马", "Bahamas", "BHS"),
    BH(48, "巴林", "Bahrain", "BHR"),
    BD(50, "孟加拉国", "Bangladesh", "BGD"),
    BB(52, "巴巴多斯", "Barbados", "BRB"),
    BY(112, "白俄罗斯", "Belarus", "BLR"),
    BE(56, "比利时", "Belgium", "BEL"),
    BZ(84, "伯利兹", "Belize", "BLZ"),
    BJ(204, "贝宁", "Benin", "BEN"),
    BM(60, "百慕大", "Bermuda", "BMU"),
    BT(64, "不丹", "Bhutan", "BTN"),
    BO(68, "玻利维亚", "Bolivia (Plurinational State of)", "BOL"),
    BQ(535, "荷兰加勒比区", "Bonaire, Sint Eustatius and Saba", "BES"),
    BA(70, "波黑", "Bosnia and Herzegovina", "BIH"),
    BW(72, "博茨瓦纳", "Botswana", "BWA"),
    BV(74, "布韦岛", "Bouvet Island", "BVT"),
    BR(76, "巴西", "Brazil", "BRA"),
    IO(86, "英属印度洋领地", "British Indian Ocean Territory", "IOT"),
    BN(96, "文莱", "Brunei Darussalam", "BRN"),
    BG(100, "保加利亚", "Bulgaria", "BGR"),
    BF(854, "布基纳法索", "Burkina Faso", "BFA"),
    BI(108, "布隆迪", "Burundi", "BDI"),
    CV(132, "佛得角", "Cabo Verde", "CPV"),
    KH(116, "柬埔寨", "Cambodia", "KHM"),
    CM(120, "喀麦隆", "Cameroon", "CMR"),
    CA(124, "加拿大", "Canada", "CAN"),
    KY(136, "开曼群岛", "Cayman Islands", "CYM"),
    CF(140, "中非", "Central African Republic", "CAF"),
    TD(148, "乍得", "Chad", "TCD"),
    CL(152, "智利", "Chile", "CHL"),
    CX(162, "圣诞岛", "Christmas Island", "CXR"),
    CC(166, "科科斯（基林）群岛", "Cocos (Keeling) Islands", "CCK"),
    CO(170, "哥伦比亚", "Colombia", "COL"),
    KM(174, "科摩罗", "Comoros", "COM"),
    CG(178, "刚果共和国", "Congo", "COG"),
    CD(180, "刚果民主共和国", "Congo (Democratic Republic of the)", "COD"),
    CK(184, "库克群岛", "Cook Islands", "COK"),
    CR(188, "哥斯达黎加", "Costa Rica", "CRI"),
    CI(384, "科特迪瓦", "Côte d'Ivoire", "CIV"),
    HR(191, "克罗地亚", "Croatia", "HRV"),
    CU(192, "古巴", "Cuba", "CUB"),
    CW(531, "库拉索", "Curaçao", "CUW"),
    CY(196, "塞浦路斯", "Cyprus", "CYP"),
    CZ(203, "捷克", "Czechia", "CZE"),
    DK(208, "丹麦", "Denmark", "DNK"),
    DJ(262, "吉布提", "Djibouti", "DJI"),
    DM(212, "多米尼克", "Dominica", "DMA"),
    DO(214, "多米尼加", "Dominican Republic", "DOM"),
    EC(218, "厄瓜多尔", "Ecuador", "ECU"),
    EG(818, "埃及", "Egypt", "EGY"),
    SV(222, "萨尔瓦多", "El Salvador", "SLV"),
    GQ(226, "赤道几内亚", "Equatorial Guinea", "GNQ"),
    ER(232, "厄立特里亚", "Eritrea", "ERI"),
    EE(233, "爱沙尼亚", "Estonia", "EST"),
    SZ(748, "斯威士兰", "Eswatini", "SWZ"),
    ET(231, "埃塞俄比亚", "Ethiopia", "ETH"),
    FK(238, "福克兰群岛", "Falkland Islands (Malvinas)", "FLK"),
    FO(234, "法罗群岛", "Faroe Islands", "FRO"),
    FJ(242, "斐济", "Fiji", "FJI"),
    FI(246, "芬兰", "Finland", "FIN"),
    GF(254, "法属圭亚那", "French Guiana", "GUF"),
    PF(258, "法属波利尼西亚", "French Polynesia", "PYF"),
    TF(260, "法属南部和南极领地", "French Southern Territories", "ATF"),
    GA(266, "加蓬", "Gabon", "GAB"),
    GM(270, "冈比亚", "Gambia", "GMB"),
    GE(268, "格鲁吉亚", "Georgia", "GEO"),
    DE(276, "德国", "Germany", "DEU"),
    GH(288, "加纳", "Ghana", "GHA"),
    GI(292, "直布罗陀", "Gibraltar", "GIB"),
    GR(300, "希腊", "Greece", "GRC"),
    GL(304, "格陵兰", "Greenland", "GRL"),
    GD(308, "格林纳达", "Grenada", "GRD"),
    GP(312, "瓜德罗普", "Guadeloupe", "GLP"),
    GU(316, "关岛", "Guam", "GUM"),
    GT(320, "危地马拉", "Guatemala", "GTM"),
    GG(831, "根西", "Guernsey", "GGY"),
    GN(324, "几内亚", "Guinea", "GIN"),
    GW(624, "几内亚比绍", "Guinea-Bissau", "GNB"),
    GY(328, "圭亚那", "Guyana", "GUY"),
    HT(332, "海地", "Haiti", "HTI"),
    HM(334, "赫德岛和麦克唐纳群岛", "Heard Island and McDonald Islands", "HMD"),
    VA(336, "梵蒂冈", "Holy See", "VAT"),
    HN(340, "洪都拉斯", "Honduras", "HND"),
    HK(344, "香港", "Hong Kong", "HKG"),
    HU(348, "匈牙利", "Hungary", "HUN"),
    IS(352, "冰岛", "Iceland", "ISL"),
    IN(356, "印度", "India", "IND"),
    ID(360, "印尼", "Indonesia", "IDN"),
    IR(364, "伊朗", "Iran (Islamic Republic of)", "IRN"),
    IQ(368, "伊拉克", "Iraq", "IRQ"),
    IE(372, "爱尔兰", "Ireland", "IRL"),
    IM(833, "马恩岛", "Isle of Man", "IMN"),
    IL(376, "以色列", "Israel", "ISR"),
    IT(380, "意大利", "Italy", "ITA"),
    JM(388, "牙买加", "Jamaica", "JAM"),
    JP(392, "日本", "Japan", "JPN"),
    JE(832, "泽西", "Jersey", "JEY"),
    JO(400, "约旦", "Jordan", "JOR"),
    KZ(398, "哈萨克斯坦", "Kazakhstan", "KAZ"),
    KE(404, "肯尼亚", "Kenya", "KEN"),
    KI(296, "基里巴斯", "Kiribati", "KIR"),
    KP(408, "朝鲜", "Korea (Democratic People's Republic of)", "PRK"),
    KR(410, "韩国", "Korea (Republic of)", "KOR"),
    KW(414, "科威特", "Kuwait", "KWT"),
    KG(417, "吉尔吉斯斯坦", "Kyrgyzstan", "KGZ"),
    LA(418, "老挝", "Lao People's Democratic Republic", "LAO"),
    LV(428, "拉脱维亚", "Latvia", "LVA"),
    LB(422, "黎巴嫩", "Lebanon", "LBN"),
    LS(426, "莱索托", "Lesotho", "LSO"),
    LR(430, "利比里亚", "Liberia", "LBR"),
    LY(434, "利比亚", "Libya", "LBY"),
    LI(438, "列支敦士登", "Liechtenstein", "LIE"),
    LT(440, "立陶宛", "Lithuania", "LTU"),
    LU(442, "卢森堡", "Luxembourg", "LUX"),
    MO(446, "澳门", "Macao", "MAC"),
    MG(450, "马达加斯加", "Madagascar", "MDG"),
    MW(454, "马拉维", "Malawi", "MWI"),
    MY(458, "马来西亚", "Malaysia", "MYS"),
    MV(462, "马尔代夫", "Maldives", "MDV"),
    ML(466, "马里", "Mali", "MLI"),
    MT(470, "马耳他", "Malta", "MLT"),
    MH(584, "马绍尔群岛", "Marshall Islands", "MHL"),
    MQ(474, "马提尼克", "Martinique", "MTQ"),
    MR(478, "毛里塔尼亚", "Mauritania", "MRT"),
    MU(480, "毛里求斯", "Mauritius", "MUS"),
    YT(175, "马约特", "Mayotte", "MYT"),
    MX(484, "墨西哥", "Mexico", "MEX"),
    FM(583, "密克罗尼西亚联邦", "Micronesia (Federated States of)", "FSM"),
    MD(498, "摩尔多瓦", "Moldova (Republic of)", "MDA"),
    MC(492, "摩纳哥", "Monaco", "MCO"),
    MN(496, "蒙古", "Mongolia", "MNG"),
    ME(499, "黑山", "Montenegro", "MNE"),
    MS(500, "蒙特塞拉特", "Montserrat", "MSR"),
    MA(504, "摩洛哥", "Morocco", "MAR"),
    MZ(508, "莫桑比克", "Mozambique", "MOZ"),
    MM(104, "缅甸", "Myanmar", "MMR"),
    NA(516, "纳米比亚", "Namibia", "NAM"),
    NR(520, "瑙鲁", "Nauru", "NRU"),
    NP(524, "尼泊尔", "Nepal", "NPL"),
    NL(528, "荷兰", "Netherlands", "NLD"),
    NC(540, "新喀里多尼亚", "New Caledonia", "NCL"),
    NZ(554, "新西兰", "New Zealand", "NZL"),
    NI(558, "尼加拉瓜", "Nicaragua", "NIC"),
    NE(562, "尼日尔", "Niger", "NER"),
    NG(566, "尼日利亚", "Nigeria", "NGA"),
    NU(570, "纽埃", "Niue", "NIU"),
    NF(574, "诺福克岛", "Norfolk Island", "NFK"),
    MK(807, "北马其顿", "North Macedonia", "MKD"),
    MP(580, "北马里亚纳群岛", "Northern Mariana Islands", "MNP"),
    NO(578, "挪威", "Norway", "NOR"),
    OM(512, "阿曼", "Oman", "OMN"),
    PK(586, "巴基斯坦", "Pakistan", "PAK"),
    PW(585, "帕劳", "Palau", "PLW"),
    PS(275, "巴勒斯坦", "Palestine, State of", "PSE"),
    PA(591, "巴拿马", "Panama", "PAN"),
    PG(598, "巴布亚新几内亚", "Papua New Guinea", "PNG"),
    PY(600, "巴拉圭", "Paraguay", "PRY"),
    PE(604, "秘鲁", "Peru", "PER"),
    PH(608, "菲律宾", "Philippines", "PHL"),
    PN(612, "皮特凯恩群岛", "Pitcairn", "PCN"),
    PL(616, "波兰", "Poland", "POL"),
    PT(620, "葡萄牙", "Portugal", "PRT"),
    PR(630, "波多黎各", "Puerto Rico", "PRI"),
    QA(634, "卡塔尔", "Qatar", "QAT"),
    RE(638, "留尼汪", "Réunion", "REU"),
    RO(642, "罗马尼亚", "Romania", "ROU"),
    RW(646, "卢旺达", "Rwanda", "RWA"),
    BL(652, "圣巴泰勒米", "Saint Barthélemy", "BLM"),
    SH(654, "圣赫勒拿、阿森松和特里斯坦-达库尼亚", "Saint Helena, Ascension and Tristan da Cunha", "SHN"),
    KN(659, "圣基茨和尼维斯", "Saint Kitts and Nevis", "KNA"),
    LC(662, "圣卢西亚", "Saint Lucia", "LCA"),
    MF(663, "法属圣马丁", "Saint Martin (French part)", "MAF"),
    PM(666, "圣皮埃尔和密克隆", "Saint Pierre and Miquelon", "SPM"),
    VC(670, "圣文森特和格林纳丁斯", "Saint Vincent and the Grenadines", "VCT"),
    WS(882, "萨摩亚", "Samoa", "WSM"),
    SM(674, "圣马力诺", "San Marino", "SMR"),
    ST(678, "圣多美和普林西比", "Sao Tome and Principe", "STP"),
    SA(682, "沙特阿拉伯", "Saudi Arabia", "SAU"),
    SN(686, "塞内加尔", "Senegal", "SEN"),
    RS(688, "塞尔维亚", "Serbia", "SRB"),
    SC(690, "塞舌尔", "Seychelles", "SYC"),
    SL(694, "塞拉利昂", "Sierra Leone", "SLE"),
    SG(702, "新加坡", "Singapore", "SGP"),
    SX(534, "荷属圣马丁", "Sint Maarten (Dutch part)", "SXM"),
    SK(703, "斯洛伐克", "Slovakia", "SVK"),
    SI(705, "斯洛文尼亚", "Slovenia", "SVN"),
    SB(90, "所罗门群岛", "Solomon Islands", "SLB"),
    SO(706, "索马里", "Somalia", "SOM"),
    ZA(710, "南非", "South Africa", "ZAF"),
    GS(239, "南乔治亚和南桑威奇群岛", "South Georgia and the South Sandwich Islands", "SGS"),
    SS(728, "南苏丹", "South Sudan", "SSD"),
    ES(724, "西班牙", "Spain", "ESP"),
    LK(144, "斯里兰卡", "Sri Lanka", "LKA"),
    SD(729, "苏丹", "Sudan", "SDN"),
    SR(740, "苏里南", "Suriname", "SUR"),
    SJ(744, "斯瓦尔巴和扬马延", "Svalbard and Jan Mayen", "SJM"),
    SE(752, "瑞典", "Sweden", "SWE"),
    CH(756, "瑞士", "Switzerland", "CHE"),
    SY(760, "叙利亚", "Syrian Arab Republic", "SYR"),
    TW(158, "中国台湾省", "Taiwan, Province of China[note 1]", "TWN"),
    TJ(762, "塔吉克斯坦", "Tajikistan", "TJK"),
    TZ(834, "坦桑尼亚", "Tanzania, United Republic of", "TZA"),
    TH(764, "泰国", "Thailand", "THA"),
    TL(626, "东帝汶", "Timor-Leste", "TLS"),
    TG(768, "多哥", "Togo", "TGO"),
    TK(772, "托克劳", "Tokelau", "TKL"),
    TO(776, "汤加", "Tonga", "TON"),
    TT(780, "特立尼达和多巴哥", "Trinidad and Tobago", "TTO"),
    TN(788, "突尼斯", "Tunisia", "TUN"),
    TR(792, "土耳其", "Turkey", "TUR"),
    TM(795, "土库曼斯坦", "Turkmenistan", "TKM"),
    TC(796, "特克斯和凯科斯群岛", "Turks and Caicos Islands", "TCA"),
    TV(798, "图瓦卢", "Tuvalu", "TUV"),
    UG(800, "乌干达", "Uganda", "UGA"),
    UA(804, "乌克兰", "Ukraine", "UKR"),
    AE(784, "阿联酋", "United Arab Emirates", "ARE"),
    UM(581, "美国本土外小岛屿", "United States Minor Outlying Islands", "UMI"),
    UY(858, "乌拉圭", "Uruguay", "URY"),
    UZ(860, "乌兹别克斯坦", "Uzbekistan", "UZB"),
    VU(548, "瓦努阿图", "Vanuatu", "VUT"),
    VE(862, "委内瑞拉", "Venezuela (Bolivarian Republic of)", "VEN"),
    VN(704, "越南", "Viet Nam", "VNM"),
    VG(92, "英属维尔京群岛", "Virgin Islands (British)", "VGB"),
    VI(850, "美属维尔京群岛", "Virgin Islands (U.S.)", "VIR"),
    WF(876, "瓦利斯和富图纳", "Wallis and Futuna", "WLF"),
    EH(732, "西撒哈拉", "Western Sahara", "ESH"),
    YE(887, "也门", "Yemen", "YEM"),
    ZM(894, "赞比亚", "Zambia", "ZMB"),
    ZW(716, "津巴布韦", "Zimbabwe", "ZWE"),
    ;

    private int value;
    private String name;
    private String englishName;
    private String code;

    CountryType(int value, String name, String englishName, String code) {
        this.value = value;
        this.name = name;
        this.englishName = englishName;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    public String getEnglishName() {
        return englishName;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CountryType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static CountryType getByName(String name) {
        return BaseEnumHelper.getByName(name, values());
    }

    public static List<CountryType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CountryType> listWithoutDefault() {
        return BaseEnumHelper.getList(values()).stream().filter(v -> v != DEFAULT).collect(Collectors.toList());
    }

    public static List<CountryType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
