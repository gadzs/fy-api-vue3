package cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive;

import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWord;
import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWordUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SensitiveWordUpdateUseCase extends AbstractUseCase<SensitiveWordUpdateUseCase.InputData, SensitiveWordUpdateUseCase.OutputData> {
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        SensitiveWordUpdater.Builder builder = SensitiveWordUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, SensitiveWordUpdater.class, inputData);

        SensitiveWord sensitiveWord = sensitiveWordService.update(builder.build());

        // 刷新
        sensitiveWordService.refresh();
        
        return OutputData.builder()
                .sensitiveWord(sensitiveWord)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "word不能为空"
        )
        private String word;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private SensitiveWord sensitiveWord;
    }
}
