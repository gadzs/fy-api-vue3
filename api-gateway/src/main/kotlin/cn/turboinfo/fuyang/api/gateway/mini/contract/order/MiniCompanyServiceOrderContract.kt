package cn.turboinfo.fuyang.api.gateway.mini.contract.order

import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniCompanyAssignStaffUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniCompanyAvailableAssignStaffListUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniCompanyCancelServiceOrderUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@CompanyScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/company/order"])
interface MiniCompanyServiceOrderContract {

    // 分配服务人员
    @SessionScope
    @AutoMappingContract(
        beanType = MiniCompanyAssignStaffUseCase::class,
    )
    fun assignStaff()

    // 取消订单
    @AutoMappingContract(
        beanType = MiniCompanyCancelServiceOrderUseCase::class,
    )
    fun cancel()

    // 可分配服务人员列表
    @AutoMappingContract(
        beanType = MiniCompanyAvailableAssignStaffListUseCase::class,
    )
    fun availableAssignStaffList()

}