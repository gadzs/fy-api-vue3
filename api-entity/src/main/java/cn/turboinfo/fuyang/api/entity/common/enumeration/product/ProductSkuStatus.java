package cn.turboinfo.fuyang.api.entity.common.enumeration.product;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 产品SKU状态
 */
@Slf4j
public enum ProductSkuStatus implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "未发布"),

    PUBLISHED(10, "已发布"),

    OFF_SHELF(20, "下架");


    private int value;
    private String name;

    ProductSkuStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProductSkuStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ProductSkuStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ProductSkuStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
