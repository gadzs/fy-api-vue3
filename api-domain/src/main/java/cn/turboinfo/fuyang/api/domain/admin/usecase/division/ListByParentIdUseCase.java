package cn.turboinfo.fuyang.api.domain.admin.usecase.division;


import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 根据父级编码查询地区列表
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ListByParentIdUseCase extends AbstractUseCase<ListByParentIdUseCase.InputData, ListByParentIdUseCase.OutputData> {

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<Division> chinaDivisionList = divisionService.findByParentId(inputData.getParentId());

        return OutputData.builder()
                .chinaDivisionList(chinaDivisionList)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "编码不能为空")
        private Long parentId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<Division> chinaDivisionList;

    }
}
