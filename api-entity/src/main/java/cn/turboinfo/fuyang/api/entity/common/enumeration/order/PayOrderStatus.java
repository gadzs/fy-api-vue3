package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 支付订单状态
 */
@Slf4j
public enum PayOrderStatus implements BaseEnum {

    UNPAID(0, "未支付"),

    POST_PAID(10, "后付费"),

    PAID(20, "已支付"),

    CANCELLED(30, "已取消"),

    TIMEOUT(50, "超时"),

    ;

    private int value;
    private String name;

    PayOrderStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PayOrderStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<PayOrderStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<PayOrderStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
