package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.exception.user.UserCredentialException;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserCredential;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserCredentialCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserCredentialUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserCredentialService {
    Optional<UserCredential> getById(Long id);

    UserCredential getByIdEnsure(Long id);

    List<UserCredential> findByIdCollection(Collection<Long> idCollection);

    UserCredential save(UserCredentialCreator creator) throws UserCredentialException;

    UserCredential update(UserCredentialUpdater updater) throws UserCredentialException;

    QResponse<UserCredential> findAll(QRequest request, QPage requestPage);

    Optional<UserCredential> findUserDefault(Long userId);

    /**
     * 验证凭据是否匹配
     *
     * @param credentialId 凭据ID
     * @param credential   凭据
     * @return 是否匹配
     */
    boolean match(Long credentialId, String credential);

    /**
     * 解密前端加密后的凭据
     *
     * @param encryptedCredential 加密后的凭据
     * @return 解密后的凭据
     */
    String decrypt(String encryptedCredential);

    /**
     * 根据用户密码创建凭据
     */
    UserCredential save(Long userId, String password);

    UserCredential getByUserIdEnsure(Long userId);

}
