package cn.turboinfo.fuyang.api.domain.admin.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopDeleteUseCase extends AbstractUseCase<HousekeepingShopDeleteUseCase.InputData, HousekeepingShopDeleteUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;
    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Optional<HousekeepingShop> housekeepingShopOptional = housekeepingShopService.getById(inputData.getId());
        if (!housekeepingShopOptional.isPresent()) {
            throw new RuntimeException("门店不存在");
        }
        housekeepingShopService.deleteById(inputData.getId());
        //更新企业门店数量
        housekeepingCompanyService.updateShopNum(housekeepingShopOptional.get().getCompanyId(), housekeepingShopService.countByCompanyId(housekeepingShopOptional.get().getCompanyId()));
        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;

        private Long companyId;
    }

    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
