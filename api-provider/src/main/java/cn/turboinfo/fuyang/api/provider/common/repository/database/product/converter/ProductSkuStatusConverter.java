package cn.turboinfo.fuyang.api.provider.common.repository.database.product.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSkuStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ProductSkuStatusConverter extends BaseEnumConverter<ProductSkuStatus> {
}
