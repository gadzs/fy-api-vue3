package cn.turboinfo.fuyang.api.gateway.admin.controller.stat;

import cn.turboinfo.fuyang.api.domain.common.service.stat.IndexStatService;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Objects;

/**
 * @author gadzs
 * @description 首页统计数据
 * @date 2023/3/7 17:07
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/index/stat")
public class IndexStatController {

    private final IndexStatService indexStatService;

    /**
     * 管理员页数据统计
     *
     * @return
     */
    @RequiresPermissions(value = {"index:stat:admin"}, logical = Logical.OR)
    @RequestMapping("/adminData")
    public RestResponseFO setStatAdminData() {
        val indexAdmin = indexStatService.getStatAdminData();
        if (Objects.isNull(indexAdmin)) {
            indexStatService.setStatAdminData();
            return RestResponseFO.ok(indexStatService.getStatAdminData());
        }
        return RestResponseFO.ok(indexAdmin);
    }
}
