package cn.turboinfo.fuyang.api.gateway.mini.contract.custom

import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.*
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@ConsumerScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/serviceCustom"])
interface MiniConsumerServiceCustomContract {

    // 创建定制服务
    @AutoMappingContract(
        beanType = MiniSubmitServiceCustomUseCase::class,
    )
    fun create()

    // 取消订单
    @AutoMappingContract(
        beanType = MiniConsumerCancelServiceCustomUseCase::class,
    )
    fun cancel()

    // 可用支付类型
    @AutoMappingContract(
        beanType = MiniConsumerAvailablePayTypeUseCase::class,
    )
    fun availablePayType()


    // 下单时的提交支付请求
    @AutoMappingContract(
        beanType = MiniConsumerSubmitCustomDepositPayUseCase::class,
    )
    fun submitDepositPay()

    // 扫码开始服务
    @AutoMappingContract(
        beanType = MiniConsumerScanStartServiceCustomUseCase::class,
    )
    fun scanStartService()

    // 可用支付类型
    @AutoMappingContract(
        beanType = MiniConsumerServiceCustomPayUnpaidAvailablePayTypeUseCase::class,
    )
    fun payRemainingUnpaidAvailablePayType()

    // 支付剩余金额
    @AutoMappingContract(
        beanType = MiniConsumerServiceCustomPayRemainingUnpaidUseCase::class,
    )
    fun payRemainingUnpaid()

    // 定金支付成功
    @AutoMappingContract(
        beanType = MiniConsumerDepositPaySuccessUseCase::class,
    )
    fun depositPaySuccess()

    // 定金支付失败
    @AutoMappingContract(
        beanType = MiniConsumerDepositPayFailureUseCase::class,
    )
    fun depositPayFailure()

    // 剩余金额支付成功
    @AutoMappingContract(
        beanType = MiniConsumerCustomRemainingUnpaidPaySuccessUseCase::class,
    )
    fun remainingUnpaidPaySuccess()

    // 剩余金额支付失败
    @AutoMappingContract(
        beanType = MiniConsumerCustomRemainingUnpaidPayFailureUseCase::class,
    )
    fun remainingUnpaidPayFailure()

}
