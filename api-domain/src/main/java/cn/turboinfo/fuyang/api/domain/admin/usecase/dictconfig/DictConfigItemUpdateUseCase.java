package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItemUpdater;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigItemUpdateUseCase extends AbstractUseCase<DictConfigItemUpdateUseCase.InputData, DictConfigItemUpdateUseCase.OutputData> {
    private final DictConfigItemService dictConfigItemService;

    private final DictConfigService dictConfigService;

    @Override
    protected OutputData doAction(InputData inputData) {

        DictConfig dictConfig = dictConfigService.getByIdEnsure(inputData.getDictConfigId());

        DictConfigItemUpdater.Builder builder = DictConfigItemUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, DictConfigItemUpdater.class, inputData);

        DictConfigItem dictConfigItem = dictConfigItemService.update(builder.withDictConfigKey(dictConfig.getDictKey()).build());

        return OutputData.builder()
                .dictConfigItem(dictConfigItem)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "字典ID不能为空"
        )
        private Long dictConfigId;

        @NotBlank(
                message = "条目值不能为空"
        )
        private String itemValue;

        @NotBlank(
                message = "条目名称不能为空"
        )
        private String itemName;

        private String description;

        private Integer itemOrder;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private DictConfigItem dictConfigItem;
    }
}
