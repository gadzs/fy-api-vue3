package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@QField
@Data
public class SysUserSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "用户名",
            operator = Operator.EQUAL
    )
    private String username;

    @QField(
            placeholder = "手机号",
            operator = Operator.EQUAL
    )
    private String mobile;

    @QField(
            control = Control.SELECT,
            placeholder = "状态",
            ref = "enableStatusList",
            template = "enum"
    )
    private net.sunshow.toolkit.core.base.enums.EnableStatus status;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
