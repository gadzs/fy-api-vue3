package cn.turboinfo.fuyang.api.entity.admin.enumeration.login;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

@Slf4j
public enum AdminLoginOperation implements BaseEnum {

    ALL(-1, "全部"),
    DEFAULT(0, "不需要操作"),
    MODIFY_PASSWORD(1, "修改密码"),
    ;

    private int value;
    private String name;

    AdminLoginOperation(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static AdminLoginOperation get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<AdminLoginOperation> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<AdminLoginOperation> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
