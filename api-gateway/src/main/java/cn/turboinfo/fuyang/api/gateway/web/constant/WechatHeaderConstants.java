package cn.turboinfo.fuyang.api.gateway.web.constant;

public final class WechatHeaderConstants {

    public static final String WECHATPAY_SIGNATURE = "Wechatpay-Signature";

    public static final String WECHATPAY_NONCE = "Wechatpay-Nonce";

    public static final String WECHATPAY_TIMESTAMP = "Wechatpay-Timestamp";

    public static final String WECHATPAY_SERIAL = "Wechatpay-Serial";

    public static final String WECHATPAY_SIGNATURE_TYPE = "Wechatpay-Signature-Type";

}
