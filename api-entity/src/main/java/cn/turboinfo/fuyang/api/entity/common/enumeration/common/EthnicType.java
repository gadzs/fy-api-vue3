package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public enum EthnicType implements BaseEnum {
    DEFAULT(0, "未知"),
    HAN(1, "汉族"),
    MAN(2, "满族"),
    MENGGU(3, "蒙古族"),
    HUI(4, "回族"),
    ZANG(5, "藏族"),
    WEIWUER(6, "维吾尔族"),
    MIAO(7, "苗族"),
    YI(8, "彝族"),
    ZHUANG(9, "壮族"),
    BUYI(10, "布依族"),
    DONG(11, "侗族"),
    YAO(12, "瑶族"),
    BAI(13, "白族"),
    TUJIA(14, "土家族"),
    HANI(15, "哈尼族"),
    HASAKE(16, "哈萨克族"),
    DAI(17, "傣族"),
    LI(18, "黎族"),
    LISU(19, "傈僳族"),
    WA(20, "佤族"),
    SHE(21, "畲族"),
    GAOSHAN(22, "高山族"),
    LAHU(23, "拉祜族"),
    SHUI(24, "水族"),
    DONGXIANG(25, "东乡族"),
    NAXI(26, "纳西族"),
    JINGPO(27, "景颇族"),
    KEERKEZI(28, "柯尔克孜族"),
    TU(29, "土族"),
    DAWOER(30, "达斡尔族"),
    MULAO(31, "仫佬族"),
    QIANG(32, "羌族"),
    BULANG(33, "布朗族"),
    SALA(34, "撒拉族"),
    MAONAN(35, "毛南族"),
    GELAO(36, "仡佬族"),
    XIBO(37, "锡伯族"),
    ACHANG(38, "阿昌族"),
    PUMI(39, "普米族"),
    CHAOXIAN(40, "朝鲜族"),
    TAJIKE(41, "塔吉克族"),
    NU(42, "怒族"),
    WUZIBIEKE(43, "乌孜别克族"),
    ELUOSI(44, "俄罗斯族"),
    EWENKE(45, "鄂温克族"),
    DEANG(46, "德昂族"),
    BAOAN(47, "保安族"),
    YUGU(48, "裕固族"),
    JING(49, "京族"),
    TATAER(50, "塔塔尔族"),
    DULONG(51, "独龙族"),
    ELUNCHUN(52, "鄂伦春族"),
    HEZHE(53, "赫哲族"),
    MENBA(54, "门巴族"),
    LUOBA(55, "珞巴族"),
    JINUO(56, "基诺族"),

    XIAERBA(101, "夏尔巴人"),
    ;

    private int value;
    private String name;

    EthnicType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static EthnicType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static EthnicType getByName(String name) {
        return BaseEnumHelper.getByName(name, values());
    }

    public static List<EthnicType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<EthnicType> listWithoutDefault() {
        return BaseEnumHelper.getList(values()).stream().filter(v -> v != DEFAULT).collect(Collectors.toList());
    }

    public static List<EthnicType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
