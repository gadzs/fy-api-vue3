package cn.turboinfo.fuyang.api.domain.mini.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.handler.product.ProductDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 根据名称查询服务
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListProductByNameUseCase extends AbstractUseCase<MiniListProductByNameUseCase.InputData, MiniListProductByNameUseCase.OutputData> {
    private final ProductService productService;

    private final ProductDataFactory productDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        String name = inputData.name;

        List<Product> productList = productService.findByNameContaining(name)
                .stream()
                .filter(it -> it.getStatus().equals(ProductStatus.PUBLISHED))
                .toList();

        // 组装附件
        productDataFactory.assembleShop(productList);
        productDataFactory.assembleAttachment(productList);

        return OutputData.builder()
                .productList(productList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank
        private String name;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<Product> productList;
    }

}
