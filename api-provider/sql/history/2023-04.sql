-- 2023-04-21 用户资料表增加机构字段
-- ALTER TABLE `user_profile` ADD COLUMN `agencies_id` BIGINT NOT NULL DEFAULT 0 COMMENT '机构ID' AFTER `id`;

-- 修改用户资料 删除字段
-- ALTER TABLE `user_profile` CHANGE COLUMN `deleted_time` `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '';
