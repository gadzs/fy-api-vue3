package cn.turboinfo.fuyang.api.entity.common.enumeration.confidence;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 放心码状态
 */
@Slf4j
public enum ConfidenceCodeStatus implements BaseEnum {

    DEFAULT(0, "默认"),
    USED(10, "已使用"),

    DISCARD(99, "已作废");

    private int value;
    private String name;

    ConfidenceCodeStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ConfidenceCodeStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ConfidenceCodeStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ConfidenceCodeStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
