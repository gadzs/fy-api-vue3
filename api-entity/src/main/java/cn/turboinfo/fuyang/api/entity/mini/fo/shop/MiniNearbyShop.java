package cn.turboinfo.fuyang.api.entity.mini.fo.shop;

import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MiniNearbyShop {

    private ViewShopFO shop;

    /**
     * 距离
     */
    private BigDecimal distance;

}
