package cn.turboinfo.fuyang.api.provider.common.service.impl.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.activity.ActivityDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.activity.ActivityPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ActivityServiceImpl extends DefaultQServiceImpl<Activity, Long, ActivityPO, ActivityDAO> implements ActivityService {
    @Override
    public List<Activity> findByStatus(EnableStatus status) {
        return convertQBeanToList(dao.findByStatusOrderByIdDesc(status));
    }
}
