package cn.turboinfo.fuyang.api.entity.common.pojo.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginNameType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class UserLogin extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户编码
     */
    private Long userId;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 登录名类型
     */
    private LoginNameType loginNameType;

    /**
     * 登录验证类型
     */
    private LoginCheckType loginCheckType;

    /**
     * 验证者编码
     */
    private Long loginCheckId;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
