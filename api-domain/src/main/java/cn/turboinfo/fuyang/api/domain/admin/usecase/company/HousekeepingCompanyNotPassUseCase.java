package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.company.HousekeepingCompanyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author gadzs
 * @description 家政企业审核不通过usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyNotPassUseCase extends AbstractUseCase<HousekeepingCompanyNotPassUseCase.InputData, HousekeepingCompanyNotPassUseCase.OutputData> {
    private final HousekeepingCompanyService housekeepingCompanyService;
    private final HousekeepingCompanyAuditRecordService housekeepingCompanyAuditRecordService;
    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val auditorId = inputData.getUserId();
        val housekeepingCompanyData = housekeepingCompanyService.getById(inputData.getId()).orElse(null);
        if (housekeepingCompanyData == null || housekeepingCompanyData.getStatus().getValue() != CompanyStatus.REGISTER.getValue()) {
            throw new HousekeepingCompanyException("数据状态不正确，操作失败");
        }

        HousekeepingCompanyUpdater.Builder builder = HousekeepingCompanyUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingCompanyUpdater.class, inputData);
        builder.withStatus(CompanyStatus.NOTPASS)
                .withReviewUserId(inputData.getUserId())
                .withReviewTime(LocalDateTime.now());
        HousekeepingCompany housekeepingCompany = housekeepingCompanyService.review(builder.build());

        // 更新审核记录
        {
            val auditorUser = sysUserService.getByIdEnsure(auditorId);

            HousekeepingCompanyAuditRecordCreator housekeepingCompanyAuditRecord = HousekeepingCompanyAuditRecordCreator.builder()
                    .withCompanyId(housekeepingCompany.getId())
                    .withAuditStatus(AuditStatus.NOT_PASS)
                    .withUserId(auditorId)
                    .withUserName(auditorUser.getUsername())
                    .withRemark(inputData.getRemark())
                    .build();
            housekeepingCompanyAuditRecordService.save(housekeepingCompanyAuditRecord);
        }

        return OutputData.builder()
                .id(housekeepingCompany.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "审核人不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "失败原因不能为空"
        )
        private String remark;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
