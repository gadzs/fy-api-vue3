package cn.turboinfo.fuyang.api.provider.common.repository.database.category;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 分类
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "category"
)
@Where(clause = "deleted = 0")
@Entity
public class CategoryPO extends SoftDeleteFYEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 分类编码
     */
    private String code;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 父级 ID
     */
    private Long parentId;

    /**
     * 排序值
     */
    private Integer sortValue;

}
