package cn.turboinfo.fuyang.api.provider.common.service.impl.profit;

import cn.turboinfo.fuyang.api.domain.common.service.profit.ProfitSharingService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharing;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharingCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.profit.ProfitSharingDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.profit.ProfitSharingPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProfitSharingServiceImpl extends DefaultQServiceImpl<ProfitSharing, Long, ProfitSharingPO, ProfitSharingDAO> implements ProfitSharingService {
    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public ProfitSharing save(ProfitSharingCreator creator) {
        ProfitSharingPO profitSharingPO = new ProfitSharingPO();
        QBeanCreatorHelper.copyCreatorField(profitSharingPO, creator);

        // 手动生成ID
        profitSharingPO.setId(idGeneratorFacade.nextId());

        return convertQBean(dao.save(profitSharingPO));
    }

    @Override
    public List<ProfitSharing> findByObjectId(Long objectId, EntityObjectType objectType) {
        return convertStreamQBeanToList(dao.findByObjectIdAndObjectType(objectId, objectType).stream());
    }

    @Override
    @Transactional
    public void updateStatus(Long id, ProfitSharingStatus status) {
        ProfitSharingPO updatePO = getEntityWithNullCheckForUpdate(id);

        updatePO.setStatus(status);

    }
}
