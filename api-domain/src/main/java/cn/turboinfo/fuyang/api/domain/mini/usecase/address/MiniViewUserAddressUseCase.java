package cn.turboinfo.fuyang.api.domain.mini.usecase.address;

import cn.turboinfo.fuyang.api.domain.common.handler.address.AddressDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.address.AddressService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.fo.address.ViewAddressFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 查看用户地址
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewUserAddressUseCase extends AbstractUseCase<MiniViewUserAddressUseCase.InputData, MiniViewUserAddressUseCase.OutputData> {

    private final AddressService addressService;

    private final AddressDataFactory addressDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long addressId = inputData.getAddressId();

        Address address = addressService.getByIdEnsure(addressId);


        if (!Objects.equals(address.getUserId(), inputData.getUserId())) {
            throw new IllegalArgumentException("地址 ID 与用户 ID 不匹配");
        }

//        addressDataFactory.maskInfo(address);

        return OutputData.builder()
                .address(BeanMapper.map(address, ViewAddressFO.class))
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 地址 ID
         */
        @NotNull(message = "地址 ID 不能为空")
        @Positive
        private Long addressId;

        /**
         * 所属用户 ID
         */
        @NotNull(message = "用户 ID 不能为空")
        @Positive
        private Long userId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private ViewAddressFO address;

    }
}
