package cn.turboinfo.fuyang.api.gateway.admin.fo.permission;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class PermissionSearchFO {

    @QField(placeholder = "ID", sortable = true)
    private Long id;

    @QField(placeholder = "权限名称", operator = Operator.LIKE)
    private String name;

    @QField(placeholder = "权限描述", operator = Operator.LIKE)
    private String description;

    @QField(placeholder = "资源编码", operator = Operator.LIKE)
    private String resource;

    @QField(placeholder = "是否可见", control = Control.SELECT, ref = "visibleStatusList", template = "enum")
    private YesNoStatus visibleStatus;

    @QField(placeholder = "创建时间", sortable = true, searchable = false)
    private LocalDateTime createdTime;
}
