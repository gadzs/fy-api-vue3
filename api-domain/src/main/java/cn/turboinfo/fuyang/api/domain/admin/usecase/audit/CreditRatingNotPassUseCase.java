package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.CreditRatingAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.credit.CreditRatingService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.CreditRatingAuditRecordCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 评价审核不通过usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class CreditRatingNotPassUseCase extends AbstractUseCase<CreditRatingNotPassUseCase.InputData, CreditRatingNotPassUseCase.OutputData> {
    private final CreditRatingService creditRatingService;
    private final CreditRatingAuditRecordService creditRatingAuditRecordService;
    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val auditorId = inputData.getUserId();

        val creditRating = creditRatingService.getByIdEnsure(inputData.getId());
        if (creditRating.getStatus() != CreditRatingStatus.PENDING) {
            throw new RuntimeException("数据状态不是待审核，操作失败");
        }

        creditRatingService.updateStatus(creditRating.getId(), CreditRatingStatus.NOT_PASS);

        // 更新审核记录
        {
            val auditorUser = sysUserService.getByIdEnsure(auditorId);

            CreditRatingAuditRecordCreator creditRatingAuditRecord = CreditRatingAuditRecordCreator.builder()
                    .withCreditRatingId(creditRating.getId())
                    .withObjectId(creditRating.getObjectId())
                    .withObjectType(creditRating.getObjectType())
                    .withAuditStatus(AuditStatus.NOT_PASS)
                    .withUserId(auditorId)
                    .withUserName(auditorUser.getUsername())
                    .withRemark(inputData.getRemark())
                    .build();
            creditRatingAuditRecordService.save(creditRatingAuditRecord);
        }

        return OutputData.builder()
                .id(creditRating.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "审核人不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "失败原因不能为空"
        )
        private String remark;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
