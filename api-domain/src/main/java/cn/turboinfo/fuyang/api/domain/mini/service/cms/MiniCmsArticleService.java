package cn.turboinfo.fuyang.api.domain.mini.service.cms;

import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;

import java.util.List;

public interface MiniCmsArticleService {

    List<CmsArticle> findHomeArticle();

}
