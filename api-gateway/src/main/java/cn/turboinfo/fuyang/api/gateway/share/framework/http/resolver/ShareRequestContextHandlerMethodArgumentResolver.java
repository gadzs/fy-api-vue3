package cn.turboinfo.fuyang.api.gateway.share.framework.http.resolver;

import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContext;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContextHolder;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class ShareRequestContextHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return ShareRequestContext.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return ShareRequestContextHolder.current();
    }

}
