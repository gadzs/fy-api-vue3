package cn.turboinfo.fuyang.api.gateway.admin.fo.activity;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class ActivitySearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "活动名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "活动描述",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            placeholder = "活动类型",
            operator = Operator.LIKE
    )
    private String activityType;

    @QField(
            placeholder = "主办单位",
            operator = Operator.LIKE
    )
    private String organizer;

    @QField(
            placeholder = "活动地点",
            operator = Operator.LIKE
    )
    private String location;

    @QField(
            placeholder = "限制数量"
    )
    private Integer limitNum;

    @QField(
            placeholder = "是否开放报名",
            control = Control.SELECT,
            ref = "enableStatusList",
            template = "enum"
    )
    private net.sunshow.toolkit.core.base.enums.EnableStatus openApply;

    @QField(
            placeholder = "活动开始时间"
    )
    private LocalDateTime startTime;

    @QField(
            placeholder = "活动结束时间"
    )
    private LocalDateTime endTime;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            ref = "enableStatusList",
            template = "enum"
    )
    private net.sunshow.toolkit.core.base.enums.EnableStatus status;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
