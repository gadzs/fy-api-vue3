package cn.turboinfo.fuyang.api.domain.common.service.company;


import cn.turboinfo.fuyang.api.entity.common.exception.company.HousekeepingCompanyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyUpdater;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface HousekeepingCompanyService extends BaseQService<HousekeepingCompany, Long> {

    HousekeepingCompany save(HousekeepingCompanyCreator creator);

    /**
     * 根据社会信用代码查询
     *
     * @param uscc
     * @return
     */
    Optional<HousekeepingCompany> findByUscc(String uscc);

    /**
     * 根据手机号码码查询
     *
     * @param contactNumber
     * @return
     */
    Optional<HousekeepingCompany> findByContactNumber(String contactNumber);

    /**
     * 审核
     *
     * @param updater
     * @return
     * @throws HousekeepingCompanyException
     */
    HousekeepingCompany review(HousekeepingCompanyUpdater updater) throws HousekeepingCompanyException;

    List<HousekeepingCompany> findByName(String name);

    void updateCreditScore(Long id, BigDecimal creditScore);

    void updateOrderNum(Long id, Long orderNum);

    void updateStaffNum(Long id, Long staffNum);

    void updateShopNum(Long id, Long shopNum);

    void updateProductNum(Long id, Long productNum);

    List<HousekeepingCompany> findByAreaCode(Long areaCode);
}
