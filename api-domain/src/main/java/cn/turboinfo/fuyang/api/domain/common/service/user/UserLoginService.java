package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginNameType;
import cn.turboinfo.fuyang.api.entity.common.exception.user.UserLoginException;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserLogin;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserLoginCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserLoginUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserLoginService extends BaseQService<UserLogin, Long> {
    Optional<UserLogin> getById(Long id);

    UserLogin getByIdEnsure(Long id);

    List<UserLogin> findByIdCollection(Collection<Long> idCollection);

    Optional<UserLogin> getByLoginNameAndLoginNameTypeAndLoginCheckTypeAndLoginCheckId(String loginName, LoginNameType loginNameType, LoginCheckType loginCheckType, Long loginCheckId);

    UserLogin getByLoginNameAndLoginNameTypeAndLoginCheckTypeAndLoginCheckIdEnsure(String loginName, LoginNameType loginNameType, LoginCheckType loginCheckType, Long loginCheckId);

    /**
     * 如果不存在就创建
     *
     * @param creator 创建内容
     * @return 是否本次创建创建
     * @throws UserLoginException
     */
    boolean saveIfAbsent(UserLoginCreator creator) throws UserLoginException;

    UserLogin update(UserLoginUpdater updater) throws UserLoginException;

    QResponse<UserLogin> findAll(QRequest request, QPage requestPage);

    List<UserLogin> findByUserId(Long userId);

    void deleteById(Long id) throws UserLoginException;

    List<UserLogin> findByLoginNameAndLoginNameType(String loginName, LoginNameType loginNameType);

    List<UserLogin> findByLoginNameAndLoginNameTypeAndLoginCheckType(String loginName, LoginNameType loginNameType, LoginCheckType loginCheckType);

}
