package cn.turboinfo.fuyang.api.domain.admin.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopListUseCase extends AbstractUseCase<HousekeepingShopListUseCase.InputData, HousekeepingShopListUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;

    @Override
    protected OutputData doAction(InputData inputData) {
        List<HousekeepingShop> housekeepingShopList = housekeepingShopService.findByCompanyId(inputData.getCompanyId());
        List<HousekeepingListShop> shopList = housekeepingShopList.stream().map(housekeepingShop -> HousekeepingListShop.builder().id(housekeepingShop.getId()).name(housekeepingShop.getName()).build()).collect(Collectors.toList());
        return OutputData.builder()
                .shopList(shopList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "companyId不能为空"
        )
        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<HousekeepingListShop> shopList;
    }

    @Data
    @Builder
    public static class HousekeepingListShop {

        private Long id;
        private String name;
    }
}
