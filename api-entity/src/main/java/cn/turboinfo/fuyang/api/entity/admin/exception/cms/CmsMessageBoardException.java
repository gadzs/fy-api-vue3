package cn.turboinfo.fuyang.api.entity.admin.exception.cms;

public class CmsMessageBoardException extends RuntimeException {
    public CmsMessageBoardException() {
        this("操作 MessageBoard 出错");
    }

    public CmsMessageBoardException(String message) {
        super(message);
    }

    public CmsMessageBoardException(Throwable cause) {
        super(cause);
    }

    public CmsMessageBoardException(String message, Throwable cause) {
        super(message, cause);
    }
}
