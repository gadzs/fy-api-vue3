package cn.turboinfo.fuyang.api.provider.common.component.order.interceptor

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus
import cn.turboinfo.fuyang.api.entity.common.event.WrapperApplicationEvent
import cn.turboinfo.fuyang.api.entity.common.event.order.ServiceOrderStatusChangedEvent
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrderStatusLog
import cn.turboinfo.fuyang.api.provider.common.framework.jpa.interceptor.FlushDirtyInterceptor
import cn.turboinfo.fuyang.api.provider.common.framework.jpa.interceptor.SaveInterceptor
import cn.turboinfo.fuyang.api.provider.common.repository.database.order.ServiceOrderPO
import org.hibernate.type.Type
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronization
import org.springframework.transaction.support.TransactionSynchronizationManager
import java.io.Serializable

/**
 * 自动记录每个状态变更的时间点
 */
@Component
class ServiceOrderStatusHistoryInterceptor(
    private val applicationContext: ApplicationContext,
) : SaveInterceptor, FlushDirtyInterceptor {

    override fun onSave(
        entity: Any,
        id: Serializable?,
        state: Array<Any>,
        propertyNames: Array<out String>,
        types: Array<out Type>
    ): Boolean {
        val index = propertyNames.indexOf(QServiceOrder.orderStatusHistory)

        (entity as ServiceOrderPO)
            .also {
                state[index] = listOf(
                    ServiceOrderStatusLog(it.orderStatus, null, it.updatedTime)
                )
            }

        return true
    }

    override fun onFlushDirty(
        entity: Any,
        id: Serializable,
        currentState: Array<Any>,
        previousState: Array<out Any>,
        propertyNames: Array<out String>,
        types: Array<out Type>
    ): Boolean {
        val index = propertyNames.indexOf(QServiceOrder.orderStatus)

        if (previousState[index] != currentState[index]) {
            (entity as ServiceOrderPO)
                .also {
                    val log = ServiceOrderStatusLog(
                        currentState[index] as ServiceOrderStatus,
                        previousState[index] as ServiceOrderStatus,
                        it.updatedTime,
                    )
                    currentState[propertyNames.indexOf(QServiceOrder.orderStatusHistory)] =
                        if (it.orderStatusHistory == null) {
                            listOf(log)
                        } else {
                            it.orderStatusHistory + log
                        }

                    TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronization {
                        override fun afterCommit() {
                            applicationContext.publishEvent(
                                WrapperApplicationEvent(
                                    ServiceOrderStatusChangedEvent.builder()
                                        .serviceOrderId(entity.id)
                                        .fromStatus(previousState[index] as ServiceOrderStatus)
                                        .status(currentState[index] as ServiceOrderStatus)
                                        .build()
                                )
                            )
                        }
                    })
                }
            return true
        }

        return false
    }

    override fun isSupported(
        entity: Any,
        id: Serializable,
        currentState: Array<out Any>,
        previousState: Array<out Any>,
        propertyNames: Array<out String>,
        types: Array<out Type>
    ): Boolean {
        return entity is ServiceOrderPO
    }

    override fun isSupported(
        entity: Any,
        id: Serializable?,
        state: Array<out Any>,
        propertyNames: Array<out String>,
        types: Array<out Type>
    ): Boolean {
        return entity is ServiceOrderPO
    }
}
