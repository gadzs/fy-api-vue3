package cn.turboinfo.fuyang.api.provider.common.repository.database.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface HousekeepingStaffDAO extends FYRepository<HousekeepingStaffPO, Long> {

    List<HousekeepingStaffPO> findByCompanyId(Long companyId);

    List<HousekeepingStaffPO> findByCompanyIdIn(Collection<Long> companyIdCollection);

    Optional<HousekeepingStaffPO> findByIdCard(String idCard);

    Optional<HousekeepingStaffPO> findByContactMobile(String contactMobile);

    Optional<HousekeepingStaffPO> findByCode(String code);

    long countByCompanyIdAndStatus(Long companyId, StaffStatus status);
}
