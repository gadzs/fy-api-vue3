package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ModifyPasswordFO {

    @NotBlank(message = "旧密码不能为空")
    private String originalPassword;

    @NotBlank(message = "新密码不能为空")
    private String newPassword;

}
