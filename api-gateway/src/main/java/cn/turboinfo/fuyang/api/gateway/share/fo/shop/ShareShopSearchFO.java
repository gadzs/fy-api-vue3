package cn.turboinfo.fuyang.api.gateway.share.fo.shop;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class ShareShopSearchFO {
    
    @QField(
            placeholder = "门店名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            ref = "enableStatusList",
            template = "enum"
    )
    private EnableStatus status;

}
