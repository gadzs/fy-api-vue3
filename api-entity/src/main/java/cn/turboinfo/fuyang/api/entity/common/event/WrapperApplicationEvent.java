package cn.turboinfo.fuyang.api.entity.common.event;

import org.springframework.context.ApplicationEvent;

public class WrapperApplicationEvent extends ApplicationEvent {

    public WrapperApplicationEvent(Object obj) {
        super(obj);
    }

}
