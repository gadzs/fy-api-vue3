CREATE TABLE `rule_group`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(32)  NOT NULL DEFAULT '',
    `description`  VARCHAR(128) NOT NULL DEFAULT '',
    `rule_key`     VARCHAR(128) NOT NULL DEFAULT '',
    `control_type` SMALLINT     NOT NULL DEFAULT 0,
    `status`       TINYINT      NOT NULL DEFAULT 0,
    `item_list`    JSON         NULL,
    `deleted_time` BIGINT       NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_controltype` (`control_type`),
    KEY `idx_rulekey` (`rule_key`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE `rule_group_rel`
(
    `id`            BIGINT   NOT NULL AUTO_INCREMENT,
    `rule_group_id` BIGINT   NOT NULL,
    `object_type`   SMALLINT NOT NULL,
    `object_id`     BIGINT   NOT NULL,
    `sort_value`    INT      NOT NULL DEFAULT 0,
    `created_time`  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_objectid_objecttype_rulegroupid` (`object_id`, `object_type`, `rule_group_id`),
    KEY `idx_rulegroupid` (`rule_group_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
