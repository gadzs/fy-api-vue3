package cn.turboinfo.fuyang.api.entity.common.enumeration.policy;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 策略类型
 */
@Slf4j
public enum PolicyType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    ;

    private int value;
    private String name;

    PolicyType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PolicyType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<PolicyType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<PolicyType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
