package cn.turboinfo.fuyang.api.entity.common.pojo.division;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 地区
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Division extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 区域编码
     */
    private Long areaCode;

    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 父级ID
     */
    private Long parentId;

    /**
     * 父级编码
     */
    private Long parentCode;

    /**
     * 省编码
     */
    private Long provinceCode;

    /**
     * 市编码
     */
    private Long cityCode;

    private List<Division> children;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
