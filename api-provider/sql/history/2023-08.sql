-- 2023-08-02 修改用户表字段
ALTER TABLE `sys_user`
    CHANGE COLUMN `deleted_time` `deleted` BIGINT NULL DEFAULT 0 COMMENT '删除时间';
ALTER TABLE `user_login`
    CHANGE COLUMN `deleted_time` `deleted` BIGINT NULL DEFAULT 0 COMMENT '删除时间';
ALTER TABLE `sys_user`
    ADD UNIQUE INDEX `uk_username_deleted` (`username`, `deleted`);
ALTER TABLE `user_login`
    ADD UNIQUE INDEX `uk_loginname_loginnametype_loginchecktype_logincheckid` (`login_name`, `login_name_type`,
                                                                               `login_check_type`, `login_check_id`,
                                                                               `deleted`);
-- 2023-08-03 用户表数据脱敏
-- 备份系统用户表
CREATE TABLE `sys_user_backup` AS (SELECT *
                                   FROM `sys_user`);
CREATE TABLE `user_login_backup` AS (SELECT *
                                     FROM `user_login`);
-- 删除备份表
# DROP TABLE IF EXISTS `sys_user_backup`;
# DROP TABLE IF EXISTS `user_login_backup`;

-- 企业表数据脱敏
ALTER TABLE `housekeeping_company`
    CHANGE `legal_person_id_card` `legal_person_id_card` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '法人身份证';
ALTER TABLE `housekeeping_company`
    CHANGE `contact_number` `contact_number` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '联系电话';
-- 备份企业表
CREATE TABLE `housekeeping_company_backup` AS (SELECT *
                                               FROM `housekeeping_company`);
-- 删除备份表
# DROP TABLE IF EXISTS `housekeeping_company_backup`;

-- 备份门店表
CREATE TABLE `housekeeping_shop_backup` AS (SELECT *
                                            FROM `housekeeping_shop`);
-- 删除备份表
-- DROP TABLE IF EXISTS `housekeeping_shop_backup`;

-- 备份家政员表
CREATE TABLE `housekeeping_staff_backup` AS (SELECT *
                                             FROM `housekeeping_staff`);
-- 删除备份表
# DROP TABLE IF EXISTS `housekeeping_staff_backup`;

-- 备份个人地址表
CREATE TABLE `address_backup` AS (SELECT *
                                  FROM `address`);
-- 删除备份表
# DROP TABLE IF EXISTS `address_backup`;

-- 备份服务订单表
CREATE TABLE `service_order_backup` AS (SELECT *
                                        FROM `service_order`);
-- 删除备份表
# DROP TABLE IF EXISTS `service_order_backup`;

-- 备份定制服务订单表
CREATE TABLE `service_custom_backup` AS (SELECT *
                                         FROM `service_custom`);
-- 删除备份表
# DROP TABLE IF EXISTS `service_custom_backup`;
