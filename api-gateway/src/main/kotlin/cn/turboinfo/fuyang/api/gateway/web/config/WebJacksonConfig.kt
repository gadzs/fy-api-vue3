package cn.turboinfo.fuyang.api.gateway.web.config

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.text.SimpleDateFormat
import java.util.*

@Configuration
class WebJacksonConfig {

    @Bean
    protected fun commonJackson2ObjectMapperBuilderCustomizer(): Jackson2ObjectMapperBuilderCustomizer {
        return Jackson2ObjectMapperBuilderCustomizer {
            it
                .dateFormat(
                    SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss",
                        Locale.CHINA
                    )
                )
                .serializerByType(Long::class.javaObjectType, ToStringSerializer())

        }
    }

}
