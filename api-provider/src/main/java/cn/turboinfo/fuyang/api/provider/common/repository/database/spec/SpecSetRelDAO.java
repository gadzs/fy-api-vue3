package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface SpecSetRelDAO extends FYRepository<SpecSetRelPO, Long> {

    List<SpecSetRelPO> findBySpecSetId(Long specSetId);

    List<SpecSetRelPO> findBySpecSetIdIn(Collection<Long> specSetIdCollection);

    Optional<SpecSetRelPO> findByObjectIdAndObjectType(Long objectId, EntityObjectType objectType);

}
