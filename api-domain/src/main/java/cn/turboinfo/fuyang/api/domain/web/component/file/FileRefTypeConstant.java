package cn.turboinfo.fuyang.api.domain.web.component.file;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * @author gadzs
 * @description 文件引用类型
 * @date 2023/2/3 17:40
 */
public class FileRefTypeConstant {

    // 公司相关附件
    public static final String COMPANY_ATTACH = "companyAttach";
    public static final String COMPANY_AUTH_LABEL_IMG = "companyAuthLabelImg";

    // 店铺相关
    public static final String SHOP_IMG = "shopImg";
    public static final String SHOP_ATTACH = "shopAttach";

    // 家政员
    public static final String STAFF_IMG = "staffImg";
    public static final String STAFF_ATTACH = "staffAttach";

    // 产品图片
    public static final String PRODUCT_IMG = "productImg";
    public static final String PRODUCT_VIDEO = "productVideo";

    // 评价附件
    public static final String CREDIT_RATING_IMG = "creditRatingImg";


    // 文章
    public static final String ARTICLE_IMG = "articleImg";

    public static final String ARTICLE_ATTACH = "articleAttach";

    // 编辑器
    public static final String TINYMCE_IMG = "tinymceImg";

    // 活动图片
    public static final String ACTIVITY_IMG = "activityImg";

    public static final String CONTRACT_FILE = "contractFile";
    public static final String CONTRACT_TMPL_FILE = "contractTmplFile";
    public static final String CONTRACT_SIGNATURE = "contractSignature";
    public static final String TEMPORARY_CONTRACT_IMG = "temporaryContractImg";

    public static final String CATEGORY_ICON = "categoryIcon";

    //无需登录的访问
    public static final Set<String> noAuthRefTypeView = Sets.newHashSet(
            SHOP_IMG,
            STAFF_IMG,
            PRODUCT_IMG,
            PRODUCT_VIDEO,
            ARTICLE_IMG,
            TINYMCE_IMG,
            COMPANY_AUTH_LABEL_IMG
    );

    public static final Set<String> noAuthRefTypeUpload = Sets.newHashSet(
            COMPANY_ATTACH
    );

    //需要验证用户的访问
    public static final Set<String> authUserRefTypeView = Sets.newHashSet(
            COMPANY_ATTACH,
            SHOP_ATTACH,
            STAFF_ATTACH,
            CREDIT_RATING_IMG
    );
}
