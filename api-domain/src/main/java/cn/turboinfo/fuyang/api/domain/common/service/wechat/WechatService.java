package cn.turboinfo.fuyang.api.domain.common.service.wechat;

import cn.turboinfo.fuyang.api.entity.common.fo.wechat.WechatAuthResult;
import cn.turboinfo.fuyang.api.entity.common.fo.wechat.WechatGetPhoneResult;

import java.io.IOException;

public interface WechatService {
    WechatAuthResult auth(String code) throws IOException;


    WechatGetPhoneResult getPhone(String code) throws IOException;
    
}
