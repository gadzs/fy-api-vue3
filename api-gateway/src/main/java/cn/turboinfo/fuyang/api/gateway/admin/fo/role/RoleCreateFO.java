package cn.turboinfo.fuyang.api.gateway.admin.fo.role;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RoleCreateFO {
    @NotBlank(message = "角色代码不能为空")
    private String code;

    @NotBlank(message = "角色名称不能为空")
    private String name;

    private String description;
}
