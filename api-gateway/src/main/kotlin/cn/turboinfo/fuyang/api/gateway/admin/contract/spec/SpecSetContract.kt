package cn.turboinfo.fuyang.api.gateway.admin.contract.spec

import cn.turboinfo.fuyang.api.domain.admin.usecase.spec.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/specSet"])
interface SpecSetContract {

    // 添加规则组
    @PermissionScope("spec:create")
    @AutoMappingContract(
        beanType = SpecSetCreateUseCase::class,
    )
    @CompanyScope
    fun create()

    // 修改规则组
    @PermissionScope("spec:update")
    @AutoMappingContract(
        beanType = SpecSetUpdateUseCase::class,
    )
    @CompanyScope
    fun update()


    // 删除规则组
    @PermissionScope("spec:delete")
    @AutoMappingContract(
        beanType = SpecSetDeleteUseCase::class,
        consumes = [],
    )
    fun delete()

}
