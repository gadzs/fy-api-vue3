package cn.turboinfo.fuyang.api.entity.common.pojo.account;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.converter.EasyExcelAccountStatusConverter;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 企业账户
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class CompanyAccount extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    @ExcelIgnore
    private Long id;

    /**
     * 企业ID
     */
    @ExcelIgnore
    private Long companyId;

    @ExcelProperty("企业名称")
    private String companyName;

    /**
     * 账户类型
     */
    @ExcelIgnore
    private AccountType type;

    /**
     * 账户
     */
    @ExcelProperty("账户")
    private String account;

    /**
     * 账户名称
     */
    @ExcelProperty("账户名称")
    private String name;

    /**
     * 关系类型
     */
    @ExcelIgnore
    private RelationType relationType;

    /**
     * 自定义关系类型
     */
    @ExcelIgnore
    private String customRelation;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态", converter = EasyExcelAccountStatusConverter.class)
    private AccountStatus status;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    @ExcelIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    @ExcelIgnore
    private LocalDateTime updatedTime;
}
