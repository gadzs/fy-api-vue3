package cn.turboinfo.fuyang.api.domain.admin.service.log;

import cn.turboinfo.fuyang.api.entity.admin.exception.log.LoginLogException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.LoginLog;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.LoginLogCreator;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface LoginLogService {
    Optional<LoginLog> getById(Long id);

    LoginLog getByIdEnsure(Long id);

    List<LoginLog> findByIdCollection(Collection<Long> idCollection);

    LoginLog save(LoginLogCreator creator) throws LoginLogException;

    QResponse<LoginLog> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws LoginLogException;

    List<LoginLog> findLatestNByUsername(String username, int n);

    LoginLog save(String loginSource, String username, Long sysUserId, YesNoStatus loginStatus, String loginRemark) throws LoginLogException;
}
