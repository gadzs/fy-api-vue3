package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

/**
 * @author hai
 */
public interface HousekeepingShopAuditRecordDAO extends FYRepository<HousekeepingShopAuditRecordPO, Long> {

    List<HousekeepingShopAuditRecordPO> findByCompanyIdOrderByCreatedTimeDesc(Long companyId);

    List<HousekeepingShopAuditRecordPO> findByShopIdOrderByCreatedTimeDesc(Long shopId);
    
}
