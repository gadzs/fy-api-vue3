package cn.turboinfo.fuyang.api.gateway.admin.fo.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class HousekeepingStaffSearchFO {

    @QField(
            placeholder = "公司编码"
    )
    private Long companyId;

    @QField(
            placeholder = "家政员名称",
            operator = Operator.EQUAL
    )
    private String name;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            template = "enum"
    )
    private StaffStatus status;

    private String staffType;

}
