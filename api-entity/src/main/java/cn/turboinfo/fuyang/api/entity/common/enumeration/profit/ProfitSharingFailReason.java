package cn.turboinfo.fuyang.api.entity.common.enumeration.profit;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 分账失败原因
 */
@Slf4j
public enum ProfitSharingFailReason implements BaseEnum {

    INIT(0, "未分账", "INIT"),

    ACCOUNT_ABNORMAL(10, "分账接收账户异常", "ACCOUNT_ABNORMAL"),

    NO_RELATION(20, "分账关系已解除", "NO_RELATION"),

    RECEIVER_HIGH_RISK(30, "高风险接收方", "RECEIVER_HIGH_RISK"),

    RECEIVER_REAL_NAME_NOT_VERIFIED(40, "接收方未实名", "RECEIVER_REAL_NAME_NOT_VERIFIED"),

    NO_AUTH(50, "分账权限已解除", "NO_AUTH"),

    RECEIVER_RECEIPT_LIMIT(60, "接收方已达收款限额", "RECEIVER_RECEIPT_LIMIT"),

    PAYER_ACCOUNT_ABNORMAL(70, "分出方账户异常", "PAYER_ACCOUNT_ABNORMAL"),

    ;

    private int value;
    private String name;
    private String code;

    ProfitSharingFailReason(int value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProfitSharingFailReason get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ProfitSharingFailReason> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ProfitSharingFailReason> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
