package cn.turboinfo.fuyang.api.gateway.admin.contract.knowledge

import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.*
import cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge.KnowledgeBaseCreateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge.KnowledgeBaseDeleteUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge.KnowledgeBaseUpdateUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/knowledge"])
interface KonwledgeContract {

    // 添加分类
    @PermissionScope("adminKnowledge:create")
    @CompanyScope
    @AutoMappingContract(
        beanType = KnowledgeBaseCreateUseCase::class,
    )
    fun create()

    // 修改分类
    @PermissionScope("adminKnowledge:update")
    @CompanyScope
    @AutoMappingContract(
        beanType = KnowledgeBaseUpdateUseCase::class,
    )
    fun update()


    @PermissionScope("adminKnowledge:delete")
    @AutoMappingContract(
        beanType = KnowledgeBaseDeleteUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/delete/{id}"]
    )
    fun delete()

}
