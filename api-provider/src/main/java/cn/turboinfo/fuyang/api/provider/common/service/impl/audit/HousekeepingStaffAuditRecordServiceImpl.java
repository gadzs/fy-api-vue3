package cn.turboinfo.fuyang.api.provider.common.service.impl.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.HousekeepingStaffAuditRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.HousekeepingStaffAuditRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class HousekeepingStaffAuditRecordServiceImpl extends DefaultQServiceImpl<HousekeepingStaffAuditRecord, Long, HousekeepingStaffAuditRecordPO, HousekeepingStaffAuditRecordDAO> implements HousekeepingStaffAuditRecordService {
    @Override
    public List<HousekeepingStaffAuditRecord> findByCompanyId(Long companyId) {
        return convertStreamQBeanToList(dao.findByCompanyIdOrderByCreatedTimeDesc(companyId).stream());
    }

    @Override
    public List<HousekeepingStaffAuditRecord> findByStaffId(Long staffId) {
        return convertStreamQBeanToList(dao.findByStaffIdOrderByCreatedTimeDesc(staffId).stream());
    }
}
