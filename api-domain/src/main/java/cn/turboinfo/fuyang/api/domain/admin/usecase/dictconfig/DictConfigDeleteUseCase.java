package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigDeleteUseCase extends AbstractUseCase<DictConfigDeleteUseCase.InputData, DictConfigDeleteUseCase.OutputData> {
    private final DictConfigService dictConfigService;

    @Override
    protected OutputData doAction(InputData inputData) {

        dictConfigService.deleteById(inputData.getDictConfigId());

        return OutputData.builder()
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long dictConfigId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
