package cn.turboinfo.fuyang.api.domain.mini.usecase.index;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 列表首页推荐分类
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniIndexListRecommendCategoryUseCase extends AbstractUseCase<MiniIndexListRecommendCategoryUseCase.InputData, MiniIndexListRecommendCategoryUseCase.OutputData> {

    private final CategoryService categoryService;

    @Override
    protected OutputData doAction(InputData inputData) {

        // TODO 暂不确定推荐怎么做 这里直接返回所有一级分类信息
        List<Category> categoryList = categoryService.findTop();

        return OutputData.builder()
                .categoryList(categoryList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 用户信息
         */
        private List<Category> categoryList;

    }
}
