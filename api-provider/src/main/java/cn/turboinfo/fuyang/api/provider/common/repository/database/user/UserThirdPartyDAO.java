package cn.turboinfo.fuyang.api.provider.common.repository.database.user;


import cn.turboinfo.fuyang.api.entity.common.enumeration.user.ThirdPartyType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserThirdPartyDAO extends BaseRepository<UserThirdPartyPO, Long> {
    List<UserThirdPartyPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<UserThirdPartyPO> findByUserId(Long userId);

    Optional<UserThirdPartyPO> findByUserIdAndThirdPartyType(Long userId, ThirdPartyType thirdPartyType);

    Optional<UserThirdPartyPO> findByThirdPartyAccountAndThirdPartyType(String thirdPartyAccount, ThirdPartyType thirdPartyType);
}
