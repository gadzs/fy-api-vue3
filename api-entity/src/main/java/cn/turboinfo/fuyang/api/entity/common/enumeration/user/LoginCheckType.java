package cn.turboinfo.fuyang.api.entity.common.enumeration.user;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

@Slf4j
public enum LoginCheckType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    FRONT_CREDENTIAL(10, "凭据"),
    FRONT_VERIFY_CODE(20, "验证码"),
    FRONT_THIRD_PARTY(30, "第三方"),

    ADMIN_CREDENTIAL(40, "后台凭据"),
    ADMIN_VERIFY_CODE(50, "后台验证码"),
    ADMIN_THIRD_PARTY(60, "后台第三方"),
    ;

    private int value;
    private String name;

    LoginCheckType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static LoginCheckType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<LoginCheckType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<LoginCheckType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
