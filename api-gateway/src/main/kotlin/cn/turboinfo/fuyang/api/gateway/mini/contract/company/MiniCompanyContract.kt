package cn.turboinfo.fuyang.api.gateway.mini.contract.company

import cn.turboinfo.fuyang.api.domain.mini.usecase.company.MiniViewCompanyUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/company"])
interface MiniCompanyContract {

    // 公司详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewCompanyUseCase::class,
        consumes = [],
        paths = ["/view/{companyId}"]
    )
    fun view()

}
