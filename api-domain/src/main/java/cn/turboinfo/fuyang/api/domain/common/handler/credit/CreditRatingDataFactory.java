package cn.turboinfo.fuyang.api.domain.common.handler.credit;


import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author hai
 */
@RequiredArgsConstructor
@Component
public class CreditRatingDataFactory {

    private final SysUserService sysUserService;

    public void assembleAppraiser(List<CreditRating> creditRatingList) {

        Set<Long> appraiserIdSet = creditRatingList.stream()
                .map(CreditRating::getAppraiserId)
                .collect(Collectors.toSet());

        Map<Long, SysUser> userMap = sysUserService.findByIdCollection(appraiserIdSet)
                .stream()
                .collect(Collectors.toMap(SysUser::getId, Function.identity()));

        creditRatingList
                .stream()
                .peek(it -> {
                    if (userMap.containsKey(it.getAppraiserId())) {
                        it.setAppraiserName(userMap.get(it.getAppraiserId()).getUsername());
                    }
                })
                .toList();
    }

    // 组装发起者信息
    public CreditRating assembleAppraiser(CreditRating creditRating) {
        val user = sysUserService.getByIdEnsure(creditRating.getAppraiserId());

        creditRating.setAppraiserName(user.getUsername());
        return creditRating;
    }

}
