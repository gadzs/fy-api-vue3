package cn.turboinfo.fuyang.api.domain.mini.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.company.CompanyDataFactory;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.fo.company.ViewCompanyFO;
import cn.turboinfo.fuyang.api.entity.common.fo.credit.ViewCreditRatingFO;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hai
 * @description 产品详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewProductUserCase extends AbstractUseCase<MiniViewProductUserCase.InputData, MiniViewProductUserCase.OutputData> {

    private final ProductService productService;

    private final FileAttachmentService fileAttachmentService;

    private final HousekeepingShopService housekeepingShopService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final CreditRatingAssembleHelper creditRatingAssembleHelper;

    private final ShopDataFactory shopDataFactory;

    private final CompanyDataFactory companyDataFactory;

    private final ServiceOrderDataFactory serviceOrderDataFactory;

    @Override
    protected MiniViewProductUserCase.OutputData doAction(MiniViewProductUserCase.InputData inputData) {

        Long productId = inputData.getProductId();
        Product product = productService.getByIdEnsure(productId);

        // 查询附件
        product.setImgList(fileAttachmentService.findByRefIdAndRefType(product.getId(), FileRefTypeConstant.PRODUCT_IMG)
                .stream().map(FileAttachment::getId).collect(Collectors.toList()));
        fileAttachmentService.findByRefIdAndRefType(product.getId(), FileRefTypeConstant.PRODUCT_VIDEO).forEach(fileAttachment -> {
            product.getVideoList().add(fileAttachment.getId());
            product.getVideoNameList().add(fileAttachment.getDisplayName());
        });

        // 查询最近的五条评价
        Collection<ViewCreditRatingFO> ratingCollection = creditRatingAssembleHelper.findLatestPageable(
                        EntityObjectType.PRODUCT, productId, 0, 5, CreditRatingAssembleHelper.ASSEMBLE_USER).getPagedData()
                .stream()
                .map(it -> {

                    ViewCreditRatingFO fo = BeanMapper.map(it, ViewCreditRatingFO.class);
                    ServiceOrder serviceOrder = serviceOrderDataFactory.maskInfo(it.getServiceOrder());
                    fo.setServiceOrder(BeanMapper.map(serviceOrder, ViewServiceOrderFO.class));
                    return fo;
                })
                .toList();

        HousekeepingShop shop = housekeepingShopService.getByIdEnsure(product.getShopId());
        // 查询门店图片
        shop.setImgList(fileAttachmentService.findByRefIdAndRefType(shop.getId(), FileRefTypeConstant.SHOP_IMG)
                .stream().map(FileAttachment::getId).collect(Collectors.toList()));
        shopDataFactory.maskInfo(shop);
        // TODO: 先不返回产品信息
//
//        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(product.getCompanyId());
//        companyDataFactory.maskInfo(company);

        return OutputData.builder()
                .product(product)
                .shop(BeanMapper.map(shop, ViewShopFO.class))
//                .company(BeanMapper.map(company, ViewCompanyFO.class))
                .creditRatingList(new ArrayList<>(ratingCollection))
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "产品编码不能为空"
        )
        private Long productId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Product product;

        private List<ViewCreditRatingFO> creditRatingList;

        private ViewShopFO shop;

        private ViewCompanyFO company;

    }
}
