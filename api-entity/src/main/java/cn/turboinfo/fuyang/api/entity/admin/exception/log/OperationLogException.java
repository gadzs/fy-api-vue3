package cn.turboinfo.fuyang.api.entity.admin.exception.log;

public class OperationLogException extends RuntimeException {
    public OperationLogException() {
        this("操作 OperationLog 出错");
    }

    public OperationLogException(String message) {
        super(message);
    }

    public OperationLogException(Throwable cause) {
        super(cause);
    }

    public OperationLogException(String message, Throwable cause) {
        super(message, cause);
    }
}
