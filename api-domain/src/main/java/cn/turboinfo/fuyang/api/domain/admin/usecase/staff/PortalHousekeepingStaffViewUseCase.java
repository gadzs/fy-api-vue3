package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.entity.common.fo.portal.PortalStaffDisplayFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class PortalHousekeepingStaffViewUseCase extends AbstractUseCase<PortalHousekeepingStaffViewUseCase.InputData, PortalHousekeepingStaffViewUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        String code = inputData.getCode();

        Optional<HousekeepingStaff> staffOptional = housekeepingStaffService.findByCode(code);

        if (staffOptional.isEmpty()) {
            return OutputData.builder()
                    .build();
        }

        HousekeepingStaff staff = staffOptional.get();

        // 企业信息
        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(staff.getCompanyId());

        // 地区信息
        Division division = divisionService.getByIdEnsure(staff.getProvinceCode());

        PortalStaffDisplayFO.PortalStaffDisplayFOBuilder foBuilder = PortalStaffDisplayFO.builder();

        BeanHelper.copyPropertiesToBuilder(foBuilder, HousekeepingStaff.class, staff);

        return OutputData.builder()
                .staff(foBuilder
                        .companyName(company.getName())
                        .idCard(IdCardUtils.maskIdCareNum(staff.getIdCard()))
                        .provinceName(division.getAreaName())
                        .age(IdCardUtils.countAge(staff.getIdCard()))
                        .build())
                .build();
    }


    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        @NotBlank(message = "放心码不能为空")
        private String code;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private PortalStaffDisplayFO staff;
    }
}
