package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SysUserCreateFO {
    @NotBlank(
            message = "用户名不能为空"
    )
    private String username;

    private String mobile;

    @NotNull(
            message = "状态不能为空"
    )
    private net.sunshow.toolkit.core.base.enums.EnableStatus status;

    @NotBlank(
            message = "密码不能为空"
    )
    private String password;

    /**
     * 机构编码
     */
    private Long agenciesId;
}
