package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.converter;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * 留言板
 */
@Converter(autoApply = true)
public class MessageBoardTypeConverter extends BaseEnumConverter<CmsMessageBoardType> {

}
