package cn.turboinfo.fuyang.api.gateway.mini.controller.user;

import cn.turboinfo.fuyang.api.gateway.mini.fo.user.MiniUserGetInfoResponse;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/mini/user")
public class MiniUserController {

    @SessionScope
    @GetMapping("/getInfo")
    public MiniUserGetInfoResponse getInfo(MiniRequestContext requestContext) {
        return MiniUserGetInfoResponse
                .builder()
                .userTypeRelList(requestContext.getUserTypeRelList())
                .userInfo(requestContext.getUser())
                .build();
    }

}
