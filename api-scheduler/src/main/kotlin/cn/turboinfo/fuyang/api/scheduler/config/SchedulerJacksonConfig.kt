package cn.turboinfo.fuyang.api.scheduler.config

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.text.SimpleDateFormat
import java.util.*

@Configuration
class SchedulerJacksonConfig {

    @Bean
    fun objectMapper(moduleList: List<Module>): ObjectMapper {
        return ObjectMapper()
            .apply {
                dateFormat = SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss",
                    Locale.CHINA
                )
                // 注册 jsr310 模块
                registerModule(JavaTimeModule())
                // 注册自定义模块
                registerModules(moduleList)
            }
    }

}
