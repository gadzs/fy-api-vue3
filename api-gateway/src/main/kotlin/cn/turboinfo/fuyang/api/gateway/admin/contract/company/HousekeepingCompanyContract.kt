package cn.turboinfo.fuyang.api.gateway.admin.contract.company

import cn.turboinfo.fuyang.api.domain.admin.usecase.company.*
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 家政企业自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/company"])
@ResponseBodyWrapper
interface HousekeepingCompanyContract {

    // 家政企业注册
    @AutoMappingContract(
        beanType = HousekeepingCompanyCreateUseCase::class
    )
    fun register()

    // 家政企业信息修改
    @CompanyScope
    @PermissionScope(value = ["company:update"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyUpdateUseCase::class
    )
    fun update()

    // 家政企业信息审核通过
    @PermissionScope(value = ["company:review"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyReviewedUseCase::class
    )
    fun reviewed()

    // 家政企业信息审核不通过
    @PermissionScope(value = ["company:review"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyNotPassUseCase::class
    )
    fun notpass()

    /**
     * 根据企业name获取列表
     */
    @PermissionScope(value = ["company:list", "shop:admin:list"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyListUseCase::class
    )
    fun listByName()

    /**
     * 信息获取
     */
    @CompanyScope
    @PermissionScope(value = ["company:update"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyViewUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = []
    )
    fun view()

    /**
     * 删除
     */
    @CompanyScope
    @PermissionScope(value = ["company:delete"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyDeleteUseCase::class
    )
    fun delete()

    // 可分配服务人员列表
    @PermissionScope(value = ["staff:list", "order:view"])
    @CompanyScope
    @AutoMappingContract(
        beanType = CompanyAvailableAssignStaffListUseCase::class,
    )
    fun availableAssignStaffList()

    // 分配服务人员
    @PermissionScope(value = ["order:view", "order:assign"])
    @CompanyScope
    @AutoMappingContract(
        beanType = HousekeepingCompanyAssignStaffUseCase::class,
    )
    fun assignStaff()

    // 取消订单
    @PermissionScope(value = ["order:view", "order:cancel"])
    @CompanyScope
    @AutoMappingContract(
        beanType = HousekeepingCompanyCancelServiceOrderUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = []
    )
    fun cancel()

    // 查看 appSecret
    @PermissionScope(value = ["company:view", "company:update"])
    @CompanyScope
    @AutoMappingContract(
        beanType = HousekeepingCompanyViewAppSecretUseCase::class,
    )
    fun viewAppSecret()

    // 家政企业审核状态查询
    @AutoMappingContract(
        beanType = HousekeepingCompanyAuditQueryUseCase::class
    )
    fun queryAudit()
}
