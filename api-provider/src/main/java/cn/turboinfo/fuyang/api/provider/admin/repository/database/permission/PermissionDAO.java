package cn.turboinfo.fuyang.api.provider.admin.repository.database.permission;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PermissionDAO extends BaseRepository<PermissionPO, Long> {

    List<PermissionPO> findByResourceIn(Collection<String> resourceCollection);

    List<PermissionPO> findByParentIdOrderBySortValueAscIdAsc(Long parentId);

    Optional<PermissionPO> findByResource(String resource);
    
}
