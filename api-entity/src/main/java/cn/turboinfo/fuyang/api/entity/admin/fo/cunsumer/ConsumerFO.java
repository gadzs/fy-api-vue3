package cn.turboinfo.fuyang.api.entity.admin.fo.cunsumer;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author gadzs
 * @description 消费者信息
 * @date 2023/2/17 17:17
 */
@Data
public class ConsumerFO {

    private Long id;
    private String mobile;
    private String nickName;
    private LocalDateTime createdTime;
    private Long orderNum = 0L;
}
