package cn.turboinfo.fuyang.api.gateway.admin.fo.permission;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PermissionCreateFO {
    @NotBlank(message = "权限名称不能为空")
    private String name;

    private String description;

    @NotBlank(message = "资源编码不能为空")
    private String resource;

    private String url;

    private String icon;

    private String component;

    @NotNull(message = "可见状态不能为空")
    private YesNoStatus visibleStatus;

    private Long parentId;

    private Integer sortValue;
}
