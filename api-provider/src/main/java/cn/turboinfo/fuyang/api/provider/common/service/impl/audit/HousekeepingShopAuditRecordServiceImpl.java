package cn.turboinfo.fuyang.api.provider.common.service.impl.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingShopAuditRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.HousekeepingShopAuditRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.HousekeepingShopAuditRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class HousekeepingShopAuditRecordServiceImpl extends DefaultQServiceImpl<HousekeepingShopAuditRecord, Long, HousekeepingShopAuditRecordPO, HousekeepingShopAuditRecordDAO> implements HousekeepingShopAuditRecordService {
    @Override
    public List<HousekeepingShopAuditRecord> findByCompanyId(Long companyId) {
        return convertStreamQBeanToList(dao.findByCompanyIdOrderByCreatedTimeDesc(companyId).stream());
    }

    @Override
    public List<HousekeepingShopAuditRecord> findByShopId(Long shopId) {
        return convertStreamQBeanToList(dao.findByShopIdOrderByCreatedTimeDesc(shopId).stream());
    }
}
