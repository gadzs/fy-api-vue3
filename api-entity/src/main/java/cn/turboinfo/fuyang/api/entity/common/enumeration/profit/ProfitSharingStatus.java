package cn.turboinfo.fuyang.api.entity.common.enumeration.profit;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 分账状态
 */
@Slf4j
public enum ProfitSharingStatus implements BaseEnum {

    INIT(0, "初始化", "INIT"),

    PROCESSING(10, "处理中", "PROCESSING"),

    FINISHED(20, "分账完成", "FINISHED"),

    ;

    private int value;
    private String name;
    private String code;

    ProfitSharingStatus(int value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProfitSharingStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ProfitSharingStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ProfitSharingStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
