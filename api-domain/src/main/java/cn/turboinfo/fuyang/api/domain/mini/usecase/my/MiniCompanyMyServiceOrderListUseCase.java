package cn.turboinfo.fuyang.api.domain.mini.usecase.my;

import cn.turboinfo.fuyang.api.domain.common.component.product.ProductAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyServiceOrderListItem;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 家政公司我的订单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniCompanyMyServiceOrderListUseCase extends AbstractUseCase<MiniCompanyMyServiceOrderListUseCase.InputData, MiniCompanyMyServiceOrderListUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    private final ServiceOrderDataFactory serviceOrderDataFactory;

    private final ProductAssembleHelper productAssembleHelper;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long companyId = inputData.getCompanyId();
        ServiceOrderStatus orderStatus = inputData.getOrderStatus();
        QPage requestPage = inputData.getRequestPage();

        QRequest request = QRequest.newInstance()
                .filterEqual(QServiceOrder.companyId, companyId);
        if (orderStatus != null) {
            request.filterEqual(QServiceOrder.orderStatus, orderStatus);
        }

        // 按创建时间降序排序
        requestPage.addOrder(QServiceOrder.createdTime, QSort.Order.DESC);

        QResponse<ServiceOrder> orderQResponse = serviceOrderService.findAll(request, requestPage);

        // 拼装企业名称
        serviceOrderDataFactory.assembleCompany(orderQResponse.getPagedData().stream().toList());

        QResponse<MiniMyServiceOrderListItem> orderList = orderQResponse
                .map(serviceOrder -> {
                            serviceOrderDataFactory.maskInfo(serviceOrder);
                            ViewServiceOrderFO fo = BeanMapper.map(serviceOrder, ViewServiceOrderFO.class);

                            return MiniMyServiceOrderListItem.builder()
                                    .serviceOrder(fo)
                                    .build();
                        }
                );

        productAssembleHelper.assembleProduct(
                orderList.getPagedData(),
                item -> item.getServiceOrder().getProductId(),
                ProductAssembleHelper.ASSEMBLE_ATTACHMENT,
                MiniMyServiceOrderListItem::setProduct);

        return OutputData.builder()
                .orderList(orderList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        @NotNull(message = "家政公司ID不能为空")
        @Positive
        private Long companyId;

        /**
         * 服务订单状态
         */
        private ServiceOrderStatus orderStatus;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 我的订单列表
         */
        private QResponse<MiniMyServiceOrderListItem> orderList;

    }
}
