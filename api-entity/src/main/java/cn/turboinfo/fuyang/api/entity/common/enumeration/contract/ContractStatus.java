package cn.turboinfo.fuyang.api.entity.common.enumeration.contract;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 合同状态
 *
 * @author hai.
 */
@Slf4j
public enum ContractStatus implements BaseEnum {

    INIT(0, "待签署"),

    SIGNED(90, "已签署"),

    INVALID(100, "已作废"),
    ;

    private int value;
    private String name;

    ContractStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ContractStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ContractStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ContractStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
