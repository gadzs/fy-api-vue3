package cn.turboinfo.fuyang.api.domain.mini.usecase.login;

import cn.turboinfo.fuyang.api.domain.common.service.SmsService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.sms.SmsType;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import nxcloud.foundation.core.validation.constraints.MobileNumber;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;

/**
 * 发送登录短信验证码
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniSendLoginSmsCodeUseCase extends AbstractUseCase<MiniSendLoginSmsCodeUseCase.InputData, MiniSendLoginSmsCodeUseCase.OutputData> {

    private final SmsService smsService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val mobile = inputData.getMobile();

        smsService.sendCode(SmsType.RegisterOrLogin, mobile);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @MobileNumber
        @NotEmpty(message = "手机号不能为空")
        private String mobile;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
