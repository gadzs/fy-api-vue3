package cn.turboinfo.fuyang.api.gateway.admin.framework.http;


import cn.turboinfo.fuyang.api.entity.admin.constant.AdminRestConstants;

public class ResponseBodyWrapperException extends RuntimeException {

    private final int code;

    private final Object data;

    public ResponseBodyWrapperException(int code, String message) {
        this(code, message, null, null);
    }

    public ResponseBodyWrapperException(String message) {
        this(message, null);
    }

    public ResponseBodyWrapperException(String message, Throwable cause) {
        this(AdminRestConstants.RC_ERROR_DEFAULT, message, null, cause);
    }

    public ResponseBodyWrapperException(int code, String message, Object data, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

}
