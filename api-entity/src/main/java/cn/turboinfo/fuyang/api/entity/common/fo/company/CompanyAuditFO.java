package cn.turboinfo.fuyang.api.entity.common.fo.company;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CompanyAuditFO {

    /**
     * 审核状态
     */
    private AuditStatus auditStatus;

    private String auditStatusName;

    /**
     * 审核人名称
     */
    private String auditUserName;

    /**
     * 备注
     */
    private String remark;

    private LocalDateTime auditTime;

}
