package cn.turboinfo.fuyang.api.provider.common.service.impl.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuditRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuditRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyAuditRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyAuditRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class HousekeepingCompanyAuditRecordServiceImpl extends DefaultQServiceImpl<HousekeepingCompanyAuditRecord, Long, HousekeepingCompanyAuditRecordPO, HousekeepingCompanyAuditRecordDAO> implements HousekeepingCompanyAuditRecordService {
    @Override
    public List<HousekeepingCompanyAuditRecord> findByCompanyId(Long companyId) {
        return convertStreamQBeanToList(dao.findByCompanyIdOrderByCreatedTimeDesc(companyId).stream());
    }
}
