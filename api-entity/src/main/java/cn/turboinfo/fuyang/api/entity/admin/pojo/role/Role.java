package cn.turboinfo.fuyang.api.entity.admin.pojo.role;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class Role extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    private String code;

    private String name;

    private String description;

    /**
     * 是否系统保留角色
     */
    private YesNoStatus isReserved;

    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
