package cn.turboinfo.fuyang.api.domain.mini.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.ServiceOrderCreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Collection;
import java.util.List;

/**
 * 查看家政人员详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewStaffUseCase extends AbstractUseCase<MiniViewStaffUseCase.InputData, MiniViewStaffUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;

    private final CreditRatingAssembleHelper creditRatingAssembleHelper;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long staffId = inputData.getStaffId();
        val staff = housekeepingStaffService.getByIdEnsure(staffId);

        // 查询最近的五条评价
        Collection<ServiceOrderCreditRating> ratingCollection = creditRatingAssembleHelper.findLatestPageable(
                EntityObjectType.STAFF, staffId, 0, 5, CreditRatingAssembleHelper.ASSEMBLE_USER).getPagedData();

        return OutputData.builder()
                .staff(staff)
                .creditRatingList(List.copyOf(ratingCollection))
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 家政人员ID
         */
        @NotNull(
                message = "家政服务人员ID不能为空"
        )
        @Positive
        private Long staffId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingStaff staff;

        private List<ServiceOrderCreditRating> creditRatingList;

    }
}
