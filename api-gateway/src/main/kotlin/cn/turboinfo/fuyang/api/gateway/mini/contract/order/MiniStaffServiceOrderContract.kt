package cn.turboinfo.fuyang.api.gateway.mini.contract.order

import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniStaffCancelServiceOrderUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniStaffConfirmServiceCompletedUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniStaffConfirmServiceOrderUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniStaffRejectServiceOrderUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@StaffScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/staff/order"])
interface MiniStaffServiceOrderContract {

    // 服务人员确认接单
    @SessionScope
    @AutoMappingContract(
        beanType = MiniStaffConfirmServiceOrderUseCase::class,
    )
    fun confirm()

    // 服务人员拒绝接单
    @SessionScope
    @AutoMappingContract(
        beanType = MiniStaffRejectServiceOrderUseCase::class,
    )
    fun reject()

    // 取消订单
    @AutoMappingContract(
        beanType = MiniStaffCancelServiceOrderUseCase::class,
    )
    fun cancel()

    // 确认完成服务并追加可能需要的额外费用
    @AutoMappingContract(
        beanType = MiniStaffConfirmServiceCompletedUseCase::class,
    )
    fun confirmServiceCompleted()

}