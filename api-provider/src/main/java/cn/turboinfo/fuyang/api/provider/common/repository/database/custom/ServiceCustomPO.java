package cn.turboinfo.fuyang.api.provider.common.repository.database.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustomStatusLog;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 定制服务
 * author: hai
 */
@Table(
        name = "service_custom"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Where(clause = "deleted = 0")
@Entity
public class ServiceCustomPO extends SoftDeleteFYEntity {
    /**
     * 用户编码
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    /**
     * 服务类型
     */
    @Column(
            name = "category_id"
    )
    private Long categoryId;

    /**
     * 公司ID
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 店铺ID
     */
    @Column(
            name = "shop_id"
    )
    private Long shopId;

    /**
     * 服务人员ID
     */
    @Column(
            name = "staff_id"
    )
    private Long staffId;

    /**
     * 预算
     */
    private BigDecimal budget;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 预付金额
     */
    private BigDecimal prepaid;

    /**
     * 定金
     */
    private BigDecimal deposit;

    /**
     * 额外费用 (由服务人员确认完成时添加)
     */
    @Column(
            name = "additional_fee"
    )
    private BigDecimal additionalFee;

    /**
     * 减免费用 (由服务人员确认完成时添加)
     */
    @Column(
            name = "discount_fee"
    )
    private BigDecimal discountFee;

    /**
     * 选择的地址ID
     */
    @Column(
            name = "address_id"
    )
    private Long addressId;

    /**
     * 联系人
     */
    @Convert(converter = DataEncryptConverter.class)
    private String contact;

    /**
     * 性别
     */
    @Column(
            name = "gender_type"
    )
    private GenderType genderType;

    /**
     * 手机号
     */
    @Convert(converter = DataEncryptConverter.class)
    private String mobile;

    /**
     * 区域编码
     */
    @Column(
            name = "division_id"
    )
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    @Column(
            name = "poi_name"
    )
    private String poiName;

    /**
     * 详细地址
     */
    @Convert(converter = DataEncryptConverter.class)
    private String detail;

    /**
     * 人数
     */
    @Column(
            name = "people_num"
    )
    private Integer peopleNum;

    /**
     * 描述
     */
    private String description;

    /**
     * 计划开始时间
     */
    @Column(
            name = "scheduled_start_time"
    )
    private LocalDateTime scheduledStartTime;

    /**
     * 计划结束时间
     */
    @Column(
            name = "scheduled_end_time"
    )
    private LocalDateTime scheduledEndTime;

    /**
     * 服务开始时间
     */
    @Column(
            name = "service_start_time"
    )
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    @Column(
            name = "service_end_time"
    )
    private LocalDateTime serviceEndTime;

    /**
     * 服务订单状态
     */
    @Column(
            name = "custom_status"
    )
    private ServiceCustomStatus customStatus;

    /**
     * 支付订单状态
     */
    @Column(
            name = "pay_status"
    )
    private ServiceOrderPayStatus payStatus;

    /**
     * 评价状态
     */
    @Column(
            name = "credit_rating_status"
    )
    private ServiceOrderCreditRatingStatus creditRatingStatus;

    /**
     * 退款订单状态
     */
    @Column(
            name = "refund_status"
    )
    private ServiceOrderRefundStatus refundStatus;

    /**
     * 分账状态
     */
    private ProfitSharingStatus profitSharingStatus;

    /**
     * 服务完成时间
     */
    @Column(
            name = "completed_time"
    )
    private LocalDateTime completedTime;

    /**
     * 服务订单状态历史
     */
    @Type(type = "json")
    @Column(name = "custom_status_history", columnDefinition = "json")
    private List<ServiceCustomStatusLog> customStatusHistory;
}
