package cn.turboinfo.fuyang.api.gateway.admin.controller.log;

import cn.turboinfo.fuyang.api.domain.admin.service.log.LoginLogService;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.LoginLog;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.QLoginLog;
import cn.turboinfo.fuyang.api.gateway.admin.fo.log.LoginLogSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/log/login")
public class LoginLogController {

    private final LoginLogService loginLogService;

    @RequiresPermissions("loginLog:list")
    @RequestMapping("/list")
    public RestResponseFO list(@Valid LoginLogSearchFO search, LimitFO page, SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QLoginLog.id + "|" + QSort.Order.DESC.name()});
        }

        QResponse<LoginLog> loginLogQResponse = loginLogService.findAll(request, page.toQPage(sort.toQSortList()));
        return RestResponseFO.ok(LimitDataFO.fromQResponse(loginLogQResponse));
    }
}
