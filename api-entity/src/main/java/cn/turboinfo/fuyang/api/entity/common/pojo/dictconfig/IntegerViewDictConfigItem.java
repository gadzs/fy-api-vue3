package cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 整数类型值的字典视图
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class IntegerViewDictConfigItem {

    /**
     * 字典Key
     */
    private String dictConfigKey;

    /**
     * 条目值
     */
    private Integer itemValue;

    /**
     * 条目名称
     */
    private String itemName;

}
