package cn.turboinfo.fuyang.api.domain.admin.usecase.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccountUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class CompanyAccountMarkAuthUseCase extends AbstractUseCase<CompanyAccountMarkAuthUseCase.InputData, CompanyAccountMarkAuthUseCase.OutputData> {
    private final CompanyAccountService companyAccountService;


    @Override
    protected OutputData doAction(InputData inputData) {

        List<Long> idList = inputData.getIdList();

        for (Long id : idList) {
            CompanyAccount companyAccount = companyAccountService.getByIdEnsure(id);

            CompanyAccountUpdater.Builder builder = CompanyAccountUpdater.builder(id);

            QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CompanyAccountUpdater.class, companyAccount);

            companyAccountService.update(builder
                    .withStatus(AccountStatus.AUTHORIZED)
                    .build());

        }

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private List<Long> idList;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
