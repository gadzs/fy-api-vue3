package cn.turboinfo.fuyang.api.entity.common.pojo.contract;

import cn.turboinfo.fuyang.api.entity.common.pojo.file.ViewAttachment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 合同管理
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ContractTmpl extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 合同名称
     */
    private String name;

    /**
     * 合同类别
     */
    private Long categoryId;
    private String categoryName;

    /**
     * 企业编码
     */
    private Long companyId;
    private String companyName;

    /**
     * 特殊场景容器 (附件列表)
     */
    private List<ViewAttachment> attachmentList;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
