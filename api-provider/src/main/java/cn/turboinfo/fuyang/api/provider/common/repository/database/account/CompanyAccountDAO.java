package cn.turboinfo.fuyang.api.provider.common.repository.database.account;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface CompanyAccountDAO extends BaseRepository<CompanyAccountPO, Long> {

    Optional<CompanyAccountPO> findFirstByCompanyIdAndTypeOrderByIdDesc(Long companyId, AccountType type);

    List<CompanyAccountPO> findByCompanyIdOrderByTypeAsc(Long companyId);

}
