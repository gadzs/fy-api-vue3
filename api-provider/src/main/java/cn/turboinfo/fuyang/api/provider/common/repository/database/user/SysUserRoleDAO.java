package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface SysUserRoleDAO extends BaseRepository<SysUserRolePO, Long> {

    List<SysUserRolePO> findBySysUserId(Long sysUserId);

    List<SysUserRolePO> findByRoleId(Long roleId);

    Optional<SysUserRolePO> findBySysUserIdAndRoleId(Long sysUserId, Long roleId);
}
