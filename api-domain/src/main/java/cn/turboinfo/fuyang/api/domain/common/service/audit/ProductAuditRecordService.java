package cn.turboinfo.fuyang.api.domain.common.service.audit;

import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 产品服务审核记录服务
 *
 * @author: hai
 */
public interface ProductAuditRecordService extends BaseQService<ProductAuditRecord, Long> {

    List<ProductAuditRecord> findByProductId(Long productId);

}
