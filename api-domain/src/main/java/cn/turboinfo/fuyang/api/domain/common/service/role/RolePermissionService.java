package cn.turboinfo.fuyang.api.domain.common.service.role;

import cn.turboinfo.fuyang.api.entity.admin.exception.user.RolePermissionException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RolePermission;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RolePermissionCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RolePermissionUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RolePermissionService {
    Optional<RolePermission> getById(Long id);

    RolePermission save(RolePermissionCreator creator) throws RolePermissionException;

    RolePermission update(RolePermissionUpdater updater) throws RolePermissionException;

    QResponse<RolePermission> findAll(QRequest request, QPage requestPage);

    List<RolePermission> findByRoleId(Long roleId);

    List<RolePermission> findByRoleIdCollection(Collection<Long> roleIdCollection);

    /**
     * 重新分配权限
     *
     * @param roleId                 角色ID
     * @param permissionIdCollection 要分配的权限ID集合
     */
    void reassign(Long roleId, Collection<Long> permissionIdCollection);
}
