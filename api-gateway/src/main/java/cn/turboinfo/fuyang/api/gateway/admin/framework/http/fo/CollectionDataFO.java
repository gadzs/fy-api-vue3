package cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * author: sunshow.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CollectionDataFO<T> {

    private Collection<T> items;

}
