package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.handler.custom.ServiceCustomDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.custom.ViewServiceCustomFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.QServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

/**
 * 定制服务订单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniStaffServiceCustomListUseCase extends AbstractUseCase<MiniStaffServiceCustomListUseCase.InputData, MiniStaffServiceCustomListUseCase.OutputData> {

    private final ServiceCustomService serviceCustomService;

    private final ServiceCustomDataFactory serviceCustomDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long staffId = inputData.getStaffId();
        List<ServiceCustomStatus> customStatus = inputData.getCustomStatus();
        QPage requestPage = inputData.getRequestPage();

        QRequest request = QRequest.newInstance()
                .filterEqual(QServiceCustom.staffId, staffId);

        if (customStatus != null && !customStatus.isEmpty()) {
            request.filterIn(QServiceCustom.customStatus, Collections.singleton(customStatus));
        }

        // 按创建时间降序排序
        requestPage.addOrder(QServiceCustom.createdTime, QSort.Order.DESC);

        QResponse<ServiceCustom> orderQResponse = serviceCustomService.findAll(request, requestPage);

        // 拼装企业名称
        serviceCustomDataFactory.assembleCompany(orderQResponse.getPagedData().stream().toList());

        QResponse<ViewServiceCustomFO> orderList = orderQResponse
                .map(serviceOrder -> {
                            serviceCustomDataFactory.maskInfo(serviceOrder);
                            return BeanMapper.map(serviceOrder, ViewServiceCustomFO.class);
                        }
                );

        return OutputData.builder()
                .customList(orderList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "家政员ID不能为空")
        private Long staffId;

        /**
         * 服务订单状态
         */
        private List<ServiceCustomStatus> customStatus;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 我的订单列表
         */
        private QResponse<ViewServiceCustomFO> customList;

    }
}
