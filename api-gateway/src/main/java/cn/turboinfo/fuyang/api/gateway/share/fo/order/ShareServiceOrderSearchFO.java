package cn.turboinfo.fuyang.api.gateway.share.fo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author gadzs
 * @description 订单检索FO
 * @date 2023/2/16 18:00
 */
@Data
@QField
public class ShareServiceOrderSearchFO {

    @QField(
            placeholder = "公司门店编码"
    )
    private Long shopId;

    @QField(
            placeholder = "分类编码"
    )
    private Long categoryId;

    @QField(
            searchable = false
    )
    private String status;

    @QField(
            searchable = false
    )
    @DateTimeFormat(pattern = "yyyy-MM-dd")                    // 表示返回时间类型
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")      // 表示接收时间类型
    private LocalDate startDate;

    @QField(
            searchable = false
    )
    @DateTimeFormat(pattern = "yyyy-MM-dd")                    // 表示返回时间类型
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")      // 表示接收时间类型
    private LocalDate endDate;
}
