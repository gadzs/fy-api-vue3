package cn.turboinfo.fuyang.api.domain.admin.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 家政公司分配服务人员
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ServiceCustomAssignStaffUseCase extends AbstractUseCase<ServiceCustomAssignStaffUseCase.InputData, ServiceCustomAssignStaffUseCase.OutputData> {

    private final ServiceCustomService serviceCustomService;

    private final HousekeepingStaffService housekeepingStaffService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long serviceOrderId = inputData.getServiceOrderId();
        Long companyId = inputData.getCompanyId();
        Long staffId = inputData.getStaffId();

        ServiceCustom serviceCustom = serviceCustomService.getByIdEnsure(serviceOrderId);

        if (!Objects.equals(companyId, serviceCustom.getCompanyId())) {
            throw new IllegalArgumentException("订单所属公司不匹配");
        }
        if (serviceCustom.getCustomStatus() != ServiceCustomStatus.PENDING_DISPATCH) {
            throw new IllegalArgumentException("服务订单状态不正确");
        }

        HousekeepingStaff staff = housekeepingStaffService.getByIdEnsure(staffId);

        // 判断服务人员是否一致
        if (!Objects.equals(staff.getCompanyId(), companyId)) {
            throw new RuntimeException("待分配服务人员不属于当前公司");
        }

        // 派单
        serviceCustomService.dispatchToStaff(serviceOrderId, staff.getId());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 所属公司ID
         */
        @NotNull(message = "所属公司不能为空")
        @Positive
        private Long companyId;

        /**
         * 服务订单ID
         */
        @NotNull(message = "服务订单不能为空")
        @Positive
        private Long serviceOrderId;

        @NotNull(message = "放心码不能为空")
        private Long staffId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
