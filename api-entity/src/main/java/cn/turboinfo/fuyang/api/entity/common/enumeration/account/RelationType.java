package cn.turboinfo.fuyang.api.entity.common.enumeration.account;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public enum RelationType implements BaseEnum {

    DEFAULT(0, "默认", "DEFAULT"),

    STORE(5, "门店", "STORE"),

    STAFF(10, "员工", "STAFF"),
    STORE_OWNER(15, "店主", "STORE_OWNER"),
    PARTNER(20, "合作伙伴", "PARTNER"),
    HEADQUARTER(25, "总部", "HEADQUARTER"),
    BRAND(30, "品牌方", "BRAND"),
    DISTRIBUTOR(35, "分销商", "DISTRIBUTOR"),
    USER(40, "用户", "USER"),
    SUPPLIER(45, "供应商", "SUPPLIER"),
    CUSTOM(50, "自定义 ", "CUSTOM"),

    ;

    private int value;
    private String name;

    private String code;

    RelationType(int value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }

    public static RelationType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static RelationType getByName(String name) {
        return BaseEnumHelper.getByName(name, values());
    }

    public static List<RelationType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<RelationType> listWithoutDefault() {
        return BaseEnumHelper.getList(values()).stream().filter(v -> v != DEFAULT).collect(Collectors.toList());
    }

    public static List<RelationType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
