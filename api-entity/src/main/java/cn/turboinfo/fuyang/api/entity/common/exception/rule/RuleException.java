package cn.turboinfo.fuyang.api.entity.common.exception.rule;

public class RuleException extends RuntimeException {
    public RuleException() {
        this("操作规则出错");
    }

    public RuleException(String message) {
        super(message);
    }

    public RuleException(Throwable cause) {
        super(cause);
    }

    public RuleException(String message, Throwable cause) {
        super(message, cause);
    }
}
