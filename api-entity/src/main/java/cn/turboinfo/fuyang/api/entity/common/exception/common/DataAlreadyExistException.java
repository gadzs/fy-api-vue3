package cn.turboinfo.fuyang.api.entity.common.exception.common;

/**
 * 数据已存在
 *
 * @author qatang
 */
public class DataAlreadyExistException extends RuntimeException {

    public DataAlreadyExistException() {
        super("数据已存在");
    }

    public DataAlreadyExistException(String message) {
        super(message);
    }
}
