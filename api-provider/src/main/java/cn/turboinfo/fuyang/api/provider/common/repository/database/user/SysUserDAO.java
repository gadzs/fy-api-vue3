package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface SysUserDAO extends BaseRepository<SysUserPO, Long> {

    Optional<SysUserPO> findByUsername(String username);

    Optional<SysUserPO> findByMobile(String mobile);

    List<SysUserPO> findByIdInOrderByIdAsc(Collection<Long> idCollection);
}
