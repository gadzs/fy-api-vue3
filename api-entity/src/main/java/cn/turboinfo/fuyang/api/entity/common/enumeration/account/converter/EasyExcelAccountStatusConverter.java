package cn.turboinfo.fuyang.api.entity.common.enumeration.account.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * author: sunshow.
 */
public class EasyExcelAccountStatusConverter implements Converter<AccountStatus> {
    @Override
    public Class<AccountStatus> supportJavaTypeKey() {
        return AccountStatus.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    @Override
    public AccountStatus convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return Converter.super.convertToJavaData(cellData, contentProperty, globalConfiguration);
    }

    /**
     * 将从数据库中查到的数据转换为 Excel 展示的数据
     *
     * @param value 枚举对象
     */
    @Override
    public WriteCellData<?> convertToExcelData(AccountStatus value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        // 将枚举类型按照 key 传值
        return new WriteCellData<>(value.getName());
    }
}
