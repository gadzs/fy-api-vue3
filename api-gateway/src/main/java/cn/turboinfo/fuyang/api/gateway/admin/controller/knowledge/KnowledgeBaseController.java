package cn.turboinfo.fuyang.api.gateway.admin.controller.knowledge;

import cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge.KnowledgeBaseSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.QKnowledgeBase;
import cn.turboinfo.fuyang.api.gateway.admin.fo.knowledge.KnowledgeBaseSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/knowledge")
public class KnowledgeBaseController {
    private final KnowledgeBaseSearchUseCase knowledgeBaseSearchUseCase;

    @RequiresPermissions("adminKnowledge:list")
    @RequestMapping("/search")
    public LimitDataFO<KnowledgeBase> list(@Valid KnowledgeBaseSearchFO search, LimitFO page,
                                           SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QKnowledgeBase.id + "|" + QSort.Order.DESC.name()});
        }

        KnowledgeBaseSearchUseCase.OutputData outputData = knowledgeBaseSearchUseCase.execute(KnowledgeBaseSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
