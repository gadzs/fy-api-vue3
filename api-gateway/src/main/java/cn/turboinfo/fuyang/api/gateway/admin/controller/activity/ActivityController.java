package cn.turboinfo.fuyang.api.gateway.admin.controller.activity;

import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.ActivityExportExcelUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.ActivitySearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.QDictConfig;
import cn.turboinfo.fuyang.api.gateway.admin.fo.activity.ActivitySearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/activity")
public class ActivityController {

    private final ActivitySearchUseCase activitySearchUseCase;

    private final ActivityExportExcelUseCase activityExportExcelUseCase;

    @RequiresPermissions("activity:list")
    @RequestMapping("/search")
    @CompanyScope
    public LimitDataFO<Activity> list(@Valid ActivitySearchFO search, LimitFO page,
                                      SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QDictConfig.id + "|" + QSort.Order.ASC.name()});
        }

        ActivitySearchUseCase.OutputData outputData = activitySearchUseCase.execute(ActivitySearchUseCase.InputData
                .builder()
                .companyId(context.getCompanyId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequestMapping("/importExcel")
    @RequiresPermissions("activity:list")
    public void importEmployeeExcel(@RequestParam Long activityId, HttpServletResponse response) {

        activityExportExcelUseCase.execute(ActivityExportExcelUseCase.InputData
                .builder()
                .activityId(activityId)
                .response(response)
                .build());
    }
}
