package cn.turboinfo.fuyang.api.gateway.admin.contract.staff

import cn.turboinfo.fuyang.api.domain.admin.usecase.staff.*
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 企业家政员自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/staff"])
@ResponseBodyWrapper
interface HousekeepingStaffContract {

    // 家政员添加
    @CompanyScope
    @PermissionScope(value = ["staff:create"])
    @AutoMappingContract(
        beanType = HousekeepingStaffCreateUseCase::class
    )
    fun create()

    // 家政员信息修改
    @CompanyScope
    @PermissionScope(value = ["staff:update"])
    @AutoMappingContract(
        beanType = HousekeepingStaffUpdateUseCase::class
    )
    fun update()

    /**
     * 家政员删除
     */
    @CompanyScope
    @PermissionScope(value = ["staff:delete"])
    @AutoMappingContract(
        beanType = HousekeepingStaffDeleteUseCase::class
    )
    fun delete()

    /**
     * 家政员审核
     */
    @CompanyScope
    @PermissionScope(value = ["staff:create", "staff:update"])
    @AutoMappingContract(
        beanType = HousekeepingStaffReviewUseCase::class
    )
    fun review()

    /**
     * 批量导入家政员
     */
    @CompanyScope
    @PermissionScope(value = ["staff:create", "staff:update"])
    @AutoMappingContract(
        beanType = HousekeepingStaffImportUseCase::class
    )
    fun batchImport()


    /**
     * 家政员详情
     */
    @CompanyScope
    @PermissionScope(value = ["shop:list", "adminAudit:shopList"])
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = HousekeepingStaffViewUseCase::class,
        consumes = [],
        paths = ["/view/{id}"]
    )
    fun view()
}
