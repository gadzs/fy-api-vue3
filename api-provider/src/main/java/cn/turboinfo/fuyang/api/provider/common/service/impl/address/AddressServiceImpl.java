package cn.turboinfo.fuyang.api.provider.common.service.impl.address;

import cn.turboinfo.fuyang.api.domain.common.service.address.AddressService;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.AddressCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.address.AddressDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.address.AddressPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class AddressServiceImpl extends DefaultQServiceImpl<Address, Long, AddressPO, AddressDAO> implements AddressService {

    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public Address save(AddressCreator creator) {
        AddressPO addressPO = new AddressPO();
        QBeanCreatorHelper.copyCreatorField(addressPO, creator);

        // 手动生成ID
        addressPO.setId(idGeneratorFacade.nextId());
        return convertQBean(dao.save(addressPO));
    }

}
