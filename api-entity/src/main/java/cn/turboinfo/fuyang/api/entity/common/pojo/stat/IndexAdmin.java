package cn.turboinfo.fuyang.api.entity.common.pojo.stat;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author gadzs
 * @description index页统计
 * @date 2023/3/7 16:00
 */
@Data
public class IndexAdmin {

    Long totalCompanyNum;
    Long totalShopNum;
    Long totalStaffNum;
    Long totalOrderNum;
    BigDecimal totalOrderAmount;
    Long totalCustomerNum;

    private List<Map<String, Object>> customerMonthTrend;
    private List<Map<String, Object>> orderMonthTrend;
    private List<Map<String, Object>> orderCategoryTrend;
    private List<Map<String, Object>> orderCompanyTrend;

    public List<Map<String, Object>> getCustomerMonthTrend() {
        if (customerMonthTrend == null) {
            customerMonthTrend = new ArrayList<>();
        }
        return customerMonthTrend;
    }

    public List<Map<String, Object>> getOrderMonthTrend() {
        if (orderMonthTrend == null) {
            orderMonthTrend = new ArrayList<>();
        }
        return orderMonthTrend;
    }

    public List<Map<String, Object>> getOrderCategoryTrend() {
        if (orderCategoryTrend == null) {
            orderCategoryTrend = new ArrayList<>();
        }
        return orderCategoryTrend;
    }

    public List<Map<String, Object>> getOrderCompanyTrend() {
        if (orderCompanyTrend == null) {
            orderCompanyTrend = new ArrayList<>();
        }
        return orderCompanyTrend;
    }
}
