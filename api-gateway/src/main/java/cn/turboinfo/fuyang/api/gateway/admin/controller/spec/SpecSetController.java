package cn.turboinfo.fuyang.api.gateway.admin.controller.spec;

import cn.turboinfo.fuyang.api.domain.admin.usecase.spec.SpecSetSearchUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.spec.SpecSetViewUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.QDictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSet;
import cn.turboinfo.fuyang.api.gateway.admin.fo.spec.SpecSetSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/specSet")
public class SpecSetController {

    private final SpecSetSearchUseCase specSetSearchUseCase;
    private final SpecSetViewUseCase specSetViewUseCase;

    @RequiresPermissions(value = {"spec:list", "product:create", "product:update"}, logical = Logical.OR)
    @RequestMapping("/list")
    @CompanyScope
    public LimitDataFO<SpecSet> list(@Valid SpecSetSearchFO search, LimitFO page,
                                     SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QDictConfig.id + "|" + QSort.Order.DESC.name()});
        }

        SpecSetSearchUseCase.OutputData outputData = specSetSearchUseCase.execute(SpecSetSearchUseCase.InputData
                .builder()
                .companyId(context.getCompanyId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }


}
