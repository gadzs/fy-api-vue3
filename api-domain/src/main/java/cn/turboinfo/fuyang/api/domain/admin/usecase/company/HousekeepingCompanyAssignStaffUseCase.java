package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 家政公司分配服务人员
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAssignStaffUseCase extends AbstractUseCase<HousekeepingCompanyAssignStaffUseCase.InputData, HousekeepingCompanyAssignStaffUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    private final HousekeepingStaffService housekeepingStaffService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long serviceOrderId = inputData.getServiceOrderId();
        Long companyId = inputData.getCompanyId();
        Long staffId = inputData.getStaffId();

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(serviceOrderId);

        if (!Objects.equals(companyId, serviceOrder.getCompanyId())) {
            throw new IllegalArgumentException("订单所属公司不匹配");
        }
        if (serviceOrder.getOrderStatus() != ServiceOrderStatus.PENDING_DISPATCH) {
            throw new IllegalArgumentException("服务订单状态不正确");
        }

        HousekeepingStaff staff = housekeepingStaffService.getByIdEnsure(staffId);

        // 判断服务人员是否一致
        if (!Objects.equals(staff.getCompanyId(), companyId)) {
            throw new RuntimeException("待分配服务人员不属于当前公司");
        }

        // 派单
        serviceOrderService.dispatchToStaff(serviceOrderId, staff.getId());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 所属公司ID
         */
        @NotNull(message = "所属公司不能为空")
        @Positive
        private Long companyId;

        /**
         * 服务订单ID
         */
        @NotNull(message = "服务订单不能为空")
        @Positive
        private Long serviceOrderId;

        @NotNull(message = "放心码不能为空")
        private Long staffId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
