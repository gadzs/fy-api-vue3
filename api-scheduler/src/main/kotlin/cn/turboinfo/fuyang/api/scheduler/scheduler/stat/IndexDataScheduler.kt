package cn.turboinfo.fuyang.api.scheduler.scheduler.stat

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.stat.ScheduleStatIndexDataUserCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 首页数据统计处理
 */
@Component
class IndexDataScheduler(
    private val scheduleStatIndexDataUserCase: ScheduleStatIndexDataUserCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 2000, fixedDelay = 3600000)
    fun run() {
        runCatching {
            scheduleStatIndexDataUserCase.execute(
                ScheduleStatIndexDataUserCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "首页数据统计处理"
            }
        }
    }

}
