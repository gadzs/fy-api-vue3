package cn.turboinfo.fuyang.api.scheduler.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConfigurationProperties(prefix = "kit.wechat.pay")
@ConstructorBinding
class SchedulerWxPayProperties(
    /**
     * 设置微信公众号或者小程序等的appid
     */
    val appId: String = "",

    /**
     * 微信支付商户号
     */
    val mchId: String = "",

    /**
     * api v3 key
     */
    val apiV3Key: String = "",

    /**
     * 商户证书序列号
     */
    val mchNo: String = "",

    /**
     * apiclient_key.pem
     */
    val keyPath: String = "",

    /**
     * apiclient_cert.pem
     */
    val certPath: String = ""
)

