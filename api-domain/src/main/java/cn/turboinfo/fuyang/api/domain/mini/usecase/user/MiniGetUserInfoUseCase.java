package cn.turboinfo.fuyang.api.domain.mini.usecase.user;

import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 信息
 *
 * @author yuhai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniGetUserInfoUseCase extends AbstractUseCase<MiniGetUserInfoUseCase.InputData, MiniGetUserInfoUseCase.OutputData> {

    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {

        SysUser sysUser = sysUserService.getByIdEnsure(inputData.userId);

        sysUser.setUsername(PhoneUtils.maskPhoneNum(sysUser.getUsername()));
        sysUser.setMobile(PhoneUtils.maskPhoneNum(sysUser.getMobile()));

        return OutputData.builder()
                .userInfo(sysUser)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户编码不能为空")
        private Long userId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 用户信息
         */
        private SysUser userInfo;

    }
}
