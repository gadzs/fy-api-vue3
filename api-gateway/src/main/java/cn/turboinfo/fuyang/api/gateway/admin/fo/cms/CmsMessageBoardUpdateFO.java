package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class CmsMessageBoardUpdateFO {
    @NotNull(
            message = "id不能为空"
    )
    private Long id;

    private Long userId;

    private String username;

    private String title;

    private String content;

    private String mobile;

    private String email;

    private CmsMessageBoardType type;

    private String busiType;

    private YesNoStatus isShow;

    private Long replyUserId;

    private String replyUsername;

    private String replyContent;

    private LocalDateTime replyTime;

    private Integer sort;

    /**
     * 意见征集id
     */
    private Long adviceId;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
