package cn.turboinfo.fuyang.api.entity.common.pojo.file;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ViewAttachment {

    private Long fileAttachmentId;

    private String displayName;

    private String url;

}
