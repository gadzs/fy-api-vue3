package cn.turboinfo.fuyang.api.domain.common.service.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrderCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 支付订单
 */
public interface PayOrderService extends BaseQService<PayOrder, Long> {

    PayOrder save(PayOrderCreator creator);

    void checkAndUpdateStatus(Long payOrderId, PayOrderStatus status, PayOrderStatus checkStatus);

    List<PayOrder> findByObjectIdAndStatus(EntityObjectType objectType, Long objectId, PayOrderStatus status);

    List<PayOrder> findByObjectId(EntityObjectType objectType, Long objectId);

    List<PayOrder> findByObjectIdAndStatusAndPayType(EntityObjectType objectType, Long objectId, PayOrderStatus status, PayType payType);
}
