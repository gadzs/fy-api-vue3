package cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive;

import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWord;
import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWordCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SensitiveWordCreateUseCase extends AbstractUseCase<SensitiveWordCreateUseCase.InputData, SensitiveWordCreateUseCase.OutputData> {
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        SensitiveWordCreator.Builder builder = SensitiveWordCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, SensitiveWordCreator.class, inputData);

        SensitiveWord sensitiveWord = sensitiveWordService.save(builder.build());

        // 刷新
        sensitiveWordService.refresh();

        return OutputData.builder()
                .sensitiveWord(sensitiveWord)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "word不能为空"
        )
        private String word;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private SensitiveWord sensitiveWord;
    }
}
