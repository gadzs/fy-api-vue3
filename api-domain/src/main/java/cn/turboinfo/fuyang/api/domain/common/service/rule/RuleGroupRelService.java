package cn.turboinfo.fuyang.api.domain.common.service.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroupRel;

import java.util.List;

/**
 * 规则组关联服务
 */
public interface RuleGroupRelService {

    /**
     * 按关联对象ID列表
     *
     * @param objectType 对象类型
     * @param objectId   对象ID
     * @return 规则组列表
     */
    List<RuleGroupRel> findByObjectId(EntityObjectType objectType, Long objectId);

}
