package cn.turboinfo.fuyang.api.provider.common.service.impl.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuthLabelService;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyAuthLabelDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyAuthLabelPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class HousekeepingCompanyAuthLabelServiceImpl extends DefaultQServiceImpl<HousekeepingCompanyAuthLabel, Long, HousekeepingCompanyAuthLabelPO, HousekeepingCompanyAuthLabelDAO> implements HousekeepingCompanyAuthLabelService {


    @Override
    public Optional<HousekeepingCompanyAuthLabel> findByName(String name) {
        return dao.findByName(name).map(this::convertQBean);
    }

    @Override
    public List<HousekeepingCompanyAuthLabel> findList() {
        return convertStreamQBeanToList(dao.findAll().stream());
    }
}
