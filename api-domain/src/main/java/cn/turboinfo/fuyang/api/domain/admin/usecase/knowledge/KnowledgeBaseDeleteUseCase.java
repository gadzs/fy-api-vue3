package cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge;

import cn.turboinfo.fuyang.api.domain.common.service.knowledge.KnowledgeBaseService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class KnowledgeBaseDeleteUseCase extends AbstractUseCase<KnowledgeBaseDeleteUseCase.InputData, KnowledgeBaseDeleteUseCase.OutputData> {
    private final KnowledgeBaseService knowledgeBaseService;

    @Override
    protected OutputData doAction(InputData inputData) {

        knowledgeBaseService.deleteById(inputData.getId());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Getter
    @Setter
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
