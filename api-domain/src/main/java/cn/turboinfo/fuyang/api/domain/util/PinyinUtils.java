package cn.turboinfo.fuyang.api.domain.util;

import com.github.houbb.pinyin.constant.enums.PinyinStyleEnum;
import com.github.houbb.pinyin.util.PinyinHelper;
import org.apache.commons.lang3.StringUtils;

public class PinyinUtils {

    /**
     * 为字符串创建拼音索引
     *
     * @param text        来源字符串
     * @param indexLength 创建索引的最大长度
     * @return 拼音索引
     */
    public static String createPinyinIndex(String text, int indexLength) {
        // 直接转换后可能包括英文单词等 以空格隔开
        String firstLetters = PinyinHelper.toPinyin(text, PinyinStyleEnum.FIRST_LETTER).replaceAll("[^A-Za-z0-9]", "");
        ;
        // 去掉空格并取最大长度
        return StringUtils.substring(firstLetters.toUpperCase(), 0, indexLength);
    }
}
