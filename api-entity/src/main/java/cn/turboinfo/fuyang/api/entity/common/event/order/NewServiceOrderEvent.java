package cn.turboinfo.fuyang.api.entity.common.event.order;

import cn.turboinfo.fuyang.api.entity.common.event.AbstractEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 有新的下单事件
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class NewServiceOrderEvent extends AbstractEvent {

    private Long serviceOrderId;

}
