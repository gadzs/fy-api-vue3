package cn.turboinfo.fuyang.api.entity.common.pojo.credit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 信用评价记录
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class CreditRating extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 评价对象类型
     */
    private EntityObjectType objectType;

    /**
     * 评价对象ID
     */
    private Long objectId;

    /**
     * 评分（存整数）
     */
    private Long score;

    /**
     * 发起评价者ID
     */
    private Long appraiserId;
    private String appraiserName;

    /**
     * 评价内容
     */
    private String comment;

    /**
     * 附件ID列表
     */
    private List<Long> imgIdList;

    private CreditRatingStatus status;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
