package cn.turboinfo.fuyang.api.domain.admin.usecase.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.service.wechat.WechatPayService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccountUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class CompanyAccountUpdateUseCase extends AbstractUseCase<CompanyAccountUpdateUseCase.InputData, CompanyAccountUpdateUseCase.OutputData> {
    private final CompanyAccountService companyAccountService;

    private final WechatPayService wechatPayService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long id = inputData.getId();

        CompanyAccount originalAccount = companyAccountService.getByIdEnsure(id);

        try {
            // 删除原始账户
            wechatPayService.deleteProfitSharingReceiver(originalAccount.getAccount(), originalAccount.getType().getCode());

        } catch (RuntimeException e) {
            log.error("删除原始账户失败", e);
        }

        // 更新账户
        CompanyAccountUpdater.Builder builder = CompanyAccountUpdater.builder(id);

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CompanyAccountUpdater.class, inputData);

        CompanyAccount companyAccount = companyAccountService.update(builder.build());


        try {
            // 添加新账户
            wechatPayService.addProfitSharingReceiver(companyAccount.getAccount(), companyAccount.getName(), companyAccount.getType().getCode(), companyAccount.getRelationType().getCode());

        } catch (RuntimeException e) {
            log.error("添加微信账户失败", e);
            throw e;
        }

        return OutputData.builder()
                .companyAccount(companyAccount)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "企业ID不能为空"
        )
        private Long companyId;

        @NotNull(
                message = "账户类型不能为空"
        )
        private AccountType type;

        @NotBlank(
                message = "账户不能为空"
        )
        private String account;

        @NotBlank(
                message = "账户名称不能为空"
        )
        private String name;

        @NotNull(
                message = "关系类型不能为空"
        )
        private RelationType relationType;

        private String customRelation;

        @NotNull(
                message = "账户状态不能为空"
        )
        private AccountStatus status;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private CompanyAccount companyAccount;
    }
}
