package cn.turboinfo.fuyang.api.gateway.admin.fo.audit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

/**
 * @author hai
 */
@Data
@QField
public class HousekeepingShopAuditRecordSearchFO {
    @QField(
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "企业编码"
    )
    private Long companyId;

    @QField(
            placeholder = "门店编码"
    )
    private Long shopId;

    @QField(
            placeholder = "审核状态",
            control = Control.SELECT,
            ref = "auditStatusList",
            template = "enum"
    )
    private AuditStatus auditStatus;

    @QField(
            placeholder = "审核人"
    )
    private Long userId;

    @QField(
            placeholder = "审核人名称",
            operator = Operator.LIKE
    )
    private String userName;

    @QField(
            placeholder = "备注",
            operator = Operator.LIKE
    )
    private String remark;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
