package cn.turboinfo.fuyang.api.provider.common.repository.database.company;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface HousekeepingCompanyAuthLabelDAO extends BaseRepository<HousekeepingCompanyAuthLabelPO, Long> {
    List<HousekeepingCompanyAuthLabelPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    @Override
    List<HousekeepingCompanyAuthLabelPO> findAll();

    Optional<HousekeepingCompanyAuthLabelPO> findByName(String name);

}
