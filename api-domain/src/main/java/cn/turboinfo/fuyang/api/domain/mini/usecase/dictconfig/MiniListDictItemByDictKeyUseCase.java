package cn.turboinfo.fuyang.api.domain.mini.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 根据字典 key 获取字典信息列表
 *
 * @author yuhai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListDictItemByDictKeyUseCase extends AbstractUseCase<MiniListDictItemByDictKeyUseCase.InputData, MiniListDictItemByDictKeyUseCase.OutputData> {

    private final DictConfigItemService dictConfigItemService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<DictConfigItem> dictConfigItemList = dictConfigItemService.findAllDictConfigItem(inputData.getDictKey());

        return OutputData.builder()
                .dictConfigItemList(dictConfigItemList)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotBlank(message = "字典key不能为空")
        private String dictKey;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 用户信息
         */
        private List<DictConfigItem> dictConfigItemList;

    }
}
