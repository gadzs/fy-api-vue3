package cn.turboinfo.fuyang.api.provider.common.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import java.util.*

@Configuration
@Import(CustomEnumJacksonBeanDefinitionRegistrar::class)
class CommonJacksonConfig