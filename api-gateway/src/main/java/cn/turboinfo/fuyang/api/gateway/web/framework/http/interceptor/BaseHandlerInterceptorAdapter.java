package cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

import java.lang.annotation.Annotation;
import java.util.Optional;

public abstract class BaseHandlerInterceptorAdapter implements AsyncHandlerInterceptor {
    protected <T extends Annotation> Optional<T> getAnnotation(HandlerMethod handlerMethod, Class<T> annotationType) {
        return this.getAnnotation(handlerMethod, annotationType, false);
    }

    protected <T extends Annotation> Optional<T> getAnnotation(HandlerMethod handlerMethod, Class<T> annotationType, boolean searchClassType) {
        T annotation = AnnotationUtils.findAnnotation(handlerMethod.getMethod(), annotationType);
        if (annotation != null) {
            return Optional.of(annotation);
        }
        if (searchClassType) {
            annotation = AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), annotationType);
        }
        return Optional.ofNullable(annotation);
    }
}
