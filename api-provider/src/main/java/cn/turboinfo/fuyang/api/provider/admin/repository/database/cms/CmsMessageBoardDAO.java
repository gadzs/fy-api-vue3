package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface CmsMessageBoardDAO extends BaseRepository<CmsMessageBoardPO, Long> {
    List<CmsMessageBoardPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);
}
