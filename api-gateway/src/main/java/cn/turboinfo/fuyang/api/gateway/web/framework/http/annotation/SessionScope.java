package cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation;

import java.lang.annotation.*;

/**
 * 需要验证用户登录
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface SessionScope {

    /**
     * 验证失败时是否直接抛出异常, 供某些场合进行特殊处理
     */
    boolean throwException() default true;

}
