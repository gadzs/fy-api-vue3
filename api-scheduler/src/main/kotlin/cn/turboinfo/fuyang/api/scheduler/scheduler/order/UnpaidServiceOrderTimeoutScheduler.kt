package cn.turboinfo.fuyang.api.scheduler.scheduler.order

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.order.SchedulerUnpaidServiceOrderTimeoutUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 未支付订单超时处理
 */
@Component
class UnpaidServiceOrderTimeoutScheduler(
    private val schedulerUnpaidServiceOrderTimeoutUseCase: SchedulerUnpaidServiceOrderTimeoutUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 300000)
    fun run() {
        runCatching {
            schedulerUnpaidServiceOrderTimeoutUseCase.execute(
                SchedulerUnpaidServiceOrderTimeoutUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "回收未支付订单到超时处理失败"
            }
        }
    }

}