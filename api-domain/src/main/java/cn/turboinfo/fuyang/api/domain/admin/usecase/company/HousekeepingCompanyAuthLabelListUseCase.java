package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuthLabelService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAuthLabelListUseCase extends AbstractUseCase<HousekeepingCompanyAuthLabelListUseCase.InputData, HousekeepingCompanyAuthLabelListUseCase.OutputData> {
    private final HousekeepingCompanyAuthLabelService housekeepingCompanyAuthLabelService;

    @Override
    protected OutputData doAction(InputData inputData) {
        return HousekeepingCompanyAuthLabelListUseCase.OutputData.builder()
                .authLabelList(housekeepingCompanyAuthLabelService.findList())
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<HousekeepingCompanyAuthLabel> authLabelList;
    }
}
