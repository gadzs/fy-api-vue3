package cn.turboinfo.fuyang.api.domain.admin.service.menu;

import cn.turboinfo.fuyang.api.entity.admin.pojo.menu.Menu;

import java.util.List;

public interface MenuService {

    List<Menu> findBySysUserId(Long sysUserId);

    List<Menu> findAllWithHierarchy();

    /**
     * 返回特定角色可访问的菜单列表:  未限制的+限制了对应角色的合集
     */
    List<Menu> findRoleAccessibleWithHierarchy(Long roleId);

    /**
     * 返回包含不可见菜单在内的所有列表
     */
    List<Menu> findBySysUserIdAll(Long sysUserId);
}
