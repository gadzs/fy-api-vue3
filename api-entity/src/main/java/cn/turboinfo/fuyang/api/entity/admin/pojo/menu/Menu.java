package cn.turboinfo.fuyang.api.entity.admin.pojo.menu;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;

import java.util.List;

@Data
public class Menu {
    private Long id;

    private String name;

    private String description;

    private String resource;

    private String url;

    private String icon;
    
    private String component;

    private YesNoStatus visibleStatus;

    private Long parentId;

    private List<Menu> subList;
}
