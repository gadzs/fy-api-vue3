package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.ThirdPartyType;
import cn.turboinfo.fuyang.api.entity.common.exception.user.UserThirdPartyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserThirdParty;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserThirdPartyCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserThirdPartyUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserThirdPartyService {
    Optional<UserThirdParty> getById(Long id);

    UserThirdParty getByIdEnsure(Long id);

    Optional<UserThirdParty> getByUserId(Long userId, ThirdPartyType thirdPartyType);

    UserThirdParty getByUserIdEnsure(Long userId, ThirdPartyType thirdPartyType);

    Optional<UserThirdParty> getByThirdPartyAccount(String thirdPartyAccount, ThirdPartyType thirdPartyType);

    List<UserThirdParty> findByIdCollection(Collection<Long> idCollection);

    List<UserThirdParty> findByUserId(Long userId);

    UserThirdParty save(UserThirdPartyCreator creator) throws UserThirdPartyException;

    UserThirdParty update(UserThirdPartyUpdater updater) throws UserThirdPartyException;

    QResponse<UserThirdParty> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws UserThirdPartyException;

    void bindThirdPartyWithLogin(UserThirdPartyCreator userThirdPartyCreator, LoginCheckType loginCheckType);

    UserThirdParty bindThirdPartyWithLogin(Long sysUserId, UserThirdPartyCreator userThirdPartyCreator, LoginCheckType loginCheckType);
}
