package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.service.order.PayOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.wechat.WechatPayService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.DateTimeFormatHelper;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrder;
import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryV3Result;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 支付调用成功接口
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerDepositPaySuccessUseCase extends AbstractUseCase<MiniConsumerDepositPaySuccessUseCase.InputData, MiniConsumerDepositPaySuccessUseCase.OutputData> {

    private final PayOrderService payOrderService;

    private final WechatPayService wechatPayService;

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long userId = inputData.getUserId();
        Long payOrderId = inputData.getPayOrderId();

        PayOrder payOrder = payOrderService.getByIdEnsure(payOrderId);

        if (!Objects.equals(userId, payOrder.getUserId())) {
            throw new IllegalArgumentException("用户不匹配");
        }
        Long serviceCustomId = payOrder.getObjectId();

        if (payOrder.getPayOrderStatus().equals(PayOrderStatus.UNPAID)) {
            ServiceCustom serviceCustom = serviceCustomService.getByIdEnsure(serviceCustomId);

            // 查询微信支付状态
            WxPayOrderQueryV3Result result = wechatPayService.findByPayOrderId(payOrderId);

            if (result != null && "SUCCESS".equals(result.getTradeState())) {
                // 更新支付订单状态
                serviceCustomService.submitCustomDepositPaid(serviceCustomId, payOrderId, serviceCustom.getDeposit(), LocalDateTime.parse(result.getSuccessTime(), DateTimeFormatHelper.wechatDataTime));
            }
        }

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        @Positive
        private Long userId;

        /**
         * 支付订单号
         */
        @NotNull(message = "支付订单号不能为空")
        @Positive
        private Long payOrderId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
