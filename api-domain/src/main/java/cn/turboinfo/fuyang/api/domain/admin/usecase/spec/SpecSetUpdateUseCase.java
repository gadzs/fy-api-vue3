package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSet;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSetUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecSetUpdateUseCase extends AbstractUseCase<SpecSetUpdateUseCase.InputData, SpecSetUpdateUseCase.OutputData> {
    private final SpecSetService specSetService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();

        SpecSetUpdater.Builder builder = SpecSetUpdater.builder(id);

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, SpecSetUpdater.class, inputData);

        SpecSet specSet = specSetService.update(builder.build());

        return OutputData.builder()
                .specSet(specSet)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        private String description;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private SpecSet specSet;
    }
}
