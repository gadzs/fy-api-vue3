package cn.turboinfo.fuyang.api.domain.scheduler.usecase.order

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService
import cn.turboinfo.fuyang.api.domain.common.service.order.PayOrderService
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService
import cn.turboinfo.fuyang.api.domain.common.service.wechat.WechatPayService
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase
import cn.turboinfo.fuyang.api.domain.util.CurrencyUnitUtils
import cn.turboinfo.fuyang.api.domain.util.DateTimeFormatHelper
import cn.turboinfo.fuyang.api.entity.common.constant.WechatAttachConstants
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QPayOrder
import net.sunshow.toolkit.core.qbean.api.request.QPage
import net.sunshow.toolkit.core.qbean.api.request.QRequest
import net.sunshow.toolkit.core.qbean.helper.component.request.QPageRequestHelper
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class SchedulerUnpaidPayOrderByWechatUseCase(
    private val payOrderService: PayOrderService,
    private val serviceOrderService: ServiceOrderService,
    private val serviceCustomService: ServiceCustomService,
    private val wechatPayService: WechatPayService,
) : AbstractUseCase<SchedulerUnpaidPayOrderByWechatUseCase.InputData, SchedulerUnpaidPayOrderByWechatUseCase.OutputData>() {

    override fun doAction(input: InputData): OutputData {
        val request = QRequest.newInstance()
            .filterEqual(QPayOrder.payOrderStatus, PayOrderStatus.UNPAID)
            .filterEqual(QPayOrder.payType, PayType.WECHAT)
        val requestPage = QPage.newInstance()
            .paging(0, 100)

        QPageRequestHelper.request(request, requestPage, payOrderService::findAll)
            .onEach {
                wechatPayService.findByPayOrderId(it.id)
                    ?.let { wechatPay ->
                        // 支付成功
                        if (wechatPay.tradeState.equals("SUCCESS", true)) {

                            // 预付款
                            if (WechatAttachConstants.WECHAT_ATTACH_PREPAY.equals(wechatPay.attach, true)) {

                                if (it.objectType.equals(EntityObjectType.SERVICE_ORDER)) {
                                    serviceOrderService.submitOrderPaid(
                                        it.objectId,
                                        it.id,
                                        CurrencyUnitUtils.getAmountByCents(wechatPay.amount.payerTotal.toLong()),
                                        LocalDateTime.parse(
                                            wechatPay.successTime,
                                            DateTimeFormatHelper.wechatDataTime
                                        )
                                    )
                                } else if (it.objectType.equals(EntityObjectType.SERVICE_CUSTOM)) {
                                    serviceCustomService.getByIdEnsure(it.id)
                                        .let { serviceCustom ->
                                            serviceCustomService.submitCustomDepositPaid(
                                                it.id,
                                                it.objectId,
                                                serviceCustom.deposit,
                                                LocalDateTime.parse(
                                                    wechatPay.successTime,
                                                    DateTimeFormatHelper.wechatDataTime
                                                )
                                            )
                                        }
                                }

                            } else if (WechatAttachConstants.WECHAT_ATTACH_REMAINING_UNPAID.equals(
                                    wechatPay.attach,
                                    true
                                )
                            ) {
                                // 剩余未支付
                                if (it.objectType.equals(EntityObjectType.SERVICE_ORDER)) {
                                    serviceOrderService.payRemainingUnpaid(it.objectId, it.id)
                                } else if (it.objectType.equals(EntityObjectType.SERVICE_CUSTOM)) {
                                    serviceCustomService.payRemainingUnpaid(it.objectId, it.id)
                                }
                            }
                        }
                    }
                    ?: logger.error { "未找到微信支付订单: ${it.id}" }
            }

        return OutputData()
    }

    class InputData : AbstractUseCase.InputData()

    class OutputData : AbstractUseCase.OutputData()

}
