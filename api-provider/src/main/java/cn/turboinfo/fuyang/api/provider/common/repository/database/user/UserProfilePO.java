package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户资料
 */
@Table(
        name = "user_profile"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(clause = "deleted = 0")
@DynamicInsert
@DynamicUpdate
public class UserProfilePO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 机构编码
     */
    private Long agenciesId;

}
