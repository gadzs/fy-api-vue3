package cn.turboinfo.fuyang.api.provider.common.repository.database.company;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 家政公司认证标签
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "housekeeping_company_auth_label"
)
@Entity
@DynamicUpdate
@Where(clause = "deleted = 0")
@DynamicInsert
public class HousekeepingCompanyAuthLabelPO extends SoftDeleteFYEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 图标
     */
    private Long iconId;

    /**
     * 权重
     */
    private Integer weight;

    private String description;
}
