package cn.turboinfo.fuyang.api.entity.common.pojo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付订单
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class PayOrder extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 支付人ID
     */
    private Long userId;

    /**
     * 支付对象类型
     */
    private EntityObjectType objectType;

    /**
     * 支付对象ID
     */
    private Long objectId;

    /**
     * 支付类型
     */
    private PayType payType;

    /**
     * 支付订单状态
     */
    private PayOrderStatus payOrderStatus;

    /**
     * 支付成功时间
     */
    private LocalDateTime paidTime;

    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 三方交易编码
     */
    private String transactionId;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
