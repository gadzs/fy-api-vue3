package cn.turboinfo.fuyang.api.provider.common.service.impl.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractService;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.Contract;
import cn.turboinfo.fuyang.api.provider.common.repository.database.contract.ContractDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.contract.ContractPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ContractServiceImpl extends DefaultQServiceImpl<Contract, Long, ContractPO, ContractDAO> implements ContractService {
    @Override
    public Optional<Contract> findByOrderId(Long orderId) {
        return dao.findByOrderId(orderId).map(this::convertQBean);
    }
}
