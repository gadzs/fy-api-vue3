CREATE TABLE `dict_config`
(
    `id`           BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `dict_name`    VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '字典名称',
    `dict_key`     VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '字典 Key',
    `description`  VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '字典描述',
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_dictkey` (`dict_key`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='数据字典';

CREATE TABLE `dict_config_item`
(
    `id`              BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `dict_config_id`  BIGINT        NOT NULL DEFAULT 0 COMMENT '字典ID',
    `dict_config_key` VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '字典Key',
    `item_value`      VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '条目值',
    `item_name`       VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '条目名称',
    `description`     VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '条目描述',
    `item_order`      INT           NOT NULL DEFAULT 0 COMMENT '条目顺序',
    `created_time`    DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`    DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_dictconfigkey_itemvalue` (`dict_config_key`, `item_value`),
    UNIQUE KEY `uk_dictconfigid_itemvalue` (`dict_config_id`, `item_value`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='数据字典条目';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('字典管理', 'dictConfig:null', '', 1, 0);
SET @dictConfigPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('字典列表', 'dictConfig:list', '/admin/dictConfig/list', 1, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('新建字典', 'dictConfig:create', '/admin/dictConfig/create', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('编辑字典', 'dictConfig:update', '/admin/dictConfig/update', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('删除字典', 'dictConfig:delete', '/admin/dictConfig/delete', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('字典条目列表', 'dictConfigItem:list', '/admin/dictConfigItem/list', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('新建字典条目', 'dictConfigItem:create', '/admin/dictConfigItem/create', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('编辑字典条目', 'dictConfigItem:update', '/admin/dictConfigItem/update', 0, @dictConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('删除字典条目', 'dictConfigItem:delete', '/admin/dictConfigItem/delete', 0, @dictConfigPermissionParentId);