CREATE TABLE `service_order`
(
    `id`                    BIGINT(1)      NOT NULL AUTO_INCREMENT,
    `user_id`               BIGINT(1)      NOT NULL COMMENT '用户编码',
    `category_id`           BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '服务类别编码',
    `product_sku_id`        BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '商品sku编码',
    `product_id`            BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '商品编码',
    `company_id`            BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '企业编码',
    `shop_id`               BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '门店编码',
    `staff_id`              BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '家政员编码',
    `spec_list`             JSON           NULL COMMENT '规格列表',
    `comment`               VARCHAR(2048)  NOT NULL DEFAULT '' COMMENT '备注',
    `scheduled_start_time`  DATETIME       NULL COMMENT '计划开始时间',
    `scheduled_end_time`    DATETIME       NULL COMMENT '计划结束时间',
    `service_start_time`    DATETIME       NULL COMMENT '服务开始时间',
    `service_end_time`      DATETIME       NULL COMMENT '服务结束时间',
    `order_status`          INT(1)         NOT NULL DEFAULT 0 COMMENT '订单状态',
    `order_status_history`  JSON           NULL COMMENT '历史订单状态',
    `price`                 DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '价格',
    `prepaid`               DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '已支付金额',
    `additional_fee`        DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '附加费用',
    `discount_fee`          DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '减免费用',
    `pay_status`            INT(1)         NOT NULL DEFAULT 0 COMMENT '支付状态',
    `refund_status`         TINYINT        NOT NULL DEFAULT 0 COMMENT '退款状态',
    `credit_rating_status`  TINYINT        NOT NULL DEFAULT 0 COMMENT '评价状态',
    `profit_sharing_status` TINYINT        NOT NULL DEFAULT 0 COMMENT '分账状态',
    `completed_time`        DATETIME       NULL COMMENT '完成时间',
    `address_id`            BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '服务地址编码',
    `contact`               VARCHAR(64)    NOT NULL DEFAULT '' COMMENT '用户姓名',
    `gender_type`           TINYINT(1)     NOT NULL DEFAULT 0 COMMENT '用户性别',
    `mobile`                VARCHAR(32)    NOT NULL DEFAULT '' COMMENT '用户手机号',
    `division_id`           BIGINT(1)      NOT NULL DEFAULT 0 COMMENT '地区编码',
    `poi_name`              VARCHAR(64)    NOT NULL DEFAULT '' COMMENT '服务地址',
    `detail`                VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '服务详细地址',
    `longitude`             decimal(9, 6)  NOT NULL DEFAULT 0 COMMENT '经度',
    `latitude`              decimal(9, 6)  NOT NULL DEFAULT 0 COMMENT '纬度',
    `deleted`               BIGINT         NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）',
    `created_time`          TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time`          TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY `idx_staffid_creditedtime` (`staff_id`, `created_time`),
    KEY `idx_companyid_creditedtime` (`company_id`, `created_time`),
    KEY `idx_shopid_creditedtime` (`shop_id`, `created_time`),
    KEY `idx_productid_creditedtime` (`product_id`, `created_time`),
    KEY `idx_paystatus_creditedtime` (`pay_status`, `created_time`),
    KEY `idx_createdtime` (`created_time`),
    KEY `idx_updatetime` (`updated_time`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`
(
    `id`               BIGINT(1)      NOT NULL AUTO_INCREMENT,
    `user_id`          BIGINT(1)      NOT NULL,
    `object_type`      INT(1)         NOT NULL DEFAULT 0,
    `object_id`        BIGINT(1)      NOT NULL DEFAULT 0,
    `pay_type`         INT(1)         NOT NULL DEFAULT 0,
    `pay_order_status` INT(1)         NOT NULL DEFAULT 0,
    `paid_time`        DATETIME       NULL,
    `amount`           DECIMAL(10, 2) NOT NULL DEFAULT 0,
    `transaction_id`   VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '三方支付订单号',
    `deleted`          BIGINT(1)      NOT NULL DEFAULT 0,
    `created_time`     DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`     DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

DROP TABLE IF EXISTS `refund_order`;
CREATE TABLE `refund_order`
(
    `id`                    BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`            BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `wx_transaction_id`     VARCHAR(128) NOT NULL DEFAULT '' COMMENT '微信支付订单号',
    `wx_refund_id`          VARCHAR(128) NOT NULL DEFAULT '' COMMENT '微信退款单号',
    `object_type`           TINYINT      NOT NULL DEFAULT 0 COMMENT '退款对象类型',
    `object_id`             BIGINT(1)    NOT NULL DEFAULT 0 COMMENT '退款对象编码',
    `pay_order_id`          BIGINT       NOT NULL DEFAULT 0 COMMENT '支付订单编码',
    `refund_type`           TINYINT      NOT NULL DEFAULT 0 COMMENT '退款类型',
    `reason`                VARCHAR(128) NOT NULL DEFAULT '' COMMENT '退款原因',
    `refund_amount`         BIGINT       NOT NULL DEFAULT 0 COMMENT '退款金额（单位分）',
    `order_amount`          BIGINT       NOT NULL DEFAULT 0 COMMENT '订单金额（单位分）',
    `user_pay_amount`       BIGINT       NOT NULL DEFAULT 0 COMMENT '用户支付金额',
    `user_refund_amount`    BIGINT       NOT NULL DEFAULT 0 COMMENT '用户退款金额',
    `refund_channel`        TINYINT      NOT NULL DEFAULT 0 COMMENT '退款渠道',
    `user_received_account` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '退款入账账户',
    `success_time`          DATETIME     NULL COMMENT '退款成功时间',
    `status`                TINYINT      NOT NULL DEFAULT 0 COMMENT '退款状态',
    `funds_account`         TINYINT      NOT NULL DEFAULT 0 COMMENT '退款资金账户',
    `deleted`               BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`          DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`          DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_ordertype_objectid` (`object_id`, `object_type`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='退款管理';

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('退款管理', 'adminRefund:null', '', '/refund', '', 1, 0, 'el-icon-refresh-left', 49);
SET
    @refundPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('退款列表', 'adminRefund:list', '', '/refund/list', '', 1, @refundPermissionParentId, 'el-icon-refresh-left',
        0),
       ('退款查看', 'adminRefund:view', '', '/refund/view', '', 0, @refundPermissionParentId, '', 0);
