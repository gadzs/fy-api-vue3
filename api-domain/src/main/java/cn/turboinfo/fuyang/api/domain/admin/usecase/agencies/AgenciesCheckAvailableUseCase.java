package cn.turboinfo.fuyang.api.domain.admin.usecase.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Slf4j
@RequiredArgsConstructor
@Component
public class AgenciesCheckAvailableUseCase extends AbstractUseCase<AgenciesCheckAvailableUseCase.InputData, AgenciesCheckAvailableUseCase.OutputData> {
    private final AgenciesService agenciesService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long agenciesId = inputData.getAgenciesId();

        String name = inputData.getName();

        boolean available = agenciesService.checkAvailable(agenciesId, name);

        return OutputData.builder()
                .available(available)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        private Long agenciesId;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private boolean available;
    }
}
