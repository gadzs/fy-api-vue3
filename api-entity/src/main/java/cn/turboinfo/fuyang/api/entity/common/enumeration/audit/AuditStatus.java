package cn.turboinfo.fuyang.api.entity.common.enumeration.audit;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 审核状态
 *
 * @author 海
 */
@Slf4j
public enum AuditStatus implements BaseEnum {

    INIT(0, "初始化"),
    
    REVIEWED(10, "已审核"),

    NOT_PASS(20, "审核未通过"),

    AGAIN(30, "再次提交");

    private int value;
    private String name;

    AuditStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static AuditStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<AuditStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<AuditStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
