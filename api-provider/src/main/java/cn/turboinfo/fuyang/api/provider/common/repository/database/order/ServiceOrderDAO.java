package cn.turboinfo.fuyang.api.provider.common.repository.database.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface ServiceOrderDAO extends FYRepository<ServiceOrderPO, Long> {

    List<ServiceOrderPO> findByStaffIdAndOrderStatusOrderByCreatedTimeDesc(Long staffId, ServiceOrderStatus status);

    List<ServiceOrderPO> findByCompanyIdAndOrderStatusOrderByCreatedTimeDesc(Long companyId, ServiceOrderStatus status);

    List<ServiceOrderPO> findByCompanyIdAndOrderStatusInOrderByCreatedTimeDesc(Long companyId, Collection<ServiceOrderStatus> statusCollection);

    List<ServiceOrderPO> findByShopIdAndOrderStatusOrderByCreatedTimeDesc(Long shopId, ServiceOrderStatus status);

    List<ServiceOrderPO> findByProductIdAndOrderStatusOrderByCreatedTimeDesc(Long productId, ServiceOrderStatus status);

    //JPA sum运算
    @Query("select sum(o.price) from ServiceOrderPO o where o.orderStatus = ?1")
    BigDecimal sumPriceByOrderStatus(ServiceOrderStatus status);

    Long countByOrderStatusAndCreatedTimeBetween(ServiceOrderStatus status, LocalDateTime start, LocalDateTime end);

    List<ServiceOrderPO> findByUserIdAndStaffIdAndOrderStatus(Long userId, Long staffId, ServiceOrderStatus status);
}
