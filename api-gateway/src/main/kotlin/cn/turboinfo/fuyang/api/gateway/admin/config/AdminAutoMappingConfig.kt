package cn.turboinfo.fuyang.api.gateway.admin.config

import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContextHolder
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import mu.KotlinLogging
import nxcloud.ext.springmvc.automapping.spi.AutoMappingRequestParameterInjector
import nxcloud.ext.springmvc.automapping.spring.AutoMappingRequestParameterTypeBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.MethodParameter
import org.springframework.web.context.request.NativeWebRequest

@Configuration
class AdminAutoMappingConfig {

    private val logger = KotlinLogging.logger {}

    /**
     * SessionScope 自动注入
     */
    @Bean
    protected fun adminSessionScopeAutoMappingRequestParameterInjector(
        autoMappingRequestParameterTypeBinding: AutoMappingRequestParameterTypeBinding,
    ): AutoMappingRequestParameterInjector {
        return object : AutoMappingRequestParameterInjector {
            override fun inject(
                parameterObj: Any,
                parameter: MethodParameter,
                resolvedParameterType: Class<*>,
                webRequest: NativeWebRequest
            ) {
                try {
                    val field = parameterObj::class.java.getDeclaredField("userId")
                    field.isAccessible = true
                    field.set(parameterObj, AdminRequestContextHolder.current().userId)
                } catch (e: NoSuchFieldException) {
                    // 忽略传递未知属性的情况
                    logger.debug { "$parameter 声明了验证 SessionScope, 但未要求 userId" }
                }
            }

            override fun isSupported(
                parameterObj: Any,
                parameter: MethodParameter,
                resolvedParameterType: Class<*>,
                webRequest: NativeWebRequest
            ): Boolean {
                return AdminRequestContextHolder.exists() && (
                        parameter.method
                            ?.let {
                                autoMappingRequestParameterTypeBinding.getAnnotation(
                                    it,
                                    SessionScope::class.java,
                                    true
                                ) != null
                                        ||
                                        autoMappingRequestParameterTypeBinding.getAnnotation(
                                            it,
                                            PermissionScope::class.java,
                                            true
                                        ) != null
                            }
                            ?: false
                        )
            }
        }
    }

    /**
     * 注入 admin companyId
     */
    @Bean
    protected fun adminCompanyScopeAutoMappingRequestParameterInjector(
        autoMappingRequestParameterTypeBinding: AutoMappingRequestParameterTypeBinding,
    ): AutoMappingRequestParameterInjector {
        return object : AutoMappingRequestParameterInjector {
            override fun inject(
                parameterObj: Any,
                parameter: MethodParameter,
                resolvedParameterType: Class<*>,
                webRequest: NativeWebRequest
            ) {
                try {
                    val field = parameterObj::class.java.getDeclaredField("companyId")
                    field.isAccessible = true
                    val companyId = AdminRequestContextHolder.current().companyId
                    if (companyId != null) {
                        field.set(parameterObj, companyId)
                    }
                } catch (e: NoSuchFieldException) {
                    // 忽略传递未知属性的情况
                    logger.info { "$parameter 声明了验证 CompanyScope, 但未要求 companyId" }
                }
            }

            override fun isSupported(
                parameterObj: Any,
                parameter: MethodParameter,
                resolvedParameterType: Class<*>,
                webRequest: NativeWebRequest
            ): Boolean {
                return AdminRequestContextHolder.exists() && (
                        parameter.method
                            ?.let {
                                autoMappingRequestParameterTypeBinding.getAnnotation(
                                    it,
                                    CompanyScope::class.java,
                                    true
                                ) != null
                            }
                            ?: false
                        )
            }
        }
    }

}