DROP TABLE IF EXISTS `knowledge_base`;
CREATE TABLE `knowledge_base`
(
    `id`           BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `type`         VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '知识类型',
    `question`     VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '问题',
    `answer`       VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '答案',
    `deleted`      BIGINT(1)     NOT NULL DEFAULT 0,
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='知识库';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`, `sort_value`)
VALUES ('知识管理', 'adminKnowledge:null', '/knowledge', 1, 0, 'el-icon-s-help', 88);
SET @knowledgePermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('知识列表', 'adminKnowledge:list', '/knowledge/list', 1, @knowledgePermissionParentId, 'el-icon-s-help'),
       ('新增知识', 'adminKnowledge:create', '', 0, @knowledgePermissionParentId, ''),
       ('更新知识', 'adminKnowledge:update', '', 0, @knowledgePermissionParentId, ''),
       ('删除知识', 'adminKnowledge:delete', '', 0, @knowledgePermissionParentId, '');

