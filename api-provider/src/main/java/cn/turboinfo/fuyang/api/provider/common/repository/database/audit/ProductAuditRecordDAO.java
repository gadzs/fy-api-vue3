package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

/**
 * @author hai
 */
public interface ProductAuditRecordDAO extends FYRepository<ProductAuditRecordPO, Long> {

    List<ProductAuditRecordPO> findByProductIdOrderByIdDesc(Long productId);
}
