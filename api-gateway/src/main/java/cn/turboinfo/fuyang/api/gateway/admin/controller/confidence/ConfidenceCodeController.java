package cn.turboinfo.fuyang.api.gateway.admin.controller.confidence;

import cn.turboinfo.fuyang.api.domain.admin.usecase.confidence.ConfidenceCodeQRViewUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.confidence.ConfidenceCodeSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.QConfidenceCode;
import cn.turboinfo.fuyang.api.gateway.admin.fo.confidence.ConfidenceCodeSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/confidence")
public class ConfidenceCodeController {
    private final ConfidenceCodeSearchUseCase confidenceCodeSearchUseCase;

    private final ConfidenceCodeQRViewUseCase confidenceCodeQRViewUseCase;

    @RequiresPermissions("adminConfidence:list")
    @RequestMapping("/search")
    public LimitDataFO<ConfidenceCode> list(@Valid ConfidenceCodeSearchFO search, LimitFO page,
                                            SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QConfidenceCode.id + "|" + QSort.Order.DESC.name()});
        }

        ConfidenceCodeSearchUseCase.OutputData outputData = confidenceCodeSearchUseCase.execute(ConfidenceCodeSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    /**
     * 下载二维码
     */
    @RequestMapping("/viewQRCode")
    public void viewQRCode(@PathParam("id") Long id, @PathParam("title") String title, @PathParam("imageType") String imageType, HttpServletResponse response) {

        // 配置文件下载
        response.setContentType("application/octet-stream");

        // 下载文件能正常显示中文
        response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(title + "-" + id + "." + imageType, StandardCharsets.UTF_8));

        confidenceCodeQRViewUseCase.execute(ConfidenceCodeQRViewUseCase.InputData
                .builder()
                .id(id)
                .imageType(imageType)
                .response(response)
                .build());
    }

}
