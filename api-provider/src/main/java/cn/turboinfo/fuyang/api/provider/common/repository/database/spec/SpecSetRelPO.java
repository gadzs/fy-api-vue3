package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 组合条目
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "spec_set_rel"
)
@Entity
public class SpecSetRelPO extends FYEntity {

    /**
     * 组合 ID
     */
    private Long specSetId;

    /**
     * 关联对象类型
     */
    private EntityObjectType objectType;

    /**
     * 关联对象ID
     */
    private Long objectId;


}
