package cn.turboinfo.fuyang.api.entity.share.fo.company;

import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShareCompanyFO {

    /**
     * 公司名称
     */
    private String name;

    /**
     * 统一社会信用代码
     */
    private String uscc;

    /**
     * 成立日期
     */
    private LocalDate establishmentDate;

    /**
     * 省id
     */
    private Long provinceCode;

    private String provinceName;

    /**
     * 市id
     */
    private Long cityCode;

    private String cityName;

    /**
     * 区id
     */
    private Long areaCode;

    private String areaName;

    /**
     * 注册地址
     */
    private String registeredAddress;

    /**
     * 注册资本
     */
    private BigDecimal registeredCapital;

    /**
     * 公司类型
     */
    private String companyType;

    /**
     * 法人
     */
    private String legalPerson;

    /**
     * 法人身份证
     */
    private String legalPersonIdCard;

    /**
     * 工商注册号
     */
    private String businessRegistrationNumber;

    /**
     * 经营范围
     */
    private String businessScope;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 员工人数
     */
    private String employeesNumber;

    /**
     * 企业状态
     */
    private CompanyStatus status;

    /**
     * 审核时间
     */
    private LocalDateTime reviewTime;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 家政员数
     */
    private Long staffNum;

    /**
     * 产品服务数
     */
    private Long productNum;

    /**
     * 门店数
     */
    private Long shopNum;

    /**
     * 认证标签
     */
    private List<String> authLabelList;

    /**
     * 认证标签权重
     */
    private Long authLabelWeight;

    /**
     * 家政公司类型
     */
    private String houseKeepingType;

}
