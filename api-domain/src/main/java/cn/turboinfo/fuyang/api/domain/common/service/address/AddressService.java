package cn.turboinfo.fuyang.api.domain.common.service.address;

import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.AddressCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

/**
 * 地址服务
 */
public interface AddressService extends BaseQService<Address, Long> {

    Address save(AddressCreator creator);

}
