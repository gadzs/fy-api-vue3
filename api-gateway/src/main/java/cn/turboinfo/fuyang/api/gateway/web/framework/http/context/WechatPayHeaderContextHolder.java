package cn.turboinfo.fuyang.api.gateway.web.framework.http.context;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public final class WechatPayHeaderContextHolder {

    private final static ThreadLocal<WechatPayHeaderContext> contextThreadLocal = new ThreadLocal<>();

    public static WechatPayHeaderContext current() {
        WechatPayHeaderContext context = contextThreadLocal.get();
        if (context == null) {
            context = new WechatPayHeaderContext();
            contextThreadLocal.set(context);
        }
        return context;
    }

    public static boolean exists() {
        return contextThreadLocal.get() != null;
    }

    public static void reset() {
        contextThreadLocal.remove();
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
    }

}
