package cn.turboinfo.fuyang.api.provider.common.component.credit.listener

import cn.turboinfo.fuyang.api.entity.common.event.WrapperApplicationEvent
import cn.turboinfo.fuyang.api.entity.common.event.credit.NewCreditRatingEvent
import cn.turboinfo.fuyang.api.provider.common.repository.database.credit.CreditRatingPO
import nxcloud.foundation.core.data.support.listener.EntityLifecycleListener
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronization
import org.springframework.transaction.support.TransactionSynchronizationManager

@Component
class CreditRatingPublishEventListener(
    private val applicationContext: ApplicationContext,
) : EntityLifecycleListener {

    override fun onPostPersist(entity: Any) {
        (entity as CreditRatingPO)
            .let {
                TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronization {
                    override fun afterCommit() {
                        applicationContext.publishEvent(
                            WrapperApplicationEvent(
                                NewCreditRatingEvent.builder()
                                    .creditRatingId(entity.id)
                                    .build()
                            )
                        )
                    }
                })
            }
    }

}