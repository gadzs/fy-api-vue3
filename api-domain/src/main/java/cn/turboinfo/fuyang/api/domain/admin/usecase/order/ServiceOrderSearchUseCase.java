package cn.turboinfo.fuyang.api.domain.admin.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.fo.order.ServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ServiceOrderSearchUseCase extends AbstractUseCase<ServiceOrderSearchUseCase.InputData, ServiceOrderSearchUseCase.OutputData> {
    private final ServiceOrderService serviceOrderService;
    private final SysUserService sysUserService;
    private final HousekeepingShopService housekeepingShopService;
    private final CategoryService categoryService;
    private final HousekeepingStaffService housekeepingStaffService;
    private final ProductService productService;

    @Override
    protected OutputData doAction(InputData inputData) {
        QResponse<ServiceOrder> response = serviceOrderService.findAll(inputData.request, inputData.getRequestPage());


        val pagedData = response.getPagedData();
        Set<Long> shopIdList = new HashSet<>();
        Set<Long> productIdList = new HashSet<>();
        Set<Long> categoryIdList = new HashSet<>();
        Set<Long> staffIdList = new HashSet<>();
        Set<Long> userIdList = new HashSet<>();
        pagedData.forEach(serviceOrder -> {
            shopIdList.add(serviceOrder.getShopId());
            productIdList.add(serviceOrder.getProductId());
            categoryIdList.add(serviceOrder.getCategoryId());
            staffIdList.add(serviceOrder.getStaffId());
            userIdList.add(serviceOrder.getUserId());
        });

        val shopMap = housekeepingShopService.findByIdCollection(shopIdList).stream().collect(Collectors.toMap(HousekeepingShop::getId, Function.identity()));
        val productMap = productService.findByIdCollection(productIdList).stream().collect(Collectors.toMap(Product::getId, Function.identity()));
        val categoryMap = categoryService.findByIdCollection(categoryIdList).stream().collect(Collectors.toMap(Category::getId, Function.identity()));
        val staffMap = housekeepingStaffService.findByIdCollection(staffIdList).stream().collect(Collectors.toMap(HousekeepingStaff::getId, Function.identity()));
        val userMap = sysUserService.findByIdCollection(userIdList).stream().collect(Collectors.toMap(SysUser::getId, Function.identity()));
        val orderFOList = pagedData.stream().map(serviceOrder -> {
            val orderFOBuilder = ServiceOrderFO.builder();

            orderFOBuilder.serviceOrder(serviceOrder)
                    .shopName(shopMap.get(serviceOrder.getShopId()).getName())
                    .categoryName(categoryMap.get(serviceOrder.getCategoryId()).getName())
                    .productName(productMap.get(serviceOrder.getProductId()).getName())
                    .staffName(staffMap.containsKey(serviceOrder.getStaffId()) ? staffMap.get(serviceOrder.getStaffId()).getName() : "")
                    .userName(userMap.get(serviceOrder.getUserId()).getMobile());
            return orderFOBuilder.build();
        }).collect(Collectors.toList());

        QResponse<ServiceOrderFO> foResponse = new QResponse<>(response.getPage(), response.getPageSize(), orderFOList, response.getTotal());

        return OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ServiceOrderFO> qResponse;
    }
}
