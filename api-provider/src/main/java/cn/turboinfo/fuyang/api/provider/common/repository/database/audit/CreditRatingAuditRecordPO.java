package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 评价审核记录
 */
@Table(
        name = "credit_rating_audit_record"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(
        clause = "deleted = 0"
)
public class CreditRatingAuditRecordPO extends SoftDeleteFYEntity {

    private Long creditRatingId;

    /**
     * 评价对象类型
     */
    @Column(
            name = "object_type"
    )
    private EntityObjectType objectType;

    /**
     * 评价对象ID
     */
    @Column(
            name = "object_id"
    )
    private Long objectId;

    /**
     * 审核状态
     */
    @Column(
            name = "audit_status"
    )
    private AuditStatus auditStatus;

    /**
     * 审核人
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    /**
     * 审核人名称
     */
    @Column(
            name = "user_name"
    )
    private String userName;

    /**
     * 备注
     */
    private String remark;
}
