package cn.turboinfo.fuyang.api.provider.common.repository.database.division;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface DivisionDAO extends FYRepository<DivisionPO, Long> {

    List<DivisionPO> findByParentIdOrderByAreaCode(Long parentId);

    List<DivisionPO> findByOrderByAreaCode();
}
