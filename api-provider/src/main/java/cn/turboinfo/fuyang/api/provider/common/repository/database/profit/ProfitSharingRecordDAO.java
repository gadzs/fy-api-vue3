package cn.turboinfo.fuyang.api.provider.common.repository.database.profit;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface ProfitSharingRecordDAO extends FYRepository<ProfitSharingRecordPO, Long> {

    List<ProfitSharingRecordPO> findByProfitSharingIdOrderByIdDesc(Long profitSharingId);

}
