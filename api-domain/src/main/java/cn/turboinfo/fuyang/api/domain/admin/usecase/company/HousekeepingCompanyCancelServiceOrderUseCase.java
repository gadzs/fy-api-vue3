package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 公司取消订单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyCancelServiceOrderUseCase extends AbstractUseCase<HousekeepingCompanyCancelServiceOrderUseCase.InputData, HousekeepingCompanyCancelServiceOrderUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        Long serviceOrderId = inputData.getServiceOrderId();

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(serviceOrderId);

        if (!Objects.equals(companyId, serviceOrder.getCompanyId())) {
            throw new IllegalArgumentException("订单所属公司不匹配");
        }

        // 取消
        serviceOrderService.companyCancel(serviceOrderId);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 公司ID
         */
        @NotNull(message = "公司ID不能为空")
        @Positive
        private Long companyId;

        /**
         * 服务订单ID
         */
        @NotNull(message = "服务订单不能为空")
        @Positive
        private Long serviceOrderId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
