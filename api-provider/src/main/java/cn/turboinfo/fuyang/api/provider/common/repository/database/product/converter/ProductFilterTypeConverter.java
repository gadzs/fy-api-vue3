package cn.turboinfo.fuyang.api.provider.common.repository.database.product.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductFilterType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: gadzs.
 */
@Converter(autoApply = true)
public class ProductFilterTypeConverter extends BaseEnumConverter<ProductFilterType> {
}
