package cn.turboinfo.fuyang.api.provider.common.repository.database.agencies;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface AgenciesDAO extends FYRepository<AgenciesPO, Long> {

    List<AgenciesPO> findByNameOrderByIdDesc(String name);

}
