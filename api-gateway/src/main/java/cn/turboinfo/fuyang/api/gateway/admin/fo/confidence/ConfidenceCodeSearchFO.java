package cn.turboinfo.fuyang.api.gateway.admin.fo.confidence;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;

import java.time.LocalDateTime;

@Data
@QField
public class ConfidenceCodeSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "使用状态",
            control = Control.SELECT,
            ref = "yesNoStatusList",
            template = "enum"
    )
    private net.sunshow.toolkit.core.base.enums.YesNoStatus used;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
