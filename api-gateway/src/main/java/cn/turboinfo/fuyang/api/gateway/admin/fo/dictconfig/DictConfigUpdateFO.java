package cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DictConfigUpdateFO {
    @NotNull(
            message = "id不能为空"
    )
    private Long id;

    @NotBlank(
            message = "字典名称不能为空"
    )
    private String dictName;

    @NotBlank(
            message = "字典 Key不能为空"
    )
    private String dictKey;

    private String description;
}
