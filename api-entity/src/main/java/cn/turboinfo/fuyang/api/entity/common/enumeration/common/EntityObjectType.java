package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 内部实体对象类型
 */
@Slf4j
public enum EntityObjectType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    PRODUCT(10, "产品"),

    PRODUCT_SKU(15, "产品SKU"),

    CATEGORY(20, "分类"),

    COMPANY(25, "公司"),

    STAFF(30, "家政员"),

    SHOP(40, "店铺"),

    SPEC(50, "规格"),

    SERVICE_ORDER(100, "服务订单"),

    SERVICE_CUSTOM(110, "定制服务订单"),

    PAY_ORDER(150, "支付订单"),

    ;

    private int value;
    private String name;

    EntityObjectType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static EntityObjectType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<EntityObjectType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<EntityObjectType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
