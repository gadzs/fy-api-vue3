package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 产品SKU和规格对应关系
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "product_sku_spec_rel"
)
@Entity
public class ProductSkuSpecRelPO extends FYEntity {

    /**
     * 产品SKU ID
     */
    private Long productSkuId;

    /**
     * 规格 ID
     */
    private Long specId;

}
