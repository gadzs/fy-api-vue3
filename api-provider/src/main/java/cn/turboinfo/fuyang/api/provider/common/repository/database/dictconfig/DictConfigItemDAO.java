package cn.turboinfo.fuyang.api.provider.common.repository.database.dictconfig;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DictConfigItemDAO extends BaseRepository<DictConfigItemPO, Long> {
    List<DictConfigItemPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    Optional<DictConfigItemPO> findByDictConfigKeyAndItemValue(String dictConfigKey, String itemValue);

    List<DictConfigItemPO> findByDictConfigKeyOrderByItemOrderAsc(String dictConfigKey);

    List<DictConfigItemPO> findByDictConfigIdOrderByItemOrderAsc(Long dictConfigId);
}
