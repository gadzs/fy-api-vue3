package cn.turboinfo.fuyang.api.gateway.admin.fo.knowledge;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * @author gadzs
 * @description
 * @date 2023/3/8 15:59
 */
@Data
@QField
public class PortalKnowledgeListFO {

    @QField(
            placeholder = "知识类型",
            operator = Operator.EQUAL
    )
    private String type;

}
