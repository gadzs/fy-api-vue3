package cn.turboinfo.fuyang.api.provider.common.repository.database.stat;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface OrderStatDAO extends FYRepository<OrderStatPO, Long> {

    Optional<OrderStatPO> findByCompanyIdAndCategoryIdAndYearAndMonth(Long companyId, Long categoryId, Integer year, Integer month);

    List<OrderStatPO> findByCompanyIdIn(Collection<Long> companyIdCollection);

    List<OrderStatPO> findByCategoryIdIn(Collection<Long> categoryIdCollection);
}
