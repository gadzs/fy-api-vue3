package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Objects;

/**
 * 消费者下单时的可用支付类型
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerAvailablePayTypeUseCase extends AbstractUseCase<MiniConsumerAvailablePayTypeUseCase.InputData, MiniConsumerAvailablePayTypeUseCase.OutputData> {

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        Long serviceCustomId = inputData.getServiceCustomId();

        ServiceCustom serviceCustom = serviceCustomService.getByIdEnsure(serviceCustomId);

        if (!Objects.equals(userId, serviceCustom.getUserId())) {
            throw new IllegalArgumentException("用户不匹配");
        }

        // TODO 根据订单类型判断可用支付类型
        // 先写死
        List<PayType> payTypeList = List.of(PayType.WECHAT);

        return OutputData.builder()
                .payTypeList(payTypeList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        @Positive
        private Long userId;

        @NotNull(message = "订单不能为空")
        @Positive
        private Long serviceCustomId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 可用支付方式列表
         */
        private List<PayType> payTypeList;

    }
}
