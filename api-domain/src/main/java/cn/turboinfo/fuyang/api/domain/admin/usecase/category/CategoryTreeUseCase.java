package cn.turboinfo.fuyang.api.domain.admin.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class CategoryTreeUseCase extends AbstractUseCase<CategoryTreeUseCase.InputData, CategoryTreeUseCase.OutputData> {
    private final CategoryService categoryService;

    @Override
    protected OutputData doAction(InputData inputData) {
        return OutputData.builder()
                .categoryList(categoryService.findAllSortedWithHierarchy())
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<Category> categoryList;
    }
}
