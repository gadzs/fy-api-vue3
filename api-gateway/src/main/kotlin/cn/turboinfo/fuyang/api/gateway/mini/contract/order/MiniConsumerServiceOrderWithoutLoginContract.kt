package cn.turboinfo.fuyang.api.gateway.mini.contract.order

import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniAvailableProductSkuForOrderUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/order"])
interface MiniConsumerServiceOrderWithoutLoginContract {

    // 下单时可选服务产品sku, 这一步不要求登录
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniAvailableProductSkuForOrderUseCase::class,
        consumes = []
    )
    fun availableProductSku()

}