package cn.turboinfo.fuyang.api.domain.common.service.credit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;

public interface CreditRatingService extends BaseQService<CreditRating, Long> {

    /**
     * 按照实体类型和实体ID批量查找信用评价记录
     */
    List<CreditRating> findByObjectIdCollection(EntityObjectType objectType, Collection<Long> objectIdCollection);

    void updateStatus(Long id, CreditRatingStatus status);
}
