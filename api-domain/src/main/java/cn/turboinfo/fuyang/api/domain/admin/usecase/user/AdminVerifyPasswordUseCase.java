package cn.turboinfo.fuyang.api.domain.admin.usecase.user;

import cn.turboinfo.fuyang.api.domain.common.service.user.UserCredentialService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserCredential;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 验证密码
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class AdminVerifyPasswordUseCase extends AbstractUseCase<AdminVerifyPasswordUseCase.InputData, AdminVerifyPasswordUseCase.OutputData> {

    private final UserCredentialService userCredentialService;


    @Override
    protected OutputData doAction(InputData inputData) {
        Long sysUserId = inputData.getUserId();

        // 验证密码
        // 解密前端加密内容
        String credential = userCredentialService.decrypt(inputData.getPassword());

        // 凭据ID
        UserCredential userDefault = userCredentialService.findUserDefault(sysUserId).orElseThrow();

        // 验证凭据
        if (!userCredentialService.match(userDefault.getId(), credential)) {
            throw new RuntimeException("密码不正确");
        }


        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        private Long userId;

        @NotNull(
                message = "password不能为空"
        )
        private String password;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
