package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecCheckAvailableUseCase extends AbstractUseCase<SpecCheckAvailableUseCase.InputData, SpecCheckAvailableUseCase.OutputData> {
    private final SpecService specService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long specId = inputData.getSpecId();

        Long parentId = inputData.getParentId();

        String name = inputData.getName();
        boolean available;

        if (specId != null && specId != 0) {
            available = specService.checkAvailable(parentId, name);
        } else {
            available = specService.checkAvailable(specId, parentId, name);
        }

        return OutputData.builder()
                .available(available)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        private Long specId;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private boolean available;
    }
}
