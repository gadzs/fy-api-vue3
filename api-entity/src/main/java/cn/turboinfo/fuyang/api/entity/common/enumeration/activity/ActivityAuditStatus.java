package cn.turboinfo.fuyang.api.entity.common.enumeration.activity;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 活动审核状态
 *
 * @author 海
 */
@Slf4j
public enum ActivityAuditStatus implements BaseEnum {

    INIT(0, "待审核"),

    PASS(10, "审核通过"),

    NOT_PASS(99, "审核未通过"),

    ;

    private int value;
    private String name;

    ActivityAuditStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ActivityAuditStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ActivityAuditStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ActivityAuditStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
