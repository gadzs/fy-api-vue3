package cn.turboinfo.fuyang.api.provider.common.repository.database.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface PayOrderDAO extends FYRepository<PayOrderPO, Long> {

    List<PayOrderPO> findByObjectTypeAndObjectIdAndPayOrderStatusOrderByCreatedTime(EntityObjectType objectType, Long objectId, PayOrderStatus payOrderStatus);

    List<PayOrderPO> findByObjectTypeAndObjectIdOrderByCreatedTime(EntityObjectType objectType, Long objectId);

    List<PayOrderPO> findByObjectTypeAndObjectIdAndPayTypeAndPayOrderStatusOrderByCreatedTime(EntityObjectType objectType, Long objectId, PayType payType, PayOrderStatus payOrderStatus);

}
