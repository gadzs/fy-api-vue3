package cn.turboinfo.fuyang.api.gateway.admin.contract.spec

import cn.turboinfo.fuyang.api.domain.admin.usecase.spec.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/spec"])
interface SpecContract {

    // 添加规则
    @PermissionScope("spec:create")
    @AutoMappingContract(
        beanType = SpecCreateUseCase::class,
    )
    fun create()

    // 修改规则
    @PermissionScope("spec:update")
    @AutoMappingContract(
        beanType = SpecUpdateUseCase::class,
    )
    fun update()

    // 查询上级规则
    @PermissionScope("spec:list")
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = SpecAncestorsUpdateUseCase::class,
        consumes = [],
    )
    fun ancestors()

    // 检查名称是否可用
    @PermissionScope("spec:list")
    @CompanyScope
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = SpecCheckAvailableUseCase::class,
        consumes = [],
    )
    fun checkAvailable()

    @PermissionScope("spec:delete")
    @AutoMappingContract(
        beanType = SpecDeleteUseCase::class,
        consumes = [],
    )
    fun delete()
}
