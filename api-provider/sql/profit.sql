DROP TABLE IF EXISTS `profit_sharing`;
CREATE TABLE `profit_sharing`
(
    `id`                BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `object_id`         BIGINT       NOT NULL DEFAULT 0 COMMENT '订单编码',
    `object_type`       TINYINT      NOT NULL DEFAULT 0 COMMENT '订单类型',
    `company_id`        BIGINT       NOT NULL DEFAULT 0 COMMENT '企业ID',
    `wx_transaction_id` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '微信支付订单号',
    `wx_order_id`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '微信分账单号',
    `status`            TINYINT      NOT NULL DEFAULT 0 COMMENT '分账状态',
    `deleted`           BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_object_type_object_id` (`object_type`, `object_id`) USING BTREE,
    INDEX `idx_companyid` (`company_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='分账管理';

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('分账管理', 'adminProfitSharing:null', '', '/profitSharing', '', 1, 0, 'el-icon-share', 48);
SET
    @profitSharingPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('分账列表', 'adminProfitSharing:list', '', '/profitSharing/list', '', 1, @profitSharingPermissionParentId,
        'el-icon-share'),
       ('分账查看', 'adminProfitSharing:view', '', '/profitSharing/view', '', 0, @profitSharingPermissionParentId, '');

DROP TABLE IF EXISTS `profit_sharing_record`;
CREATE TABLE `profit_sharing_record`
(
    `id`                BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `profit_sharing_id` BIGINT       NOT NULL DEFAULT 0 COMMENT '分账ID',
    `wx_detail_id`      VARCHAR(128) NOT NULL DEFAULT '' COMMENT '微信分账明细单号',
    `receiver_id`       BIGINT       NOT NULL DEFAULT 0 COMMENT '接收方编码',
    `receiver_type`     TINYINT      NOT NULL DEFAULT 0 COMMENT '接收方类型',
    `amount`            TINYINT      NOT NULL DEFAULT 0 COMMENT '分账金额',
    `account`           VARCHAR(128) NOT NULL DEFAULT '' COMMENT '分账账户',
    `result`            TINYINT      NOT NULL DEFAULT 0 COMMENT '分账结果',
    `fail_reason`       TINYINT      NOT NULL DEFAULT 0 COMMENT '分账失败原因',
    `description`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '分账描述',
    `finish_time`       DATETIME     NULL COMMENT '完成时间',
    `deleted`           BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_profitsharingid` (`profit_sharing_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='分账记录';
