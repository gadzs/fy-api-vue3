package cn.turboinfo.fuyang.api.domain.common.handler.address;


import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.domain.util.UsernameUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class AddressDataFactory {

    public void maskInfo(List<Address> addresseList) {
        addresseList.stream()
                .peek(this::maskInfo)
                .toList();
    }

    public void maskInfo(Address address) {
        address.setMobile(PhoneUtils.maskPhoneNum(address.getMobile()));
        address.setContact(UsernameUtils.maskUsername(address.getContact()));
    }
}
