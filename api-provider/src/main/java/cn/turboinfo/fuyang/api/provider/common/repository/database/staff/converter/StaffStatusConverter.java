package cn.turboinfo.fuyang.api.provider.common.repository.database.staff.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * @author gadzs
 * @description 家政员状态
 * @date 2023/2/7 16:47
 */
@Converter(
        autoApply = true
)
public class StaffStatusConverter extends BaseEnumConverter<StaffStatus> {
}
