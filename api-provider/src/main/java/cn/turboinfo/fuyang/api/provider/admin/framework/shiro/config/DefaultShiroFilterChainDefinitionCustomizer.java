package cn.turboinfo.fuyang.api.provider.admin.framework.shiro.config;

import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;

/**
 * author: sunshow.
 */
public interface DefaultShiroFilterChainDefinitionCustomizer {

    void apply(DefaultShiroFilterChainDefinition chainDefinition);

}
