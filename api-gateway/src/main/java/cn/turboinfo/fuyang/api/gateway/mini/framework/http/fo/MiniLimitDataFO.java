package cn.turboinfo.fuyang.api.gateway.mini.framework.http.fo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * author: sunshow.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MiniLimitDataFO<T> {

    private Collection<T> items;

    private long total;

    private int totalPage;

    public static <E> MiniLimitDataFO<E> fromQResponse(QResponse<E> qresponse) {
        // 下标从1开始 所以要+1
        return new MiniLimitDataFO<>(qresponse.getPagedData(), qresponse.getTotal(), qresponse.getPageTotal() + 1);
    }

    public static <E, D> MiniLimitDataFO<D> fromQResponse(QResponse<E> qresponse, Function<E, D> mapper) {
        // 下标从1开始 所以要+1
        return new MiniLimitDataFO<>(qresponse.getPagedData().stream().map(mapper).collect(Collectors.toList()), qresponse.getTotal(), qresponse.getPageTotal() + 1);
    }
}
