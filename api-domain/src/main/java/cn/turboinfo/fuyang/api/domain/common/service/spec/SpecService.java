package cn.turboinfo.fuyang.api.domain.common.service.spec;

import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;

/**
 * 规格服务
 */
public interface SpecService extends BaseQService<Spec, Long> {

    /**
     * @return 返回所有的带层级和排序的规格列表
     */
    List<Spec> findAllSortedWithHierarchy();

    List<Spec> findWithHierarchy(Collection<Long> specSetIdCollection);

    List<Spec> findBySpecSetIdWithHierarchy(Long specSetId);

    /**
     * @param parentId 父级编码
     * @param name     名称
     */
    List<Spec> findByParentIdAndName(Long parentId, String name);

    /**
     * @param parentId 父级编码
     */
    List<Spec> findByParentId(Long parentId);

    /**
     * 检查是否可用
     *
     * @param name 名称
     * @return
     */
    boolean checkAvailable(Long parentId, String name);

    /**
     * 更新检查是否可用
     *
     * @param specId   自身编码
     * @param parentId 父级编码
     * @param name     名称
     * @return
     */
    boolean checkAvailable(Long specId, Long parentId, String name);

}
