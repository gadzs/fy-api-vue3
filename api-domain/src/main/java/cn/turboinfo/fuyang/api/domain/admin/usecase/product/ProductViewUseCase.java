package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.handler.product.ProductDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductSkuService;
import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetRelService;
import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSet;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政企业产品usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductViewUseCase extends AbstractUseCase<ProductViewUseCase.InputData, ProductViewUseCase.OutputData> {
    private final ProductService productService;
    private final ProductSkuService productSkuService;
    private final SpecSetRelService specSetRelService;
    private final SpecSetService specSetService;
    private final FileAttachmentService fileAttachmentService;
    private final ProductAuditRecordService productAuditRecordService;
    private final ProductDataFactory productDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        Optional<Product> productOptional = productService.getById(inputData.getId());
        Product product = productOptional.orElse(null);
        List<ProductAuditRecord> auditRecordList = new ArrayList<>();
        if (product != null) {
            // 查询附件
            product.setImgList(fileAttachmentService.findByRefIdAndRefType(product.getId(), FileRefTypeConstant.PRODUCT_IMG)
                    .stream()
                    .map(FileAttachment::getId)
                    .toList());

            fileAttachmentService.findByRefIdAndRefType(product.getId(), FileRefTypeConstant.PRODUCT_VIDEO)
                    .forEach(fileAttachment -> {
                        product.getVideoList().add(fileAttachment.getId());
                        product.getVideoNameList().add(fileAttachment.getDisplayName());
                    });

            //查询属性组
            val specSetRelOptional = specSetRelService.getByRelObject(product.getId(), EntityObjectType.PRODUCT);
            if (specSetRelOptional.isPresent()) {
                product.setSpecSetId(specSetRelOptional.get().getSpecSetId());
                product.setSpecSetName(specSetService.getById(specSetRelOptional.get().getSpecSetId()).orElse(new SpecSet()).getName());
            }
            //查询sku
            product.setProductSkuList(productSkuService.findSkuByProductId(product.getId()));

            productDataFactory.assembleCompany(product);
            productDataFactory.assembleShop(product);

            // 查询审核记录
            auditRecordList = productAuditRecordService.findByProductId(product.getId());
        }


        return OutputData.builder()
                .product(product)
                .auditRecordList(auditRecordList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Product product;

        private List<ProductAuditRecord> auditRecordList;
    }

}
