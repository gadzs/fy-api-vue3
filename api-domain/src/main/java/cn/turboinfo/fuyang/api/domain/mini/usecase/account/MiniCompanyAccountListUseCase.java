package cn.turboinfo.fuyang.api.domain.mini.usecase.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class MiniCompanyAccountListUseCase extends AbstractUseCase<MiniCompanyAccountListUseCase.InputData, MiniCompanyAccountListUseCase.OutputData> {
    private final CompanyAccountService companyAccountService;

    @Override
    protected OutputData doAction(InputData inputData) {


        List<CompanyAccount> companyAccountList = companyAccountService.findByCompanyId(inputData.getCompanyId());

        return OutputData.builder()
                .accountList(companyAccountList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "企业ID不能为空"
        )
        @Positive
        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<CompanyAccount> accountList;
    }
}
