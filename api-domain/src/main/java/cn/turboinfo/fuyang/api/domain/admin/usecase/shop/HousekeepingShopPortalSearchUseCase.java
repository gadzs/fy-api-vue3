package cn.turboinfo.fuyang.api.domain.admin.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.fo.portal.PortalShopDisplayFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopPortalSearchUseCase extends AbstractUseCase<HousekeepingShopPortalSearchUseCase.InputData, HousekeepingShopPortalSearchUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;

    private final ShopDataFactory shopDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<HousekeepingShop> response = housekeepingShopService.findAll(inputData.request, inputData.getRequestPage());

        List<HousekeepingShop> shopList = new ArrayList<>(response.getPagedData());

        // 拼装企业名称
        shopDataFactory.assembleCompany(shopList);

        // 拼装图片
        shopDataFactory.assembleAttachment(shopList);

        val shopDisplayFOS = shopList.stream().map(housekeepingShop -> {
            PortalShopDisplayFO portalShopDisplayFO = new PortalShopDisplayFO();
            BeanUtils.copyProperties(housekeepingShop, portalShopDisplayFO);
            return portalShopDisplayFO;
        }).collect(Collectors.toList());

        QResponse<PortalShopDisplayFO> foResponse = new QResponse<>(response.getPage(), response.getPageSize(), shopDisplayFOS, response.getTotal());
        return OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<PortalShopDisplayFO> qResponse;
    }
}
