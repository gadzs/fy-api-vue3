package cn.turboinfo.fuyang.api.entity.common.exception.common;

/**
 * 数据不存在
 *
 * @author qatang
 */
public class DataNotExistException extends RuntimeException {

    public DataNotExistException() {
        super("数据不存在");
    }

    public DataNotExistException(String message) {
        super(message);
    }
}
