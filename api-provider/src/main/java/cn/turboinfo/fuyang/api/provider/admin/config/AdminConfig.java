package cn.turboinfo.fuyang.api.provider.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.ConfigureRedisAction;

@Configuration
public class AdminConfig {

    // 初始化时去掉注释执行一次
    /*
    @Bean
    protected DefaultUserPermissionInitListener defaultUserPermissionInitListener() {
        return new DefaultUserPermissionInitListener();
    }
    */

    @Bean
    protected static ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }
    
}
