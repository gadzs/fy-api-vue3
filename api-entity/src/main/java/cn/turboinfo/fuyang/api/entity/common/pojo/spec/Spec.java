package cn.turboinfo.fuyang.api.entity.common.pojo.spec;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 规格
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Spec extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 名称, 导入等场景时比对的名称
     */
    private String name;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 父级 ID
     */
    private Long parentId;

    /**
     * 规格组编码
     */
    private Long specSetId;

    /**
     * 排序值
     */
    private Integer sortValue;

    private List<Spec> children;

    /**
     * 是否允许删除
     */
    private YesNoStatus allowDelete;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
