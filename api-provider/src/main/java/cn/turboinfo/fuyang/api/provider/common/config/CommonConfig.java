package cn.turboinfo.fuyang.api.provider.common.config;

import cn.turboinfo.fuyang.api.provider.admin.component.config.AdminKitConfig;
import cn.turboinfo.fuyang.api.provider.admin.component.security.SecurityEncoder;
import cn.turboinfo.fuyang.api.provider.admin.component.security.impl.RSASecurityEncoder;
import cn.turboinfo.fuyang.api.provider.admin.framework.shiro.helper.PasswordHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CommonConfig {

    @Value("${shiro.password.algorithmName:SHA-256}")
    String algorithmName;

    @Value("${shiro.password.hashIterations:3}")
    int hashIterations;

    @Value("${shiro.password.retryLimit:0}")
    int retryLimit;

    @Value("${shiro.password.retryPeriodInMillis:0}")
    long retryPeriodInMillis;

    @Bean
    protected AdminKitConfig adminKitConfig() {
        return new AdminKitConfig();
    }

    @Bean
    protected PasswordHelper passwordHelper() {
        return new PasswordHelper(algorithmName, hashIterations, retryLimit, retryPeriodInMillis, TimeUnit.MILLISECONDS);
    }

    @Bean
    protected SecurityEncoder rsaSecurityEncoder(AdminKitConfig adminKitConfig) {
        return new RSASecurityEncoder(null, adminKitConfig.getBase64RSAPrivateKey());
    }

}
