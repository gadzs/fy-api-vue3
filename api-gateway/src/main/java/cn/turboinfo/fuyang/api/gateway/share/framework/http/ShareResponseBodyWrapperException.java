package cn.turboinfo.fuyang.api.gateway.share.framework.http;


import cn.turboinfo.fuyang.api.gateway.share.constant.ShareRestConstants;

public class ShareResponseBodyWrapperException extends RuntimeException {

    private final int code;

    private final Object data;

    public ShareResponseBodyWrapperException(int code, String message) {
        this(code, message, null, null);
    }

    public ShareResponseBodyWrapperException(String message) {
        this(message, null);
    }

    public ShareResponseBodyWrapperException(String message, Throwable cause) {
        this(ShareRestConstants.RC_ERROR_DEFAULT, message, null, cause);
    }

    public ShareResponseBodyWrapperException(int code, String message, Object data, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

}
