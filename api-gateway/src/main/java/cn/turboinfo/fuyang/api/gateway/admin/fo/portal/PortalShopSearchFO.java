package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * @author gadzs
 * @description
 * @date 2023/3/8 15:59
 */
@Data
@QField
public class PortalShopSearchFO {

    @QField(
            placeholder = "地区编码",
            operator = Operator.EQUAL
    )
    private String areaCode;

    @QField(
            placeholder = "分类id",
            searchable = false
    )
    private Long categoryId;

    @QField(
            placeholder = "关键字",
            searchable = false
    )
    private String keyword;
}
