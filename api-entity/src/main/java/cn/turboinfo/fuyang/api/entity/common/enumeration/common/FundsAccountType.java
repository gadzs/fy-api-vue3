package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 账户类型
 */
@Slf4j
public enum FundsAccountType implements BaseEnum {

    BASIC(0, "基本账户（含可用余额和不可用余额）"),

    UNAVAILABLE(10, "不可用余额"),

    UNSETTLED(20, "未结算资金"),

    AVAILABLE(30, "未结算资金"),

    OPERATION(40, "运营户"),


    ;

    private int value;
    private String name;

    FundsAccountType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static FundsAccountType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<FundsAccountType> list() {
        return BaseEnumHelper.getList(values());
    }

}
