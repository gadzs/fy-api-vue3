package cn.turboinfo.fuyang.api.domain.admin.service.log;

import cn.turboinfo.fuyang.api.entity.admin.exception.log.OperationLogException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.OperationLog;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.OperationLogCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.OperationLogUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface OperationLogService {
    Optional<OperationLog> getById(Long id);

    OperationLog getByIdEnsure(Long id);

    List<OperationLog> findByIdCollection(Collection<Long> idCollection);

    OperationLog save(OperationLogCreator creator) throws OperationLogException;

    OperationLog update(OperationLogUpdater updater) throws OperationLogException;

    QResponse<OperationLog> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws OperationLogException;
}
