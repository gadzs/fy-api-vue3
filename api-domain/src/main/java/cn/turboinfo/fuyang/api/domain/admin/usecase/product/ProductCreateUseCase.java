package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.HtmlHelper;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductSku;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author gadzs
 * @description 家政产品创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductCreateUseCase extends AbstractUseCase<ProductCreateUseCase.InputData, ProductCreateUseCase.OutputData> {
    private final ProductService productService;
    private final ProductAuditRecordService productAuditRecordService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getName())) {
            throw new ProductException("服务名称包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getMobileContent())) {
            throw new ProductException("内容包含敏感词汇，请修改后重新提交！");
        }

        // 检查服务介绍是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getDescription())) {
            throw new ProductException("服务介绍包含敏感词汇，请修改后重新提交");
        }

        inputData.setMaxPrice(BigDecimal.valueOf(inputData.getProductSkuList().stream().mapToDouble(productSku -> productSku.getPrice().doubleValue()).max().getAsDouble()));
        inputData.setMinPrice(BigDecimal.valueOf(inputData.getProductSkuList().stream().mapToDouble(productSku -> productSku.getPrice().doubleValue()).min().getAsDouble()));
        ProductCreator.Builder builder = ProductCreator.builder();

        if (StringUtils.isNotBlank(inputData.getMobileContent())) {
            builder.withMobileContent(HtmlHelper.safeFilter(inputData.getMobileContent()));
        }

        if (inputData.getNeedContract() == null) {
            inputData.setNeedContract(YesNoStatus.NO);
        }

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, ProductCreator.class, inputData);
        Product product = productService.save(builder
                .withStatus(ProductStatus.DEFAULT)
                .build());

        // 添加审核记录
        ProductAuditRecordCreator productAuditRecordCreator = ProductAuditRecordCreator.builder()
                .withCompanyId(product.getCompanyId())
                .withProductId(product.getId())
                .withShopId(product.getShopId())
                .withAuditStatus(AuditStatus.INIT)
                .withUserId(0L)
                .withUserName(StringUtils.EMPTY)
                .withRemark("新增产品服务")
                .build();
        productAuditRecordService.save(productAuditRecordCreator);

        return OutputData.builder()
                .Product(product)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 公司ID
         */
        private Long companyId;

        /**
         * 店铺ID
         */
        private Long shopId;

        @NotNull(
                message = "分类不能为空"
        )
        private Long categoryId;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        /**
         * 介绍
         */
        private String description;

        @NotNull(
                message = "图片不能为空"
        )
        private List<Long> imgList;

        /**
         * 视频id
         */
        private List<Long> videoList;

        @NotNull(
                message = "规格组不能为空"
        )
        private Long specSetId;

        @NotNull(
                message = "规格不能为空"
        )
        private List<ProductSku> productSkuList;

        /**
         * 移动端内容详情
         */
        private String mobileContent;

        public List<Long> getImgList() {
            if (imgList == null) {
                imgList = Lists.newArrayList();
            }
            return imgList;
        }

        public List<Long> getVideoList() {
            if (videoList == null) {
                videoList = Lists.newArrayList();
            }
            return videoList;
        }

        /**
         * 当前售价(最小)
         */
        private BigDecimal minPrice;

        /**
         * 当前售价（最大）
         */
        private BigDecimal maxPrice;

        /**
         * 是否需要合同
         */
        private YesNoStatus needContract;

        /**
         * 合同模板id
         */
        private Long contractTmplId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Product Product;
    }

}
