package cn.turboinfo.fuyang.api.gateway.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author hai.
 */

@Data
@ConfigurationProperties(prefix = "kit.wechat.pay")
public class WxPayProperties {
    /**
     * 设置微信公众号或者小程序等的appid
     */
    private String appId;

    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * api v3 key
     */
    private String apiV3Key;

    /**
     * 商户证书序列号
     */
    private String mchNo;

    /**
     * apiclient_key.pem
     */
    private String keyPath;

    /**
     * apiclient_cert.pem
     */
    private String certPath;
}
