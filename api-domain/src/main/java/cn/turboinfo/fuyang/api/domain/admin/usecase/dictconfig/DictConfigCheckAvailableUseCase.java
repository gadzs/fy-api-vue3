package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigCheckAvailableUseCase extends AbstractUseCase<DictConfigCheckAvailableUseCase.InputData, DictConfigCheckAvailableUseCase.OutputData> {
    private final DictConfigService dictConfigService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long id = inputData.getDictId();

        String dictKey = inputData.getDictKey();

        if (id != null && id > 0) {
            Optional<DictConfig> optional = dictConfigService.getById(id);
            if (optional.isPresent()) {
                DictConfig config = optional.get();
                if (config.getDictKey().equals(dictKey)) {
                    // 可以维持使用自身的不做变动
                    return OutputData.builder()
                            .available(true)
                            .build();
                }
            } else {
                return OutputData.builder()
                        .available(false)
                        .build();
            }
        }
        boolean available = dictConfigService.getByDictKey(dictKey).isEmpty();

        return OutputData.builder()
                .available(available)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private Long dictId;

        @NotBlank(
                message = "字典 Key不能为空"
        )
        private String dictKey;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private boolean available;
    }
}
