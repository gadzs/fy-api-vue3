package cn.turboinfo.fuyang.api.domain.common.service.role;

import cn.turboinfo.fuyang.api.entity.admin.exception.user.RoleException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RoleCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RoleUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RoleService {
    Optional<Role> getById(Long id);

    Role getByIdEnsure(Long id) throws RoleException;

    Optional<Role> getByCode(String code);

    Role save(RoleCreator creator) throws RoleException;

    Role update(RoleUpdater updater) throws RoleException;

    QResponse<Role> findAll(QRequest request, QPage requestPage);

    List<Role> findAll();

    void deleteById(Long id) throws RoleException;

    List<Role> findByIdCollection(Collection<Long> idCollection);

    List<Role> findByCodeCollection(Collection<String> codeCollection);
}
