CREATE TABLE `cms_category`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `name`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '栏目名称',
    `code`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '栏目编码',
    `description`  VARCHAR(128) NOT NULL DEFAULT '' COMMENT '栏目描述',
    `type`         TINYINT      NOT NULL DEFAULT 0 COMMENT '栏目类型',
    `index_show`   TINYINT      NOT NULL DEFAULT 0 COMMENT '是否首页展示',
    `sort`         INT          NOT NULL DEFAULT 0 COMMENT '排序',
    `slug_name`    VARCHAR(64)  NOT NULL DEFAULT 0 COMMENT '英文简写',
    `linked_url`   VARCHAR(128) NOT NULL DEFAULT '' COMMENT '链接',
    `parent_id`    BIGINT       NOT NULL DEFAULT 0 COMMENT '父级分类ID',
    `deleted_time` BIGINT       NOT NULL DEFAULT 0 COMMENT '',
    `created_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    KEY            `idx_slugname` (`slug_name`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='分类';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('信息发布', 'cms:null', '/cms', 1, 0, 'el-icon-files');
SET
@cmsParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('栏目管理', 'cms:category:list', '/cms/category/list', 1, @cmsParentId, 'el-icon-menu');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('新增栏目', 'cms:category:create', '', 0, @cmsParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('编辑栏目', 'cms:category:update', '', 0, @cmsParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('删除栏目', 'cms:category:delete', '', 0, @cmsParentId);


CREATE TABLE `cms_article`
(
    `id`               BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `category_id`      BIGINT        NOT NULL DEFAULT 0 COMMENT '分类',
    `type`             TINYINT       NOT NULL DEFAULT 0 COMMENT '文章类型',
    `status`           TINYINT       NOT NULL DEFAULT 0 COMMENT '文章状态',
    `title`            VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '标题',
    `subtitle`         VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '副标题',
    `author`           VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '作者',
    `company`          VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '公司',
    `linked_url`       VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '链接',
    `title_image_id`   BIGINT        NOT NULL DEFAULT 0 COMMENT '标题图片',
    `attach_id`        BIGINT        NOT NULL DEFAULT 0 COMMENT '附件',
    `abstract_content` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT '摘要',
    `content`          LONGTEXT NULL DEFAULT NULL COMMENT '内容',
    `sort`             INT           NOT NULL DEFAULT 0 COMMENT '排序',
    `published_time`   TIMESTAMP NULL COMMENT '发布时间',
    `visit_count`      BIGINT        NOT NULL DEFAULT 0 COMMENT '访问次数',
    `deleted_time`     BIGINT        NOT NULL DEFAULT 0 COMMENT '',
    `created_time`     TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`     TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='文章';

SET
@cmsParentId = (SELECT id
                    FROM `sys_permission`
                    WHERE name = '信息发布');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('内容管理', 'cms:article:list', '/cms/article/list', 1, @cmsParentId, 'el-icon-document');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('编辑内容', 'cms:article:update', '/cms/article/detail', 0, @cmsParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('删除内容', 'cms:article:delete', '', 0, @cmsParentId);


CREATE TABLE `cms_message_board`
(
    `id`             BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `user_id`        BIGINT       NOT NULL DEFAULT 0 COMMENT '留言用户',
    `username`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '留言用户',
    `title`          VARCHAR(128) NOT NULL DEFAULT '' COMMENT '标题',
    `mobile`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '电话',
    `email`          VARCHAR(128) NOT NULL DEFAULT '' COMMENT '邮箱',
    `content`        TEXT NULL COMMENT '留言内容',
    `type`           TINYINT      NOT NULL DEFAULT 0 COMMENT '类型',
    `busi_type`      VARCHAR(128) NOT NULL DEFAULT '' COMMENT '业务子类型',
    `is_show`        TINYINT      NOT NULL DEFAULT 0 COMMENT '是否展示',
    `reply_user_id`  BIGINT       NOT NULL DEFAULT 0 COMMENT '回复用户',
    `reply_username` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '回复用户',
    `reply_content`  TEXT NULL COMMENT '回复内容',
    `reply_time`     TIMESTAMP NULL COMMENT '回复时间',
    `sort`           TINYINT      NOT NULL DEFAULT 0 COMMENT '排序值',
    `advice_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '意见征集id',
    `deleted_time`   BIGINT       NOT NULL DEFAULT 0 COMMENT '',
    `created_time`   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='留言板';

SET
@cmsParentId = (SELECT id
                    FROM `sys_permission`
                    WHERE name = '信息发布');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('用户咨询', 'cms:consult:list', '/cms/messageboard/consult', 1, @cmsParentId, 'el-icon-document');
SET
@consultId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('用户咨询展示', 'cms:consult:setShow', '', 0, @consultId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('用户咨询回复', 'cms:consult:reply', '', 0, @consultId);

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('用户建议', 'cms:suggest:list', '/cms/messageboard/suggest', 1, @cmsParentId, 'el-icon-document');
SET
@consultId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('用户建议展示', 'cms:suggest:setShow', '', 0, @consultId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('用户建议回复', 'cms:suggest:reply', '', 0, @consultId);

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('富文本附件上传', 'upload:tinymce', '', 0, 0);