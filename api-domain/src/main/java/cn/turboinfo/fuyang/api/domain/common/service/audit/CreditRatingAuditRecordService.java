package cn.turboinfo.fuyang.api.domain.common.service.audit;

import cn.turboinfo.fuyang.api.entity.common.pojo.audit.CreditRatingAuditRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 评价审核记录服务
 *
 * @author: hai
 */
public interface CreditRatingAuditRecordService extends BaseQService<CreditRatingAuditRecord, Long> {
    List<CreditRatingAuditRecord> findByCreditRatingId(Long creditRatingId);

}
