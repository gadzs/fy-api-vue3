package cn.turboinfo.fuyang.api.gateway.admin.fo.agencies;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * @author hai.
 */
@Data
@QField
public class AgenciesSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "名称",
            operator = Operator.LIKE
    )
    private String name;
}
