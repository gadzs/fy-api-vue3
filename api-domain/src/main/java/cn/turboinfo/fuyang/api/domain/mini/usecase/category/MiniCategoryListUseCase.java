package cn.turboinfo.fuyang.api.domain.mini.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 分类列表
 *
 * @author gadzs
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniCategoryListUseCase extends AbstractUseCase<MiniCategoryListUseCase.InputData, MiniCategoryListUseCase.OutputData> {

    private final CategoryService categoryService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {
        List<Category> categoryList = categoryService.findAllSortedWithHierarchy();
        return OutputData.builder()
                .categoryList(categoryList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 分类
         */
        private List<Category> categoryList;

    }
}
