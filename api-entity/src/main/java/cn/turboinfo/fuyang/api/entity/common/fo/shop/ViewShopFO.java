package cn.turboinfo.fuyang.api.entity.common.fo.shop;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author hai.
 */
@Data
public class ViewShopFO {

    private Long id;

    private Long companyId;
    
    private String companyName;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 成立日期
     */
    private LocalDate foundDate;

    private String areaName;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系人电话
     */
    private String contactMobile;

    /**
     * 加密联系电话
     */
    private String contactMobileEncrypt;

    /**
     * 营业时间
     */
    private String businessTime;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 店铺图片
     */
    private List<Long> imgList;

}
