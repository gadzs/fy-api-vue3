package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@QField
public class CmsMessageBoardSearchFO {
    @QField(
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "留言用户"
    )
    private Long userId;

    @QField(
            placeholder = "留言用户名",
            operator = Operator.LIKE
    )
    private String username;

    @QField(
            placeholder = "标题",
            operator = Operator.LIKE
    )
    private String title;

    @QField(
            placeholder = "留言内容",
            operator = Operator.LIKE
    )
    private String content;

    @QField(
            placeholder = "联系方式",
            operator = Operator.LIKE
    )
    private String mobile;

    @QField(
            placeholder = "邮件",
            operator = Operator.LIKE
    )
    private String email;

    /**
     * 意见征集id
     */
    @QField(
            placeholder = "意见征集id"
    )
    private Long adviceId;

    @QField(
            placeholder = "类型",
            control = Control.SELECT,
            ref = "messageBoardTypeList",
            template = "enum"
    )
    private CmsMessageBoardType type;

    @QField(
            placeholder = "业务子类型",
            operator = Operator.LIKE
    )
    private String busiType;

    @QField(
            placeholder = "是否展示",
            control = Control.SELECT,
            ref = "yesNoStatusList",
            template = "enum"
    )
    private YesNoStatus isShow;

    @QField(
            placeholder = "回复用户"
    )
    private Long replyUserId;

    @QField(
            placeholder = "留言用户名",
            operator = Operator.LIKE
    )
    private String replyUsername;

    @QField(
            placeholder = "回复内容",
            operator = Operator.LIKE
    )
    private String replyContent;

    @QField(
            placeholder = "回复时间"
    )
    private LocalDateTime replyTime;

    @QField(
            placeholder = "排序值"
    )
    private Integer sort;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;

    @QField(
            searchable = false
    )
    @DateTimeFormat(pattern = "yyyy-MM-dd")                    // 表示返回时间类型
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")      // 表示接收时间类型
    private LocalDate startDate;

    @QField(
            searchable = false
    )
    @DateTimeFormat(pattern = "yyyy-MM-dd")                    // 表示返回时间类型
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")      // 表示接收时间类型
    private LocalDate endDate;
}
