-- 2023-11-01 给数据局创建用户
CREATE USER 'sjjshare'@'%' IDENTIFIED BY '&E7X5!2JxY';
-- 分配权限
GRANT SELECT ON fuyang.housekeeping_company TO 'sjjshare'@'%';
GRANT SELECT ON fuyang.housekeeping_shop TO 'sjjshare'@'%';
GRANT SELECT ON fuyang.housekeeping_staff TO 'sjjshare'@'%';
GRANT SELECT ON fuyang.service_order TO 'sjjshare'@'%';

FLUSH PRIVILEGES;

-- 企业表添加时间索引
ALTER TABLE `housekeeping_company`
    ADD KEY `idx_createdtime` (`created_time`);
ALTER TABLE `housekeeping_company`
    ADD KEY `idx_updatetime` (`updated_time`);

-- 家政门店表添加时间索引
ALTER TABLE `housekeeping_shop`
    ADD KEY `idx_createdtime` (`created_time`);
ALTER TABLE `housekeeping_shop`
    ADD KEY `idx_updatetime` (`updated_time`);

-- 家政员表增加时间索引
ALTER TABLE `housekeeping_staff`
    ADD KEY `idx_createdtime` (`created_time`);
ALTER TABLE `housekeeping_staff`
    ADD KEY `idx_updatetime` (`updated_time`);

-- 服务订单表添加时间索引
ALTER TABLE `service_order`
    ADD KEY `idx_createdtime` (`created_time`);
ALTER TABLE `service_order`
    ADD KEY `idx_updatetime` (`updated_time`);

-- 企业表增加字段注释
ALTER TABLE `housekeeping_company`
    MODIFY `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）';
ALTER TABLE `housekeeping_company`
    MODIFY `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE `housekeeping_company`
    MODIFY `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

-- 家政门店表增加字段注释
ALTER TABLE `housekeeping_shop`
    MODIFY `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）';
ALTER TABLE `housekeeping_shop`
    MODIFY `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE `housekeeping_shop`
    MODIFY `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';
SHOW FULL COLUMNS FROM `housekeeping_shop`;

-- 家政人员表增加字段注释
ALTER TABLE `housekeeping_staff`
    MODIFY `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）';
ALTER TABLE `housekeeping_staff`
    MODIFY `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE `housekeeping_staff`
    MODIFY `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';
SHOW FULL COLUMNS FROM `housekeeping_staff`;

-- 订单表增加字段注释
ALTER TABLE `service_order`
    MODIFY `user_id` BIGINT(1) NOT NULL COMMENT '用户编码';
ALTER TABLE `service_order`
    MODIFY `category_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '服务类别编码';
ALTER TABLE `service_order`
    MODIFY `product_sku_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '商品sku编码';
ALTER TABLE `service_order`
    MODIFY `product_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '商品编码';
ALTER TABLE `service_order`
    MODIFY `company_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '企业编码';
ALTER TABLE `service_order`
    MODIFY `shop_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '门店编码';
ALTER TABLE `service_order`
    MODIFY `staff_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '家政员编码';
ALTER TABLE `service_order`
    MODIFY `spec_list` JSON NULL COMMENT '规格列表';
ALTER TABLE `service_order`
    MODIFY `comment` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT '备注';
ALTER TABLE `service_order`
    MODIFY `scheduled_start_time` DATETIME NULL COMMENT '计划开始时间';
ALTER TABLE `service_order`
    MODIFY `scheduled_end_time` DATETIME NULL COMMENT '计划结束时间';
ALTER TABLE `service_order`
    MODIFY `service_start_time` DATETIME NULL COMMENT '服务开始时间';
ALTER TABLE `service_order`
    MODIFY `service_end_time` DATETIME NULL COMMENT '服务结束时间';
ALTER TABLE `service_order`
    MODIFY `order_status` INT(1) NOT NULL DEFAULT 0 COMMENT '订单状态';
ALTER TABLE `service_order`
    MODIFY `order_status_history` JSON NULL COMMENT '历史订单状态';
ALTER TABLE `service_order`
    MODIFY `price` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '价格';
ALTER TABLE `service_order`
    MODIFY `prepaid` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '已支付金额';
ALTER TABLE `service_order`
    MODIFY `additional_fee` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '附加费用';
ALTER TABLE `service_order`
    MODIFY `discount_fee` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '减免费用';
ALTER TABLE `service_order`
    MODIFY `pay_status` INT(1) NOT NULL DEFAULT 0 COMMENT '支付状态';
ALTER TABLE `service_order`
    MODIFY `refund_status` TINYINT NOT NULL DEFAULT 0 COMMENT '退款状态';
ALTER TABLE `service_order`
    MODIFY `credit_rating_status` TINYINT NOT NULL DEFAULT 0 COMMENT '评价状态';
ALTER TABLE `service_order`
    MODIFY `profit_sharing_status` TINYINT NOT NULL DEFAULT 0 COMMENT '分账状态';
ALTER TABLE `service_order`
    MODIFY `completed_time` DATETIME NULL COMMENT '完成时间';
ALTER TABLE `service_order`
    MODIFY `address_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '服务地址编码';
ALTER TABLE `service_order`
    MODIFY `contact` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '用户姓名';
ALTER TABLE `service_order`
    MODIFY `gender_type` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '用户性别';
ALTER TABLE `service_order`
    MODIFY `mobile` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '用户手机号';
ALTER TABLE `service_order`
    MODIFY `division_id` BIGINT(1) NOT NULL DEFAULT 0 COMMENT '地区编码';
ALTER TABLE `service_order`
    MODIFY `poi_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '服务地址';
ALTER TABLE `service_order`
    MODIFY `detail` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '服务详细地址';
ALTER TABLE `service_order`
    MODIFY `longitude` decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '经度';
ALTER TABLE `service_order`
    MODIFY `latitude` decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '纬度';
ALTER TABLE `service_order`
    MODIFY `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）';
ALTER TABLE `service_order`
    MODIFY `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE `service_order`
    MODIFY `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';
SHOW FULL COLUMNS FROM `service_order`;

-- 家政企业解密查询
SELECT `hc`.`id`                                                                AS `id`,
       `hc`.`name`                                                              AS `name`,
       `hc`.`short_name`                                                        AS `short_name`,
       IF(AES_DECRYPT(FROM_BASE64(`hc`.`uscc`), 'sGmnd1yRu5akPdvg', 0x00000000000000000000000000000000) IS NULL,
          `hc`.`uscc`, AES_DECRYPT(FROM_BASE64(`hc`.`uscc`), 'sGmnd1yRu5akPdvg',
                                   0x00000000000000000000000000000000))         AS `uscc`,
       `hc`.`establishment_date`                                                AS `establishment_date`,
       `hc`.`province_code`                                                     AS `province_code`,
       `hc`.`city_code`                                                         AS `city_code`,
       `hc`.`area_code`                                                         AS `area_code`,
       `hc`.`registered_address`                                                AS `registered_address`,
       `hc`.`registered_capital`                                                AS `registered_capital`,
       `hc`.`company_type`                                                      AS `company_type`,
       IF(AES_DECRYPT(FROM_BASE64(`hc`.`legal_person`), 'sGmnd1yRu5akPdvg', 0x00000000000000000000000000000000) IS NULL,
          `hc`.`legal_person`, AES_DECRYPT(FROM_BASE64(`hc`.`legal_person`), 'sGmnd1yRu5akPdvg',
                                           0x00000000000000000000000000000000)) AS `legal_person`,
       IF(AES_DECRYPT(FROM_BASE64(`hc`.`legal_person_id_card`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hc`.`legal_person_id_card`,
          AES_DECRYPT(FROM_BASE64(`hc`.`legal_person_id_card`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000))                      AS `legal_person_id_card`,
       `hc`.`business_registration_number`                                      AS `business_registration_number`,
       `hc`.`business_scope`                                                    AS `business_scope`,
       IF(AES_DECRYPT(FROM_BASE64(`hc`.`contact_person`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hc`.`contact_person`,
          AES_DECRYPT(FROM_BASE64(`hc`.`contact_person`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000))                      AS `contact_person`,
       IF(AES_DECRYPT(FROM_BASE64(`hc`.`contact_number`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hc`.`contact_number`,
          AES_DECRYPT(FROM_BASE64(`hc`.`contact_number`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000))                      AS `contact_number`,
       `hc`.`email`                                                             AS `email`,
       `hc`.`employees_number`                                                  AS `employees_number`,
       `hc`.`status`                                                            AS `status`,
       `hc`.`business_license_file`                                             AS `business_license_file`,
       `hc`.`legal_person_id_card_file`                                         AS `legal_person_id_card_file`,
       `hc`.`bank_account_certificate_file`                                     AS `bank_account_certificate_file`,
       `hc`.`deleted`                                                           AS `deleted`,
       `hc`.`credit_score`                                                      AS `credit_score`,
       `hc`.`review_user_id`                                                    AS `review_user_id`,
       `hc`.`review_time`                                                       AS `review_time`,
       `hc`.`shop_num`                                                          AS `shop_num`,
       `hc`.`staff_num`                                                         AS `staff_num`,
       `hc`.`product_num`                                                       AS `product_num`,
       `hc`.`order_num`                                                         AS `order_num`,
       `hc`.`auth_label_list`                                                   AS `auth_label_list`,
       `hc`.`house_keeping_type`                                                AS `house_keeping_type`,
       `hc`.`auth_label_weight`                                                 AS `auth_label_weight`,
       `hc`.`app_id`                                                            AS `app_id`,
       `hc`.`app_secret`                                                        AS `app_secret`,
       `hc`.`created_time`                                                      AS `created_time`,
       `hc`.`updated_time`                                                      AS `updated_time`
FROM `housekeeping_company` AS `hc`;

-- 家政门店解密查询
SELECT `hs`.`id`                                           AS `id`,
       `hs`.`company_id`                                   AS `company_id`,
       `hs`.`name`                                         AS `name`,
       `hs`.`found_date`                                   AS `found_date`,
       `hs`.`province_code`                                AS `province_code`,
       `hs`.`city_code`                                    AS `city_code`,
       `hs`.`area_code`                                    AS `area_code`,
       `hs`.`address`                                      AS `address`,
       `hs`.`longitude`                                    AS `longitude`,
       `hs`.`latitude`                                     AS `latitude`,
       IF(AES_DECRYPT(FROM_BASE64(`hs`.`contact_person`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hs`.`contact_person`,
          AES_DECRYPT(FROM_BASE64(`hs`.`contact_person`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `contact_person`,
       IF(AES_DECRYPT(FROM_BASE64(`hs`.`contact_mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hs`.`contact_mobile`,
          AES_DECRYPT(FROM_BASE64(`hs`.`contact_mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `contact_mobile`,
       `hs`.`business_time`                                AS `business_time`,
       `hs`.`business_license_file`                        AS `business_license_file`,
       `hs`.`status`                                       AS `status`,
       `hs`.`credit_score`                                 AS `credit_score`,
       `hs`.`order_num`                                    AS `order_num`,
       `hs`.`deleted`                                      AS `deleted`,
       `hs`.`created_time`                                 AS `created_time`,
       `hs`.`updated_time`                                 AS `updated_time`
FROM `housekeeping_shop` AS `hs`;

-- 家政人员解密查询
SELECT `hs`.`id`                                           AS `id`,
       `hs`.`company_id`                                   AS `company_id`,
       IF(AES_DECRYPT(FROM_BASE64(`hs`.`name`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hs`.`name`,
          AES_DECRYPT(FROM_BASE64(`hs`.`name`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `name`,
       `hs`.`code`                                         AS `code`,
       `hs`.`display_name`                                 AS `display_name`,
       IF(AES_DECRYPT(FROM_BASE64(`hs`.`id_card`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hs`.`id_card`,
          AES_DECRYPT(FROM_BASE64(`hs`.`id_card`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `id_card`,
       `hs`.`id_card_file`                                 AS `id_card_file`,
       `hs`.`photo_file`                                   AS `photo_file`,
       `hs`.`gender`                                       AS `gender`,
       IF(AES_DECRYPT(FROM_BASE64(`hs`.`contact_mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `hs`.`contact_mobile`,
          AES_DECRYPT(FROM_BASE64(`hs`.`contact_mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `contact_mobile`,
       `hs`.`introduction`                                 AS `introduction`,
       `hs`.`status`                                       AS `status`,
       `hs`.`credit_score`                                 AS `credit_score`,
       `hs`.`order_num`                                    AS `order_num`,
       `hs`.`employment_date`                              AS `employment_date`,
       `hs`.`seniority`                                    AS `seniority`,
       `hs`.`province_code`                                AS `province_code`,
       `hs`.`staff_type`                                   AS `staff_type`,
       `hs`.`deleted`                                      AS `deleted`,
       `hs`.`created_time`                                 AS `created_time`,
       `hs`.`updated_time`                                 AS `updated_time`
FROM `housekeeping_staff` AS `hs`;

-- 服务订单解密查询
SELECT `so`.`id`                                           AS `id`,
       `so`.`user_id`                                      AS `user_id`,
       `so`.`category_id`                                  AS `category_id`,
       `so`.`product_sku_id`                               AS `product_sku_id`,
       `so`.`product_id`                                   AS `product_id`,
       `so`.`company_id`                                   AS `company_id`,
       `so`.`shop_id`                                      AS `shop_id`,
       `so`.`staff_id`                                     AS `staff_id`,
       `so`.`spec_list`                                    AS `spec_list`,
       `so`.`comment`                                      AS `comment`,
       `so`.`scheduled_start_time`                         AS `scheduled_start_time`,
       `so`.`scheduled_end_time`                           AS `scheduled_end_time`,
       `so`.`service_start_time`                           AS `service_start_time`,
       `so`.`service_end_time`                             AS `service_end_time`,
       `so`.`order_status`                                 AS `order_status`,
       `so`.`order_status_history`                         AS `order_status_history`,
       `so`.`price`                                        AS `price`,
       `so`.`prepaid`                                      AS `prepaid`,
       `so`.`additional_fee`                               AS `additional_fee`,
       `so`.`discount_fee`                                 AS `discount_fee`,
       `so`.`pay_status`                                   AS `pay_status`,
       `so`.`refund_status`                                AS `refund_status`,
       `so`.`credit_rating_status`                         AS `credit_rating_status`,
       `so`.`profit_sharing_status`                        AS `profit_sharing_status`,
       `so`.`completed_time`                               AS `completed_time`,
       `so`.`address_id`                                   AS `address_id`,
       IF(AES_DECRYPT(FROM_BASE64(`so`.`contact`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `so`.`contact`,
          AES_DECRYPT(FROM_BASE64(`so`.`contact`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `contact`,
       `so`.`gender_type`                                  AS `gender_type`,
       IF(AES_DECRYPT(FROM_BASE64(`so`.`mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `so`.`mobile`,
          AES_DECRYPT(FROM_BASE64(`so`.`mobile`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `mobile`,
       `so`.`division_id`                                  AS `division_id`,
       `so`.`poi_name`                                     AS `poi_name`,
       IF(AES_DECRYPT(FROM_BASE64(`so`.`detail`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000) IS NULL, `so`.`detail`,
          AES_DECRYPT(FROM_BASE64(`so`.`detail`), 'sGmnd1yRu5akPdvg',
                      0x00000000000000000000000000000000)) AS `detail`,
       `so`.`longitude`                                    AS `longitude`,
       `so`.`latitude`                                     AS `latitude`,
       `so`.`deleted`                                      AS `deleted`,
       `so`.`created_time`                                 AS `created_time`,
       `so`.`updated_time`                                 AS `updated_time`
FROM `service_order` AS `so`;

-- 2023-11-08 增加审核管理
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('审核管理', 'adminAudit:null', '', '/audit', '', 1, 0, 'el-icon-s-cooperation', 71);
SET
    @auditPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('门店列表', 'adminAudit:shopList', '/audit/shop', 1, @auditPermissionParentId, 'el-icon-s-shop'),
       ('门店审核', 'adminAudit:shopAudit', '', 0, @auditPermissionParentId, '')
;

-- 门店审核记录
DROP TABLE IF EXISTS `housekeeping_shop_audit_record`;
CREATE TABLE `housekeeping_shop_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `shop_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '门店编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`),
    INDEX `idx_shopid` (`shop_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='企业门店审核记录';

-- 2023-11-10 增加家政员审核权限
SET
    @auditPermissionParentId = (SELECT `id`
                                FROM `sys_permission`
                                WHERE `resource` = 'adminAudit:null');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('家政员列表', 'adminAudit:staffList', '/audit/staff', 1, @auditPermissionParentId, 'el-icon-user'),
       ('家政员审核', 'adminAudit:staffAudit', '', 0, @auditPermissionParentId, '')
;
-- 门店审核记录
DROP TABLE IF EXISTS `housekeeping_staff_audit_record`;
CREATE TABLE `housekeeping_staff_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `staff_id`     BIGINT       NOT NULL DEFAULT 0 COMMENT '家政员编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`),
    INDEX `idx_staffid` (`staff_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='家政员审核记录';
-- 2023-11-13 家政企业增加地区编码索引
ALTER TABLE `housekeeping_company`
    ADD INDEX `idx_areacode` (`area_code`, `deleted`);

-- 2023-11-13 产品服务审核记录
DROP TABLE IF EXISTS `product_audit_record`;
CREATE TABLE `product_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `shop_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '门店编码',
    `product_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '产品编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`),
    INDEX `idx_shopid` (`shop_id`, `deleted`),
    INDEX `idx_productid` (`product_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='产品服务审核记录';

-- 增加产品服务审核权限
SET
    @auditPermissionParentId = (SELECT `id`
                                FROM `sys_permission`
                                WHERE `resource` = 'adminAudit:null');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('产品列表', 'adminAudit:productList', '/audit/product', 1, @auditPermissionParentId, 'el-icon-shopping-bag-1'),
       ('产品审核', 'adminAudit:productAudit', '', 0, @auditPermissionParentId, '');

-- 2023-11-15 评价审核
DROP TABLE IF EXISTS `credit_rating_audit_record`;
CREATE TABLE `credit_rating_audit_record`
(
    `id`               BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `credit_rating_id` BIGINT       NOT NULL DEFAULT 0 COMMENT '评价ID',
    `object_type`      TINYINT      NOT NULL DEFAULT 0 COMMENT '评价对象类型',
    `object_id`        BIGINT       NOT NULL DEFAULT 0 COMMENT '评价对象ID',
    `audit_status`     TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`          BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`        VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`           VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`          BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_objecttype_objectid` (`object_type`, `object_id`, `deleted`),
    INDEX `idx_creditratingid` (`credit_rating_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='评价审核记录';

-- 增加评价审核权限
SET
    @auditPermissionParentId = (SELECT `id`
                                FROM `sys_permission`
                                WHERE `resource` = 'adminAudit:null');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('评价列表', 'adminAudit:creditList', '/audit/credit', 1, @auditPermissionParentId, 'el-icon-tickets'),
       ('评价审核', 'adminAudit:creditAudit', '', 0, @auditPermissionParentId, '');

-- 评价状态
ALTER TABLE `credit_rating`
    ADD COLUMN `status` TINYINT NOT NULL DEFAULT 0 COMMENT '评价状态' AFTER `deleted`;

-- 敏感词管理
DROP TABLE IF EXISTS `sensitive_word`;
CREATE TABLE `sensitive_word`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `word`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '',
    `deleted`      BIGINT       NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    index `idx_word` (`word`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='敏感词管理';

-- 敏感词管理权限
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('敏感词管理', 'adminSensitive:null', '', '/sensitive', '', 1, 0, 'el-icon-turn-off-microphone', 72);
SET
    @sensitivePermissionParentId = LAST_INSERT_ID();

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('敏感词列表', 'adminSensitive:list', '/sensitive/list', 1, @sensitivePermissionParentId,
        'el-icon-turn-off-microphone'),
       ('新增敏感词', 'adminSensitive:create', '', 0, @sensitivePermissionParentId, ''),
       ('修改敏感词', 'adminSensitive:update', '', 0, @sensitivePermissionParentId, ''),
       ('删除敏感词', 'adminSensitive:delete', '', 0, @sensitivePermissionParentId, '');
