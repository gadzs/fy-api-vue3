package cn.turboinfo.fuyang.api.provider.common.repository.database.sensitive;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.List;

/**
 * @author hai
 */
public interface SensitiveWordDAO extends BaseRepository<SensitiveWordPO, Long> {

    List<SensitiveWordPO> findAllByOrderById();

}
