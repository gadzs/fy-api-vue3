package cn.turboinfo.fuyang.api.provider.common.repository.database.profit.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingFailReason;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ProfitSharingFailReasonConverter extends BaseEnumConverter<ProfitSharingFailReason> {
}
