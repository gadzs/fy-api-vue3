package cn.turboinfo.fuyang.api.entity.common.pojo.policy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 策略条目
 * author: sunshow.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PolicyItem {

    /**
     * 策略ID
     */
    private Long policyId;

    /**
     * 策略选择结果
     */
    private String result;

}
