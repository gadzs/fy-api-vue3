package cn.turboinfo.fuyang.api.entity.common.enumeration.product;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 门店筛选方式
 */
@Slf4j
public enum ProductFilterType implements BaseEnum {

    DEFAULT(0, "不限"),

    RECENTLY(10, "优质新店"),

    EARLY_BRANDED(20, "品牌老店"),

    ;

    private int value;
    private String name;

    ProductFilterType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProductFilterType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ProductFilterType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ProductFilterType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
