package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class UserCredentialException extends RuntimeException {
    public UserCredentialException() {
        this("操作 UserCredential 出错");
    }

    public UserCredentialException(String message) {
        super(message);
    }

    public UserCredentialException(Throwable cause) {
        super(cause);
    }

    public UserCredentialException(String message, Throwable cause) {
        super(message, cause);
    }
}
