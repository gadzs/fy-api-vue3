package cn.turboinfo.fuyang.api.domain.common.service.file;

import cn.turboinfo.fuyang.api.entity.common.exception.file.FileAttachmentException;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachmentCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachmentUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface FileAttachmentService {
    Optional<FileAttachment> getById(Long id);

    FileAttachment getByIdEnsure(Long id);

    List<FileAttachment> findByIdCollection(Collection<Long> idCollection);

    FileAttachment save(FileAttachmentCreator creator) throws FileAttachmentException;

    FileAttachment update(FileAttachmentUpdater updater) throws FileAttachmentException;

    QResponse<FileAttachment> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws FileAttachmentException;

    void deleteByIdCollection(Collection<Long> idCollection) throws FileAttachmentException;

    List<FileAttachment> findByRefId(Long refId);

    List<FileAttachment> findByRefAndRefType(Long refId, Collection<String> refTypeCollection);

    List<FileAttachment> findByRefIdAndRefType(Long refId, String refType);

    List<FileAttachment> findByRefIdInAndRefType(Collection<Long> refIdCollection, String refType);

    List<FileAttachment> findByRefIdCollection(Collection<Long> refIdCollection);

    Path resolveAbsolutePath(String relativePath);

    /**
     * 更新引用业务编码
     *
     * @param id    主键编码
     * @param refId 业务编码
     * @return
     * @throws FileAttachmentException
     */
    FileAttachment updateRefId(Long id, Long refId) throws FileAttachmentException;

    /**
     * 更新引用业务编码
     *
     * @param ids   主键编码
     * @param refId 业务编码
     * @return
     * @throws FileAttachmentException
     */
    void updateRefId(List<Long> ids, Long refId) throws FileAttachmentException;

    /**
     * 拼装对外访问地址
     *
     * @param fileAttachment 附件对象
     * @return 附件对象
     */
    FileAttachment assembleExternalUrl(FileAttachment fileAttachment);
}
