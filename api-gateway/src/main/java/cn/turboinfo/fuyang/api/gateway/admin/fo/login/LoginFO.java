package cn.turboinfo.fuyang.api.gateway.admin.fo.login;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginFO {

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

    private String captcha;

    private boolean forceLogin;
}
