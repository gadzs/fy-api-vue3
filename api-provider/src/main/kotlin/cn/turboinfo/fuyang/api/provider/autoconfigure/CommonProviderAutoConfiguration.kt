package cn.turboinfo.fuyang.api.provider.autoconfigure

import cn.turboinfo.fuyang.api.provider.common.framework.jpa.interceptor.CommonJpaSessionFactoryInterceptor
import nxcloud.foundation.core.data.jpa.interceptor.EmptyJpaSessionFactoryInterceptor
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(value = ["cn.turboinfo.fuyang.api.provider.common", "cn.turboinfo.fuyang.api.domain.common"])
class CommonProviderAutoConfiguration {

    @Bean
    fun commonJpaSessionFactoryInterceptor(): EmptyJpaSessionFactoryInterceptor {
        return CommonJpaSessionFactoryInterceptor()
    }

}