package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;

import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyListUseCase extends AbstractUseCase<HousekeepingCompanyListUseCase.InputData, HousekeepingCompanyListUseCase.OutputData> {
    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        List<HousekeepingCompany> housekeepingCompanyList = housekeepingCompanyService.findByName(inputData.getName());
        List<HousekeepingListCompany> companyList = housekeepingCompanyList.stream().map(housekeepingCompany -> {
            return HousekeepingListCompany.builder().id(housekeepingCompany.getId()).name(housekeepingCompany.getName()).build();
        }).collect(Collectors.toList());
        return OutputData.builder()
                .companyList(companyList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private String name;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<HousekeepingListCompany> companyList;
    }

    @Data
    @Builder
    public static class HousekeepingListCompany {

        private Long id;
        private String name;
    }
}
