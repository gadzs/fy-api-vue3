package cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DictConfigItemCreateFO {

    @NotBlank(
            message = "条目值不能为空"
    )
    private String itemValue;

    @NotBlank(
            message = "条目名称不能为空"
    )
    private String itemName;

    private String description;

    private Integer itemOrder;
}
