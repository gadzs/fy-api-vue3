CREATE TABLE `credit_rating`
(
    `id`           BIGINT(1)     NOT NULL AUTO_INCREMENT,
    `object_type`  INT(1)        NOT NULL DEFAULT 0,
    `object_id`    BIGINT(1)     NOT NULL DEFAULT 0,
    `appraiser_id` BIGINT(1)     NOT NULL DEFAULT 0,
    `score`        BIGINT(1)     NOT NULL DEFAULT 0,
    `comment`      VARCHAR(2048) NOT NULL DEFAULT '',
    `img_id_list`  JSON          NULL     DEFAULT NULL,
    `deleted`      BIGINT(1)     NOT NULL DEFAULT 0,
    `status`       TINYINT       NOT NULL DEFAULT 0 COMMENT '评价状态',
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_objectid_objecttype_deleted_createdtime` (`object_id`, `object_type`, `deleted`, `created_time`),
    KEY `idx_appraiserid_deleted_createdtime` (`appraiser_id`, `deleted`, `created_time`),
    KEY `idx_objecttype_objectid` (`object_type`, `object_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
