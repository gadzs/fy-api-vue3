package cn.turboinfo.fuyang.api.domain.admin.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class CategoryCheckAvailableUseCase extends AbstractUseCase<CategoryCheckAvailableUseCase.InputData, CategoryCheckAvailableUseCase.OutputData> {
    private final CategoryService categoryService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long categoryId = inputData.getCategoryId();

        Long parentId = inputData.getParentId();

        String name = inputData.getName();

        boolean available;

        if (categoryId != null && categoryId != 0) {
            available = categoryService.checkAvailable(categoryId, parentId, name);
        } else {
            available = categoryService.checkAvailable(parentId, name);
        }

        return OutputData.builder()
                .available(available)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        private Long categoryId;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private boolean available;
    }
}
