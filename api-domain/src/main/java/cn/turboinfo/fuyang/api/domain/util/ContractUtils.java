package cn.turboinfo.fuyang.api.domain.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * author: sunshow.
 */
@Slf4j
public class ContractUtils {

    /**
     * 合同编码
     */
    public static String generateContractCode(Long companyId, Long categoryId, Long orderId) {
        LocalDate now = LocalDate.now();

        String companyCode = StringUtils.leftPad(companyId > 100000L ? String.valueOf(companyId / 100000L) : companyId.toString(), 4, "0");
        String categoryCode = StringUtils.leftPad(categoryId.toString(), 3, "0");
        String value = StringUtils.leftPad(orderId > 1000L ? String.valueOf(companyId / 1000L) : orderId.toString(), 4, "0");
        return String.format("FY%s-%s-%s", companyCode, now.format(DateTimeFormatter.ofPattern("yyyyMMdd")), categoryCode, value);
    }

}
