package cn.turboinfo.fuyang.api.provider.common.service.impl.division;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.provider.common.repository.database.division.DivisionDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.division.DivisionPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class DivisionServiceImpl extends DefaultQServiceImpl<Division, Long, DivisionPO, DivisionDAO> implements DivisionService {

    private final DivisionDAO divisionDAO;

    @Override
    public List<Division> findByParentId(Long parentId) {
        return convertStreamQBeanToList(divisionDAO.findByParentIdOrderByAreaCode(parentId).stream());
    }

    @Override
    public List<Division> findAll() {
        return convertStreamQBeanToList(divisionDAO.findByOrderByAreaCode().stream());
    }

}
