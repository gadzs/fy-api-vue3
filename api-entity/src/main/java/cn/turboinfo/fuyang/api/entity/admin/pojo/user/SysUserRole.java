package cn.turboinfo.fuyang.api.entity.admin.pojo.user;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class SysUserRole extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    private Long sysUserId;

    private Long roleId;

    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
