package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractTmplService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmplUpdater;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractTmplUpdateUseCase extends AbstractUseCase<ContractTmplUpdateUseCase.InputData, ContractTmplUpdateUseCase.OutputData> {
    private final ContractTmplService contractTmplService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<Long> fileIdList = inputData.getFileIds();

        ContractTmplUpdater.Builder builder = ContractTmplUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, ContractTmplUpdater.class, inputData);

        ContractTmpl contractTmpl = contractTmplService.update(builder.build());

        List<FileAttachment> fileAttachmentList = fileAttachmentService.findByRefIdAndRefType(contractTmpl.getId(), FileRefTypeConstant.CONTRACT_TMPL_FILE);

        Map<Long, FileAttachment> fileAttachmentMap = fileAttachmentList
                .stream()
                .collect(Collectors.toMap(FileAttachment::getId, fileAttachment -> fileAttachment));

        // 删除需要删除的附件
        for (FileAttachment fileAttachment : fileAttachmentList) {
            if (!fileIdList.contains(fileAttachment.getId())) {
                fileAttachmentService.deleteById(fileAttachment.getId());
            }
        }
        Set<Long> fileIdSet = new HashSet<>(fileIdList);

        // 更新需要更新的附件
        for (Long imageId : fileIdList) {
            if (fileAttachmentMap.containsKey(imageId)) {
                fileIdSet.remove(imageId);
            }
        }

        if (fileIdSet.size() > 0) {
            fileAttachmentService.updateRefId(fileIdSet.stream().toList(), contractTmpl.getId());
        }

        return OutputData.builder()
                .contractTmpl(contractTmpl)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "合同名称不能为空"
        )
        private String name;

        @NotNull(
                message = "合同类别不能为空"
        )
        private Long categoryId;

        @NotNull(
                message = "企业编码不能为空"
        )
        private Long companyId;

        private List<Long> fileIds;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private ContractTmpl contractTmpl;
    }
}
