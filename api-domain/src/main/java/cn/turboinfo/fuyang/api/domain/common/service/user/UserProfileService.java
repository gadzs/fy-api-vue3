package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfile;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfileCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

public interface UserProfileService extends BaseQService<UserProfile, Long> {

    UserProfile save(UserProfileCreator creator);


}
