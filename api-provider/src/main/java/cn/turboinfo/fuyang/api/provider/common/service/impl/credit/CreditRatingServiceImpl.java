package cn.turboinfo.fuyang.api.provider.common.service.impl.credit;

import cn.turboinfo.fuyang.api.domain.common.service.credit.CreditRatingService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.provider.common.repository.database.credit.CreditRatingDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.credit.CreditRatingPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class CreditRatingServiceImpl extends DefaultQServiceImpl<CreditRating, Long, CreditRatingPO, CreditRatingDAO> implements CreditRatingService {

    @Override
    public List<CreditRating> findByObjectIdCollection(EntityObjectType objectType, Collection<Long> objectIdCollection) {
        return convertQBeanToList(dao.findByObjectTypeAndObjectIdInOrderByObjectId(objectType, objectIdCollection));
    }

    @Override
    @Transactional
    public void updateStatus(Long id, CreditRatingStatus status) {
        CreditRatingPO po = getEntityWithNullCheckForUpdate(id);
        po.setStatus(status);
    }

}
