package cn.turboinfo.fuyang.api.entity.common.pojo.audit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 产品服务审核记录
 *
 * @author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ProductAuditRecord extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 企业编码
     */
    private Long companyId;

    /**
     * 门店编码
     */
    private Long shopId;

    /**
     * 产品编码
     */
    private Long productId;

    /**
     * 审核状态
     */
    private AuditStatus auditStatus;

    /**
     * 审核人
     */
    private Long userId;

    /**
     * 审核人名称
     */
    private String userName;

    /**
     * 备注
     */
    private String remark;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
