package cn.turboinfo.fuyang.api.entity.common.exception.dictconfig;

public class DictConfigItemException extends RuntimeException {
    public DictConfigItemException() {
        this("操作 DictConfigItem 出错");
    }

    public DictConfigItemException(String message) {
        super(message);
    }

    public DictConfigItemException(Throwable cause) {
        super(cause);
    }

    public DictConfigItemException(String message, Throwable cause) {
        super(message, cause);
    }
}
