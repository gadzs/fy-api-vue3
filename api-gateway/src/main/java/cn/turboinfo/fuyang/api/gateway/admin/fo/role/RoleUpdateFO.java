package cn.turboinfo.fuyang.api.gateway.admin.fo.role;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoleUpdateFO {
    @NotNull(message = "ID不能为空")
    private Long id;

    @NotBlank(message = "角色代码不能为空")
    private String code;

    @NotBlank(message = "角色名称不能为空")
    private String name;

    private String description;
}
