package cn.turboinfo.fuyang.api.gateway.mini.framework.http.context;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public final class MiniRequestContextHolder {

    private final static ThreadLocal<MiniRequestContext> contextThreadLocal = new ThreadLocal<>();

    public static MiniRequestContext current() {
        MiniRequestContext context = contextThreadLocal.get();
        if (context == null) {
            context = new MiniRequestContext();
            contextThreadLocal.set(context);
        }
        return context;
    }

    public static boolean exists() {
        return contextThreadLocal.get() != null;
    }

    public static void reset() {
        contextThreadLocal.remove();
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
    }

}
