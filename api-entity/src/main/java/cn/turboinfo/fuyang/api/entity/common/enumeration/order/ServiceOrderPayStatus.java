package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 服务订单的支付状态
 */
@Slf4j
public enum ServiceOrderPayStatus implements BaseEnum {

    UNPAID(0, "未支付"),

    PRE_PAID(10, "全额预付费"),

    PART_PRE_PAID(20, "部分预付费"),

    POST_PAID(30, "后付费"),

    PENDING_PAY(40, "待支付"),

    PAID(50, "已支付"),

    TIMEOUT(60, "支付超时"),

    ;

    private final int value;
    private final String name;

    ServiceOrderPayStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static ServiceOrderPayStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ServiceOrderPayStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ServiceOrderPayStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}
