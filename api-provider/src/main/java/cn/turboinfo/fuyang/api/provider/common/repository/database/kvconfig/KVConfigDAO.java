package cn.turboinfo.fuyang.api.provider.common.repository.database.kvconfig;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface KVConfigDAO extends BaseRepository<KVConfigPO, Long> {

    Optional<KVConfigPO> findByConfigKey(String configKey);

    List<KVConfigPO> findByConfigKeyInOrderByIdDesc(Collection<String> configKeyCollection);

    List<KVConfigPO> findByConfigGroupOrderByGroupOrderAscIdAsc(String configGroup);

}
