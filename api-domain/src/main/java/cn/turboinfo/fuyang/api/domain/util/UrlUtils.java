package cn.turboinfo.fuyang.api.domain.util;

import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

public class UrlUtils {

    public static String encodeURIComponent(String url) {
        return URLEncoder.encode(url, StandardCharsets.UTF_8);
    }

    /**
     * 判断是否是超链接
     *
     * @param url
     * @return
     */
    public static boolean isHyperlink(String url) {
        return StringUtils.startsWithIgnoreCase(url, "http://")
                || StringUtils.startsWithIgnoreCase(url, "https://");
    }

    /**
     * 合并网址
     * 为每个碎片按需自动添加斜杠
     * 合并后末尾不包含斜杠
     *
     * @param baseUrl  基础网址
     * @param segments 网址碎片
     * @return 拼接后的网址
     */
    public static String combineUrl(String baseUrl, String... segments) {
        StringBuilder url = new StringBuilder(baseUrl);

        if (baseUrl.endsWith("/")) {
            url.deleteCharAt(url.length() - 1);
        }

        if (segments != null) {
            for (String segment : segments) {
                if (!segment.startsWith("/")) {
                    url.append("/");
                }
                url.append(segment);
                if (segment.endsWith("/")) {
                    url.deleteCharAt(url.length() - 1);
                }
            }
        }

        return url.toString();
    }

    /**
     * 合并网址和请求串
     *
     * @param baseUrl      基础网址
     * @param encoded      是否已编码
     * @param queryStrings 形如 x=y 的请求参数串
     * @return 拼接后的网址
     */
    public static String combineUrlQueryStrings(String baseUrl, boolean encoded, String... queryStrings) {
        StringBuilder url = new StringBuilder(baseUrl);

        if (baseUrl.contains("?")) {
            url.append("&");
        } else {
            url.append("?");
        }

        if (encoded) {
            url.append(StringUtils.join(queryStrings, "&"));
        } else {
            url.append(StringUtils.join(Arrays.stream(queryStrings)
                    .map(s -> URLEncoder.encode(s, StandardCharsets.UTF_8))
                    .collect(Collectors.toList()), "&")
            );
        }

        return url.toString();
    }
}
