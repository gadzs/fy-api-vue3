package cn.turboinfo.fuyang.api.provider.common.repository.database.profit.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingResult;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ProfitSharingResultConverter extends BaseEnumConverter<ProfitSharingResult> {
}
