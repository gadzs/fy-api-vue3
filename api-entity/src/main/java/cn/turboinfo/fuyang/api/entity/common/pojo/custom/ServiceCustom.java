package cn.turboinfo.fuyang.api.entity.common.pojo.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 定制服务
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ServiceCustom extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户编码
     */
    private Long userId;

    /**
     * 服务类型
     */
    private Long categoryId;

    /**
     * 公司ID
     */
    private Long companyId;
    private String companyName;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 服务人员ID
     */
    private Long staffId;
    private String staffName;

    /**
     * 预算
     */
    private BigDecimal budget;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 预付金额
     */
    private BigDecimal prepaid;

    /**
     * 定金
     */
    private BigDecimal deposit;

    /**
     * 额外费用 (由服务人员确认完成时添加)
     */
    private BigDecimal additionalFee;

    /**
     * 减免费用 (由服务人员确认完成时添加)
     */
    private BigDecimal discountFee;

    /**
     * 选择的地址ID
     */
    private Long addressId;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 手机号加密
     */
    private String mobileEncrypt;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 人数
     */
    private Integer peopleNum;

    /**
     * 描述
     */
    private String description;

    /**
     * 计划开始时间
     */
    private LocalDateTime scheduledStartTime;

    /**
     * 计划结束时间
     */
    private LocalDateTime scheduledEndTime;

    /**
     * 服务开始时间
     */
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    private LocalDateTime serviceEndTime;

    /**
     * 服务订单状态
     */
    private ServiceCustomStatus customStatus;

    /**
     * 支付订单状态
     */
    private ServiceOrderPayStatus payStatus;

    /**
     * 退款订单状态
     */
    private ServiceOrderRefundStatus refundStatus;

    /**
     * 评价状态
     */
    private ServiceOrderCreditRatingStatus creditRatingStatus;

    /**
     * 分账状态
     */
    private ProfitSharingStatus profitSharingStatus;

    /**
     * 服务完成时间
     */
    private LocalDateTime completedTime;

    /**
     * 服务订单状态历史
     */
    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private List<ServiceCustomStatusLog> customStatusHistory;


    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

    public List<ServiceCustomStatusLog> getCustomStatusHistory() {
        return customStatusHistory == null ? new ArrayList<>() : customStatusHistory;
    }

}
