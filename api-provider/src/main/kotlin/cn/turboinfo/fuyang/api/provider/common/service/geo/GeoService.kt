package cn.turboinfo.fuyang.api.provider.common.service.geo

interface GeoService {

    /**
     * @return 0: 更新成功, 1: 插入成功
     */
    fun add(store: String, member: String, longitude: Double, latitude: Double): Long

    /**
     * @return 0: 不存在, 1: 删除成功
     */
    fun remove(store: String, member: String): Long

    /**
     * 获取指定成员坐标
     */
    fun get(store: String, vararg members: String): List<GeoPosition?>

    /**
     * 搜索指定半径内的成员并返回距离
     */
    fun searchRadius(
        store: String,
        longitude: Double,
        latitude: Double,
        radiusInKilometers: Double,
        limit: Long? = null,
        desc: Boolean = false,
    ): List<GeoDistance>

}

data class GeoPosition(
    val longitude: Double,
    val latitude: Double,
)

data class GeoDistance(
    val member: String,
    val distance: Double,
)