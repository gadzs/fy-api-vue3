package cn.turboinfo.fuyang.api.provider.admin.framework.shiro.helper;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.concurrent.TimeUnit;

public class PasswordHelper {

    private final String algorithmName;

    private final int hashIterations;

    private final int retryLimit;

    private final long retryPeriod;

    private final TimeUnit retryTimeUnit;

    public PasswordHelper(String algorithmName, int hashIterations) {
        this(algorithmName, hashIterations, 0, 0, null);
    }

    public PasswordHelper(String algorithmName, int hashIterations, int retryLimit, long retryPeriod, TimeUnit retryTimeUnit) {
        this.algorithmName = algorithmName;
        this.hashIterations = hashIterations;
        this.retryLimit = retryLimit;
        this.retryPeriod = retryPeriod;
        this.retryTimeUnit = retryTimeUnit;
    }

    public String getSalt() {
        return new SecureRandomNumberGenerator().nextBytes().toHex();
    }

    public String getCredential(String password, String salt) {
        return new SimpleHash(algorithmName, password, salt, hashIterations).toHex();
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public int getHashIterations() {
        return hashIterations;
    }

    public int getRetryLimit() {
        return retryLimit;
    }

    public long getRetryPeriod() {
        return retryPeriod;
    }

    public TimeUnit getRetryTimeUnit() {
        return retryTimeUnit;
    }
}
