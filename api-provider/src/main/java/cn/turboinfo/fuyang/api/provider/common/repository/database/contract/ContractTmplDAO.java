package cn.turboinfo.fuyang.api.provider.common.repository.database.contract;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;

public interface ContractTmplDAO extends FYRepository<ContractTmplPO, Long> {

    List<ContractTmplPO> findByCompanyId(Long companyId);

    List<ContractTmplPO> findByCompanyIdIn(Collection<Long> companyIds);

}
