package cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class DictConfigItemSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "字典ID"
    )
    private Long dictConfigId;

    @QField(
            placeholder = "字典Key",
            operator = Operator.LIKE
    )
    private String dictConfigKey;

    @QField(
            placeholder = "条目值",
            operator = Operator.LIKE
    )
    private String itemValue;

    @QField(
            placeholder = "条目名称",
            operator = Operator.LIKE
    )
    private String itemName;

    @QField(
            placeholder = "条目描述",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            placeholder = "条目顺序"
    )
    private Integer itemOrder;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
