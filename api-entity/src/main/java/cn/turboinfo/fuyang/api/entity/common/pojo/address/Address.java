package cn.turboinfo.fuyang.api.entity.common.pojo.address;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 地址
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Address extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 所属用户 ID
     */
    @QBeanUpdaterIgnore
    private Long userId;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
