package cn.turboinfo.fuyang.api.provider.common.repository.database.order.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ServiceOrderStatusConverter extends BaseEnumConverter<ServiceOrderStatus> {
}
