package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 分类
 */
@Table(
        name = "cms_category"
)
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
@Getter
@Setter
public class CmsCategoryPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 栏目名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 栏目描述
     */
    private String description;

    /**
     * 栏目类型
     */
    private CmsCategoryType type;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 是否首页展示
     */
    @Column(name = "index_show")
    private YesNoStatus indexShow;

    /**
     * 英文简写路径
     */
    @Column(name = "slug_name")
    private String slugName;

    /**
     * 链接
     */
    @Column(name = "linked_url")
    private String linkedUrl;

    /**
     * 父级分类ID
     */
    @Column(name = "parent_id")
    private Long parentId;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
