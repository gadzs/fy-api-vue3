package cn.turboinfo.fuyang.api.entity.common.enumeration.account;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public enum AccountStatus implements BaseEnum {

    INIT(0, "初始化"),

    EXPORTED(10, "已导出"),

    AUTHORIZED(90, "已授权"),

    ;

    private int value;
    private String name;

    AccountStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static AccountStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static AccountStatus getByName(String name) {
        return BaseEnumHelper.getByName(name, values());
    }

    public static List<AccountStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<AccountStatus> listWithoutDefault() {
        return BaseEnumHelper.getList(values()).stream().filter(v -> v != INIT).collect(Collectors.toList());
    }

    public static List<AccountStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
