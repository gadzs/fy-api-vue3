package cn.turboinfo.fuyang.api.gateway.admin.contract.credit

import cn.turboinfo.fuyang.api.domain.admin.usecase.credit.CreditRatingViewUseCase
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 */
@AutoMappingContract(paths = ["/admin/credit"])
@ResponseBodyWrapper
@SessionScope
interface CreditRatingContract {

    /**
     * 家政员详情
     */
    @PermissionScope(value = ["adminAudit:creditList"])
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = CreditRatingViewUseCase::class,
        consumes = [],
        paths = ["/view/{id}"]
    )
    fun view()
}
