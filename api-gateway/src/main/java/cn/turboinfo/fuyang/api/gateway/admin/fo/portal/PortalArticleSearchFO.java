package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * 文章查询FO
 */
@Data
@QField
public class PortalArticleSearchFO {

    /**
     * 栏目id
     */
    @QField
    private Long categoryId;

    @QField(searchable = false)
    private String categoryCodes;

    /**
     * 文章id
     */
    @QField
    private Long articleId;

    @QField(
            placeholder = "标题",
            operator = Operator.LIKE
    )
    private String title;

    /**
     * 文章类型
     */
    @QField
    private CmsArticleType articleType;
}
