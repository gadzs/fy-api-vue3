package cn.turboinfo.fuyang.api.gateway.admin.contract.product

import cn.turboinfo.fuyang.api.domain.admin.usecase.product.*
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 企业门店产品自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/product"])
@ResponseBodyWrapper
interface ProductContract {
    

    // 家政门店产品添加
    @CompanyScope
    @PermissionScope(value = ["product:create"])
    @AutoMappingContract(
        beanType = ProductCreateUseCase::class
    )
    fun create()

    // 家政门店产品信息修改
    @CompanyScope
    @PermissionScope(value = ["product:update"])
    @AutoMappingContract(
        beanType = ProductUpdateUseCase::class
    )
    fun update()

    /**
     * 产品删除
     */
    @CompanyScope
    @PermissionScope(value = ["product:delete"])
    @AutoMappingContract(
        beanType = ProductDeleteUseCase::class
    )
    fun delete()

    /**
     * 产品发布/下架
     */
    @CompanyScope
    @PermissionScope(value = ["product:create", "product:update"])
    @AutoMappingContract(
        beanType = ProductReviewUseCase::class
    )
    fun review()

    /**
     * 产品sku发布/下架
     */
    @CompanyScope
    @PermissionScope(value = ["product:create", "product:update"])
    @AutoMappingContract(
        beanType = ProductSkuReviewUseCase::class
    )
    fun reviewSku()
}