package cn.turboinfo.fuyang.api.provider.mini.service.impl.cms;

import cn.turboinfo.fuyang.api.domain.mini.service.cms.MiniCmsArticleService;
import cn.turboinfo.fuyang.api.entity.admin.exception.cms.CmsArticleException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;
import cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.CmsArticleDAO;
import cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.CmsCategoryDAO;
import cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.CmsCategoryPO;
import net.sunshow.toolkit.core.qbean.helper.service.impl.AbstractQServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

@Service
public class MiniCmsCmsArticleServiceImpl extends AbstractQServiceImpl<CmsArticle> implements MiniCmsArticleService {

    @Autowired
    private CmsArticleDAO cmsArticleDAO;

    @Autowired
    private CmsCategoryDAO categoryDAO;

    @Override
    public List<CmsArticle> findHomeArticle() {

        CmsCategoryPO miniloop = categoryDAO.findAllByCode("MINILOOP")
                .stream()
                .filter(it -> it.getParentId().equals(0L))
                .toList()
                .stream()
                .findFirst()
                .orElse(null);

        if (miniloop == null) {
            return new ArrayList<>();
        }

        return convertStreamQBeanToList(cmsArticleDAO.findAllByCategoryId(miniloop.getId()).stream());
    }

    @Override
    protected Supplier<? extends RuntimeException> getExceptionSupplier(String message,
                                                                        Throwable cause) {
        return () -> new CmsArticleException(message, cause);
    }

}
