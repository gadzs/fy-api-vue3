package cn.turboinfo.fuyang.api.gateway.share.contract.company

import cn.turboinfo.fuyang.api.domain.share.usecase.company.ShareViewCompanyUseCase
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@ShareResponseBodyWrapper
@AutoMappingContract(paths = ["/share/company"])
interface ShareCompanyContract {

    // 公司详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = ShareViewCompanyUseCase::class,
        consumes = [],
    )
    fun view()

}
