package cn.turboinfo.fuyang.api.domain.admin.usecase.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class AgenciesDeleteUseCase extends AbstractUseCase<AgenciesDeleteUseCase.InputData, AgenciesDeleteUseCase.OutputData> {
    private final AgenciesService agenciesService;

    @Override
    protected OutputData doAction(InputData inputData) {

        agenciesService.deleteById(inputData.getId());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
