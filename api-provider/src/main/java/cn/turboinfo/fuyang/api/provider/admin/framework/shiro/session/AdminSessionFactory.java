package cn.turboinfo.fuyang.api.provider.admin.framework.shiro.session;

import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.user.CommonListUserTypeUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class AdminSessionFactory {

    protected final SysUserService sysUserService;

    protected final CommonListUserTypeUseCase listUserTypeUseCase;

    public AdminSession createAdminSession(Long sysUserId) {
        AdminSession adminSession = new AdminSession();

        refreshAdminSessionBySysUser(adminSession, sysUserId);

        return adminSession;
    }

    public void refreshAdminSessionBySysUser(AdminSession adminSession, Long sysUserId) {
        SysUser sysUser = sysUserService.getByIdEnsure(sysUserId);

        adminSession.setSysUserId(sysUser.getId());
        adminSession.setUsername(sysUser.getUsername());
        adminSession.setMobile(sysUser.getMobile());
        adminSession.setLoginOperation(sysUser.getLoginOperation());

        // 列表用户类型供后续使用
        List<UserTypeRel> userTypeRelList = listUserTypeUseCase.execute(CommonListUserTypeUseCase.InputData
                .builder()
                .userId(sysUserId)
                .build()).getUserTypeRelList();
        adminSession.setUserTypeRelList(userTypeRelList);
    }
}
