package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuthLabelService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.exception.company.HousekeepingCompanyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabelUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政企业修改usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAuthLabelUpdateUseCase extends AbstractUseCase<HousekeepingCompanyAuthLabelUpdateUseCase.InputData, HousekeepingCompanyAuthLabelUpdateUseCase.OutputData> {
    private final HousekeepingCompanyAuthLabelService housekeepingCompanyAuthLabelService;
    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Optional<HousekeepingCompanyAuthLabel> housekeepingCompanyAuthLabelOptional1 = housekeepingCompanyAuthLabelService.findByName(inputData.getName());
        if (housekeepingCompanyAuthLabelOptional1.isPresent() && !housekeepingCompanyAuthLabelOptional1.get().getId().equals(inputData.getId())) {
            throw new HousekeepingCompanyException("名称已存在");
        }
        if (inputData.getIconId() != null && inputData.getIconId().intValue() > 0) {
            fileAttachmentService.updateRefId(inputData.getIconId(), inputData.getId());
        } else {
            inputData.setIconId(0L);
        }
        HousekeepingCompanyAuthLabelUpdater.Builder builder = HousekeepingCompanyAuthLabelUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingCompanyAuthLabelUpdater.class, inputData);
        HousekeepingCompanyAuthLabel housekeepingCompanyAuthLabel = housekeepingCompanyAuthLabelService.update(builder.build());
        return OutputData.builder()
                .housekeepingCompanyAuthLabel(housekeepingCompanyAuthLabel)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "名称"
        )
        private String name;

        private Long iconId;

        @NotNull(
                message = "权重不能为空"
        )
        private Integer weight;

        private String description;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingCompanyAuthLabel housekeepingCompanyAuthLabel;
    }

}
