package cn.turboinfo.fuyang.api.entity.common.pojo.spec;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 规格组关联关系
 * author: gadzs.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class SpecSetRel extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 组合 ID
     */
    private Long specSetId;

    /**
     * 关联对象类型
     */
    private EntityObjectType objectType;

    /**
     * 关联对象ID
     */
    private Long objectId;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
