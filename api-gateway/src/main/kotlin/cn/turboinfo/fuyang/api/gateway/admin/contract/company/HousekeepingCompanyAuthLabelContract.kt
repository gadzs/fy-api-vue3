package cn.turboinfo.fuyang.api.gateway.admin.contract.company

import cn.turboinfo.fuyang.api.domain.admin.usecase.company.HousekeepingCompanyAuthLabelCreateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.company.HousekeepingCompanyAuthLabelUpdateUseCase
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 家政企业认证标签自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/company/authLabel"])
@ResponseBodyWrapper
interface HousekeepingCompanyAuthLabelContract {

    // 信息修改
    @PermissionScope(value = ["company:authLabel:update"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyAuthLabelUpdateUseCase::class
    )
    fun update()

    // 信息添加
    @PermissionScope(value = ["company:authLabel:create"])
    @AutoMappingContract(
        beanType = HousekeepingCompanyAuthLabelCreateUseCase::class
    )
    fun create()
}
