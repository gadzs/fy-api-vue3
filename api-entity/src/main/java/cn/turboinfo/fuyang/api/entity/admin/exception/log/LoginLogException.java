package cn.turboinfo.fuyang.api.entity.admin.exception.log;

public class LoginLogException extends RuntimeException {
    public LoginLogException() {
        this("操作 LoginLog 出错");
    }

    public LoginLogException(String message) {
        super(message);
    }

    public LoginLogException(Throwable cause) {
        super(cause);
    }

    public LoginLogException(String message, Throwable cause) {
        super(message, cause);
    }
}
