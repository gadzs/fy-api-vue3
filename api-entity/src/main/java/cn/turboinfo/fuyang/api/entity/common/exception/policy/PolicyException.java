package cn.turboinfo.fuyang.api.entity.common.exception.policy;

public class PolicyException extends RuntimeException {
    public PolicyException() {
        this("操作策略出错");
    }

    public PolicyException(String message) {
        super(message);
    }

    public PolicyException(Throwable cause) {
        super(cause);
    }

    public PolicyException(String message, Throwable cause) {
        super(message, cause);
    }
}
