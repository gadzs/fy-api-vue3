package cn.turboinfo.fuyang.api.entity.common.pojo.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.RuleControlType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 规则组
 * author: sunshow.
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class RuleGroup extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 规则组名称
     */
    private String name;

    /**
     * 规则组描述
     */
    private String description;

    /**
     * 规则编码
     */
    private String ruleKey;

    /**
     * 规则控制类型
     */
    private RuleControlType controlType;

    private EnableStatus status;

    /**
     * 规则明细项
     */
    private List<RuleItem> itemList;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
