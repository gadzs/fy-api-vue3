package cn.turboinfo.fuyang.api.provider.common.repository.database.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;
import java.util.Optional;

public interface ActivityApplyDAO extends FYRepository<ActivityApplyPO, Long> {

    Optional<ActivityApplyPO> findByActivityIdAndUserIdAndUserType(Long activityId, Long userId, UserType userType);

    List<ActivityApplyPO> findByActivityIdOrderByCreatedTimeDesc(Long activityId);
}
