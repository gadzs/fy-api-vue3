package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class UserLoginException extends RuntimeException {
    public UserLoginException() {
        this("操作 UserLogin 出错");
    }

    public UserLoginException(String message) {
        super(message);
    }

    public UserLoginException(Throwable cause) {
        super(cause);
    }

    public UserLoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
