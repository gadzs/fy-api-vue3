package cn.turboinfo.fuyang.api.scheduler

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * author: sunshow.
 */

@SpringBootApplication
class FuyangApiSchedulerApplication : CommandLineRunner {
    override fun run(vararg args: String?) {
        Thread.currentThread().join()
    }
}

fun main(args: Array<String>) {
    runApplication<FuyangApiSchedulerApplication>(*args)
}