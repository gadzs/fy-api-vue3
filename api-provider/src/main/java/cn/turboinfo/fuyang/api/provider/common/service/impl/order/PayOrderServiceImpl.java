package cn.turboinfo.fuyang.api.provider.common.service.impl.order;

import cn.turboinfo.fuyang.api.domain.common.service.order.PayOrderService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import cn.turboinfo.fuyang.api.entity.common.exception.common.DataNotExistException;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrderCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.order.PayOrderDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.order.PayOrderPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class PayOrderServiceImpl extends DefaultQServiceImpl<PayOrder, Long, PayOrderPO, PayOrderDAO> implements PayOrderService {

    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public PayOrder save(PayOrderCreator creator) {
        PayOrderPO payOrderPO = new PayOrderPO();
        QBeanCreatorHelper.copyCreatorField(payOrderPO, creator);

        // 手动生成ID
        payOrderPO.setId(idGeneratorFacade.nextId());
        return convertQBean(dao.save(payOrderPO));
    }

    @Transactional
    @Override
    public void checkAndUpdateStatus(Long payOrderId, PayOrderStatus status, PayOrderStatus checkStatus) {
        PayOrderPO po = getEntityWithNullCheckForUpdate(payOrderId);
        if (po.getPayOrderStatus() != checkStatus) {
            throw new RuntimeException("订单状态不正确");
        }
        po.setPayOrderStatus(status);
    }

    @Override
    public List<PayOrder> findByObjectIdAndStatus(EntityObjectType objectType, Long objectId, PayOrderStatus status) {
        return convertQBeanToList(dao.findByObjectTypeAndObjectIdAndPayOrderStatusOrderByCreatedTime(objectType, objectId, status));
    }

    @Override
    public List<PayOrder> findByObjectId(EntityObjectType objectType, Long objectId) {
        return convertQBeanToList(dao.findByObjectTypeAndObjectIdOrderByCreatedTime(objectType, objectId));
    }

    @Override
    public List<PayOrder> findByObjectIdAndStatusAndPayType(EntityObjectType objectType, Long objectId, PayOrderStatus status, PayType payType) {
        return convertQBeanToList(dao.findByObjectTypeAndObjectIdAndPayTypeAndPayOrderStatusOrderByCreatedTime(objectType, objectId, payType, status));
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataNotExistException {
        dao.deleteById(id);
    }
}
