package cn.turboinfo.fuyang.api.gateway.mini.contract.login

import cn.turboinfo.fuyang.api.domain.mini.usecase.login.MiniLoginByPasswordUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.login.MiniLoginBySmsCodeUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.login.MiniLoginByWechatUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.login.MiniSendLoginSmsCodeUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/login"])
interface MiniLoginContract {

    // 发送登录短信验证码
    @AutoMappingContract(
        beanType = MiniSendLoginSmsCodeUseCase::class,
        consumes = [],
    )
    fun sendSmsCode()

    @AutoMappingContract(
        beanType = MiniLoginBySmsCodeUseCase::class,
    )
    fun loginBySmsCode()

    @AutoMappingContract(
        beanType = MiniLoginByPasswordUseCase::class,
    )
    fun loginByPassword()

    @AutoMappingContract(
        beanType = MiniLoginByWechatUseCase::class,
    )
    fun loginByWechat()
}
