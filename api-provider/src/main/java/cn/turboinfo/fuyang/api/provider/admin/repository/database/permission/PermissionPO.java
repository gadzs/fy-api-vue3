package cn.turboinfo.fuyang.api.provider.admin.repository.database.permission;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(
        name = "sys_permission"
)
@Entity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class PermissionPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    private String name;

    private String description;

    private String resource;

    private String url;

    private String component;

    private String icon;

    @Column(name = "visible_status")
    private YesNoStatus visibleStatus;

    /**
     * 特定角色可分配
     */
    @Column(name = "role_id")
    private Long roleId;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "sort_value")
    private Integer sortValue;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
