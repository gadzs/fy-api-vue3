package cn.turboinfo.fuyang.api.gateway.admin.fo.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSortType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class ProductSearchFO {
    @QField(
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "公司编码"
    )
    private Long companyId;

    @QField(
            placeholder = "公司门店编码"
    )
    private Long shopId;

    @QField(
            placeholder = "分类编码"
    )
    private Long categoryId;

    @QField(
            searchable = false
    )
    private String categoryCode;

    @QField(
            placeholder = "门店名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            template = "enum"
    )
    private ProductStatus status;

    @QField(
            placeholder = "营业时间",
            operator = Operator.LIKE
    )
    private String businessTime;

    /**
     * 地区
     */
    @QField(
            searchable = false
    )
    private Long areaCode;

    /**
     * 排序
     */
    @QField(
            searchable = false
    )
    private ProductSortType sortType;

    /**
     * 过滤
     */
    @QField(
            searchable = false
    )
    private Long filterType;

}
