package cn.turboinfo.fuyang.api.gateway.admin.fo.account;

import lombok.Data;

import java.util.List;

@Data
public class CompanyAccountExportFO {

    private List<Long> idList;
}
