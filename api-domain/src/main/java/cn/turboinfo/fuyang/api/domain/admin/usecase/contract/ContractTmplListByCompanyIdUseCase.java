package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractTmplService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractTmplListByCompanyIdUseCase extends AbstractUseCase<ContractTmplListByCompanyIdUseCase.InputData, ContractTmplListByCompanyIdUseCase.OutputData> {
    private final ContractTmplService contractTmplService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        Set<Long> companyIdSet = new HashSet<>();
        companyIdSet.add(0L);
        companyIdSet.add(companyId);

        List<ContractTmpl> contractTmplList = contractTmplService.findByCompanyIdCollection(companyIdSet);

        return OutputData.builder()
                .templateList(contractTmplList)
                .build();
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "companyId不能为空")
        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<ContractTmpl> templateList;
    }
}
