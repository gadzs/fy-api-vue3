package cn.turboinfo.fuyang.api.entity.mini.fo.shop;

import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import lombok.Data;

import java.util.List;

/**
 * @author gadzs
 * @description 店铺详情
 * @date 2023/2/13 11:14
 */
@Data
public class MiniShop {

    /**
     * 家政员数
     */
    private Long staffNum;

    /**
     * 产品服务数
     */
    private Long productNum;

    private ViewShopFO shop;

    private List<Category> categoryList;

    private List<ViewStaffFO> staffList;
}
