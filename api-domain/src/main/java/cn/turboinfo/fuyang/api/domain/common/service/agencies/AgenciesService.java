package cn.turboinfo.fuyang.api.domain.common.service.agencies;

import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 机构管理服务
 * author: hai
 */
public interface AgenciesService extends BaseQService<Agencies, Long> {


    /**
     * 检查是否可用
     *
     * @param id   编码
     * @param name 名称
     * @return
     */
    boolean checkAvailable(Long id, String name);

    /**
     * @return 所有数据
     */
    List<Agencies> findAllList();

}
