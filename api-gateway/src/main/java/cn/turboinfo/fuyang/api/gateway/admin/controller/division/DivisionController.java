package cn.turboinfo.fuyang.api.gateway.admin.controller.division;


import cn.turboinfo.fuyang.api.domain.admin.usecase.division.ListByParentIdUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping(value = "/admin/division")
public class DivisionController {

    private final ListByParentIdUseCase listByParentIdUseCase;


    @PostMapping("/listByParentId")
    public RestResponseFO list(@RequestParam Long parentId) {

        ListByParentIdUseCase.OutputData outputData = listByParentIdUseCase.execute(ListByParentIdUseCase.InputData.builder()
                .parentId(parentId)
                .build());
        //判断用户权限
        List<Division> chinaDivisionList = outputData.getChinaDivisionList();
        return RestResponseFO.ok(chinaDivisionList);
    }
}
