package cn.turboinfo.fuyang.api.entity.common.fo.wechat;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WechatAuthResult {

    private String openid;

    private String session_key;

    private String unionid;

    private String errcode;

    private String errmsg;

}
