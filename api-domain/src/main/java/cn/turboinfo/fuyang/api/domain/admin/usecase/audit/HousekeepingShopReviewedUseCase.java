package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingShopAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.shop.HousekeepingShopException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 家政门店审核usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopReviewedUseCase extends AbstractUseCase<HousekeepingShopReviewedUseCase.InputData, HousekeepingShopReviewedUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;

    private final SysUserService sysUserService;

    private final HousekeepingShopAuditRecordService housekeepingShopAuditRecordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();
        val auditorId = inputData.getUserId();
        val shop = housekeepingShopService.getByIdEnsure(id);
        if (shop.getStatus() != ShopStatus.PENDING) {
            throw new HousekeepingShopException("数据状态不是待审核，操作失败");
        }

        // 更新状态
        HousekeepingShopUpdater.Builder builder = HousekeepingShopUpdater.builder(id);

        builder
                .withStatus(ShopStatus.REVIEWED);

        HousekeepingShop housekeepingShop = housekeepingShopService.update(builder.build());

        val auditorUser = sysUserService.getByIdEnsure(auditorId);

        // 更新审核记录
        HousekeepingShopAuditRecordCreator housekeepingShopAuditRecord = HousekeepingShopAuditRecordCreator.builder()
                .withShopId(housekeepingShop.getId())
                .withCompanyId(housekeepingShop.getCompanyId())
                .withAuditStatus(AuditStatus.REVIEWED)
                .withUserId(auditorId)
                .withUserName(auditorUser.getUsername())
                .withRemark("审核通过")
                .build();
        housekeepingShopAuditRecordService.save(housekeepingShopAuditRecord);


        return OutputData.builder()
                .id(housekeepingShop.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(message = "审核用户ID不能为空")
        private Long userId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
