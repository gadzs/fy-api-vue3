package cn.turboinfo.fuyang.api.provider.admin.repository.database.log;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 登录日志
 */
@Table(
        name = "login_log"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
public class LoginLogPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 登录用户名
     */
    private String username;

    /**
     * 系统用户ID
     */
    @Column(
            name = "sys_user_id"
    )
    private Long sysUserId;

    /**
     * 登录来源
     */
    @Column(
            name = "login_source"
    )
    private String loginSource;

    /**
     * 登录行为发生的时间
     */
    @Column(
            name = "login_time"
    )
    private LocalDateTime loginTime;

    /**
     * 是否登录成功
     */
    @Column(
            name = "login_status"
    )
    private net.sunshow.toolkit.core.base.enums.YesNoStatus loginStatus;

    /**
     * 登录备注信息
     */
    @Column(
            name = "login_remark"
    )
    private String loginRemark;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
    }
}
