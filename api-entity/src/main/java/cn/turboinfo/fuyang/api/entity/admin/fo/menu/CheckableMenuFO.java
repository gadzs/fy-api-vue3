package cn.turboinfo.fuyang.api.entity.admin.fo.menu;

import lombok.Data;

import java.util.List;

@Data
public class CheckableMenuFO {

    private Long id;

    private String name;

    private String description;

    private String resource;

    private String url;

    private Long parentId;

    private List<CheckableMenuFO> subList;

    private boolean checked;

    private boolean halfChecked;
}
