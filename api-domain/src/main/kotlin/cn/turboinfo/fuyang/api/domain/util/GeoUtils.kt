package cn.turboinfo.fuyang.api.domain.util

import java.math.BigDecimal
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

object GeoUtils {

    fun calculateDistance(lon1: BigDecimal, lat1: BigDecimal, lon2: BigDecimal, lat2: BigDecimal): BigDecimal {
        val earthRadius = 6371 // 地球半径，单位：km

        val lonDistance = Math.toRadians(lon2.toDouble() - lon1.toDouble())
        val latDistance = Math.toRadians(lat2.toDouble() - lat1.toDouble())
        val a = sin(latDistance / 2) * sin(latDistance / 2) +
                cos(Math.toRadians(lat1.toDouble())) * cos(Math.toRadians(lat2.toDouble())) *
                sin(lonDistance / 2) *
                sin(lonDistance / 2)
        val c = 2 * atan2(sqrt(a), sqrt(1 - a))
        return (earthRadius * c).toBigDecimal()
    }

}