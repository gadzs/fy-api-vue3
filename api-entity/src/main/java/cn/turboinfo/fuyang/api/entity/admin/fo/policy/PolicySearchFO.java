package cn.turboinfo.fuyang.api.entity.admin.fo.policy;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;

@Data
@QField
public class PolicySearchFO {

    @QField(placeholder = "ID", sortable = true)
    private Long id;

}
