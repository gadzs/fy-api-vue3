package cn.turboinfo.fuyang.api.domain.admin.usecase.credit;

import cn.turboinfo.fuyang.api.domain.common.handler.credit.CreditRatingDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.audit.CreditRatingAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.credit.CreditRatingService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.CreditRatingAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gadzs
 * 评价详情usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class CreditRatingViewUseCase extends AbstractUseCase<CreditRatingViewUseCase.InputData, CreditRatingViewUseCase.OutputData> {
    private final CreditRatingService creditRatingService;

    private final CreditRatingAuditRecordService creditRatingAuditRecordService;

    private final ServiceOrderService serviceOrderService;

    private final CreditRatingDataFactory creditRatingDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();

        val creditRating = creditRatingService.getByIdEnsure(id);

        creditRatingDataFactory.assembleAppraiser(creditRating);

        val serviceOrder = serviceOrderService.getByIdEnsure(creditRating.getObjectId());
        val auditRecordList = creditRatingAuditRecordService.findByCreditRatingId(creditRating.getId());

        return OutputData.builder()
                .creditRating(creditRating)
                .serviceOrder(serviceOrder)
                .auditRecordList(auditRecordList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private ServiceOrder serviceOrder;

        private CreditRating creditRating;

        private List<CreditRatingAuditRecord> auditRecordList;
    }

}
