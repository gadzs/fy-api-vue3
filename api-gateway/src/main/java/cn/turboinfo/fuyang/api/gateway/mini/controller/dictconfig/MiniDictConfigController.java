package cn.turboinfo.fuyang.api.gateway.mini.controller.dictconfig;


import cn.turboinfo.fuyang.api.domain.mini.usecase.dictconfig.MiniListDictItemByDictKeyUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import cn.turboinfo.fuyang.api.gateway.mini.fo.dictconfig.MiniDictConfigItemListResponse;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/mini/dict")
public class MiniDictConfigController {

    private final MiniListDictItemByDictKeyUseCase miniListDictItemByDictKeyUseCase;

    @GetMapping("/listByDictKey")
    public MiniDictConfigItemListResponse findAllDictConfigItem(@RequestParam String dictKey) {

        MiniListDictItemByDictKeyUseCase.OutputData outputData = miniListDictItemByDictKeyUseCase
                .execute(MiniListDictItemByDictKeyUseCase
                        .InputData
                        .builder()
                        .dictKey(dictKey)
                        .build());

        return MiniDictConfigItemListResponse
                .builder()
                .dictConfigItemList(
                        outputData.getDictConfigItemList()
                                .stream()
                                .map(DictConfigItem::toIntegerView)
                                .collect(Collectors.toList())
                )
                .build();
    }

}
