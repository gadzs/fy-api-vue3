package cn.turboinfo.fuyang.api.gateway.admin.controller.dictconfig;

import cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig.*;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.QDictConfig;
import cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig.DictConfigSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig.DictConfigUpdateFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/dictConfig")
public class DictConfigController {
    private final DictConfigSearchUseCase dictConfigSearchUseCase;

    private final DictConfigUpdateUseCase dictConfigUpdateUseCase;

    private final DictConfigDeleteUseCase dictConfigDeleteUseCase;

    private final DictConfigCheckAvailableUseCase dictConfigCheckAvailableUseCase;

    @RequiresPermissions("dictConfig:list")
    @RequestMapping("/list")
    public LimitDataFO<DictConfig> list(@Valid DictConfigSearchFO search, LimitFO page,
                                        SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QDictConfig.id + "|" + QSort.Order.DESC.name()});
        }

        DictConfigSearchUseCase.OutputData outputData = dictConfigSearchUseCase.execute(DictConfigSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions("dictConfig:update")
    @PostMapping("/update")
    public RestResponseFO update(@RequestBody @Valid DictConfigUpdateFO request) {
        DictConfigUpdateUseCase.InputData.InputDataBuilder inputDataBuilder = DictConfigUpdateUseCase.InputData.builder();

        BeanHelper.copyPropertiesToBuilder(inputDataBuilder, DictConfigUpdateFO.class, request);

        DictConfigUpdateUseCase.OutputData outputData = dictConfigUpdateUseCase
                .execute(inputDataBuilder
                        .build());

        return RestResponseFO.ok(outputData);
    }

    @RequiresPermissions("dictConfig:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RestResponseFO delete(@Valid @NotNull @RequestParam Long id) {
        dictConfigDeleteUseCase
                .execute(DictConfigDeleteUseCase.InputData.builder()
                        .dictConfigId(id)
                        .build());
        return RestResponseFO.ok();
    }

    @RequiresPermissions("dictConfig:list")
    @ResponseBody
    @GetMapping(value = "/checkAvailable")
    public boolean checkAvailable(@RequestParam String dictKey, @RequestParam(required = false) Long id) {

        DictConfigCheckAvailableUseCase.OutputData outputData = dictConfigCheckAvailableUseCase.execute(DictConfigCheckAvailableUseCase.InputData
                .builder()
                .dictId(id)
                .dictKey(dictKey)
                .build());

        return outputData.isAvailable();
    }
}
