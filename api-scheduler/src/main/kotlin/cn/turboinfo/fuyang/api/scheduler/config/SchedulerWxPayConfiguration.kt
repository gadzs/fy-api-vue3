package cn.turboinfo.fuyang.api.scheduler.config

import com.github.binarywang.wxpay.config.WxPayConfig
import com.github.binarywang.wxpay.service.WxPayService
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl
import org.apache.commons.lang3.StringUtils
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@ConditionalOnClass(WxPayService::class)
@EnableConfigurationProperties(SchedulerWxPayProperties::class)
class SchedulerWxPayConfiguration {

    @Bean
    @ConditionalOnMissingBean(WxPayService::class)
    fun wxService(properties: SchedulerWxPayProperties): WxPayService {
        val payConfig = WxPayConfig()

        payConfig.appId = StringUtils.trimToNull(properties.appId)
        payConfig.mchId = StringUtils.trimToNull(properties.mchId)
        payConfig.apiV3Key = StringUtils.trimToNull(properties.apiV3Key)
        payConfig.privateKeyPath = StringUtils.trimToNull(properties.keyPath)
        payConfig.privateCertPath = StringUtils.trimToNull(properties.certPath)
        payConfig.certSerialNo = StringUtils.trimToNull(properties.mchNo)

        // 可以指定是否使用沙箱环境
        payConfig.isUseSandboxEnv = false
        val wxPayService: WxPayService = WxPayServiceImpl()
        wxPayService.config = payConfig
        return wxPayService
    }
}
