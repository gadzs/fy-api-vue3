package cn.turboinfo.fuyang.api.entity.common.exception.company;

public class HousekeepingCompanyException extends RuntimeException {
    public HousekeepingCompanyException() {
        this("操作 HousekeepingCompany 出错");
    }

    public HousekeepingCompanyException(String message) {
        super(message);
    }

    public HousekeepingCompanyException(Throwable cause) {
        super(cause);
    }

    public HousekeepingCompanyException(String message, Throwable cause) {
        super(message, cause);
    }
}
