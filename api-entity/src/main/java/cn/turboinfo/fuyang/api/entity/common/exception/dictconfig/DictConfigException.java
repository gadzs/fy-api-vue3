package cn.turboinfo.fuyang.api.entity.common.exception.dictconfig;

public class DictConfigException extends RuntimeException {
    public DictConfigException() {
        this("操作 DictConfig 出错");
    }

    public DictConfigException(String message) {
        super(message);
    }

    public DictConfigException(Throwable cause) {
        super(cause);
    }

    public DictConfigException(String message, Throwable cause) {
        super(message, cause);
    }
}
