package cn.turboinfo.fuyang.api.domain.share.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.share.fo.staff.ShareStaffFO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 家政人员列表
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareListStaffUseCase extends AbstractUseCase<ShareListStaffUseCase.InputData, ShareListStaffUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<HousekeepingStaff> response = housekeepingStaffService.findAll(inputData.request, inputData.getRequestPage());

        List<HousekeepingStaff> housekeepingStaffList = new ArrayList<>(response.getPagedData());

        Set<Long> provinceCodeSet = housekeepingStaffList.stream()
                .map(HousekeepingStaff::getProvinceCode)
                .collect(Collectors.toSet());

        Map<Long, Division> divisionMap = divisionService.findByIdCollection(provinceCodeSet)
                .stream()
                .collect(Collectors.toMap(Division::getId, o -> o));

        List<ShareStaffFO> foList = housekeepingStaffList.stream()
                .map(it -> {
                    ShareStaffFO.ShareStaffFOBuilder builder = ShareStaffFO.builder();
                    BeanHelper.copyPropertiesToBuilder(builder, ShareStaffFO.class, it);

                    Long provinceCode = it.getProvinceCode();
                    if (provinceCode != null && divisionMap.containsKey(provinceCode)) {
                        builder.provinceName(divisionMap.get(provinceCode).getAreaName());
                    }

                    return builder
                            .age(IdCardUtils.countAge(it.getIdCard()))
                            .idCard(IdCardUtils.maskIdCareNum(it.getIdCard()))
                            .contactMobile(PhoneUtils.maskPhoneNum(it.getContactMobile()))
                            .build();
                })
                .toList();

        QResponse<ShareStaffFO> foResponse = new QResponse<>(response.getPage(), response.getPageSize(), foList, response.getTotal());

        return OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private QRequest request;

        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private QResponse<ShareStaffFO> qResponse;

    }
}
