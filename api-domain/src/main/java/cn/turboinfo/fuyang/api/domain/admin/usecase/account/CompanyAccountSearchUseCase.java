package cn.turboinfo.fuyang.api.domain.admin.usecase.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.MaskUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.QCompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class CompanyAccountSearchUseCase extends AbstractUseCase<CompanyAccountSearchUseCase.InputData, CompanyAccountSearchUseCase.OutputData> {
    private final CompanyAccountService companyAccountService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        String companyName = inputData.getCompanyName();

        QRequest request = inputData.getRequest();

        if (StringUtils.isNotBlank(companyName)) {
            List<Long> companyIdList = housekeepingCompanyService.findByName(companyName)
                    .stream()
                    .map(HousekeepingCompany::getId)
                    .toList();

            request.filterIn(QCompanyAccount.companyId, Arrays.asList(companyIdList.toArray()));
        }

        QResponse<CompanyAccount> response = companyAccountService.findAll(request, inputData.getRequestPage());

        Set<Long> companyIdSet = response.getPagedData().stream()
                .map(CompanyAccount::getCompanyId)
                .collect(Collectors.toSet());

        Map<Long, String> companyMap = housekeepingCompanyService.findByIdCollection(companyIdSet)
                .stream()
                .collect(Collectors.toMap(HousekeepingCompany::getId, HousekeepingCompany::getName));

        response.getPagedData()
                .forEach(it -> {
                    it.setCompanyName(companyMap.getOrDefault(it.getCompanyId(), StringUtils.EMPTY));
                    it.setAccount(MaskUtils.maskPartialString(it.getAccount(), 4, 4, "*", 4));
                });

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private String companyName;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<CompanyAccount> qResponse;
    }
}
