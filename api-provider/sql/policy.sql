CREATE TABLE `policy`
(
    `id`            BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `name`          VARCHAR(32)   NOT NULL DEFAULT '' COMMENT '策略名称',
    `description`   VARCHAR(256)  NOT NULL DEFAULT '' COMMENT '策略描述',
    `policy_type`   SMALLINT      NOT NULL DEFAULT 0 COMMENT '策略类型',
    `policy_script` VARCHAR(1024) NOt NULL DEFAULT '' COMMENT '策略脚本',
    `deleted_time`  BIGINT        NOT NULL DEFAULT 0,
    `created_time`  DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`  DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='策略';
