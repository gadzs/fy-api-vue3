package cn.turboinfo.fuyang.api.gateway.mini.config;

import cn.turboinfo.fuyang.api.gateway.mini.framework.http.interceptor.*;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.resolver.MiniRequestContextHandlerMethodArgumentResolver;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.resolver.MiniSessionHandlerMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * author: sunshow.
 */
@Configuration
public class MiniWebConfig implements WebMvcConfigurer {

    @Autowired
    private MiniRequestContextInterceptor requestContextInterceptor;

    @Autowired
    private MiniSessionScopeInterceptor sessionScopeInterceptor;

    @Autowired
    private MiniPlatformScopeInterceptor platformScopeInterceptor;

    @Autowired
    private MiniCompanyScopeInterceptor companyScopeInterceptor;

    @Autowired
    private MiniStaffScopeInterceptor housekeeperScopeInterceptor;

    @Autowired
    private MiniConsumerScopeInterceptor consumerScopeInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestContextInterceptor)
                .addPathPatterns("/mini/**");
        registry.addInterceptor(sessionScopeInterceptor)
                .addPathPatterns("/mini/**");
        registry.addInterceptor(platformScopeInterceptor)
                .addPathPatterns("/mini/**");
        registry.addInterceptor(companyScopeInterceptor)
                .addPathPatterns("/mini/**");
        registry.addInterceptor(housekeeperScopeInterceptor)
                .addPathPatterns("/mini/**");
        registry.addInterceptor(consumerScopeInterceptor)
                .addPathPatterns("/mini/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new MiniRequestContextHandlerMethodArgumentResolver());
        resolvers.add(new MiniSessionHandlerMethodArgumentResolver());
    }

}
