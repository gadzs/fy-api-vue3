package cn.turboinfo.fuyang.api.gateway.admin.controller.file;

import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.web.usecase.file.UploadFileAttachmentUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.gateway.admin.controller.BaseController;
import cn.turboinfo.fuyang.api.gateway.admin.fo.file.FileUploadFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.fo.file.MiniFileUploadResponse;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.provider.admin.component.config.AdminKitConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/admin/file")
public class FileAttachmentController extends BaseController {

    private final UploadFileAttachmentUseCase uploadFileAttachmentUseCase;

    private final AdminKitConfig adminKitConfig;

    private final FileAttachmentService fileAttachmentService;

    @PostMapping("/upload")
    @ResponseBodyWrapper
    public MiniFileUploadResponse upload(@Valid FileUploadFO fo) {

        UploadFileAttachmentUseCase.OutputData outputData = uploadFileAttachmentUseCase.execute(UploadFileAttachmentUseCase
                .InputData
                .builder()
                .file(fo.getFile())
                .allowExts(StringUtils.split(adminKitConfig.getAttachmentAllowExt(), "|"))
                .relativePath(fo.getRelativePath())
                .refType(fo.getRefType())
                .refId(fo.getRefId())
                .build());

        return MiniFileUploadResponse
                .builder()
                .attachment(outputData.getAttachment())
                .build();
    }

    /**
     * 查看附件
     *
     * @param fileAttachmentId
     * @param response
     * @throws IOException
     */
    @GetMapping("/view/{fileAttachmentId}")
    public void view(@PathVariable Long fileAttachmentId, HttpServletResponse response) throws IOException {
        viewAttachmentImage(fileAttachmentId, response);
    }

    /**
     * 下载附件
     *
     * @param fileAttachmentId
     * @param response
     * @throws IOException
     */
    @GetMapping("/download/{fileAttachmentId}")
    public void download(@PathVariable Long fileAttachmentId, HttpServletResponse response) throws IOException {

        Optional<FileAttachment> fileAttachmentOptional = fileAttachmentService.getById(fileAttachmentId);

        if (fileAttachmentOptional.isPresent()) {
            downloadAttachment(fileAttachmentOptional.get(), response);
        }
    }

}
