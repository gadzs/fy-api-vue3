package cn.turboinfo.fuyang.api.domain.mini.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.handler.staff.StaffDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 查看家政人员详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewStaffByIdUseCase extends AbstractUseCase<MiniViewStaffByIdUseCase.InputData, MiniViewStaffByIdUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final DivisionService divisionService;

    private final StaffDataFactory staffDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long staffId = inputData.getStaffId();

        // 根据放心码查询家政人员
        HousekeepingStaff staff = housekeepingStaffService.getByIdEnsure(staffId);

        // 根据家政人员ID查询家政公司
        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(staff.getCompanyId());

        Division division = divisionService.getByIdEnsure(staff.getProvinceCode());

        staff.setProvinceName(division.getAreaName());
        staff.setAge(IdCardUtils.countAge(staff.getIdCard()));
        staffDataFactory.maskInfo(staff);

        return OutputData.builder()
                .staff(staff)
                .company(company)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 家政员编码
         */
        @NotNull(
                message = "家政员编码不能为空"
        )
        private Long staffId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingStaff staff;

        private HousekeepingCompany company;

    }
}
