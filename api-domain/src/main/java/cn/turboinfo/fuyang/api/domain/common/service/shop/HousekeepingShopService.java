package cn.turboinfo.fuyang.api.domain.common.service.shop;

import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopCreator;
import kotlin.Pair;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.util.*;

/**
 * 家政门店服务
 * author: hai
 */
public interface HousekeepingShopService extends BaseQService<HousekeepingShop, Long> {

    HousekeepingShop save(HousekeepingShopCreator creator);

    /**
     * 根据企业id和门店名获取
     *
     * @param companyId
     * @param name
     * @return
     */
    Optional<HousekeepingShop> findByCompanyIdAndName(Long companyId, String name);

    List<HousekeepingShop> findByCompanyId(Long companyId);

    /**
     * 查找距离中心点最近的若干家门店
     *
     * @param longitude          经度
     * @param latitude           纬度
     * @param radiusInKilometers 半径（公里）
     * @param limit              返回数量
     * @return 门店信息和距离
     */
    List<Pair<HousekeepingShop, BigDecimal>> findByGeoRadius(BigDecimal longitude, BigDecimal latitude, Double radiusInKilometers, Long limit);


    void updateCreditScore(Long id, BigDecimal creditScore);

    /**
     * 查找评分最高的若干家门店
     */
    List<HousekeepingShop> findTopByCreditScore(int count);

    void updateOrderNum(Long id, Long orderNum);

    List<HousekeepingShop> findByCityCode(Long cityCode);

    List<HousekeepingShop> findByAreaCode(Long areaCode);

    /**
     * 一次性获取多个门店的服务分类
     */
    Map<Long, Set<Long>> batchFindShopCategory(Collection<Long> shopIdCollection);

    /**
     * 根据名称模糊查询
     *
     * @param name 名称
     */
    List<HousekeepingShop> findByNameContaining(String name);

    long countByCompanyId(Long companyId);


}
