package cn.turboinfo.fuyang.api.entity.common.enumeration.product;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * @author gadzs
 * @description 产品排序
 * @date 2023/2/15 15:18
 */
@Slf4j
public enum ProductSortType implements BaseEnum {

    DEFAULT(0, "不限排序"),

    CREDIT_SCORE_DESC(10, "按信用分从高到低"),

    DISTANCE_NEAREST(20, "距离最近");

    private int value;
    private String name;

    ProductSortType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSortType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSortType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSortType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
