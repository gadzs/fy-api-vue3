package cn.turboinfo.fuyang.api.domain.mini.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 服务下单后选择支付方式前的预览接口
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerPreviewServiceOrderUseCase extends AbstractUseCase<MiniConsumerPreviewServiceOrderUseCase.InputData, MiniConsumerPreviewServiceOrderUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        Long serviceOrderId = inputData.getServiceOrderId();

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(serviceOrderId);

        return OutputData.builder()
                .serviceOrder(serviceOrder)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 下单用户ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        /**
         * 订单ID
         */
        @NotNull(message = "订单ID不能为空")
        @Positive
        private Long serviceOrderId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private ServiceOrder serviceOrder;

    }
}
