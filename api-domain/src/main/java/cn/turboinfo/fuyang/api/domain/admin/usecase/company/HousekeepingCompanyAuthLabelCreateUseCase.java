package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuthLabelService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.exception.company.HousekeepingCompanyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabelCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政企业认证标签创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAuthLabelCreateUseCase extends AbstractUseCase<HousekeepingCompanyAuthLabelCreateUseCase.InputData, HousekeepingCompanyAuthLabelCreateUseCase.OutputData> {
    private final HousekeepingCompanyAuthLabelService housekeepingCompanyAuthLabelService;
    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Optional<HousekeepingCompanyAuthLabel> housekeepingCompanyAuthLabelOptional1 = housekeepingCompanyAuthLabelService.findByName(inputData.getName());
        if (housekeepingCompanyAuthLabelOptional1.isPresent()) {
            throw new HousekeepingCompanyException("名称已存在");
        }

        HousekeepingCompanyAuthLabelCreator.Builder builder = HousekeepingCompanyAuthLabelCreator.builder();
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, HousekeepingCompanyAuthLabelCreator.class, inputData);
        HousekeepingCompanyAuthLabel housekeepingCompanyAuthLabel = housekeepingCompanyAuthLabelService.save(builder.build());

        if (housekeepingCompanyAuthLabel.getIconId() != null && inputData.getIconId().intValue() > 0) {
            fileAttachmentService.updateRefId(housekeepingCompanyAuthLabel.getIconId(), housekeepingCompanyAuthLabel.getId());
        }
        return OutputData.builder()
                .housekeepingCompanyAuthLabel(housekeepingCompanyAuthLabel)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotBlank(
                message = "名称"
        )
        private String name;

        private Long iconId;

        @NotNull(
                message = "权重不能为空"
        )
        private Integer weight;

        private String description;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingCompanyAuthLabel housekeepingCompanyAuthLabel;
    }

}
