package cn.turboinfo.fuyang.api.domain.common.service.contract;

import cn.turboinfo.fuyang.api.entity.common.pojo.contract.Contract;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Optional;

/**
 * 合同管理服务
 * author: hai
 */
public interface ContractService extends BaseQService<Contract, Long> {

    Optional<Contract> findByOrderId(Long orderId);

}
