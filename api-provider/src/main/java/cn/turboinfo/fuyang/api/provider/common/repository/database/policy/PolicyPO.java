package cn.turboinfo.fuyang.api.provider.common.repository.database.policy;

import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(
        name = "policy"
)
@Entity
@Where(clause = "deleted_time = 0")
@DynamicInsert
@DynamicUpdate
@Data
public class PolicyPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 策略名称
     */
    private String name;

    /**
     * 策略描述
     */
    private String description;

    /**
     * 策略类型
     */
    @Column(name = "policy_type")
    private PolicyType policyType;

    /**
     * 策略脚本
     */
    @Column(name = "policy_script")
    private String policyScript;

    @Column(name = "deleted_time")
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }

}
