package cn.turboinfo.fuyang.api.domain.common.service

import cn.turboinfo.fuyang.api.entity.common.enumeration.sms.SmsType


/**
 * 短信服务
 */
interface SmsService {

    fun sendCode(type: SmsType, phone: String)

    fun verifyCode(type: SmsType, phone: String, code: String)

}