package cn.turboinfo.fuyang.api.gateway.mini.controller.staff;

import cn.turboinfo.fuyang.api.domain.mini.usecase.staff.MiniListStaffUseCase;
import cn.turboinfo.fuyang.api.domain.mini.usecase.staff.MiniStaffListCreditRatingUserCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.credit.ViewCreditRatingFO;
import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.QHousekeepingStaff;
import cn.turboinfo.fuyang.api.gateway.admin.fo.staff.HousekeepingStaffSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @author gadzs
 * @description 家政员列表
 * @date 2023/2/13 13:45
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/mini/staff")
public class MiniStaffController {

    private final MiniListStaffUseCase miniListStaffUseCase;

    private final MiniStaffListCreditRatingUserCase miniStaffListCreditRatingUserCase;

    @RequestMapping("/list")
    public LimitDataFO<ViewStaffFO> list(@Valid HousekeepingStaffSearchFO search, LimitFO page, SortFO sort) {
        search.setStatus(StaffStatus.PUBLISHED);
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingStaff.id + "|" + QSort.Order.DESC.name()});
        }

        MiniListStaffUseCase.OutputData outputData = miniListStaffUseCase.execute(MiniListStaffUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequestMapping("/listCreditRating")
    @StaffScope
    public LimitDataFO<ViewCreditRatingFO> listCreditRating(MiniRequestContext context, LimitFO page) {
        MiniStaffListCreditRatingUserCase.OutputData outputData = miniStaffListCreditRatingUserCase.execute(MiniStaffListCreditRatingUserCase.InputData
                .builder()
                .staffId(context.getHousekeeperId())
                .requestPage(page.toQPage())
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
