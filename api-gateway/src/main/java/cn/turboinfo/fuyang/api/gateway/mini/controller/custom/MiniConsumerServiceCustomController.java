package cn.turboinfo.fuyang.api.gateway.mini.controller.custom;

import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniConsumerServiceCustomListUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.custom.ViewServiceCustomFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Controller
@SessionScope
@MiniResponseBodyWrapper
@RequestMapping("/mini/consumer/serviceCustom")
public class MiniConsumerServiceCustomController {

    private final MiniConsumerServiceCustomListUseCase miniConsumerServiceCustomListUseCase;

    @ConsumerScope
    @RequestMapping("/serviceCustomList")
    public LimitDataFO<ViewServiceCustomFO> serviceOrderList(@RequestParam(required = false) List<ServiceCustomStatus> customStatus, LimitFO page) {

        MiniConsumerServiceCustomListUseCase.OutputData outputData = miniConsumerServiceCustomListUseCase.execute(MiniConsumerServiceCustomListUseCase.InputData
                .builder()
                .customStatus(customStatus)
                .userId(MiniRequestContextHolder.current().getUserId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getCustomList());
    }
}
