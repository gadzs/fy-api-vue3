package cn.turboinfo.fuyang.api.provider.common.repository.database.contract;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Optional;

public interface ContractDAO extends FYRepository<ContractPO, Long> {

    Optional<ContractPO> findByOrderId(Long orderId);

}
