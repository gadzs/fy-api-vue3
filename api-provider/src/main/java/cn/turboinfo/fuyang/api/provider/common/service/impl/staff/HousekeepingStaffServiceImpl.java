package cn.turboinfo.fuyang.api.provider.common.service.impl.staff;

import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.staff.HousekeepingStaffDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.staff.HousekeepingStaffPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class HousekeepingStaffServiceImpl extends DefaultQServiceImpl<HousekeepingStaff, Long, HousekeepingStaffPO, HousekeepingStaffDAO> implements HousekeepingStaffService {

    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public HousekeepingStaff save(HousekeepingStaffCreator creator) {
        HousekeepingStaffPO housekeepingStaffPO = new HousekeepingStaffPO();
        QBeanCreatorHelper.copyCreatorField(housekeepingStaffPO, creator);

        // 手动生成ID
        housekeepingStaffPO.setId(idGeneratorFacade.nextId());
        return convertQBean(dao.save(housekeepingStaffPO));
    }

    @Override
    public List<HousekeepingStaff> findByCompanyId(Long companyId) {
        return convertStreamQBeanToList(dao.findByCompanyId(companyId).stream());
    }

    @Override
    public List<HousekeepingStaff> findByCompanyIdCollection(Collection<Long> companyIdCollection) {
        return convertQBeanToList(dao.findByCompanyIdIn(companyIdCollection));
    }

    @Override
    public Optional<HousekeepingStaff> findByIdCard(String idCard) {
        return dao.findByIdCard(idCard).map(this::convertQBean);
    }

    @Override
    public Optional<HousekeepingStaff> findByCode(String code) {
        return dao.findByCode(code).map(this::convertQBean);
    }

    @Override
    public Optional<HousekeepingStaff> findByMobile(String contactMobile) {
        return dao.findByContactMobile(contactMobile).map(this::convertQBean);
    }

    @Override
    @Transactional
    public void updateStatus(Long id, StaffStatus staffStatus) {
        HousekeepingStaffPO staffPO = getEntityWithNullCheckForUpdate(id);
        staffPO.setStatus(staffStatus);
    }

    @Override
    @Transactional
    public void updateCreditScore(Long id, BigDecimal creditScore) {
        HousekeepingStaffPO staffPO = getEntityWithNullCheckForUpdate(id);
        staffPO.setCreditScore(creditScore);
    }

    @Override
    @Transactional
    public void updateOrderNum(Long id, Long orderNum) {
        HousekeepingStaffPO staffPO = getEntityWithNullCheckForUpdate(id);
        staffPO.setOrderNum(orderNum);
    }

    @Override
    public long countByCompanyId(Long companyId) {
        return dao.countByCompanyIdAndStatus(companyId, StaffStatus.PUBLISHED);
    }
}
