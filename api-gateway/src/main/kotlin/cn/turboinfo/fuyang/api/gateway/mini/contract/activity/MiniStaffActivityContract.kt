package cn.turboinfo.fuyang.api.gateway.mini.contract.activity

import cn.turboinfo.fuyang.api.domain.mini.usecase.activity.MiniStaffActivityApplyUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@StaffScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/staff/activity"])
interface MiniStaffActivityContract {

    /**
     * 活动详情
     */
    @AutoMappingContract(
        beanType = MiniStaffActivityApplyUseCase::class,
        paths = ["/apply/{activityId}"],
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun apply()

}
