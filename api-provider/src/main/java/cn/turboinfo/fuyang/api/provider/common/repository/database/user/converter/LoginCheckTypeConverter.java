package cn.turboinfo.fuyang.api.provider.common.repository.database.user.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class LoginCheckTypeConverter extends BaseEnumConverter<LoginCheckType> {
}
