package cn.turboinfo.fuyang.api.gateway.admin.fo.company;

import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@QField
public class HousekeepingCompanySearchFO {

    @QField(
            placeholder = "公司名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "统一社会信用代码"
    )
    private String uscc;

    @QField(
            placeholder = "成立日期"
    )
    private LocalDate establishmentDate;

    @QField(
            placeholder = "省id"
    )
    private Long provinceCode;

    @QField(
            placeholder = "市id"
    )
    private Long cityCode;

    @QField(
            placeholder = "区id"
    )
    private Long areaCode;

    @QField(
            placeholder = "注册地址",
            operator = Operator.LIKE
    )
    private String registeredAddress;

    @QField(
            placeholder = "注册资本"
    )
    private BigDecimal registeredCapital;

    @QField(
            placeholder = "公司类型",
            operator = Operator.LIKE
    )
    private String companyType;

    @QField(
            placeholder = "法人",
            operator = Operator.EQUAL
    )
    private String legalPerson;

    @QField(
            placeholder = "法人身份证",
            operator = Operator.EQUAL
    )
    private String legalPersonIdCard;

    @QField(
            placeholder = "工商注册号",
            operator = Operator.LIKE
    )
    private String businessRegistrationNumber;

    @QField(
            placeholder = "联系人",
            operator = Operator.EQUAL
    )
    private String contactPerson;

    @QField(
            placeholder = "联系电话",
            operator = Operator.EQUAL
    )
    private String contactNumber;

    @QField(
            placeholder = "邮箱",
            operator = Operator.LIKE
    )
    private String email;

    @QField(
            placeholder = "员工人数",
            operator = Operator.LIKE
    )
    private String employeesNumber;

    @QField(
            placeholder = "企业状态"
    )
    private CompanyStatus status;

    private String houseKeepingType;
}
