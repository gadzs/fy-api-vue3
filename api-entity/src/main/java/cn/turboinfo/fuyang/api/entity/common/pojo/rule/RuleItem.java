package cn.turboinfo.fuyang.api.entity.common.pojo.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;

/**
 * 规则条目
 * author: sunshow.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RuleItem {

    /**
     * 规则的属性值
     */
    private Integer labelValue;

    /**
     * 规则控制的值
     */
    private String controlValue;

    /**
     * 是否规则整体取反
     */
    private YesNoStatus isNegative;

    /**
     * 规则运算符
     */
    private String operator;

    private EnableStatus status;

}
