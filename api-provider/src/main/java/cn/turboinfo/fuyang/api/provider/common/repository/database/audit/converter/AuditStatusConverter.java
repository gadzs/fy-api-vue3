package cn.turboinfo.fuyang.api.provider.common.repository.database.audit.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * 审核状态转换器
 *
 * @author hai
 */
@Converter(
        autoApply = true
)
public class AuditStatusConverter extends BaseEnumConverter<AuditStatus> {
}
