package cn.turboinfo.fuyang.api.entity.common.enumeration.user;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

@Slf4j
public enum ThirdPartyType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    MINI_WECHAT(1, "微信小程序"),
    ;

    private int value;
    private String name;

    ThirdPartyType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ThirdPartyType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ThirdPartyType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ThirdPartyType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
