package cn.turboinfo.fuyang.api.entity.common.fo.contract;

import cn.turboinfo.fuyang.api.entity.common.enumeration.contract.ContractStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.ViewAttachment;
import lombok.*;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanCreatorIgnore;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanUpdaterIgnore;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 合同管理
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractFO extends AbstractQBean {

    private Long id;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 订单编码
     */
    private Long orderId;

    /**
     * 企业编码
     */
    private Long companyId;

    /**
     * 合同模板编码
     */
    private Long templateId;

    private Long productId;

    private String productName;

    private String consumerName;

    private String staffName;

    private ViewAttachment attachment;

    private ContractStatus status;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
