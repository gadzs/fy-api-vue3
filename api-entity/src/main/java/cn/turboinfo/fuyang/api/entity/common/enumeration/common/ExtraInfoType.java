package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 脱敏数据
 */
@Slf4j
public enum ExtraInfoType implements BaseEnum {

    DEFAULT(0, "默认"),

    ID_CARD(10, "身份证号"),

    MOBILE(20, "手机号"),

    NAME(30, "姓名"),

    ;

    private int value;
    private String name;

    ExtraInfoType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ExtraInfoType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ExtraInfoType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ExtraInfoType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
