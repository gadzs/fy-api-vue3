rootProject.name = "fy-api"
include(":api-entity")
include(":api-domain")
include(":api-provider")
include(":api-gateway")
include(":api-scheduler")

pluginManagement {
    repositories {
        maven {
            url = uri("https://nexus.sunshow.net:19280/repository/maven-public/")
            // isAllowInsecureProtocol = true

            credentials {
                username = "guest"
                password = "YsJR@kJ2a@uEmWb"
            }
        }
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
include("api-scheduler")
