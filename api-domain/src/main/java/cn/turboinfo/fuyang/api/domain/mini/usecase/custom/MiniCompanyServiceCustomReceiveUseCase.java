package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/**
 * 企业接单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniCompanyServiceCustomReceiveUseCase extends AbstractUseCase<MiniCompanyServiceCustomReceiveUseCase.InputData, MiniCompanyServiceCustomReceiveUseCase.OutputData> {

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        Long customId = inputData.getCustomId();
        BigDecimal price = inputData.getPrice();
        BigDecimal deposit = inputData.getDeposit();

        serviceCustomService.receive(customId, companyId, price, deposit);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "企业ID不能为空")
        @Positive
        private Long companyId;

        @NotNull(message = "服务订单ID不能为空")
        @Positive
        private Long customId;

        @NotNull(message = "订单价格不能为空")
        @Positive
        private BigDecimal price;

        @NotNull(message = "定金不能为空")
        @PositiveOrZero
        private BigDecimal deposit;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
