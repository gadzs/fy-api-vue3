package cn.turboinfo.fuyang.api.provider.admin.impl.cms;


import cn.turboinfo.fuyang.api.domain.admin.service.cms.CmsMessageBoardService;
import cn.turboinfo.fuyang.api.entity.admin.exception.cms.CmsMessageBoardException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoard;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardUpdater;
import cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.CmsMessageBoardDAO;
import cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.CmsMessageBoardPO;
import lombok.RequiredArgsConstructor;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.AbstractQServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@RequiredArgsConstructor
@Service
public class CmsMessageBoardServiceImpl extends AbstractQServiceImpl<CmsMessageBoard> implements CmsMessageBoardService {
    private final CmsMessageBoardDAO cmsMessageBoardDAO;

    @Override
    public Optional<CmsMessageBoard> getById(Long id) {
        return cmsMessageBoardDAO.findById(id).map(this::convertQBean);
    }

    @Override
    public CmsMessageBoard getByIdEnsure(Long id) {
        return getById(id).orElseThrow(this.getExceptionSupplier("未找到数据, id=" + id, null));
    }

    @Override
    public List<CmsMessageBoard> findByIdCollection(Collection<Long> idCollection) {
        return convertStreamQBeanToList(cmsMessageBoardDAO.findByIdInOrderByIdDesc(idCollection).stream());
    }

    @Override
    @Transactional
    public CmsMessageBoard save(CmsMessageBoardCreator creator) throws CmsMessageBoardException {
        CmsMessageBoardPO CmsMessageBoardPO = new CmsMessageBoardPO();

        QBeanCreatorHelper.copyCreatorField(CmsMessageBoardPO, creator);

        return convertQBean(cmsMessageBoardDAO.save(CmsMessageBoardPO));
    }

    @Override
    @Transactional
    public CmsMessageBoard update(CmsMessageBoardUpdater updater) throws CmsMessageBoardException {
        CmsMessageBoardPO CmsMessageBoardPO = getEntityWithNullCheckForUpdate(updater.getUpdateId(), cmsMessageBoardDAO);

        QBeanUpdaterHelper.copyUpdaterField(CmsMessageBoardPO, updater);

        return convertQBean(CmsMessageBoardPO);
    }

    @Override
    public QResponse<CmsMessageBoard> findAll(QRequest request, QPage requestPage) {
        return convertQResponse(findAllInternal(request, requestPage));
    }

    private Page<CmsMessageBoardPO> findAllInternal(QRequest request, QPage requestPage) {
        return cmsMessageBoardDAO.findAll(convertSpecification(request), convertPageable(requestPage));
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws CmsMessageBoardException {
        CmsMessageBoardPO cmsMessageBoardPO = getEntityWithNullCheckForUpdate(id, cmsMessageBoardDAO);
        cmsMessageBoardPO.setDeletedTime(System.currentTimeMillis());
    }

    @Override
    protected Supplier<? extends RuntimeException> getExceptionSupplier(String message,
                                                                        Throwable cause) {
        return () -> new CmsMessageBoardException(message, cause);
    }
}
