package cn.turboinfo.fuyang.api.gateway.admin.framework.http;

public class AdminServletWrapperException extends RuntimeException {

    public AdminServletWrapperException(Throwable cause) {
        this("Admin 请求异常", cause);
    }

    public AdminServletWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

}
