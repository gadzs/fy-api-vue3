package cn.turboinfo.fuyang.api.domain.common.service.division;

import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 地区服务
 */
public interface DivisionService extends BaseQService<Division, Long> {

    /**
     * @param parentId 父级编码
     * @return 地区列表
     */
    List<Division> findByParentId(Long parentId);

    List<Division> findAll();

}
