package cn.turboinfo.fuyang.api.provider.common.service.impl.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.provider.common.repository.database.agencies.AgenciesDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.agencies.AgenciesPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class AgenciesServiceImpl extends DefaultQServiceImpl<Agencies, Long, AgenciesPO, AgenciesDAO> implements AgenciesService {
    @Override
    public boolean checkAvailable(Long id, String name) {

        return dao.findByNameOrderByIdDesc(name).stream()
                .filter(it -> id == null || !id.equals(it.getId()))
                .findFirst().isEmpty();
    }

    @Override
    public List<Agencies> findAllList() {
        return convertQBeanToList(dao.findAll());
    }
}
