package cn.turboinfo.fuyang.api.gateway.admin.contract.user

import cn.turboinfo.fuyang.api.domain.admin.usecase.user.AdminVerifyPasswordUseCase
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author hai
 * @description 用户
 */
@AutoMappingContract(paths = ["/admin/user"])
@ResponseBodyWrapper
interface UserContract {

    // 验证密码
    @SessionScope
    @AutoMappingContract(
        beanType = AdminVerifyPasswordUseCase::class
    )
    fun verifyPassword()

}
