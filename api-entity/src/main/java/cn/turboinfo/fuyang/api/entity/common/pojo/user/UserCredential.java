package cn.turboinfo.fuyang.api.entity.common.pojo.user;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class UserCredential extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户编码
     */
    private Long userId;

    /**
     * 凭证
     */
    private String credential;

    /**
     * 盐
     */
    private String salt;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
