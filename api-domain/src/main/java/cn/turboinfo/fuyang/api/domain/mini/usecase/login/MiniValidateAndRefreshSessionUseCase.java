package cn.turboinfo.fuyang.api.domain.mini.usecase.login;

import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.mini.service.session.MiniSessionService;
import cn.turboinfo.fuyang.api.entity.mini.pojo.session.MiniSession;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;

/**
 * 验证并刷新会话
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniValidateAndRefreshSessionUseCase extends AbstractUseCase<MiniValidateAndRefreshSessionUseCase.InputData, MiniValidateAndRefreshSessionUseCase.OutputData> {

    private final MiniSessionService miniSessionService;

    @Override
    protected OutputData doAction(InputData inputData) {
        String token = inputData.getToken();

        MiniSession session = miniSessionService.validateAndRefreshByToken(token);

        return OutputData.builder()
                .session(session)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotEmpty(message = "会话令牌不能为空")
        private String token;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 验证成功返回会话信息
         */
        private MiniSession session;

    }
}
