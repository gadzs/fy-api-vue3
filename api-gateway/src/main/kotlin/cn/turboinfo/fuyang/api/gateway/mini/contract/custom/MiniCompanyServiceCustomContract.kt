package cn.turboinfo.fuyang.api.gateway.mini.contract.custom

import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniCompanyCancelServiceCustomUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniCompanyCustomAssignStaffUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniCompanyCustomAvailableAssignStaffListUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniCompanyServiceCustomReceiveUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@CompanyScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/company/serviceCustom"])
interface MiniCompanyServiceCustomContract {

    // 企业接单
    @AutoMappingContract(
        beanType = MiniCompanyServiceCustomReceiveUseCase::class,
    )
    fun receive()

    // 取消订单
    @AutoMappingContract(
        beanType = MiniCompanyCancelServiceCustomUseCase::class,
    )
    fun cancel()

    // 可分配服务人员列表
    @AutoMappingContract(
        beanType = MiniCompanyCustomAvailableAssignStaffListUseCase::class,
    )
    fun availableAssignStaffList()

    // 分配服务人员
    @SessionScope
    @AutoMappingContract(
        beanType = MiniCompanyCustomAssignStaffUseCase::class,
    )
    fun assignStaff()
}
