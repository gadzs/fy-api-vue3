package cn.turboinfo.fuyang.api.gateway.admin.fo.file;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * @description 文件上传
 * @date 2023/2/3 22:29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FileUploadFO {

    @NotNull(
            message = "文件不能为空"
    )
    private MultipartFile file;

    @NotEmpty(message = "关联业务类型不能为空")
    private String refType;

    private Long refId;

    @NotEmpty(message = "保存路径不能为空")
    private String relativePath;
}
