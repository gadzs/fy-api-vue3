package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 企业门店审核记录
 *
 * @author hai
 */
@Table(
        name = "housekeeping_shop_audit_record"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
public class HousekeepingShopAuditRecordPO extends SoftDeleteFYEntity {
    /**
     * 企业编码
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 门店编码
     */
    @Column(
            name = "shop_id"
    )
    private Long shopId;

    /**
     * 审核状态
     */
    @Column(
            name = "audit_status"
    )
    private AuditStatus auditStatus;

    /**
     * 审核人
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    /**
     * 审核人名称
     */
    @Column(
            name = "user_name"
    )
    private String userName;

    /**
     * 备注
     */
    private String remark;
}
