package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.handler.product.ProductDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.role.RoleService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserRoleService;
import cn.turboinfo.fuyang.api.domain.common.service.user.UserProfileService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRole;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.QProduct;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfile;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductAuditRecordSearchUseCase extends AbstractUseCase<ProductAuditRecordSearchUseCase.InputData, ProductAuditRecordSearchUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;
    private final ProductService productService;
    private final UserProfileService userProfileService;
    private final AgenciesService agenciesService;
    private final SysUserRoleService sysUserRoleService;
    private final RoleService roleService;
    private final ProductDataFactory productDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        QRequest request = inputData.getRequest();

        Long userId = inputData.getUserId();

        UserProfile userProfile = userProfileService.getByIdEnsure(userId);

        Long agenciesId = userProfile.getAgenciesId();

        Set<Long> roleIdSet = sysUserRoleService.findBySysUserId(userId)
                .stream()
                .map(SysUserRole::getRoleId)
                .collect(Collectors.toSet());

        Optional<Role> roleOptional = roleService.findByIdCollection(roleIdSet).stream()
                .filter(it -> "admin".equals(it.getCode()) || "platform".equals(it.getCode()))
                .findAny();


        // 如果是管理员或者平台运营 则不需要过滤
        if (roleOptional.isEmpty() && agenciesId != null && agenciesId > 0) {
            Agencies agencies = agenciesService.getByIdEnsure(agenciesId);

            // 查询门店
            val shopList = housekeepingShopService.findByAreaCode(agencies.getAreaCode());

            request.filterIn(QProduct.shopId, shopList
                    .stream()
                    .map(HousekeepingShop::getId)
                    .collect(Collectors.toSet()));
        }

        QResponse<Product> response = productService.findAll(request, inputData.getRequestPage());

        List<Product> productList = new ArrayList<>(response.getPagedData());

        productDataFactory.assembleCompany(productList);
        productDataFactory.assembleShop(productList);

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户ID不能为空")
        private Long userId;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<Product> qResponse;
    }
}