package cn.turboinfo.fuyang.api.entity.admin.fo.order;

import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.Builder;
import lombok.Data;

/**
 * @author gadzs
 * @description 服务订单FO
 * @date 2023/2/16 15:59
 */
@Data
@Builder
public class ServiceOrderFO {

    private ServiceOrder serviceOrder;
    private Product product;
    private String productName;
    private String productSkuName;
    private String categoryName;
    private String shopName;
    private String userName;
    private String staffName;
}
