package cn.turboinfo.fuyang.api.entity.admin.enumeration.cms;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public enum CmsArticleStatus implements BaseEnum {
    DEFAULT(0, "未发布"),
    PUBLISHED(1, "已发布"),
    CANCELED(2, "已撤回");

    private int value;
    private String name;

    CmsArticleStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CmsArticleStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<CmsArticleStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CmsArticleStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }

    public static List<Map<String, Object>> getList() {
        return getList(null, null);
    }

    public static List<Map<String, Object>> getList(String keyName, String valName) {
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (CmsArticleStatus c : CmsArticleStatus.values()) {
            Map<String, Object> data = new HashMap<>();
            data.put((keyName != null) ? keyName : "name", String.valueOf(c.getName()));
            data.put((valName != null) ? valName : "value", c.getValue());
            dataList.add(data);
        }
        return dataList;
    }
}
