DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity`
(
    `id`             BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `name`           VARCHAR(128) NOT NULL DEFAULT '' COMMENT '活动名称',
    `description`    VARCHAR(255) NOT NULL DEFAULT '' COMMENT '活动描述',
    `activity_type`  VARCHAR(128) NOT NULL DEFAULT '' COMMENT '活动类型',
    `activity_scope` JSON         NULL COMMENT '活动范围',
    `organizer`      VARCHAR(128) NOT NULL DEFAULT '' COMMENT '主办单位',
    `location`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '活动地点',
    `limit_num`      INT          NOT NULL DEFAULT 0 COMMENT '限制数量',
    `applied`        INT          NOT NULL DEFAULT 0 COMMENT '已报名',
    `open_apply`     TINYINT      NOT NULL DEFAULT 0 COMMENT '是否开放报名',
    `start_time`     DATETIME     NULL COMMENT '活动开始时间',
    `end_time`       DATETIME     NULL COMMENT '活动结束时间',
    `status`         TINYINT      NOT NULL DEFAULT 0 COMMENT '状态',
    `audit_status`   TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `audit_reason`   VARCHAR(128) NOT NULL DEFAULT '' COMMENT '失败原因',
    `company_id`     BIGINT       NOT NULL DEFAULT 0 COMMENT '发布人编码',
    `deleted`        BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='活动管理';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('活动管理', 'activity:null', '/activity', 1, 0);
SET @activityPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('活动列表', 'activity:list', '/activity/list', 1, @activityPermissionParentId),
       ('新增活动', 'activity:create', '', 0, @activityPermissionParentId),
       ('更新活动', 'activity:update', '', 0, @activityPermissionParentId),
       ('删除活动', 'activity:delete', '', 0, @activityPermissionParentId),
       ('查看活动', 'activity:view', '/activity/view', 0, @activityPermissionParentId)
;

DROP TABLE IF EXISTS `activity_apply`;
CREATE TABLE `activity_apply`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `activity_id`  BIGINT       NOT NULL DEFAULT 0 COMMENT '活动编码',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '用户编码',
    `user_type`    TINYINT      NOT NULL DEFAULT 0 COMMENT '用户类型',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '用户姓名',
    `use_mobile`   VARCHAR(128) NOT NULL DEFAULT '' COMMENT '用户电话',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='活动报名';
