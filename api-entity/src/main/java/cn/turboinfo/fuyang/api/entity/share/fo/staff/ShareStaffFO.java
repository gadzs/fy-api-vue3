package cn.turboinfo.fuyang.api.entity.share.fo.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShareStaffFO {

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 展示名称
     */
    private String displayName;

    /**
     * 家政员编码
     */
    private String code;

    /*
     * 身份证
     */
    private String idCard;

    /**
     * 性别
     */
    private GenderType gender;

    /**
     * 年龄
     */
    private Integer age;
    
    /**
     * 联系电话
     */
    private String contactMobile;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 状态
     */
    private StaffStatus status;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 工作经验年数
     */
    private Integer seniority;

    /**
     * 家政员籍贯
     */
    private Long provinceCode;
    private String provinceName;

    /**
     * 家政员类型
     */
    private String staffType;

}
