package cn.turboinfo.fuyang.api.entity.admin.fo.policy;

import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PolicyCreateFO {

    @NotNull(
            message = "策略名称不能为空"
    )
    private String name;

    private String description;

    @NotNull(
            message = "策略类型不能为空"
    )
    private PolicyType policyType;

    private String policyScript;

}
