SET NAMES `utf8mb4`;

CREATE TABLE `user_profile`
(
    `id`           BIGINT    NOT NULL AUTO_INCREMENT COMMENT '',
    `agencies_id`  BIGINT    NOT NULL DEFAULT 0 COMMENT '机构ID',
    `deleted`      BIGINT    NOT NULL DEFAULT 0 COMMENT '',
    `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='用户资料';

CREATE TABLE `user_type_rel`
(
    `id`           BIGINT    NOT NULL AUTO_INCREMENT COMMENT '',
    `user_id`      BIGINT    NOT NULL COMMENT '用户ID',
    `user_type`    SMALLINT  NOT NULL DEFAULT 0 COMMENT '用户类型',
    `object_type`  SMALLINT  NOT NULL DEFAULT 0 COMMENT '关联对象类型',
    `object_id`    BIGINT    NOT NULL DEFAULT 0 COMMENT '关联实体ID',
    `reference_id` BIGINT    NOT NULL DEFAULT 0 COMMENT '引用实体ID',
    `deleted_time` BIGINT    NOT NULL DEFAULT 0 COMMENT '',
    `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_userid_usertype_objecttype_objectid` (`user_id`, `user_type`, `object_type`, `object_id`, `deleted_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='用户类型关联表';

CREATE TABLE `user_login`
(
    `id`               BIGINT      NOT NULL AUTO_INCREMENT,
    `user_id`          BIGINT      NOT NULL,
    `login_name`       VARCHAR(64) NOT NULL,
    `login_name_type`  SMALLINT    NOT NULL DEFAULT 0,
    `login_check_type` SMALLINT    NOT NULL DEFAULT 0,
    `login_check_id`   BIGINT      NOT NULL DEFAULT 0,
    `deleted`          BIGINT      NOT NULL DEFAULT 0,
    `created_time`     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_loginname_loginnametype_loginchecktype_logincheckid` (`login_name`, `login_name_type`,
                                                                         `login_check_type`, `login_check_id`,
                                                                         `deleted`),
    KEY `idx_userid` (`user_id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `user_credential`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT,
    `user_id`      BIGINT       NOT NULL,
    `credential`   VARCHAR(512) NOT NULL,
    `salt`         VARCHAR(512) NOT NULL DEFAULT '',
    `deleted_time` BIGINT       NOT NULL DEFAULT 0,
    `created_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_userid` (`user_id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `user_third_party`
(
    `id`                  BIGINT      NOT NULL AUTO_INCREMENT,
    `user_id`             BIGINT      NOT NULL,
    `third_party_type`    SMALLINT    NOT NULL,
    `third_party_account` VARCHAR(64) NOT NULL,
    `deleted_time`        BIGINT      NOT NULL DEFAULT 0,
    `created_time`        TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`        TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_userid_thirdpartytype` (`user_id`, `third_party_type`, `deleted_time`),
    UNIQUE KEY `uk_thirdpartyaccount_thirdpartytype` (`third_party_account`, `third_party_type`, `deleted_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;
