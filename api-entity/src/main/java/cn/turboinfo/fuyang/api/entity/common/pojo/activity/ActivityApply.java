package cn.turboinfo.fuyang.api.entity.common.pojo.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 活动报名
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ActivityApply extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    @ExcelIgnore
    private Long id;

    /**
     * 活动编码
     */
    @ExcelIgnore
    private Long activityId;

    /**
     * 用户编码
     */
    @ColumnWidth(20)
    @ExcelProperty("用户编码")
    private Long userId;

    /**
     * 用户类型
     */
    @ExcelIgnore
    private UserType userType;

    /**
     * 特殊场景容器
     */
    @ColumnWidth(20)
    @ExcelProperty(value = "用户类型")
    private String userTypeName;
    
    /**
     * 用户姓名
     */
    @ColumnWidth(20)
    @ExcelProperty("用户名称")
    private String userName;

    /**
     * 用户电话
     */
    @ColumnWidth(20)
    @ExcelProperty("联系电话")
    private String useMobile;


    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    @ColumnWidth(20)
    @ExcelProperty("报名时间")
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    @ExcelIgnore
    private LocalDateTime updatedTime;
}
