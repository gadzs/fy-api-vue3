package cn.turboinfo.fuyang.api.domain.common.service.stat;

import cn.turboinfo.fuyang.api.entity.common.pojo.stat.IndexAdmin;
import cn.turboinfo.fuyang.api.entity.common.pojo.stat.IndexPortal;

/**
 * @author gadzs
 * @description index页统计
 * @date 2023/3/7 15:54
 */
public interface IndexStatService {

    void setStatAdminData();

    IndexAdmin getStatAdminData();

    void setStatPortalData();

    IndexPortal getStatPortalData();
}
