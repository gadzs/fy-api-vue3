package cn.turboinfo.fuyang.api.domain.share.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.share.fo.shop.ShareShopFO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 门店列表
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareListShopUseCase extends AbstractUseCase<ShareListShopUseCase.InputData, ShareListShopUseCase.OutputData> {

    private final HousekeepingShopService housekeepingShopService;

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<HousekeepingShop> response = housekeepingShopService.findAll(inputData.request, inputData.getRequestPage());

        List<HousekeepingShop> housekeepingShopList = new ArrayList<>(response.getPagedData());

        Set<Long> divisionCodeSet = housekeepingShopList.stream()
                .flatMap(it -> {
                    List<Long> list = new ArrayList<>();
                    list.add(it.getProvinceCode());
                    list.add(it.getCityCode());
                    list.add(it.getAreaCode());
                    return list.stream();
                })
                .collect(Collectors.toSet());

        Map<Long, Division> divisionMap = divisionService.findByIdCollection(divisionCodeSet)
                .stream()
                .collect(Collectors.toMap(Division::getId, o -> o));

        List<ShareShopFO> foList = housekeepingShopList.stream()
                .map(it -> {
                    ShareShopFO.ShareShopFOBuilder builder = ShareShopFO.builder();
                    BeanHelper.copyPropertiesToBuilder(builder, ShareShopFO.class, it);

                    Long provinceCode = it.getProvinceCode();
                    if (provinceCode != null && divisionMap.containsKey(provinceCode)) {
                        builder.provinceName(divisionMap.get(provinceCode).getAreaName());
                    }
                    Long cityCode = it.getCityCode();
                    if (cityCode != null && divisionMap.containsKey(cityCode)) {
                        builder.cityName(divisionMap.get(cityCode).getAreaName());
                    }
                    Long areaCode = it.getAreaCode();
                    if (areaCode != null && divisionMap.containsKey(areaCode)) {
                        builder.areaName(divisionMap.get(areaCode).getAreaName());
                    }
                    return builder.build();
                })
                .toList();

        QResponse<ShareShopFO> foResponse = new QResponse<>(response.getPage(), response.getPageSize(), foList, response.getTotal());

        return OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private QRequest request;

        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private QResponse<ShareShopFO> qResponse;

    }
}
