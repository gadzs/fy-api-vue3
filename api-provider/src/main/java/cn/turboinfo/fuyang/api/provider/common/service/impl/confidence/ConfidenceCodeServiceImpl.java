package cn.turboinfo.fuyang.api.provider.common.service.impl.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.user.SysUserException;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCodeCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.confidence.ConfidenceCodeDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.confidence.ConfidenceCodePO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ConfidenceCodeServiceImpl extends DefaultQServiceImpl<ConfidenceCode, Long, ConfidenceCodePO, ConfidenceCodeDAO> implements ConfidenceCodeService {

    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public ConfidenceCode save(ConfidenceCodeCreator creator) {
        ConfidenceCodePO confidenceCodePO = new ConfidenceCodePO();
        QBeanCreatorHelper.copyCreatorField(confidenceCodePO, creator);

        // 手动生成ID
        confidenceCodePO.setId(idGeneratorFacade.nextId());
        return convertQBean(dao.save(confidenceCodePO));

    }

    @Override
    @Transactional
    public ConfidenceCode tagging() {
        ConfidenceCodePO confidenceCodePO = new ConfidenceCodePO();

        // 手动生成ID
        confidenceCodePO.setId(idGeneratorFacade.nextId());
        confidenceCodePO.setStatus(ConfidenceCodeStatus.USED);
        return convertQBean(dao.save(confidenceCodePO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws SysUserException {
        dao.deleteById(id);
    }
}

