package cn.turboinfo.fuyang.api.provider.common.repository.database.knowledge;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 知识库
 * author: hai
 */
@Table(
        name = "knowledge_base"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(
        clause = "deleted = 0"
)
public class KnowledgeBasePO extends SoftDeleteFYEntity {
    /**
     * 知识类型
     */
    private String type;

    /**
     * 问题
     */
    private String question;

    /**
     * 答案
     */
    private String answer;
}
