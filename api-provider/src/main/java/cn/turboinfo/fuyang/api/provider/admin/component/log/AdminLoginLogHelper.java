package cn.turboinfo.fuyang.api.provider.admin.component.log;

import cn.turboinfo.fuyang.api.domain.admin.service.log.LoginLogService;
import cn.turboinfo.fuyang.api.provider.admin.component.config.AdminKitConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class AdminLoginLogHelper {

    private final AdminKitConfig adminKitConfig;

    private final LoginLogService loginLogService;

    public void save(String loginSource, String username, Long sysUserId, YesNoStatus loginStatus, String loginRemark) {
        if (adminKitConfig.isLoginLogEnable()) {
            loginLogService.save(loginSource, username, sysUserId, loginStatus, loginRemark);
        }
    }
}
