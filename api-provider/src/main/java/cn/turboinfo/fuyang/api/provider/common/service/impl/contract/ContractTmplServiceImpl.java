package cn.turboinfo.fuyang.api.provider.common.service.impl.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractTmplService;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import cn.turboinfo.fuyang.api.provider.common.repository.database.contract.ContractTmplDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.contract.ContractTmplPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ContractTmplServiceImpl extends DefaultQServiceImpl<ContractTmpl, Long, ContractTmplPO, ContractTmplDAO> implements ContractTmplService {
    @Override
    public List<ContractTmpl> findByCompanyId(Long companyId) {
        return convertQBeanToList(dao.findByCompanyId(companyId));
    }

    @Override
    public List<ContractTmpl> findByCompanyIdCollection(Collection<Long> companyIdCollection) {
        return convertQBeanToList(dao.findByCompanyIdIn(companyIdCollection));
    }
}
