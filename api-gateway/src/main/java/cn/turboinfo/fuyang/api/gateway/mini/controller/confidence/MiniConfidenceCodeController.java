package cn.turboinfo.fuyang.api.gateway.mini.controller.confidence;

import cn.turboinfo.fuyang.api.domain.mini.usecase.confidence.MiniConfidenceCodeQRViewUseCase;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/mini/confidence")
public class MiniConfidenceCodeController {
    private final MiniConfidenceCodeQRViewUseCase confidenceCodeQRViewUseCase;

    /**
     * 显示二维码
     */
    @StaffScope
    @RequestMapping("/viewQRCode")
    public void viewQRCode(MiniRequestContext context, HttpServletResponse response) {
        
        // 配置文件下载
        response.setContentType("application/octet-stream");

        // 下载文件能正常显示中文
        response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(context.getHousekeeperId() + ".jpg", StandardCharsets.UTF_8));

        confidenceCodeQRViewUseCase.execute(MiniConfidenceCodeQRViewUseCase.InputData
                .builder()
                .id(context.getHousekeeperId())
                .imageType("jpg")
                .response(response)
                .build());
    }

}
