package cn.turboinfo.fuyang.api.gateway.mini.controller.my;

import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniCompanyMyServiceOrderListUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyServiceOrderListItem;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@RequiredArgsConstructor
@Controller
@SessionScope
@CompanyScope
@MiniResponseBodyWrapper
@RequestMapping("/mini/company/my")
public class MiniCompanyMyController {


    private final MiniCompanyMyServiceOrderListUseCase miniCompanyMyServiceOrderListUseCase;

    @RequestMapping("/serviceOrderList")
    public LimitDataFO<MiniMyServiceOrderListItem> serviceOrderList(@RequestParam(required = false) ServiceOrderStatus orderStatus, LimitFO page) {

        MiniCompanyMyServiceOrderListUseCase.OutputData outputData = miniCompanyMyServiceOrderListUseCase.execute(MiniCompanyMyServiceOrderListUseCase.InputData
                .builder()
                .orderStatus(orderStatus)
                .userId(MiniRequestContextHolder.current().getUserId())
                .companyId(MiniRequestContextHolder.current().getCompanyId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getOrderList());
    }


}
