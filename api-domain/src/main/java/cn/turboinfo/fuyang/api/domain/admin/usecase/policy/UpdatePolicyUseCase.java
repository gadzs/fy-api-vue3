package cn.turboinfo.fuyang.api.domain.admin.usecase.policy;

import cn.turboinfo.fuyang.api.domain.common.service.policy.PolicyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.fo.policy.PolicyUpdateFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.Policy;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.PolicyUpdater;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 更新策略
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class UpdatePolicyUseCase extends AbstractUseCase<UpdatePolicyUseCase.InputData, UpdatePolicyUseCase.OutputData> {

    private final PolicyService policyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        PolicyUpdateFO updateFO = inputData.getUpdateFO();

        // 保存数据生成记录
        PolicyUpdater.Builder builder = PolicyUpdater.builder(updateFO.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, PolicyUpdater.class, updateFO);
        Policy policy = policyService.update(builder.build());

        return OutputData.builder()
                .policy(policy)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "更新对象不能为空")
        private PolicyUpdateFO updateFO;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private Policy policy;

    }
}
