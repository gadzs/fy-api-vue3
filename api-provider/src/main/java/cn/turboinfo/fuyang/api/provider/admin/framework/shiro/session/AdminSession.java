package cn.turboinfo.fuyang.api.provider.admin.framework.shiro.session;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.login.AdminLoginOperation;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdminSession implements Serializable {

    /**
     * 系统用户ID
     */
    private Long sysUserId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 登录后的操作
     */
    private AdminLoginOperation loginOperation;

    /**
     * 用户类型关系列表
     */
    private List<UserTypeRel> userTypeRelList;

}
