package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface ProductSkuDAO extends FYRepository<ProductSkuPO, Long> {

    List<ProductSkuPO> findByProductIdOrderByIdDesc(Long productId);
}
