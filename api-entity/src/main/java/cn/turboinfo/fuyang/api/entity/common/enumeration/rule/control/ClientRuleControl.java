package cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 客户端规则控制
 */
@Slf4j
public enum ClientRuleControl implements BaseEnum {

    Platform(10, "platform", "客户端平台"),

    VersionCode(15, "versionCode", "数字版本号"),
    ;

    private int value;
    private String property;
    private String name;

    ClientRuleControl(int value, String name) {
        this.value = value;
        this.name = name;
    }

    ClientRuleControl(int value, String property, String name) {
        this.value = value;
        this.property = property;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ClientRuleControl get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ClientRuleControl> list() {
        return BaseEnumHelper.getList(values());
    }

}
