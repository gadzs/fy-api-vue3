package cn.turboinfo.fuyang.api.provider.common.repository.database.confidence.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ConfidenceCodeStatusConverter extends BaseEnumConverter<ConfidenceCodeStatus> {
}
