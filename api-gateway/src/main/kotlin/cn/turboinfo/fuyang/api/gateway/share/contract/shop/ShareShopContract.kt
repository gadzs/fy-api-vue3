package cn.turboinfo.fuyang.api.gateway.share.contract.shop

import cn.turboinfo.fuyang.api.domain.share.usecase.shop.ShareAddShopUseCase
import cn.turboinfo.fuyang.api.domain.share.usecase.shop.ShareViewShopUseCase
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@ShareResponseBodyWrapper
@AutoMappingContract(paths = ["/share/shop"])
interface ShareShopContract {

    // 上传门店
    @AutoMappingContract(
        beanType = ShareAddShopUseCase::class,
    )
    fun add()

    // 门店详情
    @AutoMappingContract(
        beanType = ShareViewShopUseCase::class,
    )
    fun view()

}
