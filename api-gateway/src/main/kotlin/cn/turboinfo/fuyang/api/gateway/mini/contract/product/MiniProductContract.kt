package cn.turboinfo.fuyang.api.gateway.mini.contract.product

import cn.turboinfo.fuyang.api.domain.mini.usecase.product.MiniListProductByNameUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.product.MiniViewProductUserCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/product"])
interface MiniProductContract {

    // 产品详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewProductUserCase::class,
        consumes = [],
        paths = ["/view/{productId}"]
    )
    fun view()

    // 根据名称获取产品
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniListProductByNameUseCase::class,
        consumes = [],
    )
    fun byName()

}
