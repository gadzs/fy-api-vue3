package cn.turboinfo.fuyang.api.domain.mini.service.file;

import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;

public interface MiniFileAttachmentService {

    /**
     * 组装对外访问 url
     *
     * @param fileAttachment
     */
    FileAttachment assembleExternalUrl(FileAttachment fileAttachment);

}
