package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.ThirdPartyType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 用户三方关联表
 */
@Table(
        name = "user_third_party"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
public class UserThirdPartyPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 用户编码
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 三方类型
     */
    @Column(
            name = "third_party_type"
    )
    private ThirdPartyType thirdPartyType;

    /**
     * 三方账号
     */
    @Column(
            name = "third_party_account"
    )
    private String thirdPartyAccount;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
