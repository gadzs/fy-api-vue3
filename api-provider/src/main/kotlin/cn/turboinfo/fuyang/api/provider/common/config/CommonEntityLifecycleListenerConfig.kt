package cn.turboinfo.fuyang.api.provider.common.config

import cn.turboinfo.fuyang.api.provider.common.component.credit.listener.CreditRatingPublishEventListener
import cn.turboinfo.fuyang.api.provider.common.component.custom.listener.ServiceCustomPublishEventListener
import cn.turboinfo.fuyang.api.provider.common.component.order.listener.ServiceOrderPublishEventListener
import cn.turboinfo.fuyang.api.provider.common.component.shop.listener.ShopGeoListener
import cn.turboinfo.fuyang.api.provider.common.repository.database.credit.CreditRatingPO
import cn.turboinfo.fuyang.api.provider.common.repository.database.custom.ServiceCustomPO
import cn.turboinfo.fuyang.api.provider.common.repository.database.order.ServiceOrderPO
import cn.turboinfo.fuyang.api.provider.common.repository.database.shop.HousekeepingShopPO
import nxcloud.foundation.core.data.support.listener.EntityLifecycleListenerRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CommonEntityLifecycleListenerConfig {

    @Bean
    protected fun shopEntityLifecycleListenerRegistrationBean(listener: ShopGeoListener): EntityLifecycleListenerRegistrationBean {
        return EntityLifecycleListenerRegistrationBean(
            type = HousekeepingShopPO::class.java,
            listeners = listOf(listener)
        )
    }

    @Bean
    protected fun creditRatingEntityLifecycleListenerRegistrationBean(listener: CreditRatingPublishEventListener): EntityLifecycleListenerRegistrationBean {
        return EntityLifecycleListenerRegistrationBean(
            type = CreditRatingPO::class.java,
            listeners = listOf(listener)
        )
    }

    @Bean
    protected fun serviceOrderEntityLifecycleListenerRegistrationBean(listener: ServiceOrderPublishEventListener): EntityLifecycleListenerRegistrationBean {
        return EntityLifecycleListenerRegistrationBean(
            type = ServiceOrderPO::class.java,
            listeners = listOf(listener)
        )
    }

    @Bean
    protected fun serviceCustomEntityLifecycleListenerRegistrationBean(listener: ServiceCustomPublishEventListener): EntityLifecycleListenerRegistrationBean {
        return EntityLifecycleListenerRegistrationBean(
            type = ServiceCustomPO::class.java,
            listeners = listOf(listener)
        )
    }

}
