package cn.turboinfo.fuyang.api.provider.common.repository.database.account.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class RelationTypeConverter extends BaseEnumConverter<RelationType> {
}
