package cn.turboinfo.fuyang.api.gateway.admin.contract.sensitive

import cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive.SensitiveWordCreateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive.SensitiveWordDeleteUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive.SensitiveWordUpdateUseCase
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 敏感词管理自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/sensitive"])
@ResponseBodyWrapper
interface SensitiveContract {


    // 敏感词添加
    @CompanyScope
    @PermissionScope(value = ["adminSensitive:create"])
    @AutoMappingContract(
        beanType = SensitiveWordCreateUseCase::class
    )
    fun create()

    // 敏感词信息修改
    @CompanyScope
    @PermissionScope(value = ["adminSensitive:update"])
    @AutoMappingContract(
        beanType = SensitiveWordUpdateUseCase::class
    )
    fun update()

    /**
     * 敏感词删除
     */
    @CompanyScope
    @PermissionScope(value = ["adminSensitive:delete"])
    @AutoMappingContract(
        beanType = SensitiveWordDeleteUseCase::class
    )
    fun delete()

}
