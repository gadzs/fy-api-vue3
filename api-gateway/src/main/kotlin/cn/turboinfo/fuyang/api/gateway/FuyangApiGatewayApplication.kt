package cn.turboinfo.fuyang.api.gateway

import nxcloud.ext.springmvc.automapping.annotation.NXEnableSpringMvcAutoMapping
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * author: sunshow.
 */

@NXEnableSpringMvcAutoMapping
@SpringBootApplication
class FuyangApiGatewayApplication

fun main(args: Array<String>) {
    runApplication<FuyangApiGatewayApplication>(*args)
}