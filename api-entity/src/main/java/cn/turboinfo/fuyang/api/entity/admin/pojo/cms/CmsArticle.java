package cn.turboinfo.fuyang.api.entity.admin.pojo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleStatus;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 文章
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class CmsArticle extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 分类
     */
    private Long categoryId;
    private String categoryName;

    /**
     * 文章类型
     */
    private CmsArticleType type;

    /**
     * 文章状态
     */
    private CmsArticleStatus status;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subtitle;

    /**
     * 作者
     */
    private String author;

    /**
     * 公司
     */
    private String company;

    /**
     * 链接
     */
    private String linkedUrl;

    /**
     * 标题图片
     */
    private Long titleImageId;

    /**
     * 附件
     */
    private Long attachId;
    private String attachName;

    /**
     * 摘要
     */
    private String abstractContent;

    /**
     * 内容
     */
    private String content;

    /**
     * 发布时间
     */
    private LocalDateTime publishedTime;

    /**
     * 访问次数
     */
    private Long visitCount;

    /**
     * 排序
     */
    private Integer sort;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
