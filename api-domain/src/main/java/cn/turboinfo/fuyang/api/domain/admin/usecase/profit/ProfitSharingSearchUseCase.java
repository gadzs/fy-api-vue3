package cn.turboinfo.fuyang.api.domain.admin.usecase.profit;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.profit.ProfitSharingService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharing;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.QProfitSharing;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProfitSharingSearchUseCase extends AbstractUseCase<ProfitSharingSearchUseCase.InputData, ProfitSharingSearchUseCase.OutputData> {
    private final ProfitSharingService profitSharingService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        String companyName = inputData.getCompanyName();
        QRequest request = inputData.getRequest();

        List<HousekeepingCompany> housekeepingCompanyList = new ArrayList<>();

        if (companyId != null && companyId > 0L) {
            request.filterEqual(QProfitSharing.companyId, companyId);
        } else if (companyName != null && !companyName.isEmpty()) {

            housekeepingCompanyList = housekeepingCompanyService.findByName(companyName);

            request.filterIn(QProfitSharing.companyId, housekeepingCompanyList
                    .stream()
                    .map(HousekeepingCompany::getId)
                    .collect(Collectors.toSet()));
        }

        QResponse<ProfitSharing> response = profitSharingService.findAll(request, inputData.getRequestPage());

        if (housekeepingCompanyList.isEmpty()) {
            housekeepingCompanyList = housekeepingCompanyService.findByIdCollection(response.getPagedData()
                    .stream()
                    .map(ProfitSharing::getCompanyId)
                    .collect(Collectors.toSet()))
            ;
        }

        Map<Long, HousekeepingCompany> housekeepingCompanyMap = housekeepingCompanyList
                .stream()
                .collect(Collectors.toMap(HousekeepingCompany::getId, it -> it));

        response.getPagedData().forEach(it -> {
            if (housekeepingCompanyMap.containsKey(it.getCompanyId())) {
                it.setCompanyName(housekeepingCompanyMap.get(it.getCompanyId()).getName());
            }
        });

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private Long companyId;

        private String companyName;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ProfitSharing> qResponse;
    }
}
