package cn.turboinfo.fuyang.api.provider.common.repository.database.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.activity.ActivityAuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 活动管理
 * author: hai
 */
@Table(
        name = "activity"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Where(clause = "deleted = 0")
@Entity
public class ActivityPO extends SoftDeleteFYEntity {
    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动描述
     */
    private String description;

    /**
     * 活动类型
     */
    @Column(
            name = "activity_type"
    )
    private String activityType;

    /**
     * 活动范围
     */
    @Type(type = "json")
    @Column(name = "activity_scope", columnDefinition = "json")
    private List<UserType> activityScope;

    /**
     * 主办单位
     */
    private String organizer;

    /**
     * 活动地点
     */
    private String location;

    /**
     * 已报名
     */
    private Integer applied;

    /**
     * 限制数量
     */
    private Integer limitNum;

    /**
     * 是否开放报名
     */
    @Column(
            name = "open_apply"
    )
    private EnableStatus openApply;

    /**
     * 活动开始时间
     */
    @Column(
            name = "start_time"
    )
    private LocalDateTime startTime;

    /**
     * 活动结束时间
     */
    @Column(
            name = "end_time"
    )
    private LocalDateTime endTime;

    /**
     * 状态
     */
    private EnableStatus status;

    /**
     * 审核状态
     */
    private ActivityAuditStatus auditStatus;

    /**
     * 原因
     */
    private String auditReason;
    
    /**
     * 发布人
     */
    private Long companyId;

}
