package cn.turboinfo.fuyang.api.entity.common.exception.shop;

public class HousekeepingShopException extends RuntimeException {
    public HousekeepingShopException() {
        this("操作 Shop 出错");
    }

    public HousekeepingShopException(String message) {
        super(message);
    }

    public HousekeepingShopException(Throwable cause) {
        super(cause);
    }

    public HousekeepingShopException(String message, Throwable cause) {
        super(message, cause);
    }
}
