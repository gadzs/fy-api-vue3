package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UserTypeRelDAO extends BaseRepository<UserTypeRelPO, Long> {
    List<UserTypeRelPO> findByUserId(Long userId);

    List<UserTypeRelPO> findByUserIdAndUserType(Long userId, UserType userType);

    List<UserTypeRelPO> findByUserType(UserType userType);

    Long countByUserType(UserType userType);

    Optional<UserTypeRelPO> findByUserIdAndUserTypeAndObjectTypeAndObjectId(Long userId, UserType userType, EntityObjectType objectType, Long objectId);

    Optional<UserTypeRelPO> findByUserIdAndUserTypeAndObjectTypeAndObjectIdAndReferenceId(Long userId, UserType userType, EntityObjectType objectType, Long objectId, Long referenceId);

    Long countByUserTypeAndCreatedTimeBetween(UserType userType, LocalDateTime start, LocalDateTime end);
}
