package cn.turboinfo.fuyang.api.provider.common.repository.database.confidence;

import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 放心码
 * author: hai
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "confidence_code"
)
@Data
@Where(clause = "deleted = 0")
@Entity
@DynamicInsert
@DynamicUpdate
public class ConfidenceCodePO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 状态
     */
    private ConfidenceCodeStatus status;
}
