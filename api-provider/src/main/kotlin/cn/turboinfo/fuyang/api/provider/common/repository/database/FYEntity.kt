package cn.turboinfo.fuyang.api.provider.common.repository.database

import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity
import nxcloud.foundation.core.data.jpa.constant.JpaConstants
import nxcloud.foundation.core.data.jpa.entity.*
import org.hibernate.annotations.Filter
import org.hibernate.annotations.FilterDef
import java.time.LocalDateTime
import javax.persistence.MappedSuperclass

interface IFYEntity : BaseEntity

@MappedSuperclass
abstract class EmptyFYEntity : JpaEntity(), IFYEntity

@MappedSuperclass
abstract class FYEntity : DefaultJpaEntity(), IFYEntity

@MappedSuperclass
abstract class SoftDeleteFYEntity : SoftDeleteJpaEntity(), DeletedField, IFYEntity

@MappedSuperclass
@FilterDef(
    name = JpaConstants.FILTER_SOFT_DELETE,
    parameters = [],
)
@Filter(name = JpaConstants.FILTER_SOFT_DELETE, condition = "\$FILTER_PLACEHOLDER\$.deleted = 0")
abstract class SoftDeleteWithoutIdFYEntity : JpaEntity(), CreatedTimeField, UpdatedTimeField, DeletedField, IFYEntity {
    override var deleted: Long = 0

    override var createdTime: LocalDateTime = LocalDateTime.now()

    override var updatedTime: LocalDateTime = LocalDateTime.now()
}


@MappedSuperclass
abstract class WithoutIdFYEntity : JpaEntity(), CreatedTimeField, UpdatedTimeField, IFYEntity {

    override var createdTime: LocalDateTime = LocalDateTime.now()

    override var updatedTime: LocalDateTime = LocalDateTime.now()
}
