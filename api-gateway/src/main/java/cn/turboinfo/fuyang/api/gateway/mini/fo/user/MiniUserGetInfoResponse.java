package cn.turboinfo.fuyang.api.gateway.mini.fo.user;

import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MiniUserGetInfoResponse {

    private List<UserTypeRel> userTypeRelList;

    private SysUser userInfo;

}
