package cn.turboinfo.fuyang.api.provider.common.repository.database.account;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 企业账户
 * author: hai
 */
@Table(
        name = "company_account"
)
@EqualsAndHashCode(
        callSuper = true
)
@Where(clause = "deleted = 0")
@Data
@Entity
public class CompanyAccountPO extends SoftDeleteFYEntity {
    /**
     * 企业ID
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 账户类型
     */
    private AccountType type;

    /**
     * 账户
     */
    private String account;

    /**
     * 账户名称
     */
    private String name;

    /**
     * 关系类型
     */
    @Column(
            name = "relation_type"
    )
    private RelationType relationType;

    /**
     * 自定义关系类型
     */
    @Column(
            name = "custom_relation"
    )
    private String customRelation;
    
    /**
     * 状态
     */
    private AccountStatus status;
}
