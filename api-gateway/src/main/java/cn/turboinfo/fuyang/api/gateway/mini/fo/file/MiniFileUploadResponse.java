package cn.turboinfo.fuyang.api.gateway.mini.fo.file;

import cn.turboinfo.fuyang.api.entity.common.pojo.file.ViewAttachment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MiniFileUploadResponse {

    private ViewAttachment attachment;

}
