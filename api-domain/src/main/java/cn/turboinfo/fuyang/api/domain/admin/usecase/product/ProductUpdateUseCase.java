package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.HtmlHelper;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductSku;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductUpdater;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author gadzs
 * @description 家政企业产品创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductUpdateUseCase extends AbstractUseCase<ProductUpdateUseCase.InputData, ProductUpdateUseCase.OutputData> {
    private final ProductService productService;
    private final ProductAuditRecordService productAuditRecordService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {


        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getName())) {
            throw new ProductException("服务名称包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getMobileContent())) {
            throw new ProductException("内容包含敏感词汇，请修改后重新提交！");
        }

        // 检查服务介绍是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getDescription())) {
            throw new ProductException("服务介绍包含敏感词汇，请修改后重新提交");
        }

        inputData.setMaxPrice(BigDecimal.valueOf(inputData.getProductSkuList().stream().mapToDouble(productSku -> productSku.getPrice().doubleValue()).max().getAsDouble()));
        inputData.setMinPrice(BigDecimal.valueOf(inputData.getProductSkuList().stream().mapToDouble(productSku -> productSku.getPrice().doubleValue()).min().getAsDouble()));

        Product origProduct = productService.getByIdEnsure(inputData.getId());

        ProductUpdater.Builder builder = ProductUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, ProductUpdater.class, inputData);

        if (StringUtils.isNotBlank(inputData.getMobileContent())) {
            builder.withMobileContent(HtmlHelper.safeFilter(inputData.getMobileContent()));
        }

        Product product = productService.update(builder
                .withStatus(ProductStatus.DEFAULT)
                .build());

        if (!origProduct.getStatus().equals(ProductStatus.DEFAULT)) {
            // 创建审核订单
            ProductAuditRecordCreator productAuditRecordCreator = ProductAuditRecordCreator.builder()
                    .withCompanyId(product.getCompanyId())
                    .withProductId(product.getId())
                    .withShopId(product.getShopId())
                    .withAuditStatus(AuditStatus.AGAIN)
                    .withUserId(0L)
                    .withUserName(StringUtils.EMPTY)
                    .withRemark("修改产品服务信息")
                    .build();
            productAuditRecordService.save(productAuditRecordCreator);
        }

        return OutputData.builder()
                .Product(product)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "门店id"
        )
        private Long id;

        /**
         * 公司ID
         */
        private Long companyId;

        /**
         * 店铺ID
         */
        private Long shopId;

        @NotNull(
                message = "分类不能为空"
        )
        private Long categoryId;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        /**
         * 产品状态
         */
        private ProductStatus status;

        /**
         * 介绍
         */
        private String description;

        /**
         * 图片id
         */
        @NotNull(
                message = "图片不能为空"
        )
        private List<Long> imgList;

        /**
         * 视频id
         */
        private List<Long> videoList;

        @NotNull(
                message = "规格组不能为空"
        )
        private Long specSetId;

        @NotNull(
                message = "规格不能为空"
        )
        private List<ProductSku> productSkuList;

        /**
         * 移动端内容详情
         */
        private String mobileContent;

        /**
         * 当前售价(最小)
         */
        private BigDecimal minPrice;

        /**
         * 当前售价（最大）
         */
        private BigDecimal maxPrice;

        /**
         * 是否需要合同
         */
        private YesNoStatus needContract;

        /**
         * 合同模板id
         */
        private Long contractTmplId;

        public List<Long> getImgList() {
            if (imgList == null) {
                imgList = Lists.newArrayList();
            }
            return imgList;
        }

        public List<Long> getVideoList() {
            if (videoList == null) {
                videoList = Lists.newArrayList();
            }
            return videoList;
        }

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Product Product;
    }

}
