package cn.turboinfo.fuyang.api.domain.mini.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityApplyService;
import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityApplyCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

/**
 * 企业活动报名
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniCompanyActivityApplyUseCase extends AbstractUseCase<MiniCompanyActivityApplyUseCase.InputData, MiniCompanyActivityApplyUseCase.OutputData> {
    private final ActivityApplyService activityApplyService;
    private final ActivityService activityService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long activityId = inputData.getActivityId();
        Long userId = inputData.getUserId();
        Long companyId = inputData.getCompanyId();
        LocalDateTime now = LocalDateTime.now();

        Activity activity = activityService.getByIdEnsure(activityId);

        if (activity.getStartTime() != null && activity.getStartTime().isAfter(now)) {
            throw new RuntimeException("活动未开始");
        }

        if (activity.getEndTime() != null && activity.getEndTime().isBefore(now)) {
            throw new RuntimeException("活动已结束");
        }

        if (!activity.getActivityScope().contains(UserType.Company)) {
            throw new RuntimeException("活动不支持企业报名");
        }

        if (activity.getApplied() >= activity.getLimitNum()) {
            throw new RuntimeException("报名人数已满");
        }

        activityApplyService.findByActivityIdAndUserId(activityId, userId, UserType.Company).ifPresent(activityApply -> {
            throw new RuntimeException("已报名，不能重复报名");
        });

        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(companyId);

        ActivityApplyCreator.Builder builder = ActivityApplyCreator.builder()
                .withActivityId(activityId)
                .withUserId(userId)
                .withUserName(company.getName())
                .withUseMobile(company.getContactNumber())
                .withUserType(UserType.Company);

        activityApplyService.save(builder.build());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull
        @Positive
        private Long activityId;

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        @NotNull(message = "家政公司ID不能为空")
        @Positive
        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
