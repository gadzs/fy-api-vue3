package cn.turboinfo.fuyang.api.domain.common.component.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigHelper {

    private final DictConfigItemService dictConfigItemService;

    public Map<Integer, String> getIntegerDict(String dictConfigKey) {
        return dictConfigItemService.findAllDictConfigItem(dictConfigKey)
                .stream()
                .collect(
                        Collectors.toMap(
                                it -> Integer.valueOf(it.getItemValue()),
                                DictConfigItem::getItemName
                        )
                );
    }
}
