package cn.turboinfo.fuyang.api.gateway.admin.contract.account

import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountCreateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountDeleteUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountMarkAuthUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountUpdateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/account"])
interface AccountContract {

    // 添加企业账号
    @PermissionScope(value = ["company:update", "company:update"])
    @CompanyScope
    @AutoMappingContract(
        beanType = CompanyAccountCreateUseCase::class,
    )
    fun create()

    // 修改企业账号
    @PermissionScope(value = ["company:update", "company:update"])
    @CompanyScope
    @AutoMappingContract(
        beanType = CompanyAccountUpdateUseCase::class,
    )
    fun update()

    // 删除企业账号
    @PermissionScope(value = ["adminAccount:Delete", "company:update"])
    @CompanyScope
    @AutoMappingContract(
        beanType = CompanyAccountDeleteUseCase::class,
    )
    fun delete()

    @PermissionScope("adminAccount:update")
    @AutoMappingContract(
        beanType = CompanyAccountMarkAuthUseCase::class,
    )
    fun markAuth()
}
