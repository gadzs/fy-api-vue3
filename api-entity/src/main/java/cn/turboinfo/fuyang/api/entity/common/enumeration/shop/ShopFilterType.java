package cn.turboinfo.fuyang.api.entity.common.enumeration.shop;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 门店筛选方式
 */
@Slf4j
public enum ShopFilterType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "不限"),

    RECENTLY(10, "优质新店"),

    EARLY_BRANDED(20, "品牌老店"),

    ;

    private int value;
    private String name;

    ShopFilterType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ShopFilterType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ShopFilterType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ShopFilterType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
