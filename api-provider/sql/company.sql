DROP TABLE IF EXISTS `housekeeping_company`;
CREATE TABLE `housekeeping_company`
(
    `id`                            BIGINT         NOT NULL AUTO_INCREMENT COMMENT '主键 ID',
    `name`                          VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '公司名称',
    `short_name`                    VARCHAR(20)    NOT NULL DEFAULT '' COMMENT '公司短名',
    `uscc`                          VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '统一社会信用代码',
    `establishment_date`            TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '成立日期',
    `province_code`                 BIGINT         NOT NULL DEFAULT 0 COMMENT '省id',
    `city_code`                     BIGINT         NOT NULL DEFAULT 0 COMMENT '市id',
    `area_code`                     BIGINT         NOT NULL DEFAULT 0 COMMENT '区id',
    `registered_address`            VARCHAR(255)   NOT NULL DEFAULT '' COMMENT '注册地址',
    `registered_capital`            DECIMAL(15, 4) NOT NULL DEFAULT 0 COMMENT '注册资本',
    `company_type`                  VARCHAR(4)     NOT NULL DEFAULT '' COMMENT '公司类型',
    `legal_person`                  VARCHAR(30)    NOT NULL DEFAULT '' COMMENT '法人',
    `legal_person_id_card`          VARCHAR(20)    NOT NULL DEFAULT '' COMMENT '法人身份证',
    `business_registration_number`  VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '工商注册号',
    `business_scope`                VARCHAR(1024)  NOT NULL DEFAULT '' COMMENT '经营范围',
    `contact_person`                VARCHAR(30)    NOT NULL DEFAULT '' COMMENT '联系人',
    `contact_number`                VARCHAR(20)    NOT NULL DEFAULT '' COMMENT '联系电话',
    `email`                         VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '邮箱',
    `employees_number`              VARCHAR(4)     NOT NULL DEFAULT '' COMMENT '员工人数',
    `status`                        TINYINT        NOT NULL DEFAULT 0 COMMENT '企业状态',
    `business_license_file`         bigint(20)     NOT NULL DEFAULT 0 COMMENT '营业执照',
    `legal_person_id_card_file`     bigint(20)     NOT NULL DEFAULT 0 COMMENT '法定代表人身份证',
    `bank_account_certificate_file` bigint(20)     NOT NULL DEFAULT 0 COMMENT '银行开户证明',
    `deleted`                       BIGINT         NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）',
    `credit_score`                  decimal(5, 2)  NOT NULL DEFAULT 0 COMMENT '信用分',
    `review_user_id`                bigint(20)     NOT NULL DEFAULT 0 COMMENT '审核用户',
    `review_time`                   timestamp      NULL     DEFAULT NULL COMMENT '审核时间',
    `shop_num`                      BIGINT         NOT NULL DEFAULT 0 COMMENT '门店数',
    `staff_num`                     BIGINT         NOT NULL DEFAULT 0 COMMENT '家政员数',
    `product_num`                   BIGINT         NOT NULL DEFAULT 0 COMMENT '产品服务数',
    `order_num`                     BIGINT         NOT NULL DEFAULT 0 COMMENT '订单数',
    `auth_label_list`               JSON           NULL COMMENT '认证标签',
    `house_keeping_type`            varchar(10)    NOT NULL DEFAULT '10' COMMENT '家政企业类型',
    `auth_label_weight`             BIGINT         NOT NULL DEFAULT 0 COMMENT '认证权重',
    `app_id`                        varchar(64)    NOT NULL DEFAULT '' COMMENT 'appId',
    `app_secret`                    varchar(128)   NOT NULL DEFAULT '' COMMENT 'appSecret',
    `created_time`                  TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time`                  TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_uscc` (`uscc`, `deleted`),
    UNIQUE KEY `uk_appid` (`app_id`, `deleted`),
    INDEX `idx_creditscore_createdtime` (`credit_score`, `created_time`),
    INDEX `idx_createdtime` (`created_time`),
    INDEX `idx_updatetime` (`updated_time`),
    INDEX `idx_areacode` (`area_code`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic
  AUTO_INCREMENT = 100000 COMMENT ='家政公司';

ALTER TABLE `housekeeping_company`
    CHANGE `app_id` `app_id` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'appId';
ALTER TABLE `housekeeping_company`
    CHANGE `app_secret` `app_secret` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'appSecret';


CREATE TABLE `housekeeping_company_auth_label`
(
    `id`           bigint(20)   NOT NULL AUTO_INCREMENT COMMENT '',
    `name`         varchar(50)  NOT NULL DEFAULT '' COMMENT '名称',
    `icon_id`      bigint(20)   NOT NULL DEFAULT 0 COMMENT '图标',
    `weight`       tinyint(4)            DEFAULT 0 NULL COMMENT '排序权重',
    `description`  varchar(500) NOT NULL DEFAULT '' COMMENT '描述',
    `deleted`      BIGINT       NOT NULL DEFAULT 0 COMMENT '',
    `created_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='家政公司认证标签';

SET
    @companyPermissionParentId = (select id
                                  from sys_permission
                                  where resource = 'company:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('企业认证标签', 'company:authLabel:list', '', '/authLabel/list', '', 1, @companyPermissionParentId,
        'el-icon-collection-tag'),
       ('新增标签', 'company:authLabel:create', '', '', '', 0, @companyPermissionParentId, ''),
       ('更新标签', 'company:authLabel:update', '', '', '', 0, @companyPermissionParentId, '');

DROP TABLE IF EXISTS `housekeeping_company_audit_record`;
CREATE TABLE `housekeeping_company_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='企业审核记录';
