package cn.turboinfo.fuyang.api.domain.common.service.dictconfig;

import cn.turboinfo.fuyang.api.entity.common.exception.dictconfig.DictConfigException;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DictConfigService {
    Optional<DictConfig> getById(Long id);

    DictConfig getByIdEnsure(Long id);

    List<DictConfig> findByIdCollection(Collection<Long> idCollection);

    DictConfig save(DictConfigCreator creator) throws DictConfigException;

    DictConfig update(DictConfigUpdater updater) throws DictConfigException;

    QResponse<DictConfig> searchList(QRequest request, QPage requestPage);

    void deleteById(Long id) throws DictConfigException;

    Optional<DictConfig> getByDictKey(String dictKey);

    DictConfig getByDictKeyEnsure(String dictKey);
}
