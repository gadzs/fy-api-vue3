package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.fo.contract.ContractFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.Contract;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.ViewAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractSearchUseCase extends AbstractUseCase<ContractSearchUseCase.InputData, ContractSearchUseCase.OutputData> {
    private final ContractService contractService;

    private final ServiceOrderService serviceOrderService;

    private final ProductService productService;

    private final HousekeepingStaffService housekeepingStaffService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<Contract> response = contractService.findAll(inputData.getRequest(), inputData.getRequestPage());

        // 查询订单
        Set<Long> orderIdSet = response.getPagedData().stream()
                .map(Contract::getOrderId)
                .collect(Collectors.toSet());

        List<ServiceOrder> serviceOrderList = serviceOrderService.findByIdCollection(orderIdSet);

        Map<Long, ServiceOrder> serviceOrderMap = serviceOrderList
                .stream()
                .collect(Collectors.toMap(ServiceOrder::getId, it -> it));

        // 查询产品
        Set<Long> productIdSet = serviceOrderList.stream()
                .map(ServiceOrder::getProductId)
                .collect(Collectors.toSet());

        List<Product> productList = productService.findByIdCollection(productIdSet);

        Map<Long, Product> productMap = productList.stream()
                .collect(Collectors.toMap(Product::getId, it -> it));

        // 查询家政员
        Set<Long> staffIdSet = serviceOrderList.stream()
                .map(ServiceOrder::getStaffId)
                .collect(Collectors.toSet());
        Map<Long, HousekeepingStaff> staffMap = housekeepingStaffService.findByIdCollection(staffIdSet)
                .stream()
                .collect(Collectors.toMap(HousekeepingStaff::getId, it -> it));

        // 合同文件信息
        Set<Long> contractIdSet = response.getPagedData()
                .stream()
                .map(Contract::getId)
                .collect(Collectors.toSet());
        Map<Long, FileAttachment> fileAttachmentMap = fileAttachmentService.findByRefIdInAndRefType(contractIdSet, FileRefTypeConstant.CONTRACT_FILE)
                .stream()
                .collect(Collectors.toMap(FileAttachment::getRefId, it -> it));

        QResponse<ContractFO> foResponse = new QResponse<>();
        foResponse.setPage(response.getPage());
        foResponse.setTotal(response.getTotal());
        foResponse.setPageSize(response.getPageSize());
        foResponse.setPagedData(response.getPagedData()
                .stream()
                .map(it -> {

                    ContractFO.ContractFOBuilder foBuilder = ContractFO.builder();

                    BeanHelper.copyPropertiesToBuilder(foBuilder, ContractFO.class, it);

                    if (serviceOrderMap.containsKey(it.getOrderId())) {
                        ServiceOrder serviceOrder = serviceOrderMap.get(it.getOrderId());
                        foBuilder.productId(serviceOrder.getProductId())
                                .consumerName(serviceOrder.getContact());


                        if (productMap.containsKey(serviceOrder.getProductId())) {
                            Product product = productMap.get(serviceOrder.getProductId());
                            foBuilder.productName(product.getName());
                        }

                        if (staffMap.containsKey(serviceOrder.getStaffId())) {
                            HousekeepingStaff staff = staffMap.get(serviceOrder.getStaffId());
                            foBuilder.staffName(staff.getName());
                        }
                    }

                    if (fileAttachmentMap.containsKey(it.getId())) {
                        FileAttachment fileAttachment = fileAttachmentMap.get(it.getId());
                        foBuilder.attachment(ViewAttachment
                                .builder()
                                .fileAttachmentId(fileAttachment.getId())
                                .displayName(fileAttachment.getDisplayName())
                                .build());
                    }

                    return foBuilder
                            .build();

                })
                .toList());

        return OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull
        private Long userId;
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ContractFO> qResponse;
    }
}
