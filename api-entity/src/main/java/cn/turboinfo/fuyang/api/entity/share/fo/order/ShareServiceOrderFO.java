package cn.turboinfo.fuyang.api.entity.share.fo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShareServiceOrderFO {

    /**
     * 产品分类ID
     */
    private Long categoryId;
    private String categoryName;

    /**
     * 产品ID
     */
    private Long productId;
    private String productName;

    /**
     * 店铺ID
     */
    private Long shopId;
    private String shopName;

    /**
     * 服务人员ID
     */
    private String staffCode;
    private String staffName;

    // 冗余 SKU 对应规格信息 (树型层级结构)
    private List<String> specNameList;

    /**
     * 下单备注信息
     */
    private String comment;

    /**
     * 计划开始时间
     */
    private LocalDateTime scheduledStartTime;

    /**
     * 计划结束时间
     */
    private LocalDateTime scheduledEndTime;

    /**
     * 服务开始时间
     */
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    private LocalDateTime serviceEndTime;

    /**
     * 服务订单状态
     */
    private ServiceOrderStatus orderStatus;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 预付金额
     */
    private BigDecimal prepaid;

    /**
     * 额外费用 (由服务人员确认完成时添加)
     */
    private BigDecimal additionalFee;

    /**
     * 减免费用 (由服务人员确认完成时添加)
     */
    private BigDecimal discountFee;

    // 最终服务完成后消费者需要支付的金额: price + additionalFee - prepaid - discountFee

    /**
     * 支付订单状态
     */
    private ServiceOrderPayStatus payStatus;

    /**
     * 评价状态
     */
    private ServiceOrderCreditRatingStatus creditRatingStatus;

    /**
     * 服务完成时间
     */
    private LocalDateTime completedTime;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 性别
     */
    private GenderType contactGender;

    /**
     * 手机号
     */
    private String contactMobile;

    /**
     * 区域编码
     */
    private Long divisionId;
    private String divisionName;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

}
