package cn.turboinfo.fuyang.api.domain.mini.usecase.division;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.constant.AreaConstants;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 获取默认地区列表
 *
 * @author yuhai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListDefaultDivisionUseCase extends AbstractUseCase<MiniListDefaultDivisionUseCase.InputData, MiniListDefaultDivisionUseCase.OutputData> {

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<Division> divisionList = divisionService.findByParentId(AreaConstants.DEFAULT_CITY_ID);

        return OutputData.builder()
                .divisionList(divisionList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {


    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 地区列表
         */
        private List<Division> divisionList;

    }
}
