package cn.turboinfo.fuyang.api.provider.common.repository.database.file;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 文件附件
 */
@Data
@Table(
        name = "file_attachment"
)
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
public class FileAttachmentPO implements BaseEntity {
    @Id
    private Long id;

    /**
     * 显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 引用业务类型
     */
    @Column(name = "ref_type")
    private String refType;

    /**
     * 引用业务ID
     */
    @Column(name = "ref_id")
    private Long refId;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 相对路径
     */
    @Column(
            name = "relative_path"
    )
    private String relativePath;

    /**
     * 原始文件名
     */
    @Column(
            name = "original_filename"
    )
    private String originalFilename;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
