package cn.turboinfo.fuyang.api.domain.admin.usecase.policy;

import cn.turboinfo.fuyang.api.domain.common.service.policy.PolicyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.Policy;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 后台所有策略列表
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ListAllPolicyUseCase extends AbstractUseCase<ListAllPolicyUseCase.InputData, ListAllPolicyUseCase.OutputData> {

    private final PolicyService policyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        PolicyType policyType = inputData.getPolicyType();
        if (policyType == null) {
            policyType = PolicyType.ALL;
        }
        List<Policy> policyList = policyService.findAllByType(policyType);

        return OutputData.builder()
                .policyList(policyList)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private PolicyType policyType;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<Policy> policyList;

    }
}
