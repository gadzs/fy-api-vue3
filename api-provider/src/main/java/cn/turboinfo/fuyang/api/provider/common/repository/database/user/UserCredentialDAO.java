package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserCredentialDAO extends BaseRepository<UserCredentialPO, Long> {
    List<UserCredentialPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<UserCredentialPO> findByUserIdOrderByIdDesc(Long userId);

    Optional<UserCredentialPO> findTopByUserIdOrderByIdDesc(Long userId);

    Optional<UserCredentialPO> findByUserId(Long userId);
}
