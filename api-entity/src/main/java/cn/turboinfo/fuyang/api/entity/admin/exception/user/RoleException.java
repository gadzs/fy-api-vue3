package cn.turboinfo.fuyang.api.entity.admin.exception.user;

public class RoleException extends RuntimeException {
    public RoleException() {
        this("操作 Role 出错");
    }

    public RoleException(String message) {
        super(message);
    }

    public RoleException(Throwable cause) {
        super(cause);
    }

    public RoleException(String message, Throwable cause) {
        super(message, cause);
    }
}
