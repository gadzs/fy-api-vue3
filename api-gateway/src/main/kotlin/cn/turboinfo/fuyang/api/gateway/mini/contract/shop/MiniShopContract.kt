package cn.turboinfo.fuyang.api.gateway.mini.contract.shop

import cn.turboinfo.fuyang.api.domain.mini.usecase.shop.MiniListCategoryByShopUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.shop.MiniListShopByNameUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.shop.MiniSearchNearbyShopUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.shop.MiniViewShopUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/shop"])
interface MiniShopContract {

    // 搜索附近门店
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniSearchNearbyShopUseCase::class,
        consumes = [],
    )
    fun searchNearby()


    // 获取门店详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewShopUseCase::class,
        consumes = [],
    )
    fun getById()

    // 获取门店服务类别
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniListCategoryByShopUseCase::class,
        consumes = [],
    )
    fun listCategory()

    // 根据名称获取门店
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniListShopByNameUseCase::class,
        consumes = [],
    )
    fun byName()
}
