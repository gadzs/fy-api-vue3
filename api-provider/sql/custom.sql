DROP TABLE IF EXISTS `service_custom`;
CREATE TABLE `service_custom`
(
    `id`                    BIGINT         NOT NULL AUTO_INCREMENT COMMENT '',
    `user_id`               BIGINT         NOT NULL DEFAULT 0 COMMENT '用户编码',
    `category_id`           BIGINT         NOT NULL DEFAULT 0 COMMENT '服务类型',
    `company_id`            BIGINT         NOT NULL DEFAULT 0 COMMENT '公司ID',
    `shop_id`               BIGINT         NOT NULL DEFAULT 0 COMMENT '店铺ID',
    `staff_id`              BIGINT         NOT NULL DEFAULT 0 COMMENT '服务人员ID',
    `budget`                DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '预算',
    `price`                 DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '订单价格',
    `prepaid`               DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '预付金额',
    `deposit`               DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '定金',
    `additional_fee`        DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '额外费用 (由服务人员确认完成时添加)',
    `discount_fee`          DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '减免费用 (由服务人员确认完成时添加)',
    `address_id`            BIGINT         NOT NULL DEFAULT 0 COMMENT '选择的地址ID',
    `contact`               VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '联系人',
    `gender_type`           TINYINT        NOT NULL DEFAULT 0 COMMENT '性别',
    `mobile`                VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '手机号',
    `division_id`           BIGINT         NOT NULL DEFAULT 0 COMMENT '区域编码',
    `poi_name`              VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '兴趣点地址',
    `detail`                VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '详细地址',
    `people_num`            TINYINT        NOT NULL DEFAULT 0 COMMENT '人数',
    `description`           VARCHAR(128)   NOT NULL DEFAULT '' COMMENT '描述',
    `scheduled_start_time`  DATETIME       NULL COMMENT '计划开始时间',
    `scheduled_end_time`    DATETIME       NULL COMMENT '计划结束时间',
    `service_start_time`    DATETIME       NULL COMMENT '服务开始时间',
    `service_end_time`      DATETIME       NULL COMMENT '服务结束时间',
    `custom_status`         TINYINT        NOT NULL DEFAULT 0 COMMENT '服务订单状态',
    `pay_status`            TINYINT        NOT NULL DEFAULT 0 COMMENT '支付订单状态',
    `refund_status`         TINYINT        NOT NULL DEFAULT 0 COMMENT '退款状态',
    `profit_sharing_status` TINYINT        NOT NULL DEFAULT 0 COMMENT '分账状态',
    `credit_rating_status`  TINYINT        NOT NULL DEFAULT 0 COMMENT '评价状态',
    `completed_time`        DATETIME       NULL COMMENT '服务完成时间',
    `custom_status_history` JSON           NULL COMMENT '服务订单状态历史',
    `deleted`               BIGINT(1)      NOT NULL DEFAULT 0,
    `created_time`          DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`          DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='定制服务';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('定制服务管理', 'serviceCustom:null', '/custom', 1, 0);
SET @customPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('定制服务列表', 'serviceCustom:list', '/custom/list', 1, @customPermissionParentId)
;
