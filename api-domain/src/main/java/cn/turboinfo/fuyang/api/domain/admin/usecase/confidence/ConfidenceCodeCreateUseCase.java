package cn.turboinfo.fuyang.api.domain.admin.usecase.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCodeCreator;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ConfidenceCodeCreateUseCase extends AbstractUseCase<ConfidenceCodeCreateUseCase.InputData, ConfidenceCodeCreateUseCase.OutputData> {
    private final ConfidenceCodeService confidenceCodeService;

    @Override
    protected OutputData doAction(InputData inputData) {

        ConfidenceCodeCreator.Builder builder = ConfidenceCodeCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, ConfidenceCodeCreator.class, inputData);

        ConfidenceCode confidenceCode = confidenceCodeService.save(builder.build());

        return OutputData.builder()
                .confidenceCode(confidenceCode)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "是否使用不能为空"
        )
        private net.sunshow.toolkit.core.base.enums.YesNoStatus used;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private ConfidenceCode confidenceCode;
    }
}
