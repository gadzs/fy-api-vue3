package cn.turboinfo.fuyang.api.gateway.admin.contract.contract

import cn.turboinfo.fuyang.api.domain.admin.usecase.contract.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@AutoMappingContract(paths = ["/admin/contract/template"])
interface ContractTmplContract {

    // 添加合同模版
    @PermissionScope("contractTmpl:create")
    @AutoMappingContract(
        beanType = ContractTmplCreateUseCase::class,
    )
    fun create()

    // 修改合同模版
    @PermissionScope("contractTmpl:update")
    @AutoMappingContract(
        beanType = ContractTmplUpdateUseCase::class,
    )
    fun update()


    @PermissionScope("contractTmpl:delete")
    @AutoMappingContract(
        beanType = ContractTmplDeleteUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/delete/{id}"]
    )
    fun delete()

    @PermissionScope("contractTmpl:list")
    @CompanyScope
    @AutoMappingContract(
        beanType = ContractTmplListByCompanyIdUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun list()

}
