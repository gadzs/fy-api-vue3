package cn.turboinfo.fuyang.api.entity.admin.exception.user;

public class PermissionException extends RuntimeException {
    public PermissionException() {
        this("操作 Permission 出错");
    }

    public PermissionException(String message) {
        super(message);
    }

    public PermissionException(Throwable cause) {
        super(cause);
    }

    public PermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
