package cn.turboinfo.fuyang.api.entity.common.constant;

/**
 * 字典定义常量
 */
public class DictConstants {

    /**
     * 企业类型
     */
    public static final String DICT_KEY_COMPANY_TYPE = "companyType";

    /**
     * 公司规模
     */
    public static final String DICT_KEY_EMPLOYEE_SIZE = "employeeSize";

    /**
     * 家政员类型
     */
    public static final String DICT_KEY_STAFF_TYPE = "staffType";

    /**
     * 家政企业类型
     */
    public static final String DICT_KEY_HOUSEKEEPING_COMPANY_TYPE = "housekeepingCompanyType";

    /**
     * 活动类型
     */
    public static final String DICT_KEY_ACTIVITY_TYPE = "activityType";

}
