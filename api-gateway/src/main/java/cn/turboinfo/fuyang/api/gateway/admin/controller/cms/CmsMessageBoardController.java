package cn.turboinfo.fuyang.api.gateway.admin.controller.cms;


import cn.turboinfo.fuyang.api.domain.admin.service.cms.CmsMessageBoardService;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoard;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardUpdater;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.QCmsMessageBoard;
import cn.turboinfo.fuyang.api.gateway.admin.controller.BaseController;
import cn.turboinfo.fuyang.api.gateway.admin.fo.cms.CmsMessageBoardCreateFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.cms.CmsMessageBoardSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.cms.CmsMessageBoardUpdateFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.provider.admin.component.session.AdminSessionHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * 留言板
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/cms/messageboard")
public class CmsMessageBoardController extends BaseController {
    private final CmsMessageBoardService cmsMessageBoardService;

    private final AdminSessionHelper sessionHelper;

    @RequiresPermissions(value = {"cms:messageBoard:list", "cms:consult:list", "cms:suggest:list"}, logical = Logical.OR)
    @RequestMapping("/list")
    public RestResponseFO list(@Valid CmsMessageBoardSearchFO search, LimitFO page, SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);
        makeBetweenDate(request, QCmsMessageBoard.createdTime, search.getStartDate(), search.getEndDate());
        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QCmsMessageBoard.sort + "|" + QSort.Order.DESC.name(), QCmsMessageBoard.id + "|" + QSort.Order.DESC.name()});
        }
        QResponse<CmsMessageBoard> qresponse = cmsMessageBoardService.findAll(request, page.toQPage(sort.toQSortList()));
        return RestResponseFO.ok(LimitDataFO.fromQResponse(qresponse));
    }

    @RequiresPermissions(value = {"cms:messageBoard:list", "cms:consult:list", "cms:suggest:list"}, logical = Logical.OR)
    @GetMapping("/messageboardTypes")
    public RestResponseFO CmsMessageBoard() {
        return RestResponseFO.ok(CmsMessageBoardType.getList());
    }

    @RequiresPermissions(value = {"cms:messageBoard:list", "cms:consult:list", "cms:suggest:list"}, logical = Logical.OR)
    @GetMapping("/get/{id}")
    public RestResponseFO get(@PathVariable("id") Long id) {
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.getByIdEnsure(id);
        return RestResponseFO.ok(CmsMessageBoard);
    }

    @RequiresPermissions(value = {"cms:messageBoard:create", "cms:consult:create", "cms:suggest:create"}, logical = Logical.OR)
    @PostMapping("/create")
    public RestResponseFO create(@Valid CmsMessageBoardCreateFO fo) {
        CmsMessageBoardCreator.Builder builder = CmsMessageBoardCreator.builder();
        builder.withUserId(sessionHelper.getSession().getSysUserId());
        builder.withUsername(sessionHelper.getSession().getUsername());
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, CmsMessageBoardCreator.class, fo);
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.save(builder.build());
        return RestResponseFO.ok(CmsMessageBoard);
    }

    @RequiresPermissions(value = {"cms:messageBoard:reply", "cms:consult:reply", "cms:suggest:reply"}, logical = Logical.OR)
    @PostMapping("/reply")
    public RestResponseFO reply(@Valid CmsMessageBoardUpdateFO fo) {
        CmsMessageBoardUpdater.Builder builder = CmsMessageBoardUpdater.builder(fo.getId());
        builder.withReplyUserId(sessionHelper.getSession().getSysUserId());
        builder.withReplyUsername(sessionHelper.getSession().getUsername());
        builder.withReplyTime(LocalDateTime.now());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CmsMessageBoardUpdater.class, fo);
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.update(builder.build());
        return RestResponseFO.ok(CmsMessageBoard);
    }

    @RequiresPermissions(value = {"cms:messageBoard:setShow", "cms:consult:setShow", "cms:question:setShow"}, logical = Logical.OR)
    @PostMapping("/setShow")
    public RestResponseFO setShow(@Valid CmsMessageBoardUpdateFO fo) {
        CmsMessageBoardUpdater.Builder builder = CmsMessageBoardUpdater.builder(fo.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CmsMessageBoardUpdater.class, fo);
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.update(builder.build());
        return RestResponseFO.ok(CmsMessageBoard);
    }

    @RequiresPermissions(value = {"cms:messageBoard:update", "cms:consult:update", "cms:suggest:update"}, logical = Logical.OR)
    @PostMapping("/update")
    public RestResponseFO update(@Valid CmsMessageBoardUpdateFO fo) {
        CmsMessageBoardUpdater.Builder builder = CmsMessageBoardUpdater.builder(fo.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CmsMessageBoardUpdater.class, fo);
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.update(builder.build());
        return RestResponseFO.ok(CmsMessageBoard);
    }

    @RequiresPermissions(value = {"cms:messageBoard:delete", "cms:consult:delete", "cms:suggest:delete"}, logical = Logical.OR)
    @PostMapping("/delete/{id}")
    @ResponseBody
    public RestResponseFO delete(@PathVariable("id") Long id) {
        CmsMessageBoard CmsMessageBoard = cmsMessageBoardService.getByIdEnsure(id);
        cmsMessageBoardService.deleteById(CmsMessageBoard.getId());
        return RestResponseFO.ok();
    }
}
