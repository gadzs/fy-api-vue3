package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.handler.staff.StaffDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.site.SiteUrlService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffSearchUseCase extends AbstractUseCase<HousekeepingStaffSearchUseCase.InputData, HousekeepingStaffSearchUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final StaffDataFactory staffDataFactory;

    private final SiteUrlService siteUrlService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<HousekeepingStaff> response = housekeepingStaffService.findAll(inputData.request, inputData.getRequestPage());

        List<HousekeepingStaff> staffList = new ArrayList<>(response.getPagedData());

        // 公司名称
        staffDataFactory.assembleCompany(staffList);

        // 城市名称
        staffDataFactory.assembleCityName(staffList);

        // 身份证照片名称
        staffDataFactory.assembleAttachment(staffList);

        staffList.stream()
                .peek(it -> {
                    if (it.getCode().isBlank()) {
                        return;
                    }
                    it.setQrCodeUrl(siteUrlService.getMainSiteUrl() + "/api/admin/confidence/viewQRCode?id=" + it.getCode() + "&title=" + it.getName() + "&imageType=png");
                })
                .collect(Collectors.toList());

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<HousekeepingStaff> qResponse;
    }
}
