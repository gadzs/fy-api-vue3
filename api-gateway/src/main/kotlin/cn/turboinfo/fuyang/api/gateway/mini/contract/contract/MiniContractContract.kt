package cn.turboinfo.fuyang.api.gateway.mini.contract.contract

import cn.turboinfo.fuyang.api.domain.mini.usecase.contract.MiniContractCreateUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.contract.MiniContractPreviewUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@AutoMappingContract(paths = ["/mini/contract"])
interface MiniContractContract {

    // 生成合同
    @AutoMappingContract(
        beanType = MiniContractCreateUseCase::class,
    )
    fun generate()

    // 合同预览
    @AutoMappingContract(
        beanType = MiniContractPreviewUseCase::class,
        method = AutoMappingContract.Method.GET,
        paths = ["/preview/{orderId}"],
        consumes = []
    )
    fun preview()
}
