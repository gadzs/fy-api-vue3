package cn.turboinfo.fuyang.api.entity.common.pojo.file;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 富文本编辑器
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TinymceFileAttachment {

    private String location;

    private Long id;

}
