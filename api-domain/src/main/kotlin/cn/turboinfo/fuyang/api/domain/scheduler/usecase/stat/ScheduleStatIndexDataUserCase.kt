package cn.turboinfo.fuyang.api.domain.scheduler.usecase.stat

import cn.turboinfo.fuyang.api.domain.common.service.stat.IndexStatService
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase
import org.springframework.stereotype.Component

/**
 * @description
 * @author gadzs 首页统计数据
 * @date 2023/3/7 15:00
 */
@Component
class ScheduleStatIndexDataUserCase(
    private val indexStatService: IndexStatService,
) : AbstractUseCase<ScheduleStatIndexDataUserCase.InputData, ScheduleStatIndexDataUserCase.OutputData>() {

    override fun doAction(input: InputData): OutputData {
        indexStatService.setStatAdminData()
        indexStatService.setStatPortalData()
        return OutputData()
    }

    class InputData : AbstractUseCase.InputData()

    class OutputData : AbstractUseCase.OutputData()
}
