package cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge;

import cn.turboinfo.fuyang.api.domain.common.service.knowledge.KnowledgeBaseService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBaseUpdater;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class KnowledgeBaseUpdateUseCase extends AbstractUseCase<KnowledgeBaseUpdateUseCase.InputData, KnowledgeBaseUpdateUseCase.OutputData> {
    private final KnowledgeBaseService knowledgeBaseService;

    @Override
    protected OutputData doAction(InputData inputData) {

        KnowledgeBaseUpdater.Builder builder = KnowledgeBaseUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, KnowledgeBaseUpdater.class, inputData);

        KnowledgeBase knowledgeBase = knowledgeBaseService.update(builder.build());

        return OutputData.builder()
                .knowledgeBase(knowledgeBase)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "知识类型不能为空"
        )
        private String type;

        @NotBlank(
                message = "问题不能为空"
        )
        private String question;

        @NotBlank(
                message = "答案不能为空"
        )
        private String answer;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private KnowledgeBase knowledgeBase;
    }
}
