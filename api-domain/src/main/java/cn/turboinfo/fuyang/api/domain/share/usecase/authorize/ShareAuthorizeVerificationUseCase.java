package cn.turboinfo.fuyang.api.domain.share.usecase.authorize;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.exception.common.DataNotExistException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

/**
 * 授权验证
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareAuthorizeVerificationUseCase extends AbstractUseCase<ShareAuthorizeVerificationUseCase.InputData, ShareAuthorizeVerificationUseCase.OutputData> {

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        String uscc = inputData.getUscc();
        String appId = inputData.getAppId();
        String appSecret = inputData.getAppSecret();

        HousekeepingCompany company = housekeepingCompanyService.findByUscc(uscc).orElseThrow(DataNotExistException::new);

        return OutputData.builder()
                .company(company)
                .pass(company.getAppId().equals(appId) && company.getAppSecret().equals(appSecret))
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 所属用户 ID
         */
        @NotBlank(message = "企业信用代码不能为空")
        private String uscc;

        @NotBlank(message = "appId不能为空")
        private String appId;

        @NotBlank(message = "appSecret不能为空")
        private String appSecret;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingCompany company;

        private boolean pass;

    }
}
