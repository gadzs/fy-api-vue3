package cn.turboinfo.fuyang.api.provider.common.repository.database.address;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

public interface AddressDAO extends FYRepository<AddressPO, Long> {

}
