-- 2023-06-02 支付订单增加支付金额
ALTER TABLE `pay_order`
    ADD COLUMN `amount` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '支付金额' AFTER `paid_time`;

ALTER TABLE `service_order`
    ADD COLUMN `refund_status` TINYINT NOT NULL DEFAULT 0 COMMENT '退款状态' AFTER `pay_status`;

ALTER TABLE `service_custom`
    ADD COLUMN `refund_status` TINYINT NOT NULL DEFAULT 0 COMMENT '退款状态' AFTER `pay_status`;

-- 2023-06-05 退款订单冗余企业编码
ALTER TABLE `refund_order`
    ADD COLUMN `company_id` BIGINT NOT NULL DEFAULT 0 COMMENT '企业编码' AFTER `id`;

-- 2023-06-12 活动增加审核状态字段
ALTER TABLE `activity`
    ADD COLUMN
        `audit_status` TINYINT NOT NULL DEFAULT 0 COMMENT '审核状态' AFTER `status`;

ALTER TABLE `activity`
    ADD COLUMN
        `audit_reason` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '失败原因' AFTER `audit_status`;

ALTER TABLE `activity`
    ADD COLUMN `company_id` BIGINT NOT NULL DEFAULT 0 COMMENT '发布人编码' AFTER `audit_reason`;

-- 添加活动审核权限
SET
    @activityPermissionParentId = (select id
                                   from sys_permission
                                   where resource = 'activity:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('活动审核', 'activity:audit', '', '', '', 0, @activityPermissionParentId);

-- 2023-06-13 企业增加短名字段
ALTER TABLE `housekeeping_company`
    ADD COLUMN `short_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '公司短名' AFTER `name`;

-- 2023-06-14 规格增加 是否允许删除字段
ALTER TABLE `spec`
    ADD COLUMN `allow_delete` TINYINT NOT NULL DEFAULT 1 COMMENT '是否允许删除' AFTER `sort_value`;

UPDATE `spec` LeFT JOIN (SELECT `spec_id` FROM `product_sku_spec_rel` group by `spec_id`) as `pssr` ON `spec`.`id` = `pssr`.`spec_id`
SET `allow_delete` = IF(`pssr`.`spec_id` is null, 1, 0)
WHERE `spec`.`deleted` = 0;

-- 服务订单增加分账状态
ALTER TABLE `service_order`
    ADD COLUMN `profit_sharing_status` TINYINT NOT NULL DEFAULT 0 COMMENT '分账状态' AFTER `refund_status`;

-- 支付订单增加交易订单号
ALTER TABLE `pay_order`
    ADD COLUMN `transaction_id` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '三方支付订单号' AFTER `amount`;

-- 2023-06-15 定制服务订单增加分账状态
ALTER TABLE `service_custom`
    ADD COLUMN `profit_sharing_status` TINYINT NOT NULL DEFAULT 0 COMMENT '分账状态' AFTER `refund_status`;

-- 分账增加订单类型
ALTER TABLE `profit_sharing`
    CHANGE COLUMN `order_id` `object_id` BIGINT NOT NULL DEFAULT 0 COMMENT '订单编码' AFTER `id`;
ALTER TABLE `profit_sharing`
    ADD COLUMN
        `object_type` TINYINT NOT NULL DEFAULT 0 COMMENT '订单类型' AFTER `object_id`;

ALTER TABLE `profit_sharing`
    ADD INDEX `idx_object_type_object_id` (`object_type`, `object_id`);

ALTER TABLE `profit_sharing`
    ADD INDEX `idx_companyid` (`company_id`);
