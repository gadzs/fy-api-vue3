#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$SCRIPT_DIR"
git pull
./gradlew clean bootJar -Porg.gradle.java.installations.paths=/opt/app/jdk-17.0.2
## deploy gateway
sudo systemctl stop fy-api-gateway.service
cp "$SCRIPT_DIR"/api-gateway/build/libs/api-gateway-1.0-SNAPSHOT.jar /opt/runner/fuyang/fy-api-gateway/app.jar
chown nginx:nginx /opt/runner/fuyang/fy-api-gateway/app.jar
sudo systemctl start fy-api-gateway.service
## deploy scheduler
sudo systemctl stop fy-api-scheduler.service
cp "$SCRIPT_DIR"/api-scheduler/build/libs/api-scheduler-1.0-SNAPSHOT.jar /opt/runner/fuyang/fy-api-scheduler/app.jar
chown nginx:nginx /opt/runner/fuyang/fy-api-scheduler/app.jar
sudo systemctl start fy-api-scheduler.service
