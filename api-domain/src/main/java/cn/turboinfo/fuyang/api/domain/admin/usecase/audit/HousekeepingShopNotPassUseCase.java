package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingShopAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.shop.HousekeepingShopException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 家政门店审核不通过usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopNotPassUseCase extends AbstractUseCase<HousekeepingShopNotPassUseCase.InputData, HousekeepingShopNotPassUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;
    private final HousekeepingShopAuditRecordService housekeepingShopAuditRecordService;
    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val auditorId = inputData.getUserId();

        val housekeepingShopData = housekeepingShopService.getById(inputData.getId()).orElse(null);
        if (housekeepingShopData == null || housekeepingShopData.getStatus().getValue() != ShopStatus.PENDING.getValue()) {
            throw new HousekeepingShopException("数据状态不正确，操作失败");
        }

        HousekeepingShopUpdater.Builder builder = HousekeepingShopUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingShopUpdater.class, inputData);
        builder.withStatus(ShopStatus.NOT_PASS);
        HousekeepingShop housekeepingShop = housekeepingShopService.update(builder.build());

        // 更新审核记录
        {
            val auditorUser = sysUserService.getByIdEnsure(auditorId);

            HousekeepingShopAuditRecordCreator housekeepingShopAuditRecord = HousekeepingShopAuditRecordCreator.builder()
                    .withShopId(housekeepingShop.getId())
                    .withCompanyId(housekeepingShop.getCompanyId())
                    .withAuditStatus(AuditStatus.NOT_PASS)
                    .withUserId(auditorId)
                    .withUserName(auditorUser.getUsername())
                    .withRemark(inputData.getRemark())
                    .build();
            housekeepingShopAuditRecordService.save(housekeepingShopAuditRecord);
        }

        return OutputData.builder()
                .id(housekeepingShop.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "审核人不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "失败原因不能为空"
        )
        private String remark;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
