package cn.turboinfo.fuyang.api.entity.common.fo.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author hai.
 */
@Getter
@Setter
@EqualsAndHashCode
public class HousekeepingStaffImportData {

    /**
     * 员工姓名
     */
    @ExcelProperty("姓名")
    private String name;

    /**
     * 展示名称
     */
    @ExcelProperty("显示名称")
    private String displayName;

    /*
     * 身份证
     */
    @ExcelProperty("身份证号")
    private String idCard;

    /*
     * 身份证
     */
    @ExcelProperty("身份证照片")
    @ExcelIgnore
    private Long idCardFile;

    /**
     * 个人照片
     */
//    @ExcelProperty("个人照片")
    @ExcelIgnore
    private Long photoFile;

    /**
     * 联系电话
     */
    @ExcelProperty("联系电话")
    private String contactMobile;

    /**
     * 介绍
     */
    @ExcelProperty("家政员类型")
    private String introduction;

    /**
     * 参加工作时间
     */
    @ExcelProperty("参加工作时间")
    private LocalDate employmentDate;

    /**
     * 家政员类型
     */
    @ExcelProperty("家政员类型")
    private String staffType;

    /**
     * 家政员编码
     */
    @ExcelIgnore
    private String code;

    /**
     * 公司id
     */
    @ExcelIgnore
    private Long companyId;

    /**
     * 性别
     */
    @ExcelIgnore
    private GenderType gender;

    /**
     * 年龄
     */
    @ExcelIgnore
    private Integer age;

    /**
     * 状态
     */
    @ExcelIgnore
    private StaffStatus status;

    /**
     * 家政员籍贯
     */
    @ExcelIgnore
    private Long provinceCode;
}
