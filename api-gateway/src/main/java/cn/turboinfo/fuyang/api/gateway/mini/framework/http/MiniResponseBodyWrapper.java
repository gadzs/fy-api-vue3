package cn.turboinfo.fuyang.api.gateway.mini.framework.http;

import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@ResponseBody
public @interface MiniResponseBodyWrapper {
}
