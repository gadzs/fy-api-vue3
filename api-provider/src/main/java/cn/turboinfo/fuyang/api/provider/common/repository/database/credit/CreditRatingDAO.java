package cn.turboinfo.fuyang.api.provider.common.repository.database.credit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface CreditRatingDAO extends BaseRepository<CreditRatingPO, Long> {

    List<CreditRatingPO> findByObjectTypeAndObjectIdInOrderByObjectId(EntityObjectType objectType, Collection<Long> objectIdCollection);
    
}
