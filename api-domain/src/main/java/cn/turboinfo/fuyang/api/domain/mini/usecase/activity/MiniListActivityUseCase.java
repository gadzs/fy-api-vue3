package cn.turboinfo.fuyang.api.domain.mini.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.enumeration.activity.ActivityAuditStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 活动列表
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListActivityUseCase extends AbstractUseCase<MiniListActivityUseCase.InputData, MiniListActivityUseCase.OutputData> {
    private final ActivityService activityService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<Activity> activityList = activityService.findByStatus(EnableStatus.ENABLED)
                .stream()
                .filter(activity -> activity.getAuditStatus().equals(ActivityAuditStatus.PASS))
                .toList();

        Set<Long> activityIdSet = activityList.stream()
                .map(Activity::getId)
                .collect(Collectors.toSet());

        Map<Long, List<FileAttachment>> fileAttachmentMap = fileAttachmentService.findByRefIdInAndRefType(activityIdSet, FileRefTypeConstant.ACTIVITY_IMG)
                .stream()
                .collect(Collectors.groupingBy(FileAttachment::getRefId));

        for (Activity activity : activityList) {
            if (fileAttachmentMap.containsKey(activity.getId())) {
                activity.setImageIdList(fileAttachmentMap.get(activity.getId()).stream().map(FileAttachment::getId).toList());
            }
        }

        return OutputData.builder()
                .activityList(activityList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    public static class InputData extends AbstractUseCase.InputData {

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<Activity> activityList;
    }
}
