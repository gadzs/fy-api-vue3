package cn.turboinfo.fuyang.api.gateway.admin.config;

import cn.turboinfo.fuyang.api.provider.admin.framework.shiro.config.DefaultShiroFilterChainDefinitionCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * author: sunshow.
 */
@Configuration
public class AdminInitConfig {

    @Bean
    protected DefaultShiroFilterChainDefinitionCustomizer adminShiroFilterChainDefinitionCustomizer() {
        return (chainDefinition) -> {
            chainDefinition.addPathDefinition("/admin/login/**", "anon");
            chainDefinition.addPathDefinition("/admin/logout", "anon");
            chainDefinition.addPathDefinition("/admin/division/listByParentId", "anon");
            chainDefinition.addPathDefinition("/admin/dictConfig/**/list", "anon");
            chainDefinition.addPathDefinition("/admin/company/register", "anon");
            chainDefinition.addPathDefinition("/admin/company/queryAudit", "anon");
            chainDefinition.addPathDefinition("/admin/**", "authc");

            // 其他路径不管
            chainDefinition.addPathDefinition("/**", "anon");
        };
    }
}
