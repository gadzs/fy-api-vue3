package cn.turboinfo.fuyang.api.scheduler.scheduler.stat

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.stat.SchedulerStatOrderUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 订单统计
 */
@Component
class OrderDataScheduler(
    private val schedulerStatOrderUseCase: SchedulerStatOrderUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(cron = "10 0 */1 * * *") // 每天执行一次
//    @Scheduled(initialDelay = 2000, fixedDelay = 300000)
    fun run() {
        runCatching {
            schedulerStatOrderUseCase.execute(
                SchedulerStatOrderUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "订单统计"
            }
        }
    }

}
