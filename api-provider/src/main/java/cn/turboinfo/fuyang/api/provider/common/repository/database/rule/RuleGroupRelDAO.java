package cn.turboinfo.fuyang.api.provider.common.repository.database.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.List;

public interface RuleGroupRelDAO extends BaseRepository<RuleGroupRelPO, Long> {

    List<RuleGroupRelPO> findByObjectTypeAndObjectIdOrderBySortValueAscRuleGroupIdDesc(EntityObjectType objectType, Long objectId);

}
