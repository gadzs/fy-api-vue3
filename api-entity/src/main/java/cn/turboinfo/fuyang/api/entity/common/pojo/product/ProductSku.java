package cn.turboinfo.fuyang.api.entity.common.pojo.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSkuStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 产品SKU
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ProductSku extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 原始价
     */
    private BigDecimal originalPrice;

    /**
     * 当前售价
     */
    private BigDecimal price;

    // TODO 库存

    // 冗余 SKU 对应规格信息 (树型层级结构)
    private List<Spec> specList;

    /**
     * 产品SKU状态
     */
    private ProductSkuStatus status;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
