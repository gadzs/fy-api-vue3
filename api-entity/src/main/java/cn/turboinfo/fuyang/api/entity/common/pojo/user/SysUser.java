package cn.turboinfo.fuyang.api.entity.common.pojo.user;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.login.AdminLoginOperation;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class SysUser extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 状态
     */
    private EnableStatus status;

    /**
     * 登录后的操作
     */
    private AdminLoginOperation loginOperation;

}
