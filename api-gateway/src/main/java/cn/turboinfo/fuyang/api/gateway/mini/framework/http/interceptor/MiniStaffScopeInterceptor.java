package cn.turboinfo.fuyang.api.gateway.mini.framework.http.interceptor;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor.BaseHandlerInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nxcloud.ext.springmvc.automapping.spring.AutoMappingRequestParameterTypeBinding;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * 家政员拦截
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniStaffScopeInterceptor extends BaseHandlerInterceptorAdapter {

    private final AutoMappingRequestParameterTypeBinding autoMappingRequestParameterTypeBinding;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;

            MiniRequestContext requestContext = MiniRequestContextHolder.current();
            if (requestContext.isLogin()) {
                Optional<StaffScope> staffScopeOptional = getAnnotation(handlerMethod, StaffScope.class, true);
                StaffScope annotation = staffScopeOptional.orElseGet(() -> autoMappingRequestParameterTypeBinding.getAnnotation(handlerMethod.getMethod(), StaffScope.class, true));

                if (annotation != null) {
                    if (!StringUtils.equals(request.getHeader(MiniRestConstants.HEADER_USER_TYPE), String.valueOf(UserType.Staff.getValue()))) {
                        throw new RuntimeException("当前请求用户类型不正确");
                    }

                    try {
                        long staffId = Long.parseLong(request.getHeader(MiniRestConstants.HEADER_USER_TYPE_OBJECT));
                        if (staffId <= 0) {
                            throw new RuntimeException("非法请求的家政员编码");
                        }

                        Optional<UserTypeRel> userTypeRelOptional = requestContext.getUserTypeRelList()
                                .stream()
                                .filter(userTypeRel -> userTypeRel.getUserType() == UserType.Staff)
                                .filter(userTypeRel -> userTypeRel.getObjectId() == staffId)
                                .findAny();

                        if (userTypeRelOptional.isEmpty()) {
                            throw new RuntimeException("当前请求用户不是家政员用户");
                        }

                        // TODO: 确定公司和家政员关系表之后，拿到家政公司编码
//                        requestContext.setCompanyId();
                        requestContext.setUserType(UserType.Staff);
                        requestContext.setHousekeeperId(staffId);
                    } catch (NumberFormatException e) {
                        throw new RuntimeException("请求的家政员编码不正确");
                    }
                }
            }
        }
        return true;
    }

}
