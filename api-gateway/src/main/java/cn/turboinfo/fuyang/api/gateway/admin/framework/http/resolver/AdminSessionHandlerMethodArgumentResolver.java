package cn.turboinfo.fuyang.api.gateway.admin.framework.http.resolver;

import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContextHolder;
import cn.turboinfo.fuyang.api.provider.admin.framework.shiro.session.AdminSession;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class AdminSessionHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return AdminSession.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return AdminRequestContextHolder.current().getSession();
    }

}
