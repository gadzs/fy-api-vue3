package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;

/**
 * 栏目查询FO
 */
@Data
@QField
public class PortalCategorySearchFO {
    /**
     * 栏目父级Id
     */
    @QField
    private Long parentId;

    @QField(
            searchable = false
    )
    private String parentCode;

    /**
     * 是否首页展示
     */
    @QField
    private Integer indexShow;

    /**
     * 栏目编码
     */
    @QField
    private String code;
}
