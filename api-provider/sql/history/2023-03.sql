-- 2023-03-06
ALTER TABLE `housekeeping_company`
    ADD COLUMN `auth_label_list`   json       NULL COMMENT '认证标签' AFTER `order_num`,
    ADD COLUMN `auth_label_weight` bigint(20) NOT NULL DEFAULT 0 COMMENT '认证标签权重' AFTER `auth_label_list`;


-- 2023-03-07 初始化栏目
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (2, '新闻资讯', 'XWZX', '', 0, 1, 100, '0', '', 0, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (3, '政策法规', 'ZCFG', '', 0, 1, 80, '0', '', 0, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (4, '家政风采', 'JZFC', '', 0, 1, 60, '0', '', 0, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (5, '首页轮播', 'LOOP', '', 0, 1, 0, '0', '', 0, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (6, '行业动态', 'HYDT', '', 0, 1, 100, '0', '', 2, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (7, '本地咨询', 'BDZX', '', 0, 1, 80, '0', '', 2, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (8, '他山之石', 'TSZS', '', 0, 1, 60, '0', '', 2, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (9, '通知公告', 'TZGG', '', 0, 1, 40, '0', '', 2, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (10, '线下活动', 'OFFLINE', '', 0, 1, 40, '0', '', 4, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (11, '线上培训', 'ONLINE', '', 0, 1, 40, '0', '', 4, 0);
INSERT INTO `cms_category`(`id`, `name`, `code`, `description`, `type`, `index_show`, `sort`, `slug_name`, `linked_url`,
                           `parent_id`, `deleted_time`)
VALUES (12, '政策文件', 'ZCWJ', '', 0, 1, 40, '0', '', 3, 0);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('管理员首页数据统计', 'index:stat:admin', '', '', '', 0, 0);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('公司首页数据统计', 'index:stat:company', '', '', '', 0, 0);


-- 2023-03-22 放心码管理
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('放心码管理', 'adminConfidence:null', '', '/confidence', '', 1, 0);
SET
    @staffPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('放心码列表', 'adminConfidence:list', '', '/confidence/list', '', 1, @staffPermissionParentId),
       ('生成放心码', 'adminConfidence:generate', '', '', '', 0, @staffPermissionParentId),
       ('修改放心码', 'adminConfidence:update', '', '', '', 0, @staffPermissionParentId),
       ('删除放心码', 'adminConfidence:delete', '', '', '', 0, @staffPermissionParentId);
