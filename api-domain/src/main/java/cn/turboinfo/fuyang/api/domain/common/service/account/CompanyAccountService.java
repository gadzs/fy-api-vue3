package cn.turboinfo.fuyang.api.domain.common.service.account;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;
import java.util.Optional;

/**
 * 企业账户服务
 * author: hai
 */
public interface CompanyAccountService extends BaseQService<CompanyAccount, Long> {

    /**
     * 根据企业ID和账户类型获取企业账户
     *
     * @param companyId 企业ID
     * @param type      账户类型
     * @return 企业账户
     */
    Optional<CompanyAccount> getByCompanyIdAndType(Long companyId, AccountType type);

    /**
     * 根据企业ID获取企业账户
     *
     * @param companyId 企业ID
     * @return 企业账户
     */
    List<CompanyAccount> findByCompanyId(Long companyId);
}
