package cn.turboinfo.fuyang.api.entity.common.pojo.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 家政员
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class HousekeepingStaff extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 展示名称
     */
    private String displayName;

    /**
     * 家政员编码
     */
    private String code;
    private String qrCodeUrl;

    /**
     * 公司id
     */
    private Long companyId;
    private String companyName;

    /*
     * 身份证
     */
    private String idCard;

    /**
     * 身份证附件
     */
    private Long idCardFile;
    private String idCardFileName;

    /**
     * 性别
     */
    private GenderType gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 个人照片
     */
    private Long photoFile;

    /**
     * 联系电话
     */
    private String contactMobile;

    private String contactMobileEncrypt;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 状态
     */
    private StaffStatus status;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 工作经验年数
     */
    private Integer seniority;

    /**
     * 家政员籍贯
     */
    private Long provinceCode;
    private String provinceName;

    /**
     * 家政员类型
     */
    private String staffType;


    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
