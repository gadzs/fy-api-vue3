package cn.turboinfo.fuyang.api.provider.common.repository.database.activity;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;
import net.sunshow.toolkit.core.base.enums.EnableStatus;

import java.util.List;

public interface ActivityDAO extends FYRepository<ActivityPO, Long> {

    List<ActivityPO> findByStatusOrderByIdDesc(EnableStatus status);

}
