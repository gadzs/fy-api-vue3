package cn.turboinfo.fuyang.api.domain.common.service.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * 定制服务服务
 * author: hai
 */
public interface ServiceCustomService extends BaseQService<ServiceCustom, Long> {

    void receive(Long customId, Long companyId, BigDecimal price, BigDecimal deposit);

    /**
     * 查找服务人员的历史订单
     */
    List<ServiceCustom> findByStaffId(Long staffId, ServiceCustomStatus status);

    /**
     * 查找家政公司的历史订单
     */
    List<ServiceCustom> findByCompanyId(Long companyId, ServiceCustomStatus status);

    List<ServiceCustom> findByCompanyId(Long companyId, Collection<ServiceCustomStatus> statusCollection);

    /**
     * 查找家政门店的历史订单
     */
    List<ServiceCustom> findByShopId(Long shopId, ServiceCustomStatus status);

    /**
     * 消费者取消订单
     */
    void consumerCancel(Long serviceCustomId);

    /**
     * 家政公司取消订单
     */
    void companyCancel(Long serviceCustomId);

    /**
     * 服务人员取消订单
     */
    void staffCancel(Long serviceCustomId);

    /**
     * 标记下单时的服务订单为后付费, 并更新状态
     */
    void submitCustomPostPaid(Long serviceCustomId, Long payOrderId);

    /**
     * 完成定金支付定金, 并更新状态
     */
    void submitCustomDepositPaid(Long serviceCustomId, Long payOrderId, BigDecimal paidAmount, LocalDateTime successTime);

    /**
     * 分配服务人员
     */
    void dispatchToStaff(Long serviceCustomId, Long staffId);

    /**
     * 查找用户订单
     */
    List<ServiceCustom> findByUserIdAndStaffId(Long userId, Long staffId, ServiceCustomStatus status);

    /**
     * 标记服务订单开始服务
     */
    void startService(Long serviceCustomId, Long staffId);

    /**
     * 服务人员确认服务完成
     */
    ServiceCustom staffConfirmServiceCompleted(Long serviceCustomId, BigDecimal additionalFee, BigDecimal discountFee);

    /**
     * 服务人员确认
     */
    void staffConfirm(Long serviceCustomId);

    /**
     * 服务人员拒绝
     */
    void staffReject(Long serviceCustomId);

    /**
     * 完成订单剩余未支付金额, 并更新状态
     */
    void payRemainingUnpaid(Long serviceCustomId, Long payOrderId);

    /**
     * 提交退款状态
     *
     * @param serviceCustomId 服务订单ID
     * @param status          退款状态
     */
    void refreshRefundStatus(Long serviceCustomId, ServiceOrderRefundStatus status);
}
