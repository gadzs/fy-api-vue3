package cn.turboinfo.fuyang.api.domain.common.service.activity;

import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 活动管理服务
 * author: hai
 */
public interface ActivityService extends BaseQService<Activity, Long> {

    List<Activity> findByStatus(EnableStatus atatus);

}
