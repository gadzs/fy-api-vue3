package cn.turboinfo.fuyang.api.domain.admin.service.permission;

import cn.turboinfo.fuyang.api.entity.admin.pojo.permission.Permission;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PermissionService extends BaseQService<Permission, Long> {

    Optional<Permission> getByResource(String resource);

    List<Permission> findByResourceIn(Collection<String> resourceCollection);

    List<Permission> findByParentId(Long parentId);

}
