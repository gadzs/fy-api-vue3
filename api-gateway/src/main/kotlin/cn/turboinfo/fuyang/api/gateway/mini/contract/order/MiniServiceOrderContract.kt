package cn.turboinfo.fuyang.api.gateway.mini.contract.order

import cn.turboinfo.fuyang.api.domain.mini.usecase.order.MiniViewServiceOrderUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/order"])
interface MiniServiceOrderContract {


    // 订单详情
    @AutoMappingContract(
        beanType = MiniViewServiceOrderUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = []
    )
    fun view()

}
