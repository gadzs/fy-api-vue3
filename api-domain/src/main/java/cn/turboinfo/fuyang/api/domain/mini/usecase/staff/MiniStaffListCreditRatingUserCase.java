package cn.turboinfo.fuyang.api.domain.mini.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.handler.staff.StaffDataFactory;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.fo.credit.ViewCreditRatingFO;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.ServiceOrderCreditRating;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import static cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper.ASSEMBLE_PRODUCT;
import static cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper.ASSEMBLE_USER;

/**
 * 家政服务人员评价列表
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniStaffListCreditRatingUserCase extends AbstractUseCase<MiniStaffListCreditRatingUserCase.InputData, MiniStaffListCreditRatingUserCase.OutputData> {

    private final CreditRatingAssembleHelper creditRatingAssembleHelper;
    private final ServiceOrderDataFactory serviceOrderDataFactory;
    private final StaffDataFactory staffDataFactory;
    private final ShopDataFactory shopDataFactory;

    @Override
    protected MiniStaffListCreditRatingUserCase.OutputData doAction(MiniStaffListCreditRatingUserCase.InputData inputData) {
        Long staffId = inputData.getStaffId();
        QPage requestPage = inputData.getRequestPage();

        // 查询更多评价
        QResponse<ServiceOrderCreditRating> ratingQResponse = creditRatingAssembleHelper.findLatestPageable(
                EntityObjectType.STAFF, staffId, requestPage.getPageIndex(), requestPage.getPageSize(), ASSEMBLE_PRODUCT + ASSEMBLE_USER);

        QResponse<ViewCreditRatingFO> qResponse = ratingQResponse
                .map(rating -> {
                            ViewCreditRatingFO fo = BeanMapper.map(rating, ViewCreditRatingFO.class);

                            serviceOrderDataFactory.maskInfo(rating.getServiceOrder());

                            fo.setServiceOrder(BeanMapper.map(rating.getServiceOrder(), ViewServiceOrderFO.class));

                            staffDataFactory.maskInfo(rating.getStaff());

                            fo.setStaff(BeanMapper.map(rating.getStaff(), ViewStaffFO.class));

                            shopDataFactory.maskInfo(rating.getShop());
                            fo.setShop(BeanMapper.map(rating.getShop(), ViewShopFO.class));

                            return fo;
                        }
                );

        return OutputData.builder()
                .qResponse(qResponse)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "家政服务人员ID不能为空"
        )
        @Positive
        private Long staffId;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private QResponse<ViewCreditRatingFO> qResponse;

    }
}
