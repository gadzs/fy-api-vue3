package cn.turboinfo.fuyang.api.entity.common.exception

import nxcloud.foundation.core.lang.exception.NXRuntimeException

open class FYRuntimeException(
    message: String,
    cause: Throwable? = null
) : NXRuntimeException(message, cause) {
    constructor(cause: Throwable) : this(cause.message ?: "运行时异常", cause)

    constructor(message: String) : this(message, null)
}