package cn.turboinfo.fuyang.api.gateway.web.config;

import cn.turboinfo.fuyang.api.provider.framework.spring.ConverterCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * author: sunshow.
 */
@Configuration
@Import(WebConverterBeanDefinitionRegistrar.class)
public class DefaultWebConfig implements WebMvcConfigurer {

    @Autowired(required = false)
    private List<ConverterCollection> converterCollectionList;
    
    @Override
    public void addFormatters(FormatterRegistry registry) {
        if (converterCollectionList != null) {
            for (ConverterCollection collection : converterCollectionList) {
                for (Converter<?, ?> converter : collection.getCollection()) {
                    registry.addConverter(converter);
                }
            }
        }
    }

}
