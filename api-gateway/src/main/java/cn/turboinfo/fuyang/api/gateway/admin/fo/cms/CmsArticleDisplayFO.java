package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategory;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CmsArticleDisplayFO {

    private CmsArticle article;

    private List<FileAttachment> attachmentList;

    private CmsCategory category;

}
