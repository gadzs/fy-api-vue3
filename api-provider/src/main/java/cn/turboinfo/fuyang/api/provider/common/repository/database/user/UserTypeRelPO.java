package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 用户类型关联表
 */
@Table(
        name = "user_type_rel"
)
@Entity
@Where(clause = "deleted_time = 0")
@DynamicInsert
@DynamicUpdate
@Data
public class UserTypeRelPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 用户编码
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 用户类型
     */
    @Column(name = "user_type")
    private UserType userType;

    /**
     * 关联实体类型
     */
    @Column(name = "object_type")
    private EntityObjectType objectType;

    /**
     * 关联实体ID
     */
    @Column(name = "object_id")
    private Long objectId;

    /**
     * 引用的ID, 例如学生家长类型时引用ID为学生ID, 业务自行判断
     */
    @Column(name = "reference_id")
    private Long referenceId;

    @Column(name = "deleted_time")
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
