package cn.turboinfo.fuyang.api.gateway.admin.framework.http.interceptor

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContextHolder
import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor.BaseHandlerInterceptorAdapter
import mu.KotlinLogging
import nxcloud.ext.springmvc.automapping.spring.AutoMappingRequestParameterTypeBinding
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.jvm.optionals.getOrNull

/**
 * 家政人员拦截器
 */
@Component
class AdminStaffScopeInterceptor(
    private val autoMappingRequestParameterTypeBinding: AutoMappingRequestParameterTypeBinding,
) : BaseHandlerInterceptorAdapter() {
    private val logger = KotlinLogging.logger {}

    @OptIn(ExperimentalStdlibApi::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        if (handler is HandlerMethod) {
            val requestContext = AdminRequestContextHolder.current()

            if (requestContext.isLogin) {

                getAnnotation(
                    handler,
                    StaffScope::class.java,
                    true
                ).getOrNull()
                    ?: autoMappingRequestParameterTypeBinding.getAnnotation(
                        handler.method,
                        StaffScope::class.java,
                        true
                    )
                    ?: return true

                if (!StringUtils.equals(
                        request.getHeader(MiniRestConstants.HEADER_USER_TYPE),
                        UserType.Staff.value.toString()
                    )
                ) {
                    throw RuntimeException("当前请求用户类型不正确")
                }

                try {


                    if (requestContext.session.userTypeRelList
                            .stream()
                            .noneMatch { userTypeRel -> userTypeRel.userType === UserType.Staff }) {
                        throw RuntimeException("当前请求用户不是平台用户")
                    }

                    // 设置会话中的家政员ID
                    requestContext.staffId = requestContext.session.userTypeRelList.stream()
                        .filter { userTypeRel -> userTypeRel.userType === UserType.Staff }
                        .map(UserTypeRel::getObjectId)
                        .findFirst().orElse(null)

                    // 设置会话中的家政公司ID - 家政员绑定的公司
                    requestContext.companyId = requestContext.session.userTypeRelList.stream()
                        .filter { userTypeRel -> userTypeRel.userType === UserType.Staff }
                        .map(UserTypeRel::getReferenceId)
                        .findFirst().orElse(null)


                } catch (e: NumberFormatException) {
                    throw RuntimeException("请求的家政员编码不正确")
                }
            }
        }

        return true
    }
}