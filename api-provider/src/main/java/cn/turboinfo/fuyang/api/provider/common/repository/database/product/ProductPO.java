package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 产品
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "product"
)
@Where(clause = "deleted = 0")
@Entity
@DynamicUpdate
@DynamicInsert
public class ProductPO extends SoftDeleteFYEntity {

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 分类id
     */
    private Long categoryId;

    /**
     * 名称
     */
    private String name;

    /**
     * 产品状态
     */
    private ProductStatus status;

    /**
     * 介绍
     */
    private String description;

    /**
     * 移动端内容详情
     */
    private String mobileContent;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 当前售价(最小)
     */
    private BigDecimal minPrice;

    /**
     * 当前售价（最大）
     */
    private BigDecimal maxPrice;

    /**
     * 是否需要合同
     */
    private YesNoStatus needContract;
    
    /**
     * 合同模板id
     */
    private Long contractTmplId;
}
