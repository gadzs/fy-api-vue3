package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.EnableStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SysUserUpdateFO {
    @NotNull(
            message = "id不能为空"
    )
    private Long id;

    @NotBlank(
            message = "用户名不能为空"
    )
    private String username;

    private String mobile;

    @NotNull(
            message = "状态不能为空"
    )
    private EnableStatus status;

    /**
     * 机构编码
     */
    private Long agenciesId;
}
