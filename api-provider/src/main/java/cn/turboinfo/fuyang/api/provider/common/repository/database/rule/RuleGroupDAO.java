package cn.turboinfo.fuyang.api.provider.common.repository.database.rule;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RuleGroupDAO extends BaseRepository<RuleGroupPO, Long> {

    List<RuleGroupPO> findByIdIn(Collection<Long> idCollection);

    Optional<RuleGroupPO> findByRuleKey(String ruleKey);

}
