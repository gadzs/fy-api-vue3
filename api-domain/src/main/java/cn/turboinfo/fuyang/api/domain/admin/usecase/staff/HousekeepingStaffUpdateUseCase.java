package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.staff.HousekeepingStaffException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffUpdater;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author gadzs
 * @description 家政企业创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffUpdateUseCase extends AbstractUseCase<HousekeepingStaffUpdateUseCase.InputData, HousekeepingStaffUpdateUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final FileAttachmentService fileAttachmentService;
    private final HousekeepingStaffAuditRecordService housekeepingStaffAuditRecordService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {


        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getDisplayName())) {
            throw new HousekeepingStaffException("展示名称包含敏感词汇，请修改后重新提交！");
        }

        // 检查家政员介绍是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getIntroduction())) {
            throw new HousekeepingStaffException("家政员介绍包含敏感词汇，请修改后重新提交");
        }


        HousekeepingStaff origStaff = housekeepingStaffService.getByIdEnsure(inputData.getId());

        HousekeepingStaffUpdater.Builder builder = HousekeepingStaffUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingStaffUpdater.class, inputData);
        HousekeepingStaff housekeepingStaff = housekeepingStaffService.update(builder
                .withStatus(StaffStatus.DEFAULT)
                .build());

        //删除原附件
        List<Long> uploadFileList = Lists.newArrayList(inputData.getIdCardFile(), inputData.getPhotoFile());
        Map<Long, List<Long>> uploadFileMap = uploadFileList.stream().collect(Collectors.groupingBy(a -> a));

        List<Long> exitsFileIdList = fileAttachmentService.findByRefAndRefType(inputData.getId(), List.of(FileRefTypeConstant.STAFF_IMG, FileRefTypeConstant.STAFF_ATTACH))
                .stream()
                .map(FileAttachment::getId)
                .toList();

        List<Long> deleteFileList = exitsFileIdList.stream().filter(fileId -> !uploadFileMap.containsKey(fileId)).toList();
        if (!deleteFileList.isEmpty()) {
            fileAttachmentService.deleteByIdCollection(deleteFileList);
        }
        fileAttachmentService.updateRefId(uploadFileList, inputData.getId());
        if (origStaff.getStatus().equals(StaffStatus.PUBLISHED)) {
            // 创建审核订单
            HousekeepingStaffAuditRecordCreator housekeepingStaffAuditRecordCreator = HousekeepingStaffAuditRecordCreator.builder()
                    .withCompanyId(housekeepingStaff.getCompanyId())
                    .withStaffId(housekeepingStaff.getId())
                    .withAuditStatus(AuditStatus.AGAIN)
                    .withUserId(0L)
                    .withUserName(StringUtils.EMPTY)
                    .withRemark("修改家政员信息")
                    .build();
            housekeepingStaffAuditRecordService.save(housekeepingStaffAuditRecordCreator);
        }
        return OutputData.builder()
                .housekeepingStaff(housekeepingStaff)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "家政员id"
        )
        private Long id;

        @NotNull(
                message = "公司编码不能为空"
        )
        private Long companyId;

        @NotBlank(
                message = "展示名称不能为空"
        )
        private String displayName;


        @NotNull(
                message = "身份证附件不能为空"
        )
        private Long idCardFile;

        @NotNull(
                message = "个人照片不能为空"
        )
        private Long photoFile;

        @NotBlank(
                message = "个人介绍不能为空"
        )
        private String introduction;

        @NotNull(
                message = "性别不能为空"
        )
        private GenderType gender;

        @NotNull(
                message = "参加工作时间不能为空"
        )
        private LocalDate employmentDate;

        @NotNull(
                message = "参加工作时间不能为空"
        )
        private Integer seniority;

        /**
         * 家政员籍贯
         */
        private Long provinceCode;

        @NotBlank(
                message = "家政员类型不能为空"
        )
        private String staffType;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingStaff housekeepingStaff;
    }

}
