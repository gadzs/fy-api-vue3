package cn.turboinfo.fuyang.api.provider.common.service.impl.rule;

import cn.turboinfo.fuyang.api.domain.common.service.rule.RuleGroupRelService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroupRel;
import cn.turboinfo.fuyang.api.provider.common.repository.database.rule.RuleGroupRelDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.AbstractQServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class RuleGroupRelServiceImpl extends AbstractQServiceImpl<RuleGroupRel> implements RuleGroupRelService {

    private final RuleGroupRelDAO ruleGroupRelDAO;

    @Override
    public List<RuleGroupRel> findByObjectId(EntityObjectType objectType, Long objectId) {
        return convertStreamQBeanToList(ruleGroupRelDAO.findByObjectTypeAndObjectIdOrderBySortValueAscRuleGroupIdDesc(objectType, objectId).stream());
    }

}
