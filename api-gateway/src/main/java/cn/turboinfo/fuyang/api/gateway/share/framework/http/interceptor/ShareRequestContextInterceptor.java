package cn.turboinfo.fuyang.api.gateway.share.framework.http.interceptor;

import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor.BaseHandlerInterceptorAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class ShareRequestContextInterceptor extends BaseHandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("收到请求: {}", request.getRequestURI());
        ShareRequestContextHolder.reset();
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        ShareRequestContextHolder.reset();
        log.debug("结束请求: {}", request.getRequestURI());
    }

}
