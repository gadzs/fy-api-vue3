package cn.turboinfo.fuyang.api.provider.admin.repository.database.log;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface OperationLogDAO extends BaseRepository<OperationLogPO, Long> {
    List<OperationLogPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);
}
