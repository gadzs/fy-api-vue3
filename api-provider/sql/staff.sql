DROP TABLE IF EXISTS `housekeeping_staff`;
CREATE TABLE `housekeeping_staff`
(
    `id`              BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`      BIGINT        NOT NULL DEFAULT 0 COMMENT '所属企业',
    `name`            VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '姓名',
    `code`            VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '编码',
    `display_name`    VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '显示名称',
    `id_card`         VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '身份证',
    `id_card_file`    BIGINT        NOT NULL DEFAULT 0 COMMENT '身份证附件',
    `photo_file`      BIGINT        NOT NULL DEFAULT 0 COMMENT '个人照片',
    `gender`          TINYINT       NOT NULL DEFAULT 0 COMMENT '性别',
    `contact_mobile`  VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '联系电话',
    `introduction`    VARCHAR(512)  NOT NULL DEFAULT '' COMMENT '介绍',
    `status`          TINYINT       NOT NULL DEFAULT 0 COMMENT '状态',
    `credit_score`    decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分',
    `order_num`       BIGINT        NOT NULL DEFAULT 0 COMMENT '订单数',
    `employment_date` TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '参加工作时间',
    `seniority`       int(11)       NOT NULL DEFAULT 0 COMMENT '工作经验年数',
    `province_code`   BIGINT        NOT NULL DEFAULT 0 COMMENT '籍贯',
    `staff_type`      varchar(10)   NOT NULL DEFAULT '50' COMMENT '家政员类型',
    `deleted`         BIGINT        NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）',
    `created_time`    TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time`    TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_id_card` (`id_card`, `deleted`),
    UNIQUE KEY `uk_code` (`code`, `deleted`),
    KEY `idx_creditscore_createdtime` (`credit_score`, `created_time`),
    KEY `idx_createdtime` (`created_time`),
    KEY `idx_updatetime` (`updated_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='家政员';
