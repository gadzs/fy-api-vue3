package cn.turboinfo.fuyang.api.gateway.mini.contract.activity

import cn.turboinfo.fuyang.api.domain.mini.usecase.activity.MiniConsumerActivityApplyUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@ConsumerScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/activity"])
interface MiniConsumerActivityContract {

    /**
     * 活动详情
     */
    @AutoMappingContract(
        beanType = MiniConsumerActivityApplyUseCase::class,
        paths = ["/apply/{activityId}"],
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun apply()

}
