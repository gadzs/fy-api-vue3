package cn.turboinfo.fuyang.api.gateway.admin.fo.log;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class LoginLogSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "登录用户名",
            operator = Operator.LIKE
    )
    private String username;

    @QField(
            placeholder = "系统用户ID"
    )
    private Long sysUserId;

    @QField(
            placeholder = "登录来源"
    )
    private String loginSource;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "登录行为发生的时间"
    )
    private LocalDateTime loginTime;

    @QField(
            placeholder = "是否登录成功",
            control = Control.SELECT,
            ref = "yesNoStatusList",
            template = "enum"
    )
    private net.sunshow.toolkit.core.base.enums.YesNoStatus loginStatus;

    @QField(
            placeholder = "登录备注信息",
            operator = Operator.LIKE
    )
    private String loginRemark;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;
}
