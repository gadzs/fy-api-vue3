package cn.turboinfo.fuyang.api.provider.common.repository.database.contract.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.contract.ContractStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ContractStatusConverter extends BaseEnumConverter<ContractStatus> {
}
