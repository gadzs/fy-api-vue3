package cn.turboinfo.fuyang.api.domain.mini.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 根据名称查询企业
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListShopByNameUseCase extends AbstractUseCase<MiniListShopByNameUseCase.InputData, MiniListShopByNameUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;

    private final ShopDataFactory shopDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        String name = inputData.name;

        List<HousekeepingShop> shopList = housekeepingShopService.findByNameContaining(name);

        // 组装附件
        shopDataFactory.assembleAttachment(shopList);
        shopDataFactory.maskInfo(shopList);


        return OutputData.builder()
                .shopList(
                        shopList.stream()
                                .map(it -> {
                                    shopDataFactory.maskInfo(it);

                                    return BeanMapper.map(it, ViewShopFO.class);
                                }).toList()
                )
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank
        private String name;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<ViewShopFO> shopList;
    }

}
