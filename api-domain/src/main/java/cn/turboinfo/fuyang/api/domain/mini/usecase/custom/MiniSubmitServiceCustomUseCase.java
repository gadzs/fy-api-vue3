package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.address.AddressService;
import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustomCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 消费者提交定制服务订单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniSubmitServiceCustomUseCase extends AbstractUseCase<MiniSubmitServiceCustomUseCase.InputData, MiniSubmitServiceCustomUseCase.OutputData> {

    private final AddressService addressService;

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {

        ServiceCustomCreator.Builder builder = ServiceCustomCreator.builder();

        BeanHelper.copyPropertiesToBuilder(builder, InputData.class, inputData);

        Long addressId = inputData.getAddressId();
        Address address = addressService.getByIdEnsure(addressId);

        builder
                .withUserId(inputData.getUserId())
                .withCategoryId(inputData.getCategoryId())
                .withAddressId(addressId)
                .withPeopleNum(inputData.getPeopleNum())
                .withBudget(inputData.getBudget())
                .withScheduledStartTime(inputData.getScheduledStartTime())
                .withScheduledEndTime(inputData.getScheduledEndTime())
                .withCompanyId(0L)
                .withShopId(0L)
                .withStaffId(0L)
                .withPrice(BigDecimal.ZERO)
                .withPrepaid(BigDecimal.ZERO)
                .withAdditionalFee(BigDecimal.ZERO)
                .withDiscountFee(BigDecimal.ZERO)
                .withDeposit(BigDecimal.ZERO)
                .withContact(address.getContact())
                .withGenderType(address.getGenderType())
                .withMobile(address.getMobile())
                .withDivisionId(address.getDivisionId())
                .withPoiName(address.getPoiName())
                .withDetail(address.getDetail())
                .withDescription(inputData.getDescription())
                .withCustomStatus(ServiceCustomStatus.INIT)
                .withPayStatus(ServiceOrderPayStatus.UNPAID)
                .withCreditRatingStatus(ServiceOrderCreditRatingStatus.PENDING)
                .withProfitSharingStatus(ProfitSharingStatus.INIT)
                .withRefundStatus(ServiceOrderRefundStatus.INIT)
        ;

        ServiceCustom serviceCustom = serviceCustomService.save(builder.build());

        serviceCustom.setMobile(PhoneUtils.maskPhoneNum(serviceCustom.getMobile()));

        return OutputData.builder()
                .serviceCustom(serviceCustom)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        @Positive
        private Long userId;

        /**
         * 服务类型
         */
        @NotNull(message = "服务类型不能为空")
        @Positive
        private Long categoryId;

        /**
         * 服务地址编码
         */
        @NotNull(message = "服务地址编码不能为空")
        @Positive
        private Long addressId;

        /**
         * 人数
         */
        @NotNull(message = "人数不能为空")
        @Positive
        private Integer peopleNum;

        /**
         * 预算
         */
        @NotNull(message = "预算不能为空")
        @PositiveOrZero
        private BigDecimal budget;

        /**
         * 描述
         */
        private String description;

        /**
         * 计划开始时间
         */
        @NotNull(message = "计划开始时间不能为空")
        private LocalDateTime scheduledStartTime;

        /**
         * 计划结束时间
         */
        @NotNull(message = "计划结束时间不能为空")
        private LocalDateTime scheduledEndTime;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private ServiceCustom serviceCustom;

    }
}
