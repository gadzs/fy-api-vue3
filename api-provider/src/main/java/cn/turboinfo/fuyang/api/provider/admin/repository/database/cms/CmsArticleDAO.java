package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface CmsArticleDAO extends BaseRepository<CmsArticlePO, Long> {
    List<CmsArticlePO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<CmsArticlePO> findAllByCategoryId(Long categoryId);

    List<CmsArticlePO> findAllByCategoryIdIn(Collection<Long> idCollection);
}
