package cn.turboinfo.fuyang.api.entity.common.exception.kvconfig;

public class KVConfigException extends RuntimeException {
    public KVConfigException() {
        this("操作 KVConfig 出错");
    }

    public KVConfigException(String message) {
        super(message);
    }

    public KVConfigException(Throwable cause) {
        super(cause);
    }

    public KVConfigException(String message, Throwable cause) {
        super(message, cause);
    }
}
