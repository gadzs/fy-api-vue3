package cn.turboinfo.fuyang.api.gateway.admin.fo.credit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * @author hai
 */
@Data
@QField
public class CreditRatingSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "对象编码",
            operator = Operator.LIKE
    )
    private Long objectId;

    @QField(
            placeholder = "对象类型",
            control = Control.SELECT,
            ref = "entityObjectTypeList",
            template = "enum"
    )
    private EntityObjectType objectType;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            ref = "creditRatingStatusList",
            template = "enum"
    )
    private CreditRatingStatus status;
}
