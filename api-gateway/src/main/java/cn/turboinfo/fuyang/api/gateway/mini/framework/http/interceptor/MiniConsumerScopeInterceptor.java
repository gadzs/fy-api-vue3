package cn.turboinfo.fuyang.api.gateway.mini.framework.http.interceptor;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor.BaseHandlerInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nxcloud.ext.springmvc.automapping.spring.AutoMappingRequestParameterTypeBinding;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * 消费者拦截
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerScopeInterceptor extends BaseHandlerInterceptorAdapter {

    private final AutoMappingRequestParameterTypeBinding autoMappingRequestParameterTypeBinding;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;

            MiniRequestContext requestContext = MiniRequestContextHolder.current();
            if (requestContext.isLogin()) {
                Optional<ConsumerScope> consumerScopeOptional = getAnnotation(handlerMethod, ConsumerScope.class, true);
                ConsumerScope annotation = consumerScopeOptional.orElseGet(() -> autoMappingRequestParameterTypeBinding.getAnnotation(handlerMethod.getMethod(), ConsumerScope.class, true));

                if (annotation != null) {

                    if (!StringUtils.equals(request.getHeader(MiniRestConstants.HEADER_USER_TYPE), String.valueOf(UserType.Consumer.getValue()))) {
                        throw new RuntimeException("当前请求用户类型不正确");
                    }

                    try {
                        requestContext.setUserType(UserType.Consumer);
                        // 消费者编码 就是 用户编码
                        requestContext.setConsumerId(requestContext.getUserId());
                    } catch (NumberFormatException e) {
                        throw new RuntimeException("请求的消费者编码不正确");
                    }
                }
            }
        }
        return true;
    }

}
