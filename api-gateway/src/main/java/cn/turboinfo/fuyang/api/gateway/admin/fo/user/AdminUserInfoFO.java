package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.login.AdminLoginOperation;
import cn.turboinfo.fuyang.api.entity.admin.pojo.menu.Menu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminUserInfoFO {

    private Set<String> roles;

    private Set<String> permissions;

    private List<Menu> menus;

    private String name;

    private String avatar;

    private String introduction;

    private AdminLoginOperation loginOperation;

}
