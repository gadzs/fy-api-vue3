package cn.turboinfo.fuyang.api.entity.admin.exception.security;

/**
 * 频率限制超上限
 *
 * @author sunshow
 */
public class RateLimitReachedException extends RuntimeException {
    public RateLimitReachedException() {
        super("频率限制超上限");
    }
}
