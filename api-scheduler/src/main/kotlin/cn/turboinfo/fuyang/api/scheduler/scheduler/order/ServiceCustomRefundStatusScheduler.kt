package cn.turboinfo.fuyang.api.scheduler.scheduler.order

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.order.SchedulerServiceCustomRefundStatusUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 定制服务订单退款状态定时任务
 */
@Component
class ServiceCustomRefundStatusScheduler(
    private val serviceCustomRefundStatusScheduler: SchedulerServiceCustomRefundStatusUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 60000)
    fun run() {
        runCatching {
            serviceCustomRefundStatusScheduler.execute(
                SchedulerServiceCustomRefundStatusUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "定制服务订单退款状态定时任务执行失败"
            }
        }
    }

}
