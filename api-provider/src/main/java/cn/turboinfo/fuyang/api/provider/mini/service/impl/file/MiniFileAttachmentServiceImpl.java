package cn.turboinfo.fuyang.api.provider.mini.service.impl.file;

import cn.turboinfo.fuyang.api.domain.common.service.site.SiteUrlService;
import cn.turboinfo.fuyang.api.domain.mini.service.file.MiniFileAttachmentService;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class MiniFileAttachmentServiceImpl implements MiniFileAttachmentService {

    private final SiteUrlService siteUrlService;

    @Override
    public FileAttachment assembleExternalUrl(FileAttachment fileAttachment) {
        fileAttachment.setExternalUrl(siteUrlService.getUploadExternalImgUrl(fileAttachment.getRelativePath() + "/" + fileAttachment.getFilename()));
        return fileAttachment;
    }
}
