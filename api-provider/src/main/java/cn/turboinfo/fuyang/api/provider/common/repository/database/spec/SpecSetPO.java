package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 规格组合
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "spec_set"
)
@Entity
@Where(clause = "deleted = 0")
public class SpecSetPO extends SoftDeleteFYEntity {

    /**
     * 组合名称
     */
    private String name;

    /**
     * 企业编码
     */
    private Long companyId;

    /**
     * 描述信息
     */
    private String description;

}
