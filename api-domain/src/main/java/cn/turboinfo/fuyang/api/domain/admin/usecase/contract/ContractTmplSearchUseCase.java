package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractTmplService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.ViewAttachment;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractTmplSearchUseCase extends AbstractUseCase<ContractTmplSearchUseCase.InputData, ContractTmplSearchUseCase.OutputData> {
    private final ContractTmplService contractTmplService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<ContractTmpl> response = contractTmplService.findAll(inputData.getRequest(), inputData.getRequestPage());

        // 查询企业
        Set<Long> compayIdSet = response.getPagedData()
                .stream()
                .filter(it -> it.getCompanyId() != null && it.getCompanyId() > 0)
                .map(ContractTmpl::getCompanyId)
                .collect(Collectors.toSet());

        Map<Long, HousekeepingCompany> companyMap = housekeepingCompanyService.findByIdCollection(compayIdSet)
                .stream()
                .collect(Collectors.toMap(HousekeepingCompany::getId, it -> it));

        // 查询附件
        Set<Long> contractIdSet = response.getPagedData()
                .stream()
                .map(ContractTmpl::getId)
                .collect(Collectors.toSet());
        Map<Long, List<FileAttachment>> fileMap = fileAttachmentService.findByRefIdInAndRefType(contractIdSet, FileRefTypeConstant.CONTRACT_TMPL_FILE)
                .stream()
                .collect(Collectors.groupingBy(FileAttachment::getRefId));

        response.getPagedData().forEach(it -> {
            if (companyMap.containsKey(it.getCompanyId())) {
                it.setCompanyName(companyMap.get(it.getCompanyId()).getName());
            }

            if (fileMap.containsKey(it.getId())) {
                List<ViewAttachment> attachmentList = fileMap.get(it.getId()).stream()
                        .map(attachment -> {
                            ViewAttachment.ViewAttachmentBuilder builder = ViewAttachment.builder()
                                    .fileAttachmentId(attachment.getId())
                                    .displayName(attachment.getDisplayName());
                            return builder.build();
                        })
                        .toList();

                it.setAttachmentList(attachmentList);
            }
        });

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ContractTmpl> qResponse;
    }
}
