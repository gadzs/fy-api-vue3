DROP TABLE IF EXISTS `company_account`;
CREATE TABLE `company_account`
(
    `id`              BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '企业ID',
    `type`            TINYINT      NOT NULL DEFAULT 0 COMMENT '账户类型',
    `account`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '账户',
    `name`            VARCHAR(128) NOT NULL DEFAULT '' COMMENT '账户名称',
    `relation_type`   TINYINT      NOT NULL DEFAULT 0 COMMENT '关系类型',
    `custom_relation` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '自定义关系类型',
    `status`          TINYINT      NOT NULL DEFAULT 0 COMMENT '状态',
    `deleted`         BIGINT       NOT NULL DEFAULT 0,
    `created_time`    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_companyid_type` (`company_id`, `type`, `deleted`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='企业账户';

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('企业账户', 'adminAccount:null', '', '/account', '', 1, 0, 'el-icon-wallet', 47);
SET
    @accountPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('分账账户列表', 'adminAccount:list', '', '/account/list', '', 1, @accountPermissionParentId, 'el-icon-wallet',
        0),
       ('分账账户创建', 'adminAccount:create', '', '', '', 0, @accountPermissionParentId, '', 0),
       ('分账账户修改', 'adminAccount:update', '', '', '', 0, @accountPermissionParentId, '', 0),
       ('分账账户查看', 'adminAccount:view', '', '', '', 0, @accountPermissionParentId, '', 0),
       ('分账账户删除', 'adminAccount:delete', '', '', '', 0, @accountPermissionParentId, '', 0),
       ('导出分账账户', 'adminAccount:export', '', '', '', 0, @accountPermissionParentId, '', 0)
;

