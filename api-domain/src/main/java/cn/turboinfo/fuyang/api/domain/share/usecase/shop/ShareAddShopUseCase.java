package cn.turboinfo.fuyang.api.domain.share.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.common.DataAlreadyExistException;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 添加门店
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareAddShopUseCase extends AbstractUseCase<ShareAddShopUseCase.InputData, ShareAddShopUseCase.OutputData> {

    private final HousekeepingShopService housekeepingShopService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        // 查询门店已存在列表
        List<HousekeepingShop> housekeepingShopList = housekeepingShopService.findByCompanyId(companyId);
        Map<Long, HousekeepingShop> shopMap = housekeepingShopList
                .stream()
                .collect(Collectors.toMap(HousekeepingShop::getId, item -> item));

        Map<String, HousekeepingShop> nameShopMap = housekeepingShopList
                .stream()
                .collect(Collectors.toMap(HousekeepingShop::getName, item -> item));

        List<HousekeepingShop> shopList = inputData.getShopList();
        List<String> errorMsgList = new ArrayList<>();
        if (shopList != null && !shopList.isEmpty()) {
            shopList.forEach(item -> {

                if (shopMap.containsKey(item.getId())) {
                    HousekeepingShop shop = shopMap.get(item.getId());

                    HousekeepingShopUpdater.Builder builder = HousekeepingShopUpdater.builder(shop.getId());

                    QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingShopUpdater.class, item);

                    housekeepingShopService.update(builder.build());
                } else {
                    if (nameShopMap.containsKey(item.getName())) {
                        errorMsgList.add(String.format("门店名称 [%s] 已存在", item.getName()));
                    } else {
                        HousekeepingShopCreator.Builder builder = HousekeepingShopCreator.builder();

                        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, HousekeepingShopCreator.class, item);

                        housekeepingShopService.save(builder
                                .withCompanyId(companyId)
                                .withStatus(ShopStatus.PENDING)
                                .build());
                    }

                }
            });

        }
        if (!errorMsgList.isEmpty()) {
            throw new DataAlreadyExistException(StringUtils.join(errorMsgList, " ,"));
        }

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "企业编码不能为空"
        )
        @Positive
        private Long companyId;

        @NotNull(
                message = "门店列表不能为空"
        )
        private List<HousekeepingShop> shopList;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
