package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 家政员审核记录
 */
@Table(
        name = "housekeeping_staff_audit_record"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(
        clause = "deleted = 0"
)
public class HousekeepingStaffAuditRecordPO extends SoftDeleteFYEntity {
    /**
     * 企业编码
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 家政员编码
     */
    @Column(
            name = "staff_id"
    )
    private Long staffId;

    /**
     * 审核状态
     */
    @Column(
            name = "audit_status"
    )
    private AuditStatus auditStatus;

    /**
     * 审核人
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    /**
     * 审核人名称
     */
    @Column(
            name = "user_name"
    )
    private String userName;

    /**
     * 备注
     */
    private String remark;
}
