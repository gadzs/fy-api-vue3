package cn.turboinfo.fuyang.api.gateway.share.config;

import cn.turboinfo.fuyang.api.gateway.share.framework.http.interceptor.ShareAccountAuthInterceptor;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.interceptor.ShareRequestContextInterceptor;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.resolver.ShareRequestContextHandlerMethodArgumentResolver;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * author: sunshow.
 */
@Configuration
public class ShareWebConfig implements WebMvcConfigurer {

    @Autowired
    private ShareRequestContextInterceptor requestContextInterceptor;

    @Autowired
    private ShareAccountAuthInterceptor shareAccountAuthInterceptor;

    @Override
    public void addInterceptors(@NotNull InterceptorRegistry registry) {
        registry.addInterceptor(requestContextInterceptor)
                .addPathPatterns("/share/**");
        registry.addInterceptor(shareAccountAuthInterceptor)
                .addPathPatterns("/share/**");
    }


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new ShareRequestContextHandlerMethodArgumentResolver());
    }
}
