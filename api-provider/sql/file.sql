SET NAMES `utf8mb4`;

CREATE TABLE `file_attachment`
(
    `id`                BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `display_name`      VARCHAR(128) NOT NULL DEFAULT '' COMMENT '显示名称',
    `ref_type`          VARCHAR(64)  NOT NULL DEFAULT '' COMMENT '引用业务类型',
    `ref_id`            BIGINT       NOT NULL DEFAULT 0 COMMENT '引用业务ID',
    `filename`          VARCHAR(128) NOT NULL DEFAULT '' COMMENT '文件名',
    `relative_path`     VARCHAR(128) NOT NULL DEFAULT '' COMMENT '相对路径',
    `original_filename` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '原始文件名',
    `deleted_time`      BIGINT       NOT NULL DEFAULT 0 COMMENT '',
    `created_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    KEY idx_refid_reftype (`ref_id`, `ref_type`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='文件附件';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('附件管理', 'fileAttachment:null', '', 1, 0);
SET @filePermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('附件列表', 'fileAttachment:list', '/admin/fileAttachment/list', 1, @filePermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('新增附件', 'fileAttachment:create', '/admin/fileAttachment/create', 0, @filePermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('编辑附件', 'fileAttachment:update', '/admin/fileAttachment/update', 0, @filePermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('删除附件', 'fileAttachment:delete', '/admin/fileAttachment/delete', 0, @filePermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('上传附件', 'fileAttachment:upload', '/admin/fileAttachment/upload', 0, @filePermissionParentId);