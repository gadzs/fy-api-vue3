package cn.turboinfo.fuyang.api.domain.mini.usecase.my;

import cn.turboinfo.fuyang.api.domain.common.component.product.ProductAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.credit.CreditRatingService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMySubmittedCreditRatingListItem;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 已评价列表
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniMySubmittedCreditRatingListUseCase extends AbstractUseCase<MiniMySubmittedCreditRatingListUseCase.InputData, MiniMySubmittedCreditRatingListUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    private final CreditRatingService creditRatingService;

    private final ProductAssembleHelper productAssembleHelper;

    private final ServiceOrderDataFactory serviceOrderDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        QPage requestPage = inputData.getRequestPage();

        QRequest request = QRequest.newInstance()
                .filterEqual(QServiceOrder.userId, userId);
        request.filterEqual(QServiceOrder.creditRatingStatus, ServiceOrderCreditRatingStatus.SUBMITTED);

        // 按创建时间降序排序
        requestPage.addOrder(QServiceOrder.createdTime, QSort.Order.DESC);

        QResponse<ServiceOrder> orderQResponse = serviceOrderService.findAll(request, requestPage);

        // 拼装企业名称
        serviceOrderDataFactory.assembleCompany(orderQResponse.getPagedData().stream().toList());

        // 取评价内容, 可能每个订单有多条评价, 都拿出来再分组后按评价时间升序排序
        Set<Long> orderIdSet = orderQResponse.getPagedData().stream()
                .map(ServiceOrder::getId)
                .collect(
                        Collectors.toSet()
                );
        Map<Long, List<CreditRating>> orderIdRatingListMap = creditRatingService.findByObjectIdCollection(EntityObjectType.SERVICE_ORDER, orderIdSet).stream()
                .collect(
                        Collectors.groupingBy(
                                CreditRating::getObjectId,
                                Collectors.toList()
                        )
                );

        QResponse<MiniMySubmittedCreditRatingListItem> itemList = orderQResponse.map(serviceOrder -> {
                    List<CreditRating> ratingList = orderIdRatingListMap.get(serviceOrder.getId());
                    if (ratingList != null) {
                        ratingList.sort(Comparator.comparing(CreditRating::getCreatedTime));
                    }

                    serviceOrderDataFactory.maskInfo(serviceOrder);
                    return MiniMySubmittedCreditRatingListItem.builder()
                            .serviceOrder(BeanMapper.map(serviceOrder, ViewServiceOrderFO.class))
                            .creditRatingList(ratingList)
                            .build();
                }
        );

        productAssembleHelper.assembleProduct(
                itemList.getPagedData(),
                item -> item.getServiceOrder().getProductId(),
                ProductAssembleHelper.ASSEMBLE_ATTACHMENT,
                MiniMySubmittedCreditRatingListItem::setProduct);

        return OutputData.builder()
                .itemList(itemList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        private Long userId;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 已评价列表
         */
        private QResponse<MiniMySubmittedCreditRatingListItem> itemList;

    }
}
