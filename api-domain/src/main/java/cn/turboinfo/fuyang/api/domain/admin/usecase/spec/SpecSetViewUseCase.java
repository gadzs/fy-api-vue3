package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSet;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecSetViewUseCase extends AbstractUseCase<SpecSetViewUseCase.InputData, SpecSetViewUseCase.OutputData> {
    private final SpecSetService specSetService;

    @Override
    protected OutputData doAction(InputData inputData) {

        // 查询规格组
        val specSetOptional = specSetService.getById(inputData.getId());
        if(specSetOptional.isPresent()) {
            val specList = specSetService.findWithHierarchy(inputData.getId());
            specSetOptional.get().setChildren(specList);
        }

        return OutputData.builder()
                .specSet(specSetOptional.get())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private SpecSet specSet;
    }
}
