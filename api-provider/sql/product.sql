DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`
(
    `id`               bigint(1)      NOT NULL AUTO_INCREMENT,
    `company_id`       bigint(1)      NOT NULL DEFAULT '0',
    `shop_id`          bigint(1)      NOT NULL DEFAULT '0',
    `category_id`      bigint(1)      NOT NULL DEFAULT '0',
    `name`             varchar(64)    NOT NULL DEFAULT '',
    `status`           int(1)         NOT NULL DEFAULT '0',
    `description`      varchar(255)            DEFAULT NULL,
    `mobile_content`   text COMMENT '移动端详情',
    `credit_score`     decimal(5, 2)  NOT NULL DEFAULT '0.00' COMMENT '信用分',
    `order_num`        bigint(20)              DEFAULT '0' COMMENT '订单数',
    `min_price`        decimal(10, 2) NOT NULL COMMENT '最小价格',
    `max_price`        decimal(10, 2) NOT NULL COMMENT '最大价格',
    `need_contract`    TINYINT        NOT NULL DEFAULT 0 COMMENT '是否需要合同',
    `contract_tmpl_id` BIGINT         NOT NULL DEFAULT 0 COMMENT '合同模版编码',
    `deleted`          bigint(1)      NOT NULL DEFAULT '0',
    `created_time`     datetime       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`     datetime       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_shopid_createdtime` (`shop_id`, `created_time`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;


DROP TABLE IF EXISTS `product_sku`;
CREATE TABLE `product_sku`
(
    `id`             BIGINT(1)      NOT NULL AUTO_INCREMENT,
    `product_id`     BIGINT(1)      NOT NULL,
    `original_price` DECIMAL(10, 2) NOT NULL,
    `price`          DECIMAL(10, 2) NOT NULL,
    `status`         INT(1)         NOT NULL DEFAULT 0,
    `spec_list`      JSON           NULL,
    `deleted`        BIGINT(1)      NOT NULL DEFAULT 0,
    `created_time`   DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`   DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_productid` (`product_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

DROP TABLE IF EXISTS `product_sku_spec_rel`;
CREATE TABLE `product_sku_spec_rel`
(
    `id`             BIGINT(1) NOT NULL AUTO_INCREMENT,
    `product_sku_id` BIGINT(1) NOT NULL,
    `spec_id`        BIGINT(1) NOT NULL,
    `created_time`   DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`   DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_productskuid_specid` (`product_sku_id`, `spec_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('产品服务管理', 'product:null', '', '', '', 1, 0);
SET
    @productPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('产品服务列表', 'product:list', '', '/product/list', '', 1, @productPermissionParentId),
       ('修改产品服务', 'product:update', '', '/product/edit', '', 0, @productPermissionParentId),
       ('新增产品服务', 'product:create', '', '/product/create', '', 0, @productPermissionParentId),
       ('删除产品服务', 'product:delete', '', '', '', 0, @productPermissionParentId);
