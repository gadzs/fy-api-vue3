package cn.turboinfo.fuyang.api.gateway.admin.controller.company;


import cn.turboinfo.fuyang.api.domain.admin.usecase.company.HousekeepingCompanyAuthLabelSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.QHousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.gateway.admin.fo.company.HousekeepingCompanyAuthLabelSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * 家政公司管理
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/company/authLabel")
public class HousekeepingCompanyAuthLabelController {

    private final HousekeepingCompanyAuthLabelSearchUseCase housekeepingCompanyAuthLabelSearchUseCase;

    @RequiresPermissions("company:authLabel:list")
    @RequestMapping("/list")
    public LimitDataFO<HousekeepingCompanyAuthLabel> authLabelList(@Valid HousekeepingCompanyAuthLabelSearchFO search, LimitFO page, SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingCompanyAuthLabel.id + "|" + QSort.Order.DESC.name()});
        }
        HousekeepingCompanyAuthLabelSearchUseCase.OutputData outputData = housekeepingCompanyAuthLabelSearchUseCase.execute(HousekeepingCompanyAuthLabelSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }
}
