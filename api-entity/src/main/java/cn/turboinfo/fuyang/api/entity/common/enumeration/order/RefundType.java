package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 退款方式
 */
@Slf4j
public enum RefundType implements BaseEnum {

    NO_REFUND(0, "无需退款"),

    OFFLINE(10, "线下退款"),

    WECHAT(20, "微信退款"),

    ;

    private int value;
    private String name;

    RefundType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static RefundType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<RefundType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<RefundType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
