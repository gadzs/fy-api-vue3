package cn.turboinfo.fuyang.api.provider.common.repository.database.company.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * @author gadzs
 * @description 企业状态转换器
 * @date 2023/1/29 15:44
 */
@Converter(
        autoApply = true
)
public class CompanyStatusConverter extends BaseEnumConverter<CompanyStatus> {
}
