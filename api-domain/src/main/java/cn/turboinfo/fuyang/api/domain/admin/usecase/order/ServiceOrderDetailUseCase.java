package cn.turboinfo.fuyang.api.domain.admin.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.fo.order.ServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.exception.order.ServiceOrderException;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ServiceOrderDetailUseCase extends AbstractUseCase<ServiceOrderDetailUseCase.InputData, ServiceOrderDetailUseCase.OutputData> {
    private final ServiceOrderService serviceOrderService;
    private final SysUserService sysUserService;
    private final HousekeepingShopService housekeepingShopService;
    private final CategoryService categoryService;
    private final HousekeepingStaffService housekeepingStaffService;
    private final ProductService productService;

    @Override
    protected OutputData doAction(InputData inputData) {

        val serviceOrderOptional = serviceOrderService.getById(inputData.id);
        if (serviceOrderOptional.isPresent()) {
            val serviceOrder = serviceOrderOptional.get();
            if (inputData.companyId != null && !inputData.companyId.equals(serviceOrder.getCompanyId())) {
                throw new ServiceOrderException("没有查看权限");
            }

            val shop = housekeepingShopService.getById(serviceOrder.getShopId()).orElse(new HousekeepingShop());
            val product = productService.getById(serviceOrder.getProductId()).orElse(new Product());
            val category = categoryService.getById(serviceOrder.getCategoryId()).orElse(new Category());
            val staff = housekeepingStaffService.getById(serviceOrder.getStaffId()).orElse(new HousekeepingStaff());
            val sysUser = sysUserService.getById(serviceOrder.getUserId()).orElse(new SysUser());
            val orderFO = ServiceOrderFO.builder()
                    .serviceOrder(serviceOrder)
                    .shopName(shop.getName())
                    .product(product)
                    .categoryName(category.getName())
                    .userName(sysUser.getMobile())
                    .staffName(staff.getName()).build();

            return OutputData.builder()
                    .serviceOrder(orderFO)
                    .build();
        }

        return OutputData.builder()
                .serviceOrder(null)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private Long id;

        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private ServiceOrderFO serviceOrder;
    }
}
