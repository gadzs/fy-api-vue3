package cn.turboinfo.fuyang.api.entity.common.fo.address;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hai.
 */
@Data
public class ViewAddressFO {
    private Long id;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;
}
