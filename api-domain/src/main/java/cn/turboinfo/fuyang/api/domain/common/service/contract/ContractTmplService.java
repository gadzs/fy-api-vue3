package cn.turboinfo.fuyang.api.domain.common.service.contract;

import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;

/**
 * 合同管理服务
 * author: hai
 */
public interface ContractTmplService extends BaseQService<ContractTmpl, Long> {

    List<ContractTmpl> findByCompanyId(Long companyId);

    List<ContractTmpl> findByCompanyIdCollection(Collection<Long> companyIdCollection);
}
