package cn.turboinfo.fuyang.api.domain.admin.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingShopAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.shop.HousekeepingShopException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShopCreator;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author gadzs
 * @description 家政企业创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopCreateUseCase extends AbstractUseCase<HousekeepingShopCreateUseCase.InputData, HousekeepingShopCreateUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;
    private final FileAttachmentService fileAttachmentService;
    private final HousekeepingShopAuditRecordService housekeepingShopAuditRecordService;
    private final HousekeepingCompanyService housekeepingCompanyService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Optional<HousekeepingShop> housekeepingShopOptional = housekeepingShopService.findByCompanyIdAndName(inputData.getCompanyId(), inputData.getName());
        if (housekeepingShopOptional.isPresent()) {
            throw new HousekeepingShopException("门店名称已存在");
        }

        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getName())) {
            throw new HousekeepingShopException("门店名称包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getAddress())) {
            throw new HousekeepingShopException("门店地址包含敏感词汇，请修改后重新提交！");
        }

        // 检查联系人是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getContactPerson())) {
            throw new HousekeepingShopException("联系人包含敏感词汇，请修改后重新提交");
        }

        // 检查营业时间是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getBusinessTime())) {
            throw new HousekeepingShopException("营业时间包含敏感词汇，请修改后重新提交");
        }

        HousekeepingShopCreator.Builder builder = HousekeepingShopCreator.builder();
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, HousekeepingShopCreator.class, inputData);
        HousekeepingShop housekeepingShop = housekeepingShopService.save(builder
                .withStatus(ShopStatus.PENDING)
                .build());

        //保存附件关系
        fileAttachmentService.updateRefId(Stream.of(inputData.getImgList(), Lists.newArrayList(inputData.getBusinessLicenseFile())).flatMap(Collection::stream).collect(Collectors.toList()), housekeepingShop.getId());

        // 审核成功之后更新企业门店数量
        housekeepingCompanyService.updateShopNum(housekeepingShop.getCompanyId(), housekeepingShopService.countByCompanyId(housekeepingShop.getCompanyId()));

        // 创建审核订单
        {
            HousekeepingShopAuditRecordCreator housekeepingShopAuditRecordCreator = HousekeepingShopAuditRecordCreator.builder()
                    .withCompanyId(housekeepingShop.getCompanyId())
                    .withShopId(housekeepingShop.getId())
                    .withAuditStatus(AuditStatus.INIT)
                    .withUserId(0L)
                    .withUserName(StringUtils.EMPTY)
                    .withRemark("新增门店")
                    .build();
            housekeepingShopAuditRecordService.save(housekeepingShopAuditRecordCreator);
        }

        return OutputData.builder()
                .housekeepingShop(housekeepingShop)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "公司编码不能为空"
        )
        private Long companyId;

        @NotBlank(
                message = "门店名称不能为空"
        )
        private String name;

        @NotNull(
                message = "成立日期不能为空"
        )
        private LocalDate foundDate;

        @NotNull(
                message = "省编码不能为空"
        )
        private Long provinceCode;

        @NotNull(
                message = "市编码不能为空"
        )
        private Long cityCode;

        @NotNull(
                message = "区编码不能为空"
        )
        private Long areaCode;

        @NotBlank(
                message = "地址不能为空"
        )
        private String address;

        @NotNull(
                message = "经度不能为空"
        )
        private BigDecimal longitude;

        @NotNull(
                message = "纬度不能为空"
        )
        private BigDecimal latitude;

        @NotNull(
                message = "联系人"
        )
        private String contactPerson;

        @NotNull(
                message = "联系人电话"
        )
        private String contactMobile;

        //        @NotBlank(
//                message = "营业时间不能为空"
//        )
        private String businessTime;

        @NotNull(
                message = "营业执照"
        )
        private Long businessLicenseFile;

        /**
         * 店铺图片
         */
        private List<Long> imgList;

        public List<Long> getImgList() {
            if (imgList == null) {
                imgList = Lists.newArrayList();
            }
            return imgList;
        }

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingShop housekeepingShop;
    }

}
