package cn.turboinfo.fuyang.api.provider.common.repository.database.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 活动报名
 * author: hai
 */
@Table(
        name = "activity_apply"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(clause = "deleted = 0")
public class ActivityApplyPO extends SoftDeleteFYEntity {
    /**
     * 活动编码
     */
    @Column(
            name = "activity_id"
    )
    private Long activityId;

    /**
     * 用户编码
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    /**
     * 用户类型
     */
    @Column(
            name = "user_type"
    )
    private UserType userType;

    /**
     * 用户姓名
     */
    @Column(
            name = "user_name"
    )
    private String userName;

    /**
     * 用户电话
     */
    @Column(
            name = "use_mobile"
    )
    private String useMobile;
}
