package cn.turboinfo.fuyang.api.domain.common.service.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharing;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharingCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 分账管理服务
 * author: hai
 */
public interface ProfitSharingService extends BaseQService<ProfitSharing, Long> {

    ProfitSharing save(ProfitSharingCreator creator);

    /**
     * 根据订单ID查询分账记录
     *
     * @param objectId   订单ID
     * @param objectType 订单类型
     * @return 分账记录
     */
    List<ProfitSharing> findByObjectId(Long objectId, EntityObjectType objectType);

    void updateStatus(Long id, ProfitSharingStatus status);
}
