package cn.turboinfo.fuyang.api.domain.common.service.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政员服务
 * @date 2023/2/7 16:58
 */
public interface HousekeepingStaffService extends BaseQService<HousekeepingStaff, Long> {

    HousekeepingStaff save(HousekeepingStaffCreator creator);

    List<HousekeepingStaff> findByCompanyId(Long companyId);

    List<HousekeepingStaff> findByCompanyIdCollection(Collection<Long> companyIdCollection);

    Optional<HousekeepingStaff> findByIdCard(String idCard);

    Optional<HousekeepingStaff> findByCode(String code);

    Optional<HousekeepingStaff> findByMobile(String contactMobile);

    void updateStatus(Long id, StaffStatus staffStatus);

    void updateCreditScore(Long id, BigDecimal creditScore);

    void updateOrderNum(Long id, Long orderNum);

    long countByCompanyId(Long companyId);
}
