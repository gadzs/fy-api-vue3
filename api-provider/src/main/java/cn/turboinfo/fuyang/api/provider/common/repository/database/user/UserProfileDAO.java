package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface UserProfileDAO extends BaseRepository<UserProfilePO, Long> {
    List<UserProfilePO> findByIdInOrderByIdDesc(Collection<Long> idCollection);
}
