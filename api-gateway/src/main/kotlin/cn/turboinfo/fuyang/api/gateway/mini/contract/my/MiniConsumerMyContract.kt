package cn.turboinfo.fuyang.api.gateway.mini.contract.my

import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniConsumerMyUnpaidServiceOrderListUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@ConsumerScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/my"])
interface MiniConsumerMyContract {

    // 待支付订单列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniConsumerMyUnpaidServiceOrderListUseCase::class,
        consumes = []
    )
    fun unpaidServiceOrderList()


}