package cn.turboinfo.fuyang.api.entity.admin.pojo.role;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class RolePermission extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    private Long roleId;

    private Long permissionId;

    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
