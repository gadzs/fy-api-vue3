package cn.turboinfo.fuyang.api.provider.common.repository.database.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanCreatorIgnore;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanUpdaterIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 家政员
 * author: GADZS
 */
@Table(
        name = "housekeeping_staff"
)
@EqualsAndHashCode(
        callSuper = true
)
@Where(clause = "deleted = 0")
@Data
@Entity
@DynamicUpdate
@DynamicInsert
public class HousekeepingStaffPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 员工姓名
     */
    @Convert(converter = DataEncryptConverter.class)
    private String name;

    /**
     * 展示名称
     */
    private String displayName;

    /**
     * 家政员编码
     */
    private String code;

    /**
     * 公司id
     */
    private Long companyId;

    /*
     * 身份证
     */
    @Convert(converter = DataEncryptConverter.class)
    private String idCard;

    /**
     * 身份证附件
     */
    private Long idCardFile;

    /**
     * 性别
     */
    private GenderType gender;

    /**
     * 个人照片
     */
    private Long photoFile;

    /**
     * 联系电话
     */
    @Convert(converter = DataEncryptConverter.class)
    private String contactMobile;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 状态
     */
    private StaffStatus status;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 工作经验年数
     */
    private Integer seniority;

    /**
     * 家政员籍贯
     */
    private Long provinceCode;

    /**
     * 家政员类型
     */
    private String staffType;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
