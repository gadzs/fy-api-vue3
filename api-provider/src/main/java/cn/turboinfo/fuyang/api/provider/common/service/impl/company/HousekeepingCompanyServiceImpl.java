package cn.turboinfo.fuyang.api.provider.common.service.impl.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.entity.common.exception.company.HousekeepingCompanyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyUpdater;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.company.HousekeepingCompanyPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import nxcloud.foundation.core.idgenerator.IdGeneratorFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class HousekeepingCompanyServiceImpl extends DefaultQServiceImpl<HousekeepingCompany, Long, HousekeepingCompanyPO, HousekeepingCompanyDAO> implements HousekeepingCompanyService {

    private final IdGeneratorFacade<Long> idGeneratorFacade;

    @Override
    @Transactional
    public HousekeepingCompany save(HousekeepingCompanyCreator creator) {
        HousekeepingCompanyPO housekeepingCompanyPO = new HousekeepingCompanyPO();
        QBeanCreatorHelper.copyCreatorField(housekeepingCompanyPO, creator);

        // 手动生成ID
        housekeepingCompanyPO.setId(idGeneratorFacade.nextId());
        return convertQBean(dao.save(housekeepingCompanyPO));
    }

    /**
     * 根据社会信用代码查询
     *
     * @param uscc
     * @return
     */
    @Override
    public Optional<HousekeepingCompany> findByUscc(String uscc) {
        return dao.findByUscc(uscc).map(this::convertQBean);
    }

    /**
     * 根据手机号码码查询
     *
     * @param contactNumber
     * @return
     */
    @Override
    public Optional<HousekeepingCompany> findByContactNumber(String contactNumber) {
        return dao.findByContactNumber(contactNumber).map(this::convertQBean);
    }

    @Override
    @Transactional
    public HousekeepingCompany review(HousekeepingCompanyUpdater updater) throws HousekeepingCompanyException {
        HousekeepingCompanyPO housekeepingCompanyPO = getEntityWithNullCheckForUpdate(updater.getUpdateId());
        QBeanUpdaterHelper.copyUpdaterField(housekeepingCompanyPO, updater);
        return convertQBean(housekeepingCompanyPO);
    }

    @Override
    public List<HousekeepingCompany> findByName(String name) {
        return convertStreamQBeanToList(dao.findByName(name).stream());
    }

    @Override
    @Transactional
    public void updateCreditScore(Long id, BigDecimal creditScore) {
        HousekeepingCompanyPO po = getEntityWithNullCheckForUpdate(id);
        po.setCreditScore(creditScore);
    }

    @Override
    @Transactional
    public void updateOrderNum(Long id, Long orderNum) {
        HousekeepingCompanyPO po = getEntityWithNullCheckForUpdate(id);
        po.setOrderNum(orderNum);
    }

    @Override
    @Transactional
    public void updateStaffNum(Long id, Long staffNum) {
        HousekeepingCompanyPO po = getEntityWithNullCheckForUpdate(id);
        po.setStaffNum(staffNum);
    }

    @Override
    @Transactional
    public void updateShopNum(Long id, Long shopNum) {
        HousekeepingCompanyPO po = getEntityWithNullCheckForUpdate(id);
        po.setShopNum(shopNum);
    }

    @Override
    @Transactional
    public void updateProductNum(Long id, Long productNum) {
        HousekeepingCompanyPO po = getEntityWithNullCheckForUpdate(id);
        po.setProductNum(productNum);
    }

    @Override
    public List<HousekeepingCompany> findByAreaCode(Long areaCode) {
        return convertStreamQBeanToList(dao.findByAreaCode(areaCode).stream());
    }
}
