package cn.turboinfo.fuyang.api.entity.common.event.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.event.AbstractEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 服务订单状态变更事件
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceCustomStatusChangedEvent extends AbstractEvent {

    private Long serviceCustomId;

    private ServiceCustomStatus status;

    private ServiceCustomStatus fromStatus;

}
