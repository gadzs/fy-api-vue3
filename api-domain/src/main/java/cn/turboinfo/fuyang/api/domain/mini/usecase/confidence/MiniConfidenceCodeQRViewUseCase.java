package cn.turboinfo.fuyang.api.domain.mini.usecase.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.QRCodeUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 查看二维码
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConfidenceCodeQRViewUseCase extends AbstractUseCase<MiniConfidenceCodeQRViewUseCase.InputData, MiniConfidenceCodeQRViewUseCase.OutputData> {
    private final ConfidenceCodeService confidenceCodeService;

    private final HousekeepingStaffService housekeepingStaffService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long id = inputData.getId();
        HttpServletResponse response = inputData.getResponse();

        HousekeepingStaff staff = housekeepingStaffService.getByIdEnsure(id);

        ConfidenceCode confidenceCode = confidenceCodeService.getByIdEnsure(Long.parseLong(staff.getCode()));
        if (confidenceCode.getStatus().equals(ConfidenceCodeStatus.DISCARD)) {
            throw new RuntimeException("放心码已作废");
        }

        try {
            QRCodeUtils.qrCodeGenerate(confidenceCode.getId().toString(), 500, inputData.getImageType(), response.getOutputStream());

        } catch (Exception e) {

            throw new RuntimeException(e);
        }

        return OutputData.builder()
                .response(response)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        private String imageType;

        @NotNull()
        private HttpServletResponse response;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HttpServletResponse response;
    }
}
