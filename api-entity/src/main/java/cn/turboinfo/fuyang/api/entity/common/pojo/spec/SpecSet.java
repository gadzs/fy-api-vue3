package cn.turboinfo.fuyang.api.entity.common.pojo.spec;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 规格组合
 * 可用于关联多种对象, 例如一个商品类别关联一个组合, 便于管理
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class SpecSet extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 组合名称
     */
    private String name;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 企业编码
     */
    private Long companyId;

    /**
     * 特定场景容器 -- 规格
     */
    private List<Spec> children;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
