package cn.turboinfo.fuyang.api.domain.common.service.kvconfig;


import cn.turboinfo.fuyang.api.entity.common.exception.kvconfig.KVConfigException;
import cn.turboinfo.fuyang.api.entity.common.pojo.kvconfig.KVConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.kvconfig.KVConfigCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.kvconfig.KVConfigUpdater;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface KVConfigService {
    Optional<KVConfig> getById(Long id);

    KVConfig getByIdEnsure(Long id);

    List<KVConfig> findByIdCollection(Collection<Long> idCollection);

    KVConfig save(KVConfigCreator creator) throws KVConfigException;

    KVConfig update(KVConfigUpdater updater) throws KVConfigException;

    void deleteById(Long id) throws KVConfigException;

    Optional<KVConfig> getByConfigKey(String configKey);

    KVConfig getByConfigKeyEnsure(String configKey);

    List<KVConfig> findByConfigKeyCollection(Collection<String> configKeyCollection);

    List<KVConfig> findByConfigGroup(String configGroup);

    KVConfig getByConfigKeyOrElse(String configKey, String defaultValue);
}
