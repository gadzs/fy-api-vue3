package cn.turboinfo.fuyang.api.entity.common.enumeration.credit;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 评价状态
 */
@Slf4j
public enum CreditRatingStatus implements BaseEnum {

    PENDING(0, "待审核"),

    REVIEWED(10, "已审核"),

    NOT_PASS(30, "审核未通过"),
    ;

    private int value;
    private String name;

    CreditRatingStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CreditRatingStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<CreditRatingStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CreditRatingStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
