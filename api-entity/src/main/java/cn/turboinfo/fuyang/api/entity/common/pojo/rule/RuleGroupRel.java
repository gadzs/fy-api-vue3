package cn.turboinfo.fuyang.api.entity.common.pojo.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 规则组关联关系
 * author: sunshow.
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class RuleGroupRel extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 规则组ID
     */
    private Long ruleGroupId;

    /**
     * 对象类型
     */
    private EntityObjectType objectType;

    /**
     * 对象ID
     */
    private Long objectId;

    /**
     * 排序值
     */
    private Integer sortValue;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
