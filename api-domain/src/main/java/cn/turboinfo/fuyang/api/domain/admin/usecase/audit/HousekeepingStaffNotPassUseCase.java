package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.staff.HousekeepingStaffException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 家政人员审核不通过usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffNotPassUseCase extends AbstractUseCase<HousekeepingStaffNotPassUseCase.InputData, HousekeepingStaffNotPassUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final HousekeepingStaffAuditRecordService housekeepingStaffAuditRecordService;
    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val auditorId = inputData.getUserId();

        val housekeepingStaffData = housekeepingStaffService.getById(inputData.getId()).orElse(null);
        if (housekeepingStaffData == null || housekeepingStaffData.getStatus().getValue() != StaffStatus.DEFAULT.getValue()) {
            throw new HousekeepingStaffException("数据状态不正确，操作失败");
        }

        HousekeepingStaffUpdater.Builder builder = HousekeepingStaffUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingStaffUpdater.class, inputData);
        builder.withStatus(StaffStatus.NOT_PASS);
        HousekeepingStaff housekeepingStaff = housekeepingStaffService.update(builder.build());

        // 更新审核记录
        {
            val auditorUser = sysUserService.getByIdEnsure(auditorId);

            HousekeepingStaffAuditRecordCreator housekeepingStaffAuditRecord = HousekeepingStaffAuditRecordCreator.builder()
                    .withStaffId(housekeepingStaff.getId())
                    .withCompanyId(housekeepingStaff.getCompanyId())
                    .withAuditStatus(AuditStatus.NOT_PASS)
                    .withUserId(auditorId)
                    .withUserName(auditorUser.getUsername())
                    .withRemark(inputData.getRemark())
                    .build();
            housekeepingStaffAuditRecordService.save(housekeepingStaffAuditRecord);
        }

        return OutputData.builder()
                .id(housekeepingStaff.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "审核人不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "失败原因不能为空"
        )
        private String remark;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
