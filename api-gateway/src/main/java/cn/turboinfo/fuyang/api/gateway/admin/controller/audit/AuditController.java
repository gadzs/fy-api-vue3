package cn.turboinfo.fuyang.api.gateway.admin.controller.audit;

import cn.turboinfo.fuyang.api.domain.admin.usecase.audit.CreditRatingAuditRecordSearchUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.audit.HousekeepingShopAuditRecordSearchUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.audit.HousekeepingStaffAuditRecordSearchUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.audit.ProductAuditRecordSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.QCreditRatingAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.QHousekeepingShopAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.QProduct;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.QHousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.QHousekeepingStaff;
import cn.turboinfo.fuyang.api.gateway.admin.fo.credit.CreditRatingSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.product.ProductSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.shop.HousekeepingShopSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.staff.HousekeepingStaffSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/audit")
public class AuditController {
    private final HousekeepingShopAuditRecordSearchUseCase housekeepingShopAuditRecordSearchUseCase;

    private final HousekeepingStaffAuditRecordSearchUseCase housekeepingStaffAuditRecordSearchUseCase;

    private final ProductAuditRecordSearchUseCase productAuditRecordSearchUseCase;

    private final CreditRatingAuditRecordSearchUseCase creditRatingAuditRecordSearchUseCase;

    @RequiresPermissions("adminAudit:shopList")
    @RequestMapping("/shop/list")
    public LimitDataFO<HousekeepingShop> list(
            @Valid HousekeepingShopSearchFO search, LimitFO page, SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (search.getStatus() == null) {
            request.filter(Operator.IN, QHousekeepingShop.status, ShopStatus.PENDING, ShopStatus.NOT_PASS, ShopStatus.REVIEWED);
        }

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingShopAuditRecord.id + "|" + QSort.Order.DESC.name()});
        }

        HousekeepingShopAuditRecordSearchUseCase.OutputData outputData = housekeepingShopAuditRecordSearchUseCase.execute(HousekeepingShopAuditRecordSearchUseCase.InputData
                .builder()
                .userId(context.getUserId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions("adminAudit:staffList")
    @RequestMapping("/staff/list")
    public LimitDataFO<HousekeepingStaff> list(
            @Valid HousekeepingStaffSearchFO search, LimitFO page, SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (search.getStatus() == null) {
            request.filter(Operator.IN, QHousekeepingStaff.status, StaffStatus.DEFAULT, StaffStatus.NOT_PASS, StaffStatus.PUBLISHED);
        }

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingShopAuditRecord.id + "|" + QSort.Order.DESC.name()});
        }

        HousekeepingStaffAuditRecordSearchUseCase.OutputData outputData = housekeepingStaffAuditRecordSearchUseCase.execute(HousekeepingStaffAuditRecordSearchUseCase.InputData
                .builder()
                .userId(context.getUserId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions("adminAudit:productList")
    @RequestMapping("/product/list")
    public LimitDataFO<Product> list(
            @Valid ProductSearchFO search, LimitFO page, SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (search.getStatus() == null) {
            request.filter(Operator.IN, QProduct.status, ProductStatus.DEFAULT, ProductStatus.NOT_PASS, ProductStatus.PASSED);
        }

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingShopAuditRecord.id + "|" + QSort.Order.DESC.name()});
        }

        ProductAuditRecordSearchUseCase.OutputData outputData = productAuditRecordSearchUseCase.execute(ProductAuditRecordSearchUseCase.InputData
                .builder()
                .userId(context.getUserId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions("adminAudit:creditList")
    @RequestMapping("/credit/list")
    public LimitDataFO<CreditRating> list(@Valid CreditRatingSearchFO search,
                                          LimitFO page, SortFO sort, AdminRequestContext context) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QCreditRatingAuditRecord.id + "|" + QSort.Order.DESC.name()});
        }

        CreditRatingAuditRecordSearchUseCase.OutputData outputData = creditRatingAuditRecordSearchUseCase.execute(CreditRatingAuditRecordSearchUseCase.InputData
                .builder()
                .userId(context.getUserId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }
}
