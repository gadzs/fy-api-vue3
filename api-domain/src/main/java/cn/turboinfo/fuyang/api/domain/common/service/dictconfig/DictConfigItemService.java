package cn.turboinfo.fuyang.api.domain.common.service.dictconfig;

import cn.turboinfo.fuyang.api.entity.common.exception.dictconfig.DictConfigItemException;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItemCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItemUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DictConfigItemService {
    Optional<DictConfigItem> getById(Long id);

    DictConfigItem getByIdEnsure(Long id);

    List<DictConfigItem> findByIdCollection(Collection<Long> idCollection);

    DictConfigItem save(DictConfigItemCreator creator) throws DictConfigItemException;

    DictConfigItem update(DictConfigItemUpdater updater) throws DictConfigItemException;

    QResponse<DictConfigItem> searchList(QRequest request, QPage requestPage);

    void deleteById(Long id) throws DictConfigItemException;

    Optional<DictConfigItem> getDictConfigItem(String dictConfigKey, String itemValue);

    DictConfigItem getDictConfigItemEnsure(String dictConfigKey, String itemValue);

    List<DictConfigItem> findAllDictConfigItem(String dictConfigKey);

    void updateDictConfigKey(Long dictConfigId, String dictConfigKey);
}
