package cn.turboinfo.fuyang.api.domain.common.service.site;


/**
 * 网址服务
 */
public interface SiteUrlService {

    /**
     * @param relativePath 文件保存的相对路径
     * @return 上传图片文件外部访问路径
     */
    String getUploadExternalImgUrl(String relativePath);

    String getMainSiteUrl();

}
