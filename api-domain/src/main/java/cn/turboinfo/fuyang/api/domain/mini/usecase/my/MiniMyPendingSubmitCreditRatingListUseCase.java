package cn.turboinfo.fuyang.api.domain.mini.usecase.my;

import cn.turboinfo.fuyang.api.domain.common.component.product.ProductAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyPendingSubmitCreditRatingListItem;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 待评价列表
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniMyPendingSubmitCreditRatingListUseCase extends AbstractUseCase<MiniMyPendingSubmitCreditRatingListUseCase.InputData, MiniMyPendingSubmitCreditRatingListUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;
    private final ServiceOrderDataFactory serviceOrderDataFactory;

    private final ProductAssembleHelper productAssembleHelper;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        QPage requestPage = inputData.getRequestPage();

        QRequest request = QRequest.newInstance()
                .filterEqual(QServiceOrder.userId, userId);
        request.filterEqual(QServiceOrder.orderStatus, ServiceOrderStatus.COMPLETED);

        // 按创建时间降序排序
        requestPage.addOrder(QServiceOrder.createdTime, QSort.Order.DESC);

        QResponse<ServiceOrder> orderQResponse = serviceOrderService.findAll(request, requestPage);

        // 拼装企业名称
        serviceOrderDataFactory.assembleCompany(orderQResponse.getPagedData().stream().toList());

        QResponse<MiniMyPendingSubmitCreditRatingListItem> itemList = orderQResponse.map(serviceOrder -> {

                    serviceOrderDataFactory.maskInfo(serviceOrder);

                    return MiniMyPendingSubmitCreditRatingListItem.builder()
                            .serviceOrder(BeanMapper.map(serviceOrder, ViewServiceOrderFO.class))
                            .build();
                }
        );

        productAssembleHelper.assembleProduct(
                itemList.getPagedData(),
                item -> item.getServiceOrder().getProductId(),
                ProductAssembleHelper.ASSEMBLE_ATTACHMENT,
                MiniMyPendingSubmitCreditRatingListItem::setProduct);

        return OutputData.builder()
                .itemList(itemList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        private Long userId;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 待评价列表
         */
        private QResponse<MiniMyPendingSubmitCreditRatingListItem> itemList;

    }
}
