package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 支付方式
 */
@Slf4j
public enum PayType implements BaseEnum {

    POST_PAID(0, "服务后付费"),

    OFFLINE(10, "线下付款"),

    WECHAT(20, "微信支付"),

    ;

    private int value;
    private String name;

    PayType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PayType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<PayType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<PayType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
