package cn.turboinfo.fuyang.api.provider.common.repository.database.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import io.hypersistence.utils.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付订单
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(
        name = "pay_order"
)
@Where(clause = "deleted = 0")
@Entity
public class PayOrderPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 支付人ID
     */
    private Long userId;

    /**
     * 支付对象类型
     */
    private EntityObjectType objectType;

    /**
     * 支付对象ID
     */
    private Long objectId;

    /**
     * 支付类型
     */
    private PayType payType;

    /**
     * 支付订单状态
     */
    private PayOrderStatus payOrderStatus;

    /**
     * 支付成功时间
     */
    private LocalDateTime paidTime;

    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 三方交易编码
     */
    private String transactionId;
}
