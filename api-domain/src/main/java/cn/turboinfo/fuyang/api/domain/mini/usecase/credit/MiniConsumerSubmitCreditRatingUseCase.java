package cn.turboinfo.fuyang.api.domain.mini.usecase.credit;

import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

/**
 * 消费者提交评价
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerSubmitCreditRatingUseCase extends AbstractUseCase<MiniConsumerSubmitCreditRatingUseCase.InputData, MiniConsumerSubmitCreditRatingUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;
    private final SysUserService sysUserService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {


        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getComment())) {
            throw new ProductException("评价内容包含敏感词汇，请修改后重新提交！");
        }

        Long userId = inputData.getUserId();
        Long serviceOrderId = inputData.getServiceOrderId();
        Long score = inputData.getScore();
        String comment = inputData.getComment();
        List<Long> imageIdList = inputData.getImageIdList();

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(serviceOrderId);

        if (serviceOrder.getOrderStatus() != ServiceOrderStatus.COMPLETED) {
            throw new IllegalArgumentException("订单完成后才能评价");
        }

        val user = sysUserService.getByIdEnsure(userId);

        serviceOrderService.submitCreditRating(serviceOrderId, userId, user.getUsername(), score, comment, imageIdList);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        @Positive
        private Long userId;

        @NotNull(message = "订单不能为空")
        @Positive
        private Long serviceOrderId;

        /**
         * 评分
         */
        @NotNull(message = "评分不能为空")
        @Positive
        private Long score;

        private String comment;

        private List<Long> imageIdList;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
