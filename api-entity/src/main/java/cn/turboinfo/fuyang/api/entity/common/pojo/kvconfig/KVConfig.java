package cn.turboinfo.fuyang.api.entity.common.pojo.kvconfig;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * KV 配置
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class KVConfig extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 配置组名称
     */
    private String configGroup;

    /**
     * 配置 Key
     */
    private String configKey;

    /**
     * 配置 Value
     */
    private String configValue;


    /**
     * 描述
     */
    private String description;

    /**
     * 组内顺序
     */
    private Integer groupOrder;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
