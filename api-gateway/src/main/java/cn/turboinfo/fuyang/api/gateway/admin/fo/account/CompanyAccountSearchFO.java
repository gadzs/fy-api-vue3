package cn.turboinfo.fuyang.api.gateway.admin.fo.account;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class CompanyAccountSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "企业ID"
    )
    private Long companyId;

    @QField(
            placeholder = "应用ID",
            operator = Operator.LIKE
    )
    private String appid;

    @QField(
            placeholder = "账户类型",
            control = Control.SELECT,
            ref = "accountTypeList",
            template = "enum"
    )
    private AccountType type;

    @QField(
            placeholder = "账户",
            operator = Operator.LIKE
    )
    private String account;

    @QField(
            placeholder = "账户名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "关系类型",
            control = Control.SELECT,
            ref = "relationTypeList",
            template = "enum"
    )
    private RelationType relationType;

    @QField(
            placeholder = "自定义关系类型",
            operator = Operator.LIKE
    )
    private String customRelation;

    @QField(
            placeholder = "账户状态",
            control = Control.SELECT,
            ref = "accountStatusList",
            template = "enum"
    )
    private AccountStatus status;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
