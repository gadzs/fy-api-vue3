package cn.turboinfo.fuyang.api.gateway.admin.fo.role;

import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import lombok.Data;

@Data
public class CheckableRoleFO {

    private Role role;

    private boolean checked;

}
