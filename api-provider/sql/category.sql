DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`
(
    `id`           BIGINT(1)    NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(64)  NOT NULL DEFAULT '',
    `display_name` VARCHAR(64)  NOT NULL DEFAULT '',
    `code`         VARCHAR(64)  NOT NULL DEFAULT '',
    `description`  VARCHAR(255) NOT NULL DEFAULT '',
    `parent_id`    BIGINT(1)    NOT NULL DEFAULT 0,
    `sort_value`   INT(1)       NOT NULL DEFAULT 0,
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('家政服务', '家政服务', 'c1', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('日常保洁', '日常保洁', 'c1_1', '', @categoryParentId, 0),
       ('深度保洁', '深度保洁', 'c1_2', '', @categoryParentId, 0),
       ('开荒保洁', '开荒保洁', 'c1_3', '', @categoryParentId, 0),
       ('抽油烟机清洗', '抽油烟机清洗', 'c1_4', '', @categoryParentId, 0),
       ('冰箱清洗', '冰箱清洗', 'c1_5', '', @categoryParentId, 0),
       ('洗衣机清洗', '洗衣机清洗', 'c1_6', '', @categoryParentId, 0),
       ('空调清洗', '空调清洗', 'c1_7', '', @categoryParentId, 0),
       ('擦玻璃', '擦玻璃', 'c1_8', '', @categoryParentId, 0),
       ('除尘除螨', '除尘除螨', 'c1_9', '', @categoryParentId, 0),
       ('除甲醛', '除甲醛', 'c1_10', '', @categoryParentId, 0);

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('保姆月嫂', '保姆月嫂', 'c2', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('保姆', '保姆', 'c2_1', '', @categoryParentId, 0),
       ('月嫂', '月嫂', 'c2_2', '', @categoryParentId, 0),
       ('育儿嫂', '育儿嫂', 'c2_3', '', @categoryParentId, 0),
       ('老人陪护', '老人陪护', 'c2_4', '', @categoryParentId, 0),
       ('病人陪护', '病人陪护', 'c2_5', '', @categoryParentId, 0);

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('家电维修', '家电维修', 'c3', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('冰箱维修', '冰箱维修', 'c3_1', '', @categoryParentId, 0),
       ('洗衣机维修', '洗衣机维修', 'c3_2', '', @categoryParentId, 0),
       ('油烟机维修', '油烟机维修', 'c3_3', '', @categoryParentId, 0),
       ('电视维修', '电视维修', 'c3_4', '', @categoryParentId, 0),
       ('空调维修', '空调维修', 'c3_5', '', @categoryParentId, 0),
       ('小家电维修', '小家电维修', 'c3_6', '', @categoryParentId, 0);

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('管道疏通', '管道疏通', 'c4', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('浴缸疏通', '浴缸疏通', 'c4_1', '', @categoryParentId, 0),
       ('马桶疏通', '马桶疏通', 'c4_2', '', @categoryParentId, 0),
       ('洗手盆疏通', '洗手盆疏通', 'c4_3', '', @categoryParentId, 0),
       ('洗菜盆疏通', '洗菜盆疏通', 'c4_4', '', @categoryParentId, 0),
       ('地漏疏通', '地漏疏通', 'c4_5', '', @categoryParentId, 0),
       ('下水道疏通', '下水道疏通', 'c4_6', '', @categoryParentId, 0),
       ('管道疏通', '管道疏通', 'c4_7', '', @categoryParentId, 0);
