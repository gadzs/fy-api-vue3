package cn.turboinfo.fuyang.api.domain.common.service.profit;

import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharingRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 分账记录服务
 * author: hai
 */
public interface ProfitSharingRecordService extends BaseQService<ProfitSharingRecord, Long> {

    List<ProfitSharingRecord> findByProfitSharingId(Long profitSharingId);

}
