package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;
import net.sunshow.toolkit.core.base.enums.EnableStatus;

import java.time.LocalDateTime;

@Data
public class SysUserRespFO {

    private Long id;

    private String username;

    private String mobile;

    private EnableStatus status;

    private Long agenciesId;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

}
