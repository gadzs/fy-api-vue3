package cn.turboinfo.fuyang.api.provider.admin.component.security;

public interface SecurityEncoder {

    String getPublicKey();

    String decrypt(String encryptText);

}
