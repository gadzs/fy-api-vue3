package cn.turboinfo.fuyang.api.domain.admin.usecase.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.AgenciesUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class AgenciesUpdateUseCase extends AbstractUseCase<AgenciesUpdateUseCase.InputData, AgenciesUpdateUseCase.OutputData> {
    private final AgenciesService agenciesService;

    @Override
    protected OutputData doAction(InputData inputData) {

        AgenciesUpdater.Builder builder = AgenciesUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, AgenciesUpdater.class, inputData);

        Agencies agencies = agenciesService.update(builder.build());

        return OutputData.builder()
                .agencies(agencies)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "name不能为空"
        )
        private String name;

        @NotNull(
                message = "地址编码不能为空"
        )
        private Long areaCode;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Agencies agencies;
    }
}
