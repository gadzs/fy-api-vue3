package cn.turboinfo.fuyang.api.gateway.admin.fo.dictconfig;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class DictConfigSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "字典名称",
            operator = Operator.LIKE
    )
    private String dictName;

    @QField(
            placeholder = "字典 Key",
            operator = Operator.LIKE
    )
    private String dictKey;

    @QField(
            placeholder = "字典描述",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
