package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuthLabelService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAuthLabelSearchUseCase extends AbstractUseCase<HousekeepingCompanyAuthLabelSearchUseCase.InputData, HousekeepingCompanyAuthLabelSearchUseCase.OutputData> {
    private final HousekeepingCompanyAuthLabelService housekeepingCompanyAuthLabelService;

    @Override
    protected OutputData doAction(InputData inputData) {
        QResponse<HousekeepingCompanyAuthLabel> response = housekeepingCompanyAuthLabelService.findAll(inputData.request, inputData.getRequestPage());
        return HousekeepingCompanyAuthLabelSearchUseCase.OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<HousekeepingCompanyAuthLabel> qResponse;
    }
}
