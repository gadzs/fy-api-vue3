package cn.turboinfo.fuyang.api.entity.admin.enumeration.cms;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public enum CmsArticleType implements BaseEnum {
    DEFAULT(0, "默认"),
    ARTICLE(1, "文章"),
    PICTURE(2, "图片"),
    LINK(3, "链接"),
    DOC(4, "文件");

    private int value;
    private String name;

    CmsArticleType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CmsArticleType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<CmsArticleType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CmsArticleType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }

    public static List<Map<String, Object>> getList() {
        return getList(null, null);
    }

    public static List<Map<String, Object>> getList(String keyName, String valName) {
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (CmsArticleType c : CmsArticleType.values()) {
            Map<String, Object> data = new HashMap<>();
            data.put((keyName != null) ? keyName : "name", String.valueOf(c.getName()));
            data.put((valName != null) ? valName : "value", c.getValue());
            dataList.add(data);
        }
        return dataList;
    }
}
