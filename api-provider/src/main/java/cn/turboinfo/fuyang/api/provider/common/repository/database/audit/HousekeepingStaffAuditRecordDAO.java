package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface HousekeepingStaffAuditRecordDAO extends FYRepository<HousekeepingStaffAuditRecordPO, Long> {
    List<HousekeepingStaffAuditRecordPO> findByCompanyIdOrderByCreatedTimeDesc(Long companyId);

    List<HousekeepingStaffAuditRecordPO> findByStaffIdOrderByCreatedTimeDesc(Long staffId);

}
