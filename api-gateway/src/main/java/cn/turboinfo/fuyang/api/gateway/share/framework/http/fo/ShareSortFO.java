package cn.turboinfo.fuyang.api.gateway.share.framework.http.fo;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class ShareSortFO {

    private String[] sortFields;

    public List<QSort> toQSortList() {
        List<QSort> sortList = new ArrayList<>();
        if (sortFields != null) {
            for (String sortField : sortFields) {
                String[] values = StringUtils.split(sortField, '|');
                sortList.add(new QSort(values[0], QSort.Order.valueOf(values[1])));
            }
        }
        return sortList;
    }

}
