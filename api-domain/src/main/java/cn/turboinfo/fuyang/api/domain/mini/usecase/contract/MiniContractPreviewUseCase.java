package cn.turboinfo.fuyang.api.domain.mini.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractService;
import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.order.PayOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.mini.helper.ContractHelper;
import cn.turboinfo.fuyang.api.domain.util.ContractUtils;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileAttachmentHelper;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.contract.ContractStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.Contract;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachmentCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class MiniContractPreviewUseCase extends AbstractUseCase<MiniContractPreviewUseCase.InputData, MiniContractPreviewUseCase.OutputData> {

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final ServiceOrderService serviceOrderService;

    private final FileAttachmentService fileAttachmentService;

    private final ContractService contractService;

    private final DivisionService divisionService;

    private final ProductService productService;

    private final PayOrderService payOrderService;

    private final FileAttachmentHelper fileAttachmentHelper;
    private final ContractHelper contractHelper;

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy 年 MM 月 dd 日");
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

    @Override
    protected OutputData doAction(InputData inputData) {

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(inputData.getOrderId());

        ContractTmpl contractTmpl = contractHelper.getContractTmpl(serviceOrder);

        // 没找到模版抛出异常
        if (contractTmpl == null) {
            throw new RuntimeException("没有合同模版");
        }

        String displayName = String.format("%s.pdf",
                contractTmpl.getName()
        );

        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(serviceOrder.getCompanyId());
        LocalDate now = LocalDate.now();
        String dateStr = now.format(dateFormatter);

        // 替换内容
        Map<String, Object> mappingValueMap = new HashMap<>();
        mappingValueMap.put("contractId", ContractUtils.generateContractCode(serviceOrder.getCompanyId(), serviceOrder.getCategoryId(), serviceOrder.getId()));
        mappingValueMap.put("contractDate", dateStr);
        mappingValueMap.put("companyName", company.getName());
        mappingValueMap.put("legalPerson", company.getLegalPerson());
        mappingValueMap.put("uscc", company.getUscc());
        mappingValueMap.put("companyMobile", company.getContactNumber());
        mappingValueMap.put("companyAddress", company.getRegisteredAddress());
        mappingValueMap.put("consumer", serviceOrder.getContact());
        mappingValueMap.put("consumerMobile", serviceOrder.getMobile());
        mappingValueMap.put("serviceMode", "________________");
        mappingValueMap.put("serviceDate", String.format("%s 到 %s", serviceOrder.getScheduledStartTime().format(dateFormatter), serviceOrder.getScheduledEndTime().format(dateFormatter)));
        mappingValueMap.put("serviceTime", String.format("%s 到 %s", serviceOrder.getScheduledStartTime().format(timeFormatter), serviceOrder.getScheduledEndTime().format(timeFormatter)));
        mappingValueMap.put("serviceCharge", serviceOrder.getPrice());
        mappingValueMap.put("settlementMethod", "________________");
        mappingValueMap.put("paymentMethod", StringUtils.EMPTY);
        mappingValueMap.put("paymentDate", "  年  月  日");
        mappingValueMap.put("consumerSignature", "________________");
        mappingValueMap.put("signatureDate", "  年  月  日");
        mappingValueMap.put("serviceAddress", "________________________________");
        mappingValueMap.put("serviceContent", "________________");

        // 查询地区
        Division division = divisionService.getById(serviceOrder.getDivisionId()).orElse(null);
        if (division != null) {
            Map<Long, Division> divisionMap = divisionService.findByIdCollection(Arrays.asList(Long.parseLong(StringUtils.rightPad(division.getProvinceCode().toString(), 6, "0")), Long.parseLong(StringUtils.rightPad(division.getCityCode().toString(), 6, "0"))))
                    .stream()
                    .collect(Collectors.toMap(Division::getAreaCode, o -> o));
            String address = StringUtils.EMPTY;
            if (divisionMap.containsKey(division.getProvinceCode())) {
                address += divisionMap.get(division.getProvinceCode()).getAreaName() + " 省";
            }
            if (divisionMap.containsKey(division.getCityCode())) {
                address += " " + divisionMap.get(division.getCityCode()).getAreaName() + " 市";
            }
            address += " " + division.getAreaName() + " " + serviceOrder.getPoiName() + " " + serviceOrder.getDetail();
            mappingValueMap.put("serviceAddress", address);
        }

        // 查询产品
        productService.getById(serviceOrder.getProductId())
                .ifPresent(product -> mappingValueMap.put("serviceContent", product.getName()));

        // 查询支付
        payOrderService.findByObjectIdAndStatus(EntityObjectType.SERVICE_ORDER, serviceOrder.getId(), PayOrderStatus.PAID)
                .stream()
                .findFirst()
                .ifPresent(payOrder -> {
                    mappingValueMap.put("paymentMethod", payOrder.getPayType().getName());
                    mappingValueMap.put("paymentDate", payOrder.getPaidTime().format(dateFormatter));
                });

        // 查询是否签署过合同
        Optional<Contract> optionalContract = contractService.findByOrderId(serviceOrder.getId());

        if (optionalContract.isPresent()) {
            Contract contract = optionalContract.get();
            FileAttachment signature = fileAttachmentService.findByRefIdAndRefType(contract.getId(), FileRefTypeConstant.CONTRACT_SIGNATURE)
                    .stream()
                    .findFirst()
                    .orElse(null);

            if (signature != null) {
                // 签名文件
                File signatureFile = fileAttachmentHelper.readFileAttachment(signature);
                try (InputStream is = new FileInputStream(signatureFile)) {

                    mappingValueMap.put("consumerSignature", IOUtils.toByteArray(is));
                    mappingValueMap.put("signatureDate", dateStr);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        String relativePath = String.format("temporary/contract/%s/%s", now.getYear(), now.getMonthValue());
        Path absolutePath = fileAttachmentService.resolveAbsolutePath(relativePath);

        // 生成合同
        List<String> imageNameList = contractHelper.generateContractImage(contractTmpl, mappingValueMap, absolutePath);
        List<Long> fileIdList = new ArrayList<>();
        for (String imageName : imageNameList) {
            FileAttachmentCreator creator = FileAttachmentCreator.builder()
                    .withDisplayName(displayName)
                    .withFilename(imageName)
                    .withOriginalFilename(displayName)
                    .withRefType(FileRefTypeConstant.TEMPORARY_CONTRACT_IMG)
                    .withRefId(serviceOrder.getId())
                    .withRelativePath(relativePath)
                    .build();
            FileAttachment f = fileAttachmentService.save(creator);
            fileIdList.add(f.getId());
        }

        return OutputData.builder()
                .fileIdList(fileIdList)
                .status(optionalContract.isEmpty() ? ContractStatus.INIT : optionalContract.get().getStatus())
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "订单编码不能为空"
        )
        private Long orderId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<Long> fileIdList;

        private ContractStatus status;

    }
}
