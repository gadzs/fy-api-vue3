package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.admin.exception.user.SysUserRoleException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRole;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRoleCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRoleUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface SysUserRoleService {
    Optional<SysUserRole> getById(Long id);

    SysUserRole save(SysUserRoleCreator creator) throws SysUserRoleException;

    SysUserRole update(SysUserRoleUpdater updater) throws SysUserRoleException;

    QResponse<SysUserRole> findAll(QRequest request, QPage requestPage);

    List<SysUserRole> findBySysUserId(Long sysUserId);

    /**
     * 重新分配角色
     *
     * @param sysUserId        系统用户ID
     * @param roleIdCollection 要分配的角色ID集合
     */
    void reassign(Long sysUserId, Collection<Long> roleIdCollection);

    SysUserRole assignByCode(Long sysUserId, String code);
}
