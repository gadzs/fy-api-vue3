package cn.turboinfo.fuyang.api.entity.common.pojo.category;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 类别
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Category extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 分类编码
     */
    private String code;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 父级 ID
     */
    private Long parentId;

    /**
     * 排序值
     */
    private Integer sortValue;

    /**
     * 图标
     */
    private Long iconId;

    private List<Category> children;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
