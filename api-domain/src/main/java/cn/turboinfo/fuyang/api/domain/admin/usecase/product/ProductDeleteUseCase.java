package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProductDeleteUseCase extends AbstractUseCase<ProductDeleteUseCase.InputData, ProductDeleteUseCase.OutputData> {
    private final ProductService productService;

    @Override
    protected OutputData doAction(InputData inputData) {
        productService.deleteById(inputData.getId());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;

        private Long companyId;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
