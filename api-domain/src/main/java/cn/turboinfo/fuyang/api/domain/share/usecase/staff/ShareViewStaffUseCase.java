package cn.turboinfo.fuyang.api.domain.share.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.entity.common.exception.common.DataNotExistException;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.share.fo.staff.ShareStaffFO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 家政员详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareViewStaffUseCase extends AbstractUseCase<ShareViewStaffUseCase.InputData, ShareViewStaffUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;

    private final DivisionService divisionService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        String code = inputData.getCode();
        HousekeepingStaff staff = housekeepingStaffService
                .findByCode(code)
                .orElseThrow(DataNotExistException::new);

        if (!staff.getCompanyId().equals(companyId)) {
            throw new RuntimeException("家政员不属于该企业");
        }

        Division division = divisionService.getById(staff.getProvinceCode()).orElse(new Division());

        ShareStaffFO.ShareStaffFOBuilder builder = ShareStaffFO.builder();
        BeanHelper.copyPropertiesToBuilder(builder, ShareStaffFO.class, staff);

        return OutputData.builder()
                .staff(builder
                        .provinceName(division.getAreaName())
                        .age(IdCardUtils.countAge(staff.getIdCard()))
                        .idCard(IdCardUtils.maskIdCareNum(staff.getIdCard()))
                        .contactMobile(PhoneUtils.maskPhoneNum(staff.getContactMobile()))
                        .build())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "公司ID不能为空")
        @Positive
        private Long companyId;

        @NotBlank(message = "家政员ID不能为空")
        private String code;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private ShareStaffFO staff;

    }
}
