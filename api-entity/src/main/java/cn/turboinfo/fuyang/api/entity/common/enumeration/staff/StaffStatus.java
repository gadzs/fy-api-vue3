package cn.turboinfo.fuyang.api.entity.common.enumeration.staff;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * @author gadzs
 * @description 家政员状态
 * @date 2023/2/7 16:45
 */
@Slf4j
public enum StaffStatus implements BaseEnum {

    DEFAULT(0, "待审核"),

    PUBLISHED(10, "已审核"),

    LEAVE(20, "已离职"),

    NOT_PASS(30, "审核未通过"),

    BLACK(99, "黑名单");

    private int value;
    private String name;

    StaffStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static StaffStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<StaffStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<StaffStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
