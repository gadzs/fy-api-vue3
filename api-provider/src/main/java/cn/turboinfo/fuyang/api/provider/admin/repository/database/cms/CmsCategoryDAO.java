package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface CmsCategoryDAO extends BaseRepository<CmsCategoryPO, Long> {
    List<CmsCategoryPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<CmsCategoryPO> findAllByOrderByParentIdDescSortDescIdAsc();

    List<CmsCategoryPO> findAllByParentId(Long parentId);

    List<CmsCategoryPO> findAllByCode(String code);
}
