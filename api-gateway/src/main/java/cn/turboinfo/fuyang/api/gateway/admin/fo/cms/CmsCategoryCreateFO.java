package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CmsCategoryCreateFO {
    @NotBlank(
            message = "名称不能为空"
    )
    private String name;

    @NotBlank(
            message = "编码不能为空"
    )
    private String code;

    private String description;

    @NotNull(
            message = "类型不能为空"
    )
    private CmsCategoryType type;

    private String slugName;

    /**
     * 链接
     */
    private String linkedUrl;

    private Long parentId;

    private Integer indexShow;

    private Integer sort;
}
