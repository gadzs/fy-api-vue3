package cn.turboinfo.fuyang.api.domain.util;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * 用户名工具
 */
public class UsernameUtils {

    /**
     * 用户名正则
     */
    private static final Pattern PATTERN_USERNAME = Pattern.compile("^[a-zA-Z\\d\\u4e00-\\u9fa5]+$");

    private static final Pattern PATTERN_PHONE_NUM = Pattern.compile("^1\\d{10}$");

    /**
     * 用户名是否合法
     *
     * @param username 用户名
     * @return 是否合法
     */
    public static boolean isValidUsername(String username) {
        return Optional.ofNullable(username).isPresent() && PATTERN_USERNAME.matcher(username).matches();
    }

    public static boolean isPhoneNum(String username) {
        return Optional.ofNullable(username).isPresent() && PATTERN_PHONE_NUM.matcher(username).matches();
    }

    public static String maskUsername(String username) {
        return maskUsername(username, 1, 1);
    }

    public static String maskUsername(String username, int leftRest, int rightRest) {
        return MaskUtils.maskPartialString(username, leftRest, rightRest, "*", 1);
    }
}
