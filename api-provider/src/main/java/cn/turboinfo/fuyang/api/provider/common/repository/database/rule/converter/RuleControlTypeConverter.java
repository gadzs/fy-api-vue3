package cn.turboinfo.fuyang.api.provider.common.repository.database.rule.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.RuleControlType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: sunshow.
 */
@Converter(autoApply = true)
public class RuleControlTypeConverter extends BaseEnumConverter<RuleControlType> {
}
