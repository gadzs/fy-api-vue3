package cn.turboinfo.fuyang.api.domain.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author gadzs
 * @description 货币单位工具类
 * @date 2023/3/28 15:07
 */
public class CurrencyUnitUtils {

    /**
     * 获取以分为单位的金额
     *
     * @param amount 金额
     * @return 以分为单位的金额
     */
    public static Long getCentsAmount(BigDecimal amount) {
        return amount.multiply(new BigDecimal(100)).longValue();
    }

    /**
     * 获取以元为单位的金额
     *
     * @param centsAmount 以分为单位的金额
     * @return 以元为单位的金额
     */
    public static BigDecimal getAmountByCents(Long centsAmount) {
        return BigDecimal.valueOf(centsAmount).divide(new BigDecimal(100));
    }

    /**
     * 获取以元为单位的金额
     *
     * @param centsAmount 以分为单位的金额
     * @return 以元为单位的金额
     */
    public static BigDecimal getAmountByCents(BigDecimal centsAmount) {
        return centsAmount.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }
}
