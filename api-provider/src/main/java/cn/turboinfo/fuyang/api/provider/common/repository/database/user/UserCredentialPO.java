package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(
        name = "user_credential"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
public class UserCredentialPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    @Column(
            name = "user_id"
    )
    private Long userId;

    private String credential;

    private String salt;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
