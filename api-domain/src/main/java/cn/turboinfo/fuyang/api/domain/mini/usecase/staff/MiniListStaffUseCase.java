package cn.turboinfo.fuyang.api.domain.mini.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 查看门店详情
 *
 * @author gadzs
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniListStaffUseCase extends AbstractUseCase<MiniListStaffUseCase.InputData, MiniListStaffUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;
    private final DivisionService divisionService;

    @Override
    protected MiniListStaffUseCase.OutputData doAction(MiniListStaffUseCase.InputData inputData) {

        QResponse<HousekeepingStaff> response = housekeepingStaffService.findAll(inputData.request, inputData.getRequestPage());

        List<HousekeepingStaff> staffList = new ArrayList<>(response.getPagedData());

        // 地区编码
        List<Long> provinceCodeList = new ArrayList<>();

        staffList.forEach(staff -> {
            provinceCodeList.add(staff.getProvinceCode());
        });

        val divisionMap = divisionService.findByIdCollection(provinceCodeList).stream().collect(Collectors.toMap(Division::getId, Function.identity()));


        List<ViewStaffFO> foList = staffList
                .stream()
                .map(staff -> {
                    ViewStaffFO ViewStaffFO = BeanMapper.map(staff, ViewStaffFO.class);

                    if (divisionMap.containsKey(staff.getProvinceCode())) {
                        ViewStaffFO.setProvinceName(divisionMap.get(staff.getProvinceCode()).getAreaName());
                    }
                    ViewStaffFO.setAge(IdCardUtils.countAge(staff.getIdCard()));
                    return ViewStaffFO;
                })
                .collect(Collectors.toList());

        QResponse<ViewStaffFO> foResponse = new QResponse<>(response.getPage(), response.getPageSize(), foList, response.getTotal());

        return MiniListStaffUseCase.OutputData.builder()
                .qResponse(foResponse)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ViewStaffFO> qResponse;
    }
}
