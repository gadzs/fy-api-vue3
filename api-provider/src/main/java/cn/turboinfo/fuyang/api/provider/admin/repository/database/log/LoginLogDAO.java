package cn.turboinfo.fuyang.api.provider.admin.repository.database.log;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface LoginLogDAO extends BaseRepository<LoginLogPO, Long> {
    List<LoginLogPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<LoginLogPO> findByUsernameOrderByLoginTimeDesc(String username, Pageable pagable);
}
