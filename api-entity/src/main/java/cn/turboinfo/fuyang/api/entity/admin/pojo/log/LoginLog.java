package cn.turboinfo.fuyang.api.entity.admin.pojo.log;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.QBean;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanCreator;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanCreatorIgnore;
import net.sunshow.toolkit.core.qbean.api.annotation.QBeanID;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 登录日志
 */
@Getter
@Setter
@QBean
@QBeanCreator
public class LoginLog extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 登录用户名
     */
    private String username;

    /**
     * 系统用户ID
     */
    private Long sysUserId;

    /**
     * 登录来源
     */
    private String loginSource;

    /**
     * 登录行为发生的时间
     */
    private LocalDateTime loginTime;

    /**
     * 是否登录成功
     */
    private YesNoStatus loginStatus;

    /**
     * 登录备注信息
     */
    private String loginRemark;

    @QBeanCreatorIgnore
    private LocalDateTime createdTime;

}
