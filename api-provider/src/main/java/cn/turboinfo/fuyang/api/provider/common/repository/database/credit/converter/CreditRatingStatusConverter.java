package cn.turboinfo.fuyang.api.provider.common.repository.database.credit.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * 评价状态转换器
 *
 * @author hai
 */
@Converter(
        autoApply = true
)
public class CreditRatingStatusConverter extends BaseEnumConverter<CreditRatingStatus> {
}
