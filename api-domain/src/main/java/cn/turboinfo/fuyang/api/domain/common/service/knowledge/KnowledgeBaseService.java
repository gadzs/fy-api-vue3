package cn.turboinfo.fuyang.api.domain.common.service.knowledge;

import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 知识库服务
 * author: hai
 */
public interface KnowledgeBaseService extends BaseQService<KnowledgeBase, Long> {

    List<KnowledgeBase> findAllByType(String type);

}
