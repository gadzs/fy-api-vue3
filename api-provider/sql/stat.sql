SET NAMES `utf8mb4`;

DROP TABLE IF EXISTS `order_stat`;
CREATE TABLE `order_stat`
(
    `id`           BIGINT   NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT   NOT NULL DEFAULT 0 COMMENT '企业编码',
    `category_id`  BIGINT   NOT NULL DEFAULT 0 COMMENT '分类编码',
    `year`         INT      NOT NULL DEFAULT 0 COMMENT '年份',
    `month`        INT      NOT NULL DEFAULT 0 COMMENT '月份',
    `order_count`  BIGINT   NOT NULL DEFAULT 0 COMMENT '订单数量',
    `created_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_companyid_categoryid_year_month` (`company_id`, `category_id`, `year`, `month`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='订单统计';
