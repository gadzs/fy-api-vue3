package cn.turboinfo.fuyang.api.gateway.admin.contract.account

import cn.turboinfo.fuyang.api.domain.mini.usecase.account.MiniCompanyAccountAuthUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.account.MiniCompanyAccountListUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/mini/account"])
@SessionScope
interface MiniAccountContract {

    // 企业账号列表
    @CompanyScope
    @AutoMappingContract(
        beanType = MiniCompanyAccountListUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun list()

    @CompanyScope
    @AutoMappingContract(
        beanType = MiniCompanyAccountAuthUseCase::class,
    )
    fun accountAuth()
}
