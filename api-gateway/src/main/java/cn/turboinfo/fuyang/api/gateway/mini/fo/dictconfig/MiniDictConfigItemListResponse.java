package cn.turboinfo.fuyang.api.gateway.mini.fo.dictconfig;

import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.IntegerViewDictConfigItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MiniDictConfigItemListResponse {

    private List<IntegerViewDictConfigItem> dictConfigItemList;

}
