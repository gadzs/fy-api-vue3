-- 2023-02-02 产品规格权限
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('规格管理', 'spec:null', '', '/spec', '', 1, 0);
SET
    @specPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('规格组列表', 'spec:list', '', '/spec/list', '', 1, @specPermissionParentId),
       ('新增规格组', 'spec:create', '', '', '', 0, @specPermissionParentId),
       ('更新规格组', 'spec:update', '', '', '', 0, @specPermissionParentId),
       ('删除规格组', 'spec:delete', '', '', '', 0, @specPermissionParentId);

-- 2023-02-02 添加门店菜单
SET
    @shopPermissionParentId = (select id
                               from sys_permission
                               where resource = 'shop:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('新建门店', 'shop:create', '', '', '', 0, @shopPermissionParentId);

-- 2023-02-02 调整规格
-- 规格增加规格组编码
ALTER TABLE `spec`
    ADD COLUMN `spec_set_id` BIGINT(1) NOT NULL DEFAULT 0 AFTER `parent_id`;

-- 规格组删掉关联对象字段
ALTER TABLE `spec_set`
    DROP INDEX `idx_objectid`;
ALTER TABLE `spec_set`
    DROP
        COLUMN `object_id`;
ALTER TABLE `spec_set`
    DROP
        COLUMN `object_type`;

-- 规格组关联表增加字段
ALTER TABLE `spec_set_rel`
    DROP INDEX `uk_specsetid_specid`;
ALTER TABLE `spec_set_rel`
    DROP
        COLUMN `spec_id`;
ALTER TABLE `spec_set_rel`
    ADD COLUMN `object_type` INT(1) NOT NULL DEFAULT 0 AFTER `spec_set_id`;
ALTER TABLE `spec_set_rel`
    ADD COLUMN `object_id` BIGINT(1) NOT NULL DEFAULT 0 AFTER `object_type`;

-- 2023.2 商品管理
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('产品服务管理', 'product:null', '', '', '', 1, 0);
SET
    @productPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('产品服务列表', 'product:list', '', '/product/list', '', 1, @productPermissionParentId),
       ('修改产品服务', 'product:update', '', '/product/edit', '', 0, @productPermissionParentId),
       ('新增产品服务', 'product:create', '', '/product/create', '', 0, @productPermissionParentId),
       ('删除产品服务', 'product:delete', '', '', '', 0, @productPermissionParentId);

-- 2023-02-03 规格组关联表增加删除字段
ALTER TABLE `spec_set_rel`
    ADD COLUMN `deleted` BIGINT(1) NOT NULL DEFAULT 0 AFTER `object_id`;

ALTER TABLE `spec_set_rel`
    DROP INDEX `uk_specsetid_objectid_objecttype`;
ALTER TABLE `spec_set_rel`
    ADD UNIQUE KEY `uk_specsetid_objectid_objecttype` (`spec_set_id`, `object_id`, `object_type`, `deleted`);

-- 2023-02-07 规格组增加企业编码
ALTER TABLE `spec_set`
    ADD COLUMN `company_id` BIGINT(1) NOT NULL DEFAULT 0 AFTER `name`;

-- 2023-02-07 家政员管理
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('家政员管理', 'staff:null', '', '/staff', '', 1, 0);
SET
    @staffPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('家政员列表', 'staff:list', '', '/staff/list', '', 1, @staffPermissionParentId),
       ('新增家政员', 'staff:create', '', '', '', 0, @staffPermissionParentId),
       ('更新家政员', 'staff:update', '', '', '', 0, @staffPermissionParentId),
       ('删除家政员', 'staff:delete', '', '', '', 0, @staffPermissionParentId);

-- 2023.2.8 基础表添加字段
ALTER TABLE `housekeeping_shop`
    ADD COLUMN `credit_score` decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分' AFTER `status`,
    ADD COLUMN `order_num`    bigint(20)    NULL     DEFAULT 0 COMMENT '订单数' AFTER `credit_score`;
ALTER TABLE `housekeeping_staff`
    ADD COLUMN `credit_score`    decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分' AFTER `status`,
    ADD COLUMN `order_num`       bigint(20)    NULL     DEFAULT 0 COMMENT '订单数' AFTER `credit_score`,
    ADD COLUMN `employment_date` TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '参加工作时间' AFTER `order_num`,
    ADD COLUMN `seniority`       int(11)       NULL     DEFAULT 0 COMMENT '工作经验年数' AFTER `employment_date`;
ALTER TABLE `product`
    ADD COLUMN `mobile_content` text          NULL COMMENT '移动端详情' AFTER `description`,
    ADD COLUMN `credit_score`   decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分' AFTER `mobile_content`,
    ADD COLUMN `order_num`      bigint(20)    NULL     DEFAULT 0 COMMENT '订单数' AFTER `credit_score`;
ALTER TABLE `housekeeping_staff`
    ADD COLUMN `province_code` BIGINT NOT NULL DEFAULT 0 COMMENT '省id' AFTER `seniority`;
ALTER TABLE `housekeeping_company`
    ADD COLUMN `credit_score` decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分' AFTER `status`,
    ADD COLUMN `order_num`    bigint(20)    NULL     DEFAULT 0 COMMENT '订单数' AFTER `credit_score`;

-- 2023.2.9 产品添加价格字段
ALTER TABLE `product`
    ADD COLUMN `min_price` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '最小价格' AFTER `order_num`,
    ADD COLUMN `max_price` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '最大价格' AFTER `min_price`;

-- 2023.2.10 分类添加测试数据
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('家政服务', '家政服务', 'c1', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('日常保洁', '日常保洁', 'c1_1', '', @categoryParentId, 0),
       ('深度保洁', '深度保洁', 'c1_2', '', @categoryParentId, 0),
       ('开荒保洁', '开荒保洁', 'c1_3', '', @categoryParentId, 0),
       ('抽油烟机清洗', '抽油烟机清洗', 'c1_4', '', @categoryParentId, 0),
       ('冰箱清洗', '冰箱清洗', 'c1_5', '', @categoryParentId, 0),
       ('洗衣机清洗', '洗衣机清洗', 'c1_6', '', @categoryParentId, 0),
       ('空调清洗', '空调清洗', 'c1_7', '', @categoryParentId, 0),
       ('擦玻璃', '擦玻璃', 'c1_8', '', @categoryParentId, 0),
       ('除尘除螨', '除尘除螨', 'c1_9', '', @categoryParentId, 0),
       ('除甲醛', '除甲醛', 'c1_10', '', @categoryParentId, 0);

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('保姆月嫂', '保姆月嫂', 'c2', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('保姆', '保姆', 'c2_1', '', @categoryParentId, 0),
       ('月嫂', '月嫂', 'c2_2', '', @categoryParentId, 0),
       ('育儿嫂', '育儿嫂', 'c2_3', '', @categoryParentId, 0),
       ('老人陪护', '老人陪护', 'c2_4', '', @categoryParentId, 0),
       ('病人陪护', '病人陪护', 'c2_5', '', @categoryParentId, 0);

INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`,
                        `sort_value`) VALUE ('管道疏通', '管道疏通', 'c3', '', 0, 1);
SET @categoryParentId = LAST_INSERT_ID();
INSERT INTO `category` (`name`, display_name, `code`, `description`, `parent_id`, `sort_value`)
VALUES ('浴缸疏通', '浴缸疏通', 'c3_1', '', @categoryParentId, 0),
       ('马桶疏通', '马桶疏通', 'c3_2', '', @categoryParentId, 0),
       ('洗手盆疏通', '洗手盆疏通', 'c3_3', '', @categoryParentId, 0),
       ('洗菜盆疏通', '洗菜盆疏通', 'c3_4', '', @categoryParentId, 0),
       ('地漏疏通', '地漏疏通', 'c3_5', '', @categoryParentId, 0),
       ('下水道疏通', '下水道疏通', 'c3_6', '', @categoryParentId, 0),
       ('管道疏通', '管道疏通', 'c3_7', '', @categoryParentId, 0);


-- 2023-02-16 菜单添加icon
UPDATE `sys_permission`
SET `icon` = 'el-icon-tickets'
WHERE `resource` = 'dictConfig:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-files'
WHERE `resource` = 'fileAttachment:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-s-tools'
WHERE `resource` = 'kvConfig:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-notebook-1'
WHERE `resource` = 'log:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-notebook-2'
WHERE `resource` = 'loginLog:list';
UPDATE `sys_permission`
SET `icon` = 'el-icon-notebook-2'
WHERE `resource` = 'operationLog:list';
UPDATE `sys_permission`
SET `icon` = 'el-icon-menu'
WHERE `resource` = 'category:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-menu'
WHERE `resource` = 'company:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-menu'
WHERE `resource` = 'company:list';
UPDATE `sys_permission`
SET `icon` = 'el-icon-c-scale-to-original'
WHERE `resource` = 'company:review';
UPDATE `sys_permission`
SET `icon` = 'el-icon-edit-outline'
WHERE `resource` = 'company:update';
UPDATE `sys_permission`
SET `icon` = 'el-icon-s-grid'
WHERE `resource` = 'shop:null';
UPDATE `sys_permission`
SET `icon` = 'product'
WHERE `resource` = 'product:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-price-tag'
WHERE `resource` = 'spec:null';
UPDATE `sys_permission`
SET `icon` = 'peoples'
WHERE `resource` = 'staff:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-setting'
WHERE `resource` = 'adminSys:null';
UPDATE `sys_permission`
SET `icon` = 'el-icon-menu'
WHERE `resource` = 'adminPermission:list';
UPDATE `sys_permission`
SET `icon` = 'el-icon-menu'
WHERE `resource` = 'adminRole:list';
UPDATE `sys_permission`
SET `icon` = 'el-icon-user-solid'
WHERE `component` = '/user/list';


INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `icon`,
                              `parent_id`)
VALUES ('服务订单管理', 'order:null', '', '/order', '', 1, 'el-icon-s-order', 0);
SET
    @orderPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('订单管理', 'serviceOrder:list', '', '/order/list', '', 1, @orderPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('订单查看', 'serviceOrder:view', '', '/order/view', '', 0, @orderPermissionParentId);

-- 2023-02-17 添加消费者列表菜单
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`)
VALUES ('消费者列表', 'consumer:list', '', '/consumer/list', '', 1, 0, 'el-icon-s-custom.sql');

-- 2023-02-27 分离独立的订单评价状态
ALTER TABLE `service_order`
    ADD `credit_rating_status` INT(1) NOT NULL DEFAULT 0 AFTER `pay_status`;
