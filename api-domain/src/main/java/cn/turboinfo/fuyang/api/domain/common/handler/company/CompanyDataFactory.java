package cn.turboinfo.fuyang.api.domain.common.handler.company;


import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.domain.util.SecurityUtils;
import cn.turboinfo.fuyang.api.domain.util.UsernameUtils;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class CompanyDataFactory {

    @Value("${kit.security.rsa.public-key:}")
    private String base64RSAPublicKey;

    public void maskInfo(List<HousekeepingCompany> companyList) {
        companyList.stream()
                .peek(this::maskInfo)
                .toList();
    }

    public void maskInfo(HousekeepingCompany company) {
        company.setContactMobileEncrypt(SecurityUtils.encodeBase64(SecurityUtils.encryptRSA(company.getContactNumber().getBytes(), SecurityUtils.decodeBase64(base64RSAPublicKey))));
        company.setContactNumber(PhoneUtils.maskPhoneNum(company.getContactNumber()));
        company.setContactPerson(UsernameUtils.maskUsername(company.getContactPerson()));
        company.setLegalPersonIdCard(IdCardUtils.maskIdCareNum(company.getLegalPersonIdCard()));
        company.setLegalPerson(UsernameUtils.maskUsername(company.getLegalPerson()));
    }
}
