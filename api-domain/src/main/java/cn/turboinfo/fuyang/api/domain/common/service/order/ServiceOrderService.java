package cn.turboinfo.fuyang.api.domain.common.service.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrderCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * 服务订单
 */
public interface ServiceOrderService extends BaseQService<ServiceOrder, Long> {

    ServiceOrder save(ServiceOrderCreator creator);

    /**
     * 查找服务人员的历史订单
     */
    List<ServiceOrder> findByStaffId(Long staffId, ServiceOrderStatus status);

    /**
     * 查找家政公司的历史订单
     */
    List<ServiceOrder> findByCompanyId(Long companyId, ServiceOrderStatus status);

    /**
     * 查找家政公司的历史订单
     */
    List<ServiceOrder> findByCompanyId(Long companyId, Collection<ServiceOrderStatus> statusCollection);

    /**
     * 查找家政门店的历史订单
     */
    List<ServiceOrder> findByShopId(Long shopId, ServiceOrderStatus status);

    /**
     * 查找单个产品的历史订单
     */
    List<ServiceOrder> findByProductId(Long productId, ServiceOrderStatus status);

    /**
     * 标记下单时的服务订单为后付费, 并更新状态
     */
    void submitOrderPostPaid(Long serviceOrderId, Long payOrderId);

    /**
     * 标记下单时的服务订单为已支付, 并更新状态
     */
    void submitOrderPaid(Long serviceOrderId, Long payOrderId, BigDecimal prepaid, LocalDateTime successTime);

    /**
     * 完成订单剩余未支付金额, 并更新状态
     */
    void payRemainingUnpaid(Long serviceOrderId, Long payOrderId);

    /**
     * 标记服务订单开始服务
     */
    void startService(Long serviceOrderId, Long staffId);

    /**
     * 分配服务人员
     */
    void dispatchToStaff(Long serviceOrderId, Long staffId);

    /**
     * 服务人员确认
     */
    void staffConfirm(Long serviceOrderId);

    /**
     * 服务人员拒绝
     */
    void staffReject(Long serviceOrderId);

    /**
     * 消费者取消订单
     */
    void consumerCancel(Long serviceOrderId);

    /**
     * 家政公司取消订单
     */
    void companyCancel(Long serviceOrderId);

    /**
     * 服务人员取消订单
     */
    void staffCancel(Long serviceOrderId);

    /**
     * 服务人员确认服务完成
     */
    ServiceOrder staffConfirmServiceCompleted(Long serviceOrderId, BigDecimal additionalFee, BigDecimal discountFee);

    /**
     * 提交评价
     */
    void submitCreditRating(Long serviceOrderId, Long userId, String userName, Long score, String comment, List<Long> imageIdList);

    /**
     * 服务订单未支付超时回收
     */
    void recycleUnpaidTimeout(Long serviceOrderId);

    BigDecimal sumAmountByStatus(ServiceOrderStatus status);

    Long countOrder(ServiceOrderStatus status, LocalDateTime start, LocalDateTime end);

    /**
     * 查找用户订单
     */
    List<ServiceOrder> findByUserIdAndStaffId(Long userId, Long staffId, ServiceOrderStatus status);

    /**
     * 提交退款状态
     *
     * @param serviceOrderId 服务订单ID
     * @param status         退款状态
     */
    void refreshRefundStatus(Long serviceOrderId, ServiceOrderRefundStatus status);
}
