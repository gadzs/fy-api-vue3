package cn.turboinfo.fuyang.api.provider.common.repository.database.shop.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopSortType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: sunshow.
 */
@Converter(autoApply = true)
public class ShopSortTypeConverter extends BaseEnumConverter<ShopSortType> {
}
