package cn.turboinfo.fuyang.api.gateway.share.framework.http.fo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * author: sunshow.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShareLimitDataFO<T> {

    private Collection<T> items;

    private int pageIndex;

    private int pageSize;

    private long total;

    private int totalPage;

    public static <E> ShareLimitDataFO<E> fromQResponse(QResponse<E> qresponse) {

        return new ShareLimitDataFO<>(qresponse.getPagedData(), qresponse.getPage(), qresponse.getPageSize(), qresponse.getTotal(), qresponse.getPageTotal());
    }

    public static <E, D> ShareLimitDataFO<D> fromQResponse(QResponse<E> qresponse, Function<E, D> mapper) {

        return new ShareLimitDataFO<>(qresponse.getPagedData().stream().map(mapper).collect(Collectors.toList()), qresponse.getPage(), qresponse.getPageSize(), qresponse.getTotal(), qresponse.getPageTotal());
    }
}
