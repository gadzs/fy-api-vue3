package cn.turboinfo.fuyang.api.provider.common.repository.database.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingFailReason;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingResult;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 分账记录
 * author: hai
 */
@Table(
        name = "profit_sharing_record"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
public class ProfitSharingRecordPO extends FYEntity {
    /**
     * 分账ID
     */
    @Column(
            name = "profit_sharing_id"
    )
    private Long profitSharingId;

    /**
     * 微信分账明细单号
     */
    @Column(
            name = "wx_detail_id"
    )
    private String wxDetailId;

    /**
     * 接收方编码
     */
    @Column(
            name = "receiver_id"
    )
    private Long receiverId;

    /**
     * 接收方类型
     */
    @Column(
            name = "receiver_type"
    )
    private AccountType receiverType;

    /**
     * 分账金额
     */
    private BigDecimal amount;

    /**
     * 分账账户
     */
    private String account;

    /**
     * 分账结果
     */
    private ProfitSharingResult result;

    /**
     * 分账失败原因
     */
    @Column(
            name = "fail_reason"
    )
    private ProfitSharingFailReason failReason;

    /**
     * 分账描述
     */
    private String description;

    /**
     * 完成时间
     */
    @Column(
            name = "finish_time"
    )
    private LocalDateTime finishTime;
}
