package cn.turboinfo.fuyang.api.scheduler.scheduler.order

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.order.SchedulerServiceOrderRefundStatusUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 服务订单退款状态定时任务
 */
@Component
class ServiceOrderRefundStatusScheduler(
    private val schedulerServiceOrderRefundStatusUseCase: SchedulerServiceOrderRefundStatusUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 60000)
    fun run() {
        runCatching {
            schedulerServiceOrderRefundStatusUseCase.execute(
                SchedulerServiceOrderRefundStatusUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "服务订单退款状态定时任务执行失败"
            }
        }
    }

}
