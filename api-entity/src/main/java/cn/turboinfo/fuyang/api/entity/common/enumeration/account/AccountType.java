package cn.turboinfo.fuyang.api.entity.common.enumeration.account;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public enum AccountType implements BaseEnum {

    DEFAULT(0, "默认", "DEFAULT"),

    MERCHANT_ID(10, "商户ID", "MERCHANT_ID"),

    PERSONAL_OPENID(20, "个人openid", "PERSONAL_OPENID"),

    ;

    private int value;
    private String name;

    private String code;

    AccountType(int value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }

    public static AccountType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static AccountType getByName(String name) {
        return BaseEnumHelper.getByName(name, values());
    }

    public static List<AccountType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<AccountType> listWithoutDefault() {
        return BaseEnumHelper.getList(values()).stream().filter(v -> v != DEFAULT).collect(Collectors.toList());
    }

    public static List<AccountType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
