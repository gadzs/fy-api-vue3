package cn.turboinfo.fuyang.api.gateway.admin.fo.shop;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@QField
public class HousekeepingShopSearchFO {
    @QField(
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "公司编码"
    )
    private Long companyId;

    @QField(
            placeholder = "门店名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "成立日期"
    )
    private LocalDate foundDate;

    @QField(
            placeholder = "省编码"
    )
    private Long provinceCode;

    @QField(
            placeholder = "市编码"
    )
    private Long cityCode;

    @QField(
            placeholder = "区编码"
    )
    private Long areaCode;

    @QField(
            placeholder = "地址",
            operator = Operator.LIKE
    )
    private String address;

    @QField(
            placeholder = "经度"
    )
    private BigDecimal longitude;

    @QField(
            placeholder = "纬度"
    )
    private BigDecimal latitude;

    @QField(
            placeholder = "状态",
            control = Control.SELECT,
            ref = "enableStatusList",
            template = "enum"
    )
    private ShopStatus status;

    @QField(
            placeholder = "营业时间",
            operator = Operator.LIKE
    )
    private String businessTime;

}
