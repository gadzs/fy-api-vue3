package cn.turboinfo.fuyang.api.gateway.admin.config;

import cn.turboinfo.fuyang.api.gateway.admin.framework.http.interceptor.*;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.resolver.AdminRequestContextHandlerMethodArgumentResolver;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.resolver.AdminSessionHandlerMethodArgumentResolver;
import cn.turboinfo.fuyang.api.provider.admin.component.config.AdminKitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * author: sunshow.
 */
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

    @Autowired
    private AdminRequestContextInterceptor requestContextInterceptor;

    @Autowired(required = false)
    private AdminLogInterceptor logInterceptor;

    @Autowired
    private AdminSessionScopeInterceptor sessionScopeInterceptor;

    @Autowired
    private AdminPermissionScopeInterceptor permissionScopeInterceptor;

    @Autowired
    private AdminKitConfig adminKitConfig;

    @Autowired
    private AdminPlatformScopeInterceptor platformScopeInterceptor;

    @Autowired
    private AdminCompanyScopeInterceptor companyScopeInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestContextInterceptor)
                .addPathPatterns("/admin/**");

        if (adminKitConfig.isOperationLogEnable()) {
            if (logInterceptor != null) {
                registry.addInterceptor(logInterceptor)
                        .addPathPatterns("/admin/**");
            }
        }

        registry.addInterceptor(sessionScopeInterceptor)
                .addPathPatterns("/admin/**");

        registry.addInterceptor(permissionScopeInterceptor)
                .addPathPatterns("/admin/**");

        registry.addInterceptor(platformScopeInterceptor)
                .addPathPatterns("/admin/**");

        registry.addInterceptor(companyScopeInterceptor)
                .addPathPatterns("/admin/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new AdminRequestContextHandlerMethodArgumentResolver());
        resolvers.add(new AdminSessionHandlerMethodArgumentResolver());
    }

}
