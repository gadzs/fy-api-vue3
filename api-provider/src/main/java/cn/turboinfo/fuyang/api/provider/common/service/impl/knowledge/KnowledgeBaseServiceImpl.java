package cn.turboinfo.fuyang.api.provider.common.service.impl.knowledge;

import cn.turboinfo.fuyang.api.domain.common.service.knowledge.KnowledgeBaseService;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import cn.turboinfo.fuyang.api.provider.common.repository.database.knowledge.KnowledgeBaseDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.knowledge.KnowledgeBasePO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class KnowledgeBaseServiceImpl extends DefaultQServiceImpl<KnowledgeBase, Long, KnowledgeBasePO, KnowledgeBaseDAO> implements KnowledgeBaseService {
    @Override
    public List<KnowledgeBase> findAllByType(String type) {
        return convertQBeanToList(dao.findByTypeOrderByIdDesc(type));
    }
}
