package cn.turboinfo.fuyang.api.entity.mini.fo.product;

import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author gadzs
 * @description 产品详情
 * @date 2023/2/15 15:50
 */
@Data
public class MiniProduct {

    private Product product;

    /**
     * 距离
     */
    private BigDecimal distance;
}
