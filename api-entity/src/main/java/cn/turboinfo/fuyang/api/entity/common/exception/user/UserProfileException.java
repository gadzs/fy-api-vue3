package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class UserProfileException extends RuntimeException {
    public UserProfileException() {
        this("操作 UserProfile 出错");
    }

    public UserProfileException(String message) {
        super(message);
    }

    public UserProfileException(Throwable cause) {
        super(cause);
    }

    public UserProfileException(String message, Throwable cause) {
        super(message, cause);
    }
}
