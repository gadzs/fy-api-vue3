package cn.turboinfo.fuyang.api.domain.admin.usecase.policy;

import cn.turboinfo.fuyang.api.domain.common.service.policy.PolicyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.Policy;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

/**
 * 后台策略列表
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ListPolicyUseCase extends AbstractUseCase<ListPolicyUseCase.InputData, ListPolicyUseCase.OutputData> {

    private final PolicyService policyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<Policy> pageResponse = policyService.findAll(inputData.request, inputData.getRequestPage());

        return OutputData.builder()
                .pageResponse(pageResponse)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private QRequest request;

        private QPage requestPage;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private QResponse<Policy> pageResponse;

    }
}
