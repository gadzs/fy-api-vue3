package cn.turboinfo.fuyang.api.domain.share.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.BeanHelper;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.share.fo.company.ShareCompanyFO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cn.turboinfo.fuyang.api.entity.common.constant.DictConstants.*;

/**
 * 查看公司详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShareViewCompanyUseCase extends AbstractUseCase<ShareViewCompanyUseCase.InputData, ShareViewCompanyUseCase.OutputData> {

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final DivisionService divisionService;

    private final DictConfigItemService dictConfigItemService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();

        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(companyId);

        // 地区
        List<Long> divisionIds = new ArrayList<>();
        divisionIds.add(company.getProvinceCode());
        divisionIds.add(company.getCityCode());
        divisionIds.add(company.getAreaCode());

        Map<Long, Division> divisionMap = divisionService.findByIdCollection(divisionIds).stream()
                .collect(Collectors.toMap(Division::getId, o -> o));

        if (divisionMap.containsKey(company.getProvinceCode())) {
            company.setProvinceName(divisionMap.get(company.getProvinceCode()).getAreaName());
        }
        if (divisionMap.containsKey(company.getCityCode())) {
            company.setCityName(divisionMap.get(company.getCityCode()).getAreaName());
        }
        if (divisionMap.containsKey(company.getAreaCode())) {
            company.setAreaName(divisionMap.get(company.getAreaCode()).getAreaName());
        }

        ShareCompanyFO.ShareCompanyFOBuilder builder = ShareCompanyFO.builder();

        BeanHelper.copyPropertiesToBuilder(builder, ShareCompanyFO.class, company);

        // 公司规模
        DictConfigItem employeeSize = dictConfigItemService.getDictConfigItem(DICT_KEY_EMPLOYEE_SIZE, company.getEmployeesNumber()).orElse(null);

        if (employeeSize != null) {
            builder.employeesNumber(employeeSize.getItemName());
        }

        // 公司类型
        DictConfigItem companyType = dictConfigItemService.getDictConfigItem(DICT_KEY_COMPANY_TYPE, company.getEmployeesNumber()).orElse(null);

        if (companyType != null) {
            builder.companyType(companyType.getItemName());
        }
        // 家政企业类型
        DictConfigItem housekeepingCompanyType = dictConfigItemService.getDictConfigItem(DICT_KEY_HOUSEKEEPING_COMPANY_TYPE, company.getEmployeesNumber()).orElse(null);

        if (housekeepingCompanyType != null) {
            builder.houseKeepingType(housekeepingCompanyType.getItemName());
        }

        // 认证标签
        if (company.getAuthLabelList() != null) {
            builder.authLabelList(company.getAuthLabelList().stream().map(HousekeepingCompanyAuthLabel::getName).collect(Collectors.toList()));
        } else {
            builder.authLabelList(new ArrayList<>());
        }

        return OutputData.builder()
                .company(builder.build())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "企业编码不能为空"
        )
        @Positive
        private Long companyId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private ShareCompanyFO company;

    }
}
