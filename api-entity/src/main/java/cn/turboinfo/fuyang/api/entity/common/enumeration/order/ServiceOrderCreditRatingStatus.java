package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 服务订单评价状态
 */
@Slf4j
public enum ServiceOrderCreditRatingStatus implements BaseEnum {

    INIT(0, "无需评价"),

    PENDING(10, "待评价"),

    SUBMITTED(20, "已提交"),

    ;

    private int value;
    private String name;

    ServiceOrderCreditRatingStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ServiceOrderCreditRatingStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ServiceOrderCreditRatingStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ServiceOrderCreditRatingStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
