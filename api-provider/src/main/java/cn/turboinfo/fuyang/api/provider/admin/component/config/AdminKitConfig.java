package cn.turboinfo.fuyang.api.provider.admin.component.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class AdminKitConfig {

    @Value("${kit.admin.operation.log.enable:false}")
    private boolean operationLogEnable;

    @Value("${kit.admin.login.log.enable:false}")
    private boolean loginLogEnable;

    /**
     * 是否启用验证码登录
     */
    @Value("${kit.admin.login.captcha.enable:false}")
    private boolean loginCaptchaEnable;

    /**
     * 是否允许相同用户同时登录
     */
    @Value("${kit.admin.login.concurrent.enable:true}")
    private boolean loginConcurrentEnable;

    /**
     * 相同用户登录超时时间 (即超过多少毫秒没活动就将之前的会话踢出)
     */
    @Value("${kit.admin.login.concurrent.timeout:300000}")
    private long loginConcurrentTimeout;

    /**
     * 附件上传目录
     */
    @Value("${kit.file.attachment.upload-path:}")
    private String uploadPath;

    /**
     * RSA私钥
     */
    @Value("${kit.security.rsa.private-key:}")
    private String base64RSAPrivateKey;

    /**
     * 文章允许上传的附件类型
     */
    @Value("${kit.attachment.allowExt:}")
    private String attachmentAllowExt;

    /**
     * 文章图片允许的类型
     */
    @Value("${kit.image.allowExt:}")
    private String imageAllowExt;
}
