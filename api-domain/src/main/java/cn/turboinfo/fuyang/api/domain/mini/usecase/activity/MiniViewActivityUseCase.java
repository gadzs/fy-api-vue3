package cn.turboinfo.fuyang.api.domain.mini.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * 活动详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewActivityUseCase extends AbstractUseCase<MiniViewActivityUseCase.InputData, MiniViewActivityUseCase.OutputData> {
    private final ActivityService activityService;

    private final FileAttachmentService fileAttachmentService;

    private final DictConfigItemService dictConfigItemService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long activityId = inputData.getActivityId();

        Activity activity = activityService.getByIdEnsure(activityId);

        List<FileAttachment> fileAttachmentList = fileAttachmentService.findByRefIdAndRefType(activityId, FileRefTypeConstant.ACTIVITY_IMG);

        Optional<DictConfigItem> activityType = dictConfigItemService.getDictConfigItem("activityType", activity.getActivityType());

        activityType.ifPresent(dictConfigItem -> activity.setActivityType(dictConfigItem.getItemName()));
        
        activity.setImageIdList(fileAttachmentList.stream().map(FileAttachment::getId).toList());

        return OutputData.builder()
                .activity(activity)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "活动ID不能为空")
        private Long activityId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private Activity activity;
    }
}
