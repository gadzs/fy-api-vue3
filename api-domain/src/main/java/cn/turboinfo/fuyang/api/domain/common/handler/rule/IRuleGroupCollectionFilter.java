package cn.turboinfo.fuyang.api.domain.common.handler.rule;

import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroup;

import java.util.List;

public interface IRuleGroupCollectionFilter extends IRuleGroupFilter {

    /**
     * 按照规则组对集合对象进行过滤
     *
     * @param elementList   要过滤的集合
     * @param ruleGroup     规则组定义
     * @param controlObject 规则组对应的控制对象
     * @param <T>
     * @return
     */
    <T> List<T> filterCollection(List<T> elementList, RuleGroup ruleGroup, Object controlObject);

}
