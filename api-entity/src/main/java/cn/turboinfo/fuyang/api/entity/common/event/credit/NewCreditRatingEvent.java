package cn.turboinfo.fuyang.api.entity.common.event.credit;

import cn.turboinfo.fuyang.api.entity.common.event.AbstractEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 有新的评价事件
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class NewCreditRatingEvent extends AbstractEvent {

    private Long creditRatingId;

}
