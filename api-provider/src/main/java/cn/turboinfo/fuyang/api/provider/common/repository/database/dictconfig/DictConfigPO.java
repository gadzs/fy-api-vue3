package cn.turboinfo.fuyang.api.provider.common.repository.database.dictconfig;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 数据字典
 */
@Table(
        name = "dict_config"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
public class DictConfigPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 字典名称
     */
    @Column(
            name = "dict_name"
    )
    private String dictName;

    /**
     * 字典 Key
     */
    @Column(
            name = "dict_key"
    )
    private String dictKey;

    /**
     * 字典描述
     */
    private String description;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
