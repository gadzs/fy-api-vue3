package cn.turboinfo.fuyang.api.gateway.mini.framework.http;


import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants;

public class MiniResponseBodyWrapperException extends RuntimeException {

    private final int code;

    private final Object data;

    public MiniResponseBodyWrapperException(int code, String message) {
        this(code, message, null, null);
    }

    public MiniResponseBodyWrapperException(String message) {
        this(message, null);
    }

    public MiniResponseBodyWrapperException(String message, Throwable cause) {
        this(MiniRestConstants.RC_ERROR_DEFAULT, message, null, cause);
    }

    public MiniResponseBodyWrapperException(int code, String message, Object data, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

}
