package cn.turboinfo.fuyang.api.gateway.mini.contract.activity

import cn.turboinfo.fuyang.api.domain.mini.usecase.activity.MiniCompanyActivityApplyUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@CompanyScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/company/activity"])
interface MiniCompanyActivityContract {

    /**
     * 活动详情
     */
    @AutoMappingContract(
        beanType = MiniCompanyActivityApplyUseCase::class,
        paths = ["/apply/{activityId}"],
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun apply()

}
