package cn.turboinfo.fuyang.api.gateway.admin.controller.user;

import cn.turboinfo.fuyang.api.domain.admin.service.menu.MenuService;
import cn.turboinfo.fuyang.api.domain.admin.service.permission.PermissionService;
import cn.turboinfo.fuyang.api.domain.common.service.role.RolePermissionService;
import cn.turboinfo.fuyang.api.domain.common.service.role.RoleService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserRoleService;
import cn.turboinfo.fuyang.api.entity.admin.pojo.menu.Menu;
import cn.turboinfo.fuyang.api.entity.admin.pojo.permission.Permission;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.RolePermission;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRole;
import cn.turboinfo.fuyang.api.gateway.admin.fo.user.AdminUserInfoFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.provider.admin.component.session.AdminSessionHelper;
import cn.turboinfo.fuyang.api.provider.admin.framework.shiro.session.AdminSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping(value = "/admin/user")
public class AdminUserInfoController {

    private final AdminSessionHelper sessionHelper;

    private final RoleService roleService;

    private final SysUserRoleService sysUserRoleService;

    private final RolePermissionService rolePermissionService;

    private final PermissionService permissionService;

    private final MenuService menuService;

    @GetMapping(value = "/info")
    public AdminUserInfoFO info() {
        AdminSession session = sessionHelper.getSession();

        List<SysUserRole> sysUserRoleList = sysUserRoleService.findBySysUserId(session.getSysUserId());

        Set<String> roleCodeSet = roleService.findByIdCollection(sysUserRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList()))
                .stream().map(Role::getCode).collect(Collectors.toSet());

        List<RolePermission> rolePermissionList = rolePermissionService.findByRoleIdCollection(sysUserRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toSet()));

        Set<String> permissionResourceSet = permissionService.findByIdCollection(rolePermissionList.stream().map(RolePermission::getPermissionId).collect(Collectors.toSet()))
                .stream().map(Permission::getResource).collect(Collectors.toSet());

        List<Menu> menuList = menuService.findBySysUserIdAll(session.getSysUserId());

        return new AdminUserInfoFO(roleCodeSet, permissionResourceSet, menuList, session.getUsername(), "", "", session.getLoginOperation());
    }

}
