CREATE TABLE `address`
(
    `id`           BIGINT(1)     NOT NULL AUTO_INCREMENT,
    `user_id`      BIGINT(1)     NOT NULL DEFAULT 0,
    `contact`      VARCHAR(64)   NOT NULL DEFAULT '',
    `gender_type`  TINYINT(1)    NOT NULL DEFAULT 0,
    `mobile`       VARCHAR(32)   NOT NULL DEFAULT '',
    `division_id`  BIGINT(1)     NOT NULL DEFAULT 0,
    `poi_name`     VARCHAR(64)   NOT NULL DEFAULT '',
    `detail`       VARCHAR(128)  NOT NULL DEFAULT '',
    `longitude`    decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '经度',
    `latitude`     decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '纬度',
    `deleted`      BIGINT(1)     NOT NULL DEFAULT 0,
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_userid` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
