package cn.turboinfo.fuyang.api.domain.admin.usecase.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.AgenciesCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class AgenciesCreateUseCase extends AbstractUseCase<AgenciesCreateUseCase.InputData, AgenciesCreateUseCase.OutputData> {
    private final AgenciesService agenciesService;

    @Override
    protected OutputData doAction(InputData inputData) {

        AgenciesCreator.Builder builder = AgenciesCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, AgenciesCreator.class, inputData);

        Agencies agencies = agenciesService.save(builder.build());

        return OutputData.builder()
                .agencies(agencies)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "name不能为空"
        )
        private String name;

        @NotNull(
                message = "地址编码不能为空"
        )
        private Long areaCode;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Agencies agencies;
    }
}
