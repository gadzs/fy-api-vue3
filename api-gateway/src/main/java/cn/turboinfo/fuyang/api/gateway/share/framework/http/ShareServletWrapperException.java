package cn.turboinfo.fuyang.api.gateway.share.framework.http;

public class ShareServletWrapperException extends RuntimeException {

    public ShareServletWrapperException(Throwable cause) {
        this("内部接口请求异常", cause);
    }

    public ShareServletWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

}
