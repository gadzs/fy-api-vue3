package cn.turboinfo.fuyang.api.gateway.admin.fo.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginTokenFO {

    private String token;

}
