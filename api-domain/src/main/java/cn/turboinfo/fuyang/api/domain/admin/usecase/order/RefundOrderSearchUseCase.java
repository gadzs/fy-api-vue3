package cn.turboinfo.fuyang.api.domain.admin.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.order.RefundOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QRefundOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.RefundOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class RefundOrderSearchUseCase extends AbstractUseCase<RefundOrderSearchUseCase.InputData, RefundOrderSearchUseCase.OutputData> {
    private final RefundOrderService refundOrderService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        String companyName = inputData.getCompanyName();
        QRequest request = inputData.getRequest();
        Map<Long, HousekeepingCompany> companyMap;
        Set<Long> companyIdSet = null;

        // 根据家政公司名称查询家政公司ID
        if (StringUtils.isNotBlank(companyName)) {
            companyIdSet = housekeepingCompanyService.findByName(companyName)
                    .stream()
                    .map(HousekeepingCompany::getId)
                    .collect(Collectors.toSet());

            // 如果家政公司名称查询不到家政公司，则直接返回空
            if (companyIdSet.isEmpty()) {
                return OutputData.builder()
                        .qResponse(new QResponse<RefundOrder>())
                        .build();
            }

            if (companyId != null) {
                // 如果家政公司ID不为空，且家政公司名称查询到了多个家政公司，则只查询家政公司ID对应的家政公司
                if (companyIdSet.contains(companyId)) {
                    companyIdSet = Set.of(companyId);
                } else {
                    // 如果家政公司ID不为空，且家政公司名称查询到了多个家政公司，但是家政公司ID不在查询结果中，则直接返回空
                    return OutputData.builder()
                            .qResponse(new QResponse<RefundOrder>())
                            .build();
                }
            }
        } else if (companyId != null) { // 如果家政公司ID不为空，且家政公司名称为空，则只查询家政公司ID对应的家政公司
            companyIdSet = Set.of(companyId);
        }

        if (companyIdSet != null) {
            request.filterIn(QRefundOrder.companyId, Arrays.asList(companyIdSet.toArray()));
        }

        QResponse<RefundOrder> response = refundOrderService.findAll(request, inputData.getRequestPage());

        if (companyIdSet == null) {
            companyIdSet = response.getPagedData()
                    .stream()
                    .map(RefundOrder::getCompanyId)
                    .collect(Collectors.toSet());
        }

        companyMap = housekeepingCompanyService.findByIdCollection(companyIdSet)
                .stream()
                .collect(Collectors.toMap(HousekeepingCompany::getId, it -> it));

        response.getPagedData().forEach(it -> {
            if (companyMap.containsKey(it.getCompanyId())) {
                it.setCompanyName(companyMap.get(it.getCompanyId()).getName());
            }
        });

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private Long companyId;

        private String companyName;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<RefundOrder> qResponse;
    }
}
