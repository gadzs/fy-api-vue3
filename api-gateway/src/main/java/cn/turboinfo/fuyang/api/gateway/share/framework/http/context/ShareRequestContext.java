package cn.turboinfo.fuyang.api.gateway.share.framework.http.context;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShareRequestContext implements Serializable {

    private String uscc;

    private Long companyId;

}
