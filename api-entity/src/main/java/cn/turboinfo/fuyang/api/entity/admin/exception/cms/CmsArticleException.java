package cn.turboinfo.fuyang.api.entity.admin.exception.cms;

public class CmsArticleException extends RuntimeException {
    public CmsArticleException() {
        this("操作 Article 出错");
    }

    public CmsArticleException(String message) {
        super(message);
    }

    public CmsArticleException(Throwable cause) {
        super(cause);
    }

    public CmsArticleException(String message, Throwable cause) {
        super(message, cause);
    }
}
