package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * @author gadzs
 * @description
 * @date 2023/3/8 15:59
 */
@Data
@QField
public class PortalStaffSearchFO {

    @QField(
            placeholder = "放心码",
            operator = Operator.EQUAL
    )
    private String code;

}
