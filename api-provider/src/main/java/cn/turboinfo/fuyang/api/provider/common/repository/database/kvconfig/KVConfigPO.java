package cn.turboinfo.fuyang.api.provider.common.repository.database.kvconfig;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * KV 配置
 */
@Table(
        name = "kv_config"
)
@Data
@Entity
@Where(clause = "deleted_time = 0")
@DynamicInsert
@DynamicUpdate
public class KVConfigPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 配置组名称
     */
    @Column(
            name = "config_group"
    )
    private String configGroup;

    /**
     * 配置 Key
     */
    @Column(
            name = "config_key"
    )
    private String configKey;

    /**
     * 配置 Value
     */
    @Column(
            name = "config_value"
    )
    private String configValue;

    /**
     * 描述
     */
    private String description;

    /**
     * 组内顺序
     */
    @Column(
            name = "group_order"
    )
    private Integer groupOrder;

    @Column(name = "deleted_time")
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
