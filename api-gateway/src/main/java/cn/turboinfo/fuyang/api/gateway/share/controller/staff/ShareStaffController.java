package cn.turboinfo.fuyang.api.gateway.share.controller.staff;

import cn.turboinfo.fuyang.api.domain.share.usecase.staff.ShareListStaffUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.QHousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.share.fo.staff.ShareStaffFO;
import cn.turboinfo.fuyang.api.gateway.share.fo.staff.ShareStaffSearchFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContext;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitDataFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareSortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * 家政员列表
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ShareResponseBodyWrapper
@RequestMapping("/share/staff")
public class ShareStaffController {

    private final ShareListStaffUseCase shareListStaffUseCase;

    @RequestMapping("/list")
    public ShareLimitDataFO<ShareStaffFO> list(@Valid ShareStaffSearchFO search, ShareLimitFO page, ShareSortFO sort, ShareRequestContext context) {

        if (page.getPageSize() == 0) {
            page.setPageSize(20);
        }

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        request.filterEqual(QHousekeepingStaff.companyId, context.getCompanyId());

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingStaff.id + "|" + QSort.Order.DESC.name()});
        }

        ShareListStaffUseCase.OutputData outputData = shareListStaffUseCase.execute(ShareListStaffUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return ShareLimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
