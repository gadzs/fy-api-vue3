package cn.turboinfo.fuyang.api.gateway.mini.contract.index

import cn.turboinfo.fuyang.api.domain.mini.usecase.activity.MiniListActivityUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.index.MiniIndexListBannerUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.index.MiniIndexListNearbyProductUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.index.MiniIndexListNearbyStaffUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.index.MiniIndexListRecommendCategoryUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.staff.MiniViewStaffByIdUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/index"])
interface MiniIndexContract {

    // 首页推荐服务分类列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniIndexListRecommendCategoryUseCase::class,
        consumes = [],
    )
    fun recommendedCategories()

    // 首页附近服务列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniIndexListNearbyProductUseCase::class,
        consumes = [],
    )
    fun nearByProducts()

    // 首页附近服务人员列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniIndexListNearbyStaffUseCase::class,
        consumes = [],
    )
    fun nearByStaffs()

    // 首页banner
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniIndexListBannerUseCase::class,
        consumes = [],
    )
    fun banner()

    // 活动列表
    @AutoMappingContract(
        beanType = MiniListActivityUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun activity()

    // 家政服务人员详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewStaffByIdUseCase::class,
        consumes = [],
        paths = ["/viewById/{staffId}"]
    )
    fun viewById()

}
