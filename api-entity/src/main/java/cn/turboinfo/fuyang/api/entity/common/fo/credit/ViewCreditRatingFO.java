package cn.turboinfo.fuyang.api.entity.common.fo.credit;

import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.fo.shop.ViewShopFO;
import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.Data;

import java.util.List;

/**
 * 带用户信息显示的评价详情
 */
@Data
public class ViewCreditRatingFO {

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 头像
     */
    private String avatarUrl;

    /**
     * 订单ID
     */
    private Long serviceOrderId;

    /**
     * 订单信息
     */
    private ViewServiceOrderFO serviceOrder;

    /**
     * 门店信息
     */
    private ViewShopFO shop;

    /**
     * 服务人员信息
     */
    private ViewStaffFO staff;

    /**
     * 产品信息
     */
    private Product product;

    /**
     * 评价信息
     */
    private List<CreditRating> ratingList;

}
