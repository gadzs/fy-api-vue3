package cn.turboinfo.fuyang.api.gateway.admin.contract.profit

import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.*
import cn.turboinfo.fuyang.api.domain.admin.usecase.profit.ProfitSharingViewUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/profit"])
interface ProfitSharingContract {

    // 查看分账记录
    @PermissionScope("adminProfitSharing:view")
    @CompanyScope
    @AutoMappingContract(
        beanType = ProfitSharingViewUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/view/{id}"]
    )
    fun view()


}
