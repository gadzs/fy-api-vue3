package cn.turboinfo.fuyang.api.gateway.admin.fo.contract;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class ContractTmplSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "合同名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "合同类别"
    )
    private Long categoryId;

    @QField(
            placeholder = "企业编码"
    )
    private Long companyId;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
