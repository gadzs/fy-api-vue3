package cn.turboinfo.fuyang.api.gateway.admin.controller.log;

import cn.turboinfo.fuyang.api.domain.admin.service.log.OperationLogService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.OperationLog;
import cn.turboinfo.fuyang.api.entity.admin.pojo.log.QOperationLog;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import cn.turboinfo.fuyang.api.gateway.admin.fo.log.OperationLogSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/log/operation")
public class OeprationLogController {

    private final OperationLogService operationLogService;
    private final SysUserService sysUserService;

    @RequiresPermissions("operationLog:list")
    @RequestMapping("/list")
    public RestResponseFO list(@Valid OperationLogSearchFO search, LimitFO page, SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QOperationLog.id + "|" + QSort.Order.DESC.name()});
        }

        QResponse<OperationLog> operationLogQResponse = operationLogService.findAll(request, page.toQPage(sort.toQSortList()));
        Collection<OperationLog> operationLogList = operationLogQResponse.getPagedData();
        for (OperationLog oLog : operationLogList) {
            Optional<SysUser> sysUser = sysUserService.getById(oLog.getSysUserId());
            if (sysUser.isPresent()) {
                oLog.setUsername(sysUser.get().getUsername());
            }
        }
        return RestResponseFO.ok(LimitDataFO.fromQResponse(operationLogQResponse));
    }
}
