package cn.turboinfo.fuyang.api.entity.common.enumeration.company;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * @author gadzs
 * @description 企业状态
 * @date 2023/1/29 15:46
 */
@Slf4j
public enum CompanyStatus implements BaseEnum {

    REGISTER(0, "注册待审核"),
    REVIEWED(1, "已审核"),

    NOTPASS(2, "审核未通过");

    private int value;
    private String name;

    CompanyStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CompanyStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<CompanyStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CompanyStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
