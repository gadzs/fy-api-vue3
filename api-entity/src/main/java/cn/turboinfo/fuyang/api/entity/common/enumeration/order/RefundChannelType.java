package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 退款渠道
 */
@Slf4j
public enum RefundChannelType implements BaseEnum {

    ORIGINAL(0, "原路退款"),

    BALANCE(10, "退回到余额"),

    OTHER_BALANCE(20, "原账户异常退到其他余额账户"),

    OTHER_BANKCARD(30, "原银行卡异常退到其他银行卡"),

    ;

    private int value;
    private String name;

    RefundChannelType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static RefundChannelType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<RefundChannelType> list() {
        return BaseEnumHelper.getList(values());
    }

}
