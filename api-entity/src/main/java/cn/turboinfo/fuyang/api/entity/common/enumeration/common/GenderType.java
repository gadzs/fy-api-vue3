package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

@Slf4j
public enum GenderType implements BaseEnum {
    ALL(-1, "全部"),
    DEFAULT(0, "默认"),
    MALE(1, "男"),
    FEMALE(2, "女"),
    ;

    private int value;
    private String name;

    GenderType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static GenderType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<GenderType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<GenderType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
