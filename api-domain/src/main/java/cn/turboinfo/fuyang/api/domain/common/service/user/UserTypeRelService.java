package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.exception.user.UserTypeRelException;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户类型服务
 */
public interface UserTypeRelService {

    List<UserTypeRel> findByUserId(Long userId);

    /**
     * 创建公司和用户关联
     *
     * @param userId
     * @param companyId
     * @return
     * @throws UserTypeRelException
     */
    UserTypeRel createCompanyRel(Long userId, Long companyId) throws UserTypeRelException;

    /**
     * 是否有公司关联
     */
    boolean hasCompanyRel(Long userId, Long companyId);

    /**
     * 创建家政员和用户关联
     *
     * @param userId
     * @param staffId
     * @return
     * @throws UserTypeRelException
     */
    UserTypeRel createStaffRel(Long userId, Long staffId) throws UserTypeRelException;


    /**
     * 删除创建家政员和用户关联
     *
     * @param userId
     * @param staffId
     * @return
     * @throws UserTypeRelException
     */
    void deleteStaffRel(Long userId, Long staffId) throws UserTypeRelException;

    /**
     * 是否有家政员关联
     */
    boolean hasStaffRel(Long userId, Long staffId);

    /**
     * 创建普通消费者关联
     */
    UserTypeRel createConsumerRel(Long userId) throws UserTypeRelException;

    /**
     * 是否有消费者关联
     */
    boolean hasConsumerRel(Long userId);

    QResponse<UserTypeRel> findAll(QRequest request, QPage requestPage);

    List<UserTypeRel> findByUserType(UserType userType);

    Long countByUserType(UserType userType);

    Long countUser(UserType userType, LocalDateTime start, LocalDateTime end);

}
