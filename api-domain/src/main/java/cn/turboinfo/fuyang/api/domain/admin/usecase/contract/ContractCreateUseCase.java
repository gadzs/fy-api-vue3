package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.Contract;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractCreator;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractCreateUseCase extends AbstractUseCase<ContractCreateUseCase.InputData, ContractCreateUseCase.OutputData> {
    private final ContractService contractService;

    @Override
    protected OutputData doAction(InputData inputData) {

        ContractCreator.Builder builder = ContractCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, ContractCreator.class, inputData);

        Contract contract = contractService.save(builder.build());

        return OutputData.builder()
                .contract(contract)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "订单编码不能为空"
        )
        private Long orderId;

        @NotNull(
                message = "企业编码不能为空"
        )
        private Long companyId;

        @NotNull(
                message = "合同模板编码不能为空"
        )
        private Long templateId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Contract contract;
    }
}
