package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecCreateUseCase extends AbstractUseCase<SpecCreateUseCase.InputData, SpecCreateUseCase.OutputData> {
    private final SpecService specService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long parentId = inputData.getParentId();
        String name = inputData.getName();
        String displayName = inputData.getDisplayName();
        String description = inputData.getDescription();
        if (parentId != null && parentId != 0) {
            // 判断父级是否存在
            specService.getByIdEnsure(parentId);
        }

        // 检查分类名称是否存在
        if (!specService.checkAvailable(parentId, name)) {
            throw new RuntimeException(String.format("%s 已存在", name));
        }

        SpecCreator.Builder builder = SpecCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, SpecCreator.class, inputData);

        if (displayName == null) {
            builder.withDisplayName(StringUtils.EMPTY);
        }
        if (description == null) {
            builder.withDescription(StringUtils.EMPTY);
        }

        Spec spec = specService.save(builder
                .withAllowDelete(YesNoStatus.YES)
                .build());

        return OutputData.builder()
                .spec(spec)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        private String displayName;

        private String description;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;

        /**
         * 规格组编码
         */
        private Long specSetId;

        @NotNull(
                message = "排序值不能为空"
        )
        private Integer sortValue;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Spec spec;
    }
}
