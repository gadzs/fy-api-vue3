-- 门店审核记录
DROP TABLE IF EXISTS `housekeeping_shop_audit_record`;
CREATE TABLE `housekeeping_shop_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `shop_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '门店编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='企业门店审核记录';

-- 门店审核记录
DROP TABLE IF EXISTS `housekeeping_staff_audit_record`;
CREATE TABLE `housekeeping_staff_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `staff_id`     BIGINT       NOT NULL DEFAULT 0 COMMENT '家政员编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`),
    INDEX `idx_staffid` (`staff_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='家政员审核记录';

-- 产品服务审核记录
DROP TABLE IF EXISTS `product_audit_record`;
CREATE TABLE `product_audit_record`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `shop_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '门店编码',
    `product_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '产品编码',
    `audit_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`      BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`    VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`       VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_company` (`company_id`, `deleted`),
    INDEX `idx_shopid` (`shop_id`, `deleted`),
    INDEX `idx_productid` (`product_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='产品服务审核记录';

-- 评价审核
DROP TABLE IF EXISTS `credit_rating_audit_record`;
CREATE TABLE `credit_rating_audit_record`
(
    `id`               BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `credit_rating_id` BIGINT       NOT NULL DEFAULT 0 COMMENT '评价ID',
    `object_type`      TINYINT      NOT NULL DEFAULT 0 COMMENT '评价对象类型',
    `object_id`        BIGINT       NOT NULL DEFAULT 0 COMMENT '评价对象ID',
    `audit_status`     TINYINT      NOT NULL DEFAULT 0 COMMENT '审核状态',
    `user_id`          BIGINT       NOT NULL DEFAULT 0 COMMENT '审核人',
    `user_name`        VARCHAR(128) NOT NULL DEFAULT '' COMMENT '审核人名称',
    `remark`           VARCHAR(128) NOT NULL DEFAULT '' COMMENT '备注',
    `deleted`          BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `idx_objecttype_objectid` (`object_type`, `object_id`, `deleted`),
    INDEX `idx_creditratingid` (`credit_rating_id`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='评价审核记录';
