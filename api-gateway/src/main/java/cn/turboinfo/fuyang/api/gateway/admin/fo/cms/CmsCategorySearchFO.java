package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@QField
@Data
public class CmsCategorySearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "编码",
            operator = Operator.LIKE
    )
    private String code;

    @QField(
            placeholder = "描述",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            placeholder = "类型",
            control = Control.SELECT,
            ref = "categoryTypeList",
            template = "enum"
    )
    private CmsCategoryType type;

    @QField(
            placeholder = "英文简写"
    )
    private String slugName;

    @QField(placeholder = "父级分类ID", control = Control.SELECT, ref = "parentCategoryList", template = "vo")
    private Long parentId;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
