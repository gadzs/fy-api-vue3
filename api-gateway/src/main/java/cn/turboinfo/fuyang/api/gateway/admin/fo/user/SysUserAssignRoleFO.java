package cn.turboinfo.fuyang.api.gateway.admin.fo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SysUserAssignRoleFO {

    @NotNull(message = "角色不能为空")
    private Long sysUserId;

    private Long[] checkedRoleIds;

}
