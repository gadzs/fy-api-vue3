package cn.turboinfo.fuyang.api.entity.admin.exception.user;

public class SysUserNotLoginException extends RuntimeException {
    public SysUserNotLoginException() {
        this("系统用户未登录");
    }

    public SysUserNotLoginException(String message) {
        super(message);
    }

    public SysUserNotLoginException(Throwable cause) {
        super(cause);
    }

    public SysUserNotLoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
