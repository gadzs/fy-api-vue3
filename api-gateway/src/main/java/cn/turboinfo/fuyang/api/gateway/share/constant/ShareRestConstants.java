package cn.turboinfo.fuyang.api.gateway.share.constant;

public final class ShareRestConstants {

    public static final int RC_SUCCESS = 0;

    public static final int RC_ERROR_DEFAULT = -1;

    public static final int RC_ERROR_BAD_REQUEST = 40000;

    public static final int RC_ERROR_UNAUTHENTICATED = 40100;

    public static final int RC_ERROR_UNAUTHORIZED = 40300;
}
