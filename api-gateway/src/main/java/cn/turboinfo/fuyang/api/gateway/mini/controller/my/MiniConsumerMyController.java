package cn.turboinfo.fuyang.api.gateway.mini.controller.my;

import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniConsumerMyServiceOrderListUseCase;
import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniMyAddressListUseCase;
import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniMyPendingSubmitCreditRatingListUseCase;
import cn.turboinfo.fuyang.api.domain.mini.usecase.my.MiniMySubmittedCreditRatingListUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyAddressListItem;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyPendingSubmitCreditRatingListItem;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyServiceOrderListItem;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMySubmittedCreditRatingListItem;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContextHolder;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@RequiredArgsConstructor
@Controller
@SessionScope
@MiniResponseBodyWrapper
@RequestMapping("/mini/consumer/my")
public class MiniConsumerMyController {


    private final MiniConsumerMyServiceOrderListUseCase miniConsumerMyServiceOrderListUseCase;

    private final MiniMyAddressListUseCase miniMyAddressListUseCase;

    private final MiniMyPendingSubmitCreditRatingListUseCase miniMyPendingSubmitCreditRatingListUseCase;

    private final MiniMySubmittedCreditRatingListUseCase miniMySubmittedCreditRatingListUseCase;

    @ConsumerScope
    @RequestMapping("/serviceOrderList")
    public LimitDataFO<MiniMyServiceOrderListItem> serviceOrderList(@RequestParam(required = false) ServiceOrderStatus orderStatus, LimitFO page) {

        MiniConsumerMyServiceOrderListUseCase.OutputData outputData = miniConsumerMyServiceOrderListUseCase.execute(MiniConsumerMyServiceOrderListUseCase.InputData
                .builder()
                .orderStatus(orderStatus)
                .userId(MiniRequestContextHolder.current().getUserId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getOrderList());
    }

    @RequestMapping("/addressList")
    public LimitDataFO<MiniMyAddressListItem> addressList(LimitFO page) {

        MiniMyAddressListUseCase.OutputData outputData = miniMyAddressListUseCase.execute(MiniMyAddressListUseCase.InputData
                .builder()
                .userId(MiniRequestContextHolder.current().getUserId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getAddressList());
    }

    /**
     * 待评价列表
     */
    @ConsumerScope
    @RequestMapping("/pendingSubmitCreditRatingList")
    public LimitDataFO<MiniMyPendingSubmitCreditRatingListItem> pendingSubmitCreditRatingList(LimitFO page) {

        MiniMyPendingSubmitCreditRatingListUseCase.OutputData outputData = miniMyPendingSubmitCreditRatingListUseCase.execute(MiniMyPendingSubmitCreditRatingListUseCase.InputData
                .builder()
                .userId(MiniRequestContextHolder.current().getUserId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getItemList());
    }

    @ConsumerScope
    @RequestMapping("/submittedCreditRatingList")
    public LimitDataFO<MiniMySubmittedCreditRatingListItem> submittedCreditRatingList(LimitFO page) {

        MiniMySubmittedCreditRatingListUseCase.OutputData outputData = miniMySubmittedCreditRatingListUseCase.execute(MiniMySubmittedCreditRatingListUseCase.InputData
                .builder()
                .userId(MiniRequestContextHolder.current().getUserId())
                .requestPage(page.toQPage(null))
                .build());

        return LimitDataFO.fromQResponse(outputData.getItemList());
    }

}
