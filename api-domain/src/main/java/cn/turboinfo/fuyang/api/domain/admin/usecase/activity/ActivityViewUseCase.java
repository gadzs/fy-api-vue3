package cn.turboinfo.fuyang.api.domain.admin.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityApplyService;
import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityApply;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class ActivityViewUseCase extends AbstractUseCase<ActivityViewUseCase.InputData, ActivityViewUseCase.OutputData> {
    private final ActivityService activityService;

    private final FileAttachmentService fileAttachmentService;

    private final ActivityApplyService activityApplyService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Activity activity = activityService.getByIdEnsure(inputData.getId());

        if (activity.getCompanyId() != null && activity.getCompanyId() != 0L) {
            activity.setCompanyName(housekeepingCompanyService.getByIdEnsure(activity.getCompanyId()).getName());
        } else {
            activity.setCompanyName("阜阳市商务局");
        }

        List<FileAttachment> fileAttachmentList = fileAttachmentService.findByRefIdAndRefType(activity.getId(), FileRefTypeConstant.ACTIVITY_IMG);

        activity.setImageIdList(fileAttachmentList.stream().map(FileAttachment::getId).toList());

        // 活动报名人员
        List<ActivityApply> activityApplyList = activityApplyService.findByActivityId(activity.getId());

        return OutputData.builder()
                .activity(activity)
                .activityApplyList(activityApplyList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private Activity activity;

        private List<ActivityApply> activityApplyList;

    }
}
