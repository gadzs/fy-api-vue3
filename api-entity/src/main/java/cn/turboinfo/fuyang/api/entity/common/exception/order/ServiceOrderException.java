package cn.turboinfo.fuyang.api.entity.common.exception.order;

/**
 * @author gadzs
 * @description 服务订单异常
 * @date 2023/2/24 13:44
 */
public class ServiceOrderException extends RuntimeException {
    public ServiceOrderException() {
        this("操作 ServiceOrder 出错");
    }

    public ServiceOrderException(String message) {
        super(message);
    }

    public ServiceOrderException(Throwable cause) {
        super(cause);
    }

    public ServiceOrderException(String message, Throwable cause) {
        super(message, cause);
    }
}
