package cn.turboinfo.fuyang.api.entity.admin.pojo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 留言板
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class CmsMessageBoard extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 留言用户
     */
    private Long userId;
    private String username;

    /**
     * 标题
     */
    private String title;

    /**
     * 留言内容
     */
    private String content;

    private String mobile;

    private String email;

    /**
     * 类型
     */
    private CmsMessageBoardType type;

    /**
     * 业务子类型
     */
    private String busiType;

    /**
     * 是否展示
     */
    private YesNoStatus isShow;

    /**
     * 回复用户
     */
    private Long replyUserId;
    private String replyUsername;

    /**
     * 回复内容
     */
    private String replyContent;

    /**
     * 回复时间
     */
    private LocalDateTime replyTime;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 意见征集id
     */
    private Long adviceId;


    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
