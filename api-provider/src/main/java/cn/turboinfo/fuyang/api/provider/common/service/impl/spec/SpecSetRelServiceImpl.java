package cn.turboinfo.fuyang.api.provider.common.service.impl.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetRelService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSetRel;
import cn.turboinfo.fuyang.api.provider.common.repository.database.spec.SpecSetRelDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.spec.SpecSetRelPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author gadzs
 * @description 规格组关联
 * @date 2023/2/6 11:51
 */
@Slf4j
@RequiredArgsConstructor
@EnableSoftDelete
@Service
public class SpecSetRelServiceImpl extends DefaultQServiceImpl<SpecSetRel, Long, SpecSetRelPO, SpecSetRelDAO> implements SpecSetRelService {
    @Override
    public List<SpecSetRel> findBySpecSetId(Long specSetId) {
        return convertQBeanToList(dao.findBySpecSetId(specSetId));
    }

    @Override
    public Optional<SpecSetRel> getByRelObject(Long objectId, EntityObjectType objectType) {
        return dao.findByObjectIdAndObjectType(objectId, objectType).map(this::convertQBean);
    }
}
