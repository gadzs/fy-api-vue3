CREATE TABLE `kv_config`
(
    `id`           BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `config_group` VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '配置组名称',
    `config_key`   VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '配置 Key',
    `config_value` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '配置 Value',
    `description`  VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '配置描述',
    `group_order`  INT           NOT NULL DEFAULT 0 COMMENT '组内顺序',
    `deleted_time` BIGINT        NOT NULL DEFAULT 0,
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_configkey_deletedtime` (`config_key`, `deleted_time`),
    KEY `idx_configgroup` (`config_group`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='KV 配置';

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('配置管理', 'kvConfig:null', '', 1, 0);
SET @kvConfigPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('配置列表', 'kvConfig:list', '/admin/kvConfig/list', 1, @kvConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('新建配置', 'kvConfig:create', '/admin/kvConfig/create', 0, @kvConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('编辑配置', 'kvConfig:update', '/admin/kvConfig/update', 0, @kvConfigPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('删除配置', 'kvConfig:delete', '/admin/kvConfig/delete', 0, @kvConfigPermissionParentId);