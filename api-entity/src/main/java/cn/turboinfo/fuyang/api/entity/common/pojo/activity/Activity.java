package cn.turboinfo.fuyang.api.entity.common.pojo.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.activity.ActivityAuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 活动管理
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Activity extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动描述
     */
    private String description;

    /**
     * 活动类型
     */
    private String activityType;

    /**
     * 活动范围
     */
//    @QBeanCreatorIgnore
//    @QBeanUpdaterIgnore
    private List<UserType> activityScope;

    /**
     * 主办单位
     */
    private String organizer;

    /**
     * 活动地点
     */
    private String location;

    /**
     * 已报名
     */
    private Integer applied;

    /**
     * 限制数量
     */
    private Integer limitNum;

    /**
     * 是否开放报名
     */
    private EnableStatus openApply;

    /**
     * 活动开始时间
     */
    private LocalDateTime startTime;

    /**
     * 活动结束时间
     */
    private LocalDateTime endTime;

    /**
     * 状态
     */
    private EnableStatus status;

    /**
     * 审核状态
     */
    private ActivityAuditStatus auditStatus;

    /**
     * 原因
     */
    private String auditReason;

    /**
     * 发布人
     */
    private Long companyId;

    private String companyName;

    /**
     * 图片编码列表(特殊场景容器)
     */
    private List<Long> imageIdList;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
