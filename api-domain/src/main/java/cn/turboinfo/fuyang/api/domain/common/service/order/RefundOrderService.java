package cn.turboinfo.fuyang.api.domain.common.service.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.RefundOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.RefundOrderCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 退款管理服务
 * author: hai
 */
public interface RefundOrderService extends BaseQService<RefundOrder, Long> {

    RefundOrder save(RefundOrderCreator creator);

    /**
     * 检查并更新退款状态
     *
     * @param refundOrderId 退款订单ID
     * @param status        退款订单状态
     * @param checkStatus   检查状态
     */
    void checkAndUpdateStatus(Long refundOrderId, RefundStatus status, RefundStatus checkStatus);

    /**
     * 根据服务订单类型 查询退款订单
     *
     * @param objectType 服务订单类型
     * @param objectId   服务订单ID
     * @return 退款订单列表
     */
    List<RefundOrder> findByObjectId(EntityObjectType objectType, Long objectId);

}
