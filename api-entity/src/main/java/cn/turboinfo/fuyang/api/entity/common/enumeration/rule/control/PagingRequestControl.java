package cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 分页请求规则控制
 */
@Slf4j
public enum PagingRequestControl implements BaseEnum {

    // ALL(-1, "全部"),

    // DEFAULT(0, "默认"),

    // 页码从0开始
    PageIndex(5, "页码"),

    PageSize(10, "分页大小"),

    Sort(15, "排序策略"),

    ;

    private int value;
    private String property;
    private String name;

    PagingRequestControl(int value, String name) {
        this.value = value;
        this.name = name;
    }

    PagingRequestControl(int value, String property, String name) {
        this.value = value;
        this.property = property;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PagingRequestControl get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<PagingRequestControl> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<PagingRequestControl> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
