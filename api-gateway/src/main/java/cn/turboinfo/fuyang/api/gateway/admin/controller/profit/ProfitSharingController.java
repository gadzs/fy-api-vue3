package cn.turboinfo.fuyang.api.gateway.admin.controller.profit;

import cn.turboinfo.fuyang.api.domain.admin.usecase.profit.ProfitSharingSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharing;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.QProfitSharing;
import cn.turboinfo.fuyang.api.gateway.admin.fo.profit.ProfitSharingSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/profit")
public class ProfitSharingController {
    private final ProfitSharingSearchUseCase profitSharingSearchUseCase;

    @RequiresPermissions("adminProfitSharing:list")
    @RequestMapping("/search")
    @CompanyScope
    public LimitDataFO<ProfitSharing> list(@Valid ProfitSharingSearchFO search, LimitFO page,
                                           SortFO sort, AdminRequestContext context) {

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QProfitSharing.id + "|" + QSort.Order.DESC.name()});
        }

        ProfitSharingSearchUseCase.OutputData outputData = profitSharingSearchUseCase.execute(ProfitSharingSearchUseCase.InputData
                .builder()
                .companyId(context.getCompanyId())
                .companyName(search.getCompanyName())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
