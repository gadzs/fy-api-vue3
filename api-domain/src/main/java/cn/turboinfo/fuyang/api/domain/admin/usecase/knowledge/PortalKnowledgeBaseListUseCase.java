package cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge;

import cn.turboinfo.fuyang.api.domain.common.service.knowledge.KnowledgeBaseService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class PortalKnowledgeBaseListUseCase extends AbstractUseCase<PortalKnowledgeBaseListUseCase.InputData, PortalKnowledgeBaseListUseCase.OutputData> {
    private final KnowledgeBaseService knowledgeBaseService;

    @Override
    protected OutputData doAction(InputData inputData) {

        String type = inputData.getType();
        List<KnowledgeBase> knowledgeBaseList = knowledgeBaseService.findAllByType(type);

        return OutputData.builder()
                .knowledgeList(knowledgeBaseList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotBlank(
                message = "类型不能为空"
        )
        private String type;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<KnowledgeBase> knowledgeList;
    }
}
