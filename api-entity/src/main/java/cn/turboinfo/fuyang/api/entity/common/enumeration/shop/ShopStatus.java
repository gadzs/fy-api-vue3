package cn.turboinfo.fuyang.api.entity.common.enumeration.shop;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 门店筛选方式
 *
 * @author hai
 */
@Slf4j
public enum ShopStatus implements BaseEnum {

    PENDING(0, "待审核"),

    REVIEWED(1, "已审核"),

    NOT_PASS(2, "审核未通过"),
    COLSED(99, "闭店"),
    ;

    private int value;
    private String name;

    ShopStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ShopStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ShopStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ShopStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
