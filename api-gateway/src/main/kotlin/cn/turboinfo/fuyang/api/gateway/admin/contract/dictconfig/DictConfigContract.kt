package cn.turboinfo.fuyang.api.gateway.admin.contract.dictconfig

import cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig.DictConfigCreateUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/dictConfig"])
interface DictConfigContract {

    // 字典列表
    @PermissionScope(value = ["dictConfig:create"])
    @AutoMappingContract(
        beanType = DictConfigCreateUseCase::class,
    )
    fun create()
}