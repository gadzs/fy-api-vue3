package cn.turboinfo.fuyang.api.entity.common.enumeration.property;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 属性数据类型
 */
@Slf4j
public enum PropertyDataType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    STRING(10, "字符串"),

    NON_WHITESPACE_STRING(11, "无空格字符串"),

    NUMBER(20, "数字"),

    DATE(30, "日期"),

    DATETIME(40, "时间日期"),

    ;

    private int value;
    private String name;

    PropertyDataType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PropertyDataType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<PropertyDataType> list() {
        return BaseEnumHelper.getList(values());
    }

}
