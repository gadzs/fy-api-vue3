package cn.turboinfo.fuyang.api.domain.common.service.confidence;

import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCodeCreator;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

/**
 * 放心码服务
 * author: hai
 */
public interface ConfidenceCodeService extends BaseQService<ConfidenceCode, Long> {

    ConfidenceCode save(ConfidenceCodeCreator creator);

    ConfidenceCode tagging();

}
