package cn.turboinfo.fuyang.api.gateway.web.framework.http.context;

import lombok.Data;

import java.io.Serializable;

@Data
public class WechatPayHeaderContext implements Serializable {

    private String wechatSignature;

    private String wechatNonce;

    private String wechatTimestamp;

    private String wechatSerial;

    private String wechatSignatureType;

}
