package cn.turboinfo.fuyang.api.provider.common.repository.database.credit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.credit.CreditRatingStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import io.hypersistence.utils.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * 信用评价记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(
        name = "credit_rating"
)
@Entity
@DynamicUpdate
@Where(clause = "deleted = 0")
@DynamicInsert
public class CreditRatingPO extends SoftDeleteFYEntity {

    /**
     * 评价对象类型
     */
    private EntityObjectType objectType;

    /**
     * 评价对象ID
     */
    private Long objectId;

    /**
     * 评分（存整数）
     */
    private Long score;

    /**
     * 发起评价者ID
     */
    private Long appraiserId;

    /**
     * 评价内容
     */
    private String comment;

    /**
     * 附件ID列表
     */
    @Type(type = "json")
    @Column(name = "img_id_list", columnDefinition = "json")
    private List<Long> imgIdList;

    /**
     * 评价状态
     */
    private CreditRatingStatus status;
}
