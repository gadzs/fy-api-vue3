package cn.turboinfo.fuyang.api.entity.common.fo.company;

import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * 家政公司
 * author: sunshow.
 */
@Data
public class ViewCompanyFO {

    private Long id;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司短名
     */
    private String shortName;

    /**
     * 成立日期
     */
    private LocalDate establishmentDate;

    /**
     * 注册地址
     */
    private String registeredAddress;

    /**
     * 注册资本
     */
    private BigDecimal registeredCapital;

    /**
     * 公司类型
     */
    private String companyType;

    /**
     * 法人
     */
    private String legalPerson;

    /**
     * 法人身份证
     */
    private String legalPersonIdCard;

    /**
     * 工商注册号
     */
    private String businessRegistrationNumber;

    /**
     * 经营范围
     */
    private String businessScope;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 加密联系电话
     */
    private String contactMobileEncrypt;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 员工人数
     */
    private String employeesNumber;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 家政员数
     */
    private Long staffNum;

    /**
     * 产品服务数
     */
    private Long productNum;

    /**
     * 门店数
     */
    private Long shopNum;

    /**
     * 认证标签
     */
    private List<HousekeepingCompanyAuthLabel> authLabelList;

    /**
     * 家政公司类型
     */
    private String houseKeepingType;

}
