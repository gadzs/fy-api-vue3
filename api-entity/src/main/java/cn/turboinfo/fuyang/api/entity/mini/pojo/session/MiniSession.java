package cn.turboinfo.fuyang.api.entity.mini.pojo.session;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MiniSession implements Serializable {

    /**
     * 用户ID
     */
    private Long userId;

}
