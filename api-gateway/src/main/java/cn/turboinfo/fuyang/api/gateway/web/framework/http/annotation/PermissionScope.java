package cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation;

import java.lang.annotation.*;

/**
 * 需要验证用户权限
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface PermissionScope {

    String[] value();

}
