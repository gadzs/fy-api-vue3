SET NAMES `utf8mb4`;

CREATE TABLE `login_log`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `username`     VARCHAR(128) NOT NULL DEFAULT '' COMMENT '登录用户名',
    `sys_user_id`  BIGINT       NOT NULL DEFAULT 0 COMMENT '系统用户ID',
    `login_source` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '登录来源',
    `login_time`   DATETIME     NULL COMMENT '登录行为发生的时间',
    `login_status` TINYINT      NOT NULL DEFAULT 0 COMMENT '是否登录成功',
    `login_remark` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '登录备注信息',
    `deleted_time` BIGINT       NOT NULL DEFAULT 0 COMMENT '',
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    KEY `idx_loginsource_createdtime` (`login_source`, `created_time`),
    KEY `idx_username_logintime` (`username`, `login_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='登录日志';

CREATE TABLE `operation_log`
(
    `id`           BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `sys_user_id`  BIGINT        NOT NULL DEFAULT 0 COMMENT '系统用户ID',
    `username`     VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '系统用户名',
    `url`          VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '访问地址',
    `method`       VARCHAR(32)   NOT NULL DEFAULT '' COMMENT '请求方式',
    `params`       TEXT          NULL COMMENT '请求参数',
    `deleted_time` BIGINT        NOT NULL DEFAULT 0 COMMENT '',
    `created_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    KEY `idx_createdtime` (`created_time`),
    KEY `idx_sysuserid_createdtime` (`sys_user_id`, `created_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='登录用户操作日志';

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('日志管理', 'log:null', '', 1, 0);
SET @logPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('用户登录日志', 'loginLog:list', '/admin/log/login/list', 1, @logPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `visible_status`, `parent_id`)
VALUES ('用户操作日志', 'operationLog:list', '/admin/log/operation/list', 1, @logPermissionParentId);