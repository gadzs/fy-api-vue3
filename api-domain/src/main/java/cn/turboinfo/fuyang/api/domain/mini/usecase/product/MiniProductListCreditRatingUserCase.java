package cn.turboinfo.fuyang.api.domain.mini.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.ServiceOrderCreditRating;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 产品评价列表
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniProductListCreditRatingUserCase extends AbstractUseCase<MiniProductListCreditRatingUserCase.InputData, MiniProductListCreditRatingUserCase.OutputData> {

    private final CreditRatingAssembleHelper creditRatingAssembleHelper;

    @Override
    protected MiniProductListCreditRatingUserCase.OutputData doAction(MiniProductListCreditRatingUserCase.InputData inputData) {
        Long productId = inputData.getProductId();
        QPage requestPage = inputData.getRequestPage();

        // 查询更多评价
        QResponse<ServiceOrderCreditRating> ratingQResponse = creditRatingAssembleHelper.findLatestPageable(
                EntityObjectType.PRODUCT, productId, requestPage.getPageIndex(), requestPage.getPageSize(), CreditRatingAssembleHelper.ASSEMBLE_USER);

        return OutputData.builder()
                .qResponse(ratingQResponse)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "产品编码不能为空"
        )
        @Positive
        private Long productId;

        @NotNull(message = "分页参数不能为空")
        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private QResponse<ServiceOrderCreditRating> qResponse;

    }
}
