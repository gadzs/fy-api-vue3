package cn.turboinfo.fuyang.api.entity.mini.fo.my;

import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MiniMyPendingSubmitCreditRatingListItem {

    private ViewServiceOrderFO serviceOrder;

    private Product product;

}
