package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigCreateUseCase extends AbstractUseCase<DictConfigCreateUseCase.InputData, DictConfigCreateUseCase.OutputData> {
    private final DictConfigService dictConfigService;

    @Override
    protected OutputData doAction(InputData inputData) {

        DictConfigCreator.Builder builder = DictConfigCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, DictConfigCreator.class, inputData);

        DictConfig dictConfig = dictConfigService.save(builder.build());

        return OutputData.builder()
                .dictConfig(dictConfig)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "字典名称不能为空"
        )
        private String dictName;

        @NotBlank(
                message = "字典 Key不能为空"
        )
        private String dictKey;

        private String description;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private DictConfig dictConfig;
    }
}
