package cn.turboinfo.fuyang.api.entity.admin.exception.user;

public class SysUserRoleException extends RuntimeException {
    public SysUserRoleException() {
        this("操作 SysUserRole 出错");
    }

    public SysUserRoleException(String message) {
        super(message);
    }

    public SysUserRoleException(Throwable cause) {
        super(cause);
    }

    public SysUserRoleException(String message, Throwable cause) {
        super(message, cause);
    }
}
