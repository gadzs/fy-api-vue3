package cn.turboinfo.fuyang.api.provider.common.service.impl.site;

import cn.turboinfo.fuyang.api.domain.common.service.site.SiteUrlService;
import cn.turboinfo.fuyang.api.domain.util.UrlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class SiteUrlServiceImpl implements SiteUrlService {

    /**
     * 附件上传图片文件外部访问前缀
     */
    @Value("${deploy.site.upload-external-img-url:}")
    private String uploadExternalImgUrl;

    @Value("${deploy.site.main-url:}")
    private String mainUrl;

    @Override
    public String getUploadExternalImgUrl(String relativePath) {
        return UrlUtils.combineUrl(uploadExternalImgUrl, relativePath);
    }

    @Override
    public String getMainSiteUrl() {
        return mainUrl;
    }
}
