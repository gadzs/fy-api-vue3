package cn.turboinfo.fuyang.api.gateway.mini.contract.custom

import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniStaffCancelServiceCustomUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniStaffConfirmServiceCustomCompletedUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniStaffConfirmServiceCustomUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.custom.MiniStaffRejectServiceCustomUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.StaffScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@StaffScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/staff/serviceCustom"])
interface MiniStaffServiceCustomContract {

    // 取消订单
    @AutoMappingContract(
        beanType = MiniStaffCancelServiceCustomUseCase::class,
    )
    fun cancel()

    // 服务人员确认接单
    @SessionScope
    @AutoMappingContract(
        beanType = MiniStaffConfirmServiceCustomUseCase::class,
    )
    fun confirm()

    // 服务人员拒绝接单
    @SessionScope
    @AutoMappingContract(
        beanType = MiniStaffRejectServiceCustomUseCase::class,
    )
    fun reject()

    // 确认完成服务并追加可能需要的额外费用
    @AutoMappingContract(
        beanType = MiniStaffConfirmServiceCustomCompletedUseCase::class,
    )
    fun confirmServiceCompleted()
}
