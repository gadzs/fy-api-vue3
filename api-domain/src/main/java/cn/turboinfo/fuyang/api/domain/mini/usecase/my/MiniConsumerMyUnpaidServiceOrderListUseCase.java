package cn.turboinfo.fuyang.api.domain.mini.usecase.my;

import cn.turboinfo.fuyang.api.domain.common.component.product.ProductAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.mini.fo.my.MiniMyServiceOrderListItem;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.mapper.BeanMapper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QPageRequestHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 未支付订单列表
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerMyUnpaidServiceOrderListUseCase extends AbstractUseCase<MiniConsumerMyUnpaidServiceOrderListUseCase.InputData, MiniConsumerMyUnpaidServiceOrderListUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    private final ServiceOrderDataFactory serviceOrderDataFactory;

    private final ProductAssembleHelper productAssembleHelper;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();

        QPage requestPage = QPage.newInstance().paging(0, 100);

        QRequest request = QRequest.newInstance()
                .filterEqual(QServiceOrder.userId, userId)
                .filterIn(QServiceOrder.orderStatus, List.of(ServiceOrderStatus.INIT, ServiceOrderStatus.SERVICE_COMPLETED));
        request.filterIn(QServiceOrder.payStatus, List.of(ServiceOrderPayStatus.UNPAID, ServiceOrderPayStatus.PENDING_PAY));

        // 按创建时间降序排序
        requestPage.addOrder(QServiceOrder.createdTime, QSort.Order.DESC);

        List<ServiceOrder> serviceOrderList = QPageRequestHelper.request(request, requestPage, serviceOrderService::findAll);

        // 排序 服务完成待支付的优先显示, 其他按创建时间降序
        serviceOrderList.sort((o1, o2) -> {
            if (o1.getPayStatus() == ServiceOrderPayStatus.PENDING_PAY && o2.getPayStatus() != ServiceOrderPayStatus.PENDING_PAY) {
                return -1;
            } else if (o1.getPayStatus() != ServiceOrderPayStatus.PENDING_PAY && o2.getPayStatus() == ServiceOrderPayStatus.PENDING_PAY) {
                return 1;
            } else {
                return o2.getCreatedTime().compareTo(o1.getCreatedTime());
            }
        });

        // 拼装企业名称
        serviceOrderDataFactory.assembleCompany(serviceOrderList);

        List<MiniMyServiceOrderListItem> orderList = serviceOrderList.stream()
                .map(serviceOrder -> {
                            serviceOrderDataFactory.maskInfo(serviceOrder);
                            ViewServiceOrderFO fo = BeanMapper.map(serviceOrder, ViewServiceOrderFO.class);

                            return MiniMyServiceOrderListItem.builder()
                                    .serviceOrder(fo)
                                    .build();
                        }
                )
                .toList();

        productAssembleHelper.assembleProduct(
                orderList,
                item -> item.getServiceOrder().getProductId(),
                ProductAssembleHelper.ASSEMBLE_ATTACHMENT,
                MiniMyServiceOrderListItem::setProduct);

        return OutputData.builder()
                .orderList(orderList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        private Long userId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 我的订单列表
         */
        private List<MiniMyServiceOrderListItem> orderList;

    }
}
