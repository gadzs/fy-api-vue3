package cn.turboinfo.fuyang.api.gateway.mini.framework.http.context;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import cn.turboinfo.fuyang.api.entity.mini.pojo.session.MiniSession;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MiniRequestContext implements Serializable {

    private Long userId;

    private MiniSession session;

    /**
     * 用户类型关系列表
     */
    private List<UserTypeRel> userTypeRelList;

    /**
     * 用户类型
     */
    private UserType userType;

    private SysUser user;

    /**
     * 当前如果是平台身份请求时, 请求会话中的主管机构ID
     */
    private Long platformId;

    /**
     * 当前如果是家政企业用户身份请求时, 请求会话中的公司ID
     * 当前如果是家政员身份请求时，保存对应家政员所在公司的ID方便后续使用
     */
    private Long companyId;

    /**
     * 当前如果是家政员身份请求时, 请求会话中的家政员ID
     */
    private Long housekeeperId;

    /**
     * 当前如果是消费者身份请求时, 请求会话中的消费者ID
     */
    private Long consumerId;

    public boolean isLogin() {
        return session != null;
    }
}
