package cn.turboinfo.fuyang.api.domain.admin.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.CategoryCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class CategoryCreateUseCase extends AbstractUseCase<CategoryCreateUseCase.InputData, CategoryCreateUseCase.OutputData> {
    private final CategoryService categoryService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long parentId = inputData.getParentId();
        String name = inputData.getName();
        String displayName = inputData.getDisplayName();
        String description = inputData.getDescription();
        if (parentId != null && parentId != 0) {
            // 判断父级是否存在
            categoryService.getByIdEnsure(parentId);
        }

        // 检查分类名称是否存在
        if (!categoryService.checkAvailable(parentId, name)) {
            throw new RuntimeException(String.format("%s 已存在", name));
        }

        CategoryCreator.Builder builder = CategoryCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, CategoryCreator.class, inputData);

        if (displayName == null) {
            builder.withDisplayName(StringUtils.EMPTY);
        }
        if (description == null) {
            builder.withDescription(StringUtils.EMPTY);
        }

        Category category = categoryService.save(builder.build());

        // 更新图标关联
        fileAttachmentService.updateRefId(inputData.getIconId(), category.getId());

        return OutputData.builder()
                .category(category)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "名称不能为空"
        )
        private String name;
        @NotBlank(
                message = "名称不能为空"
        )
        private String displayName;

        @NotBlank(
                message = "编码不能为空"
        )
        private String code;

        private String description;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;

        @NotNull(
                message = "排序值不能为空"
        )
        private Integer sortValue;

        @NotNull(
                message = "图标 ID 不能为空"
        )
        private Long iconId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Category category;
    }
}
