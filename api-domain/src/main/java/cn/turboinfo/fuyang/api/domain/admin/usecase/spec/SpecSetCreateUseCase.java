package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecSetService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSetCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecSetCreateUseCase extends AbstractUseCase<SpecSetCreateUseCase.InputData, SpecSetCreateUseCase.OutputData> {
    private final SpecSetService specSetService;

    @Override
    protected OutputData doAction(InputData inputData) {
        String name = inputData.getName();
        String description = inputData.getDescription();
        Long companyId = inputData.getCompanyId() != null ? inputData.getCompanyId() : 0;

        SpecSetCreator creator = SpecSetCreator.builder()
                .withName(name)
                .withCompanyId(companyId)
                .withDescription(description)
                .build();

        specSetService.save(creator);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        private String description;

        private Long companyId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Spec spec;
    }
}
