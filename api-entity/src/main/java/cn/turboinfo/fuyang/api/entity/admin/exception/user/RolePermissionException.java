package cn.turboinfo.fuyang.api.entity.admin.exception.user;

public class RolePermissionException extends RuntimeException {
    public RolePermissionException() {
        this("操作 RolePermission 出错");
    }

    public RolePermissionException(String message) {
        super(message);
    }

    public RolePermissionException(Throwable cause) {
        super(cause);
    }

    public RolePermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
