package cn.turboinfo.fuyang.api.provider.common.repository.database.address;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 地址
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "address"
)
@Entity
@Where(clause = "deleted = 0")
public class AddressPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 所属用户 ID
     */
    private Long userId;

    /**
     * 联系人
     */
    @Convert(converter = DataEncryptConverter.class)
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    @Convert(converter = DataEncryptConverter.class)
    private String mobile;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    @Convert(converter = DataEncryptConverter.class)
    private String detail;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

}
