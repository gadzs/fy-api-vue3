package cn.turboinfo.fuyang.api.domain.mini.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * 家政服务人员确认订单服务完成
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniStaffConfirmServiceCompletedUseCase extends AbstractUseCase<MiniStaffConfirmServiceCompletedUseCase.InputData, MiniStaffConfirmServiceCompletedUseCase.OutputData> {

    private final ServiceOrderService serviceOrderService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        Long staffId = inputData.getStaffId();
        Long serviceOrderId = inputData.getServiceOrderId();
        BigDecimal additionalFee = inputData.getAdditionalFee();
        BigDecimal discountFee = inputData.getDiscountFee();

        ServiceOrder serviceOrder = serviceOrderService.getByIdEnsure(serviceOrderId);

        if (!Objects.equals(staffId, serviceOrder.getStaffId())) {
            throw new IllegalArgumentException("订单所属服务人员不匹配");
        }
        if (serviceOrder.getOrderStatus() != ServiceOrderStatus.IN_SERVICE) {
            throw new IllegalArgumentException("服务订单状态不正确");
        }

        // 确认
        serviceOrderService.staffConfirmServiceCompleted(serviceOrderId, additionalFee, discountFee);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 家政服务人员ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        /**
         * 服务人员ID
         */
        @NotNull(message = "服务人员ID不能为空")
        @Positive
        private Long staffId;

        /**
         * 服务订单ID
         */
        @NotNull(message = "服务订单不能为空")
        @Positive
        private Long serviceOrderId;

        /**
         * 需要补足支付的额外费用
         */
        @PositiveOrZero
        private BigDecimal additionalFee;

        /**
         * 减免费用 (由服务人员确认完成时添加)
         */
        @PositiveOrZero
        private BigDecimal discountFee;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
