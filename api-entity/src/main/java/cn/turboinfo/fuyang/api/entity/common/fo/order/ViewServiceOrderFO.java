package cn.turboinfo.fuyang.api.entity.common.fo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author hai.
 */
@Data
public class ViewServiceOrderFO {

    private Long id;

    /**
     * 产品分类ID
     */
    private Long categoryId;

    /**
     * 产品SKU
     */
    private Long productSkuId;

    /**
     * 产品ID
     */
    private Long productId;
    private String productName;

    /**
     * 公司ID
     */
    private Long companyId;
    private String companyName;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 服务人员ID
     */
    private Long staffId;

    // 冗余 SKU 对应规格信息 (树型层级结构)
    private List<Spec> specList;

    /**
     * 下单备注信息
     */
    private String comment;

    /**
     * 计划开始时间
     */
    private LocalDateTime scheduledStartTime;

    /**
     * 计划结束时间
     */
    private LocalDateTime scheduledEndTime;

    /**
     * 服务开始时间
     */
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    private LocalDateTime serviceEndTime;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 预付金额
     */
    private BigDecimal prepaid;

    /**
     * 额外费用 (由服务人员确认完成时添加)
     */
    private BigDecimal additionalFee;

    /**
     * 减免费用 (由服务人员确认完成时添加)
     */
    private BigDecimal discountFee;

    /**
     * 服务完成时间
     */
    private LocalDateTime completedTime;

    /**
     * 选择的地址ID
     */
    private Long addressId;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    private String mobile;
    
    /**
     * 加密联系电话
     */
    private String mobileEncrypt;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 服务订单状态
     */
    private ServiceOrderStatus orderStatus;

    /**
     * 支付订单状态
     */
    private ServiceOrderPayStatus payStatus;

    /**
     * 退款订单状态
     */
    private ServiceOrderRefundStatus refundStatus;

    /**
     * 评价状态
     */
    private ServiceOrderCreditRatingStatus creditRatingStatus;


}
