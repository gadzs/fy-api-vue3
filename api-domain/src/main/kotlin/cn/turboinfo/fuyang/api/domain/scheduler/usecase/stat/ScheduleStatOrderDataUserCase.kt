package cn.turboinfo.fuyang.api.domain.scheduler.usecase.stat

import cn.turboinfo.fuyang.api.domain.common.service.stat.IndexStatService
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase
import org.springframework.stereotype.Component

/**
 * 订单统计
 */
@Component
class ScheduleStatOrderDataUserCase(
    private val indexStatService: IndexStatService,
) : AbstractUseCase<ScheduleStatOrderDataUserCase.InputData, ScheduleStatOrderDataUserCase.OutputData>() {

    override fun doAction(input: InputData): OutputData {
        indexStatService.setStatAdminData()
        indexStatService.setStatPortalData()
        return OutputData()
    }

    class InputData : AbstractUseCase.InputData()

    class OutputData : AbstractUseCase.OutputData()
}
