package cn.turboinfo.fuyang.api.gateway.admin.fo.sensitive;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SensitiveWordUpdateFO {
    @NotNull(
            message = "id不能为空"
    )
    private Long id;

    @NotBlank(
            message = "word不能为空"
    )
    private String word;
}
