package cn.turboinfo.fuyang.api.provider.framework.spring;

import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * author: sunshow.
 */
public class ConverterCollection {

    private final List<Converter<?, ?>> converterList;

    public ConverterCollection() {
        converterList = new ArrayList<>();
    }

    public void add(Converter<?, ?> converter) {
        converterList.add(converter);
    }

    public Collection<Converter<?, ?>> getCollection() {
        return Collections.unmodifiableList(converterList);
    }
}
