package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginNameType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 用户登陆表
 */
@Table(
        name = "user_login"
)
@Entity
@DynamicInsert
@DynamicUpdate
@EqualsAndHashCode(
        callSuper = true
)
@Where(clause = "deleted = 0")
@Data
public class UserLoginPO extends SoftDeleteFYEntity {

    /**
     * 用户编码
     */
    @Column(name = "user_id")
    private Long userId;


    /**
     * 登录名
     */
    @Convert(converter = DataEncryptConverter.class)
    private String loginName;

    /**
     * 登录名类型
     */
    @Column(
            name = "login_name_type"
    )
    private LoginNameType loginNameType;

    /**
     * 登录验证类型
     */
    @Column(
            name = "login_check_type"
    )
    private LoginCheckType loginCheckType;

    /**
     * 验证者编码
     */
    @Column(
            name = "login_check_id"
    )
    private Long loginCheckId;
}
