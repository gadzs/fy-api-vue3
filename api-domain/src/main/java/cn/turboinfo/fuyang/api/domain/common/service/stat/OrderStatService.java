package cn.turboinfo.fuyang.api.domain.common.service.stat;

import cn.turboinfo.fuyang.api.entity.common.pojo.stat.OrderStat;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 订单统计服务
 * author: hai
 */
public interface OrderStatService extends BaseQService<OrderStat, Long> {

    Optional<OrderStat> getByCompanyIdAndCategory(Long companyId, Long categoryId, Integer year, Integer month);

    List<OrderStat> findByCompanyIdCollection(Collection<Long> companyIdCollection);

    List<OrderStat> findByCategoryIdCollection(Collection<Long> categoryIdCollection);
}
