package cn.turboinfo.fuyang.api.domain.common.service.audit;

import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 家政员审核记录服务
 *
 * @author: hai
 */
public interface HousekeepingStaffAuditRecordService extends BaseQService<HousekeepingStaffAuditRecord, Long> {
    List<HousekeepingStaffAuditRecord> findByCompanyId(Long companyId);

    List<HousekeepingStaffAuditRecord> findByStaffId(Long staffId);

}
