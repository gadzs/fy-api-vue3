package cn.turboinfo.fuyang.api.domain.admin.usecase.agencies;

import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class AgenciesListUseCase extends AbstractUseCase<AgenciesListUseCase.InputData, AgenciesListUseCase.OutputData> {
    private final AgenciesService agenciesService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<Agencies> agenciesList = agenciesService.findAllList();

        return OutputData.builder()
                .agenciesList(agenciesList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    public static class InputData extends AbstractUseCase.InputData {


    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<Agencies> agenciesList;
    }
}
