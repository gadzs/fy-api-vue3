package cn.turboinfo.fuyang.api.domain.common.service.policy;

import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import cn.turboinfo.fuyang.api.entity.common.exception.policy.PolicyException;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.Policy;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.PolicyCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.PolicyUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PolicyService {
    Optional<Policy> getById(Long id);

    Policy getByIdEnsure(Long id);

    List<Policy> findByIdCollection(Collection<Long> idCollection);

    Policy save(PolicyCreator creator) throws PolicyException;

    Policy update(PolicyUpdater updater) throws PolicyException;

    QResponse<Policy> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws PolicyException;

    List<Policy> findAllByType(PolicyType policyType);
}
