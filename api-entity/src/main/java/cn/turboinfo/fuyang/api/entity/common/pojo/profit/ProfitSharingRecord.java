package cn.turboinfo.fuyang.api.entity.common.pojo.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingFailReason;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingResult;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 分账记录
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ProfitSharingRecord extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 分账ID
     */
    private Long profitSharingId;

    /**
     * 微信分账明细单号
     */
    private String wxDetailId;

    /**
     * 接收方编码
     */
    private Long receiverId;

    /**
     * 接收方类型
     */
    private AccountType receiverType;

    /**
     * 分账金额
     */
    private BigDecimal amount;

    /**
     * 分账账户
     */
    private String account;

    /**
     * 分账结果
     */
    private ProfitSharingResult result;

    /**
     * 分账失败原因
     */
    private ProfitSharingFailReason failReason;

    /**
     * 分账描述
     */
    private String description;

    /**
     * 完成时间
     */
    private LocalDateTime finishTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
