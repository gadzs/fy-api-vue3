package cn.turboinfo.fuyang.api.provider.common.repository.database.profit.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ProfitSharingStatusConverter extends BaseEnumConverter<ProfitSharingStatus> {
}
