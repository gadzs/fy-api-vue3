package cn.turboinfo.fuyang.api.provider.autoconfigure

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(value = ["cn.turboinfo.fuyang.api.provider.web", "cn.turboinfo.fuyang.api.domain.web"])
class WebProviderAutoConfiguration