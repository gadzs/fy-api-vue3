package cn.turboinfo.fuyang.api.provider.common.service.impl.profit;

import cn.turboinfo.fuyang.api.domain.common.service.profit.ProfitSharingRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.profit.ProfitSharingRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.profit.ProfitSharingRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.profit.ProfitSharingRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProfitSharingRecordServiceImpl extends DefaultQServiceImpl<ProfitSharingRecord, Long, ProfitSharingRecordPO, ProfitSharingRecordDAO> implements ProfitSharingRecordService {
    @Override
    public List<ProfitSharingRecord> findByProfitSharingId(Long profitSharingId) {
        return convertQBeanToList(dao.findByProfitSharingIdOrderByIdDesc(profitSharingId));
    }
}
