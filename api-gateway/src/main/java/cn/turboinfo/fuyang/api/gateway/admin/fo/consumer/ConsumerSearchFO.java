package cn.turboinfo.fuyang.api.gateway.admin.fo.consumer;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;

/**
 * @author gadzs
 * @description 消费者
 * @date 2023/2/17 17:24
 */
@Data
@QField
public class ConsumerSearchFO {

    @QField(
            placeholder = "手机号",
            searchable = false
    )
    private String mobile;
}
