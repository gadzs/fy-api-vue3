package cn.turboinfo.fuyang.api.gateway.share.controller.shop;

import cn.turboinfo.fuyang.api.domain.share.usecase.shop.ShareListShopUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.QHousekeepingShop;
import cn.turboinfo.fuyang.api.entity.share.fo.shop.ShareShopFO;
import cn.turboinfo.fuyang.api.gateway.share.fo.shop.ShareShopSearchFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContext;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitDataFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareSortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * 家政员列表
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ShareResponseBodyWrapper
@RequestMapping("/share/shop")
public class ShareShopController {

    private final ShareListShopUseCase shareListShopUseCase;

    @RequestMapping("/list")
    public ShareLimitDataFO<ShareShopFO> list(@Valid ShareShopSearchFO search, ShareLimitFO page, ShareSortFO sort, ShareRequestContext context) {

        if (page.getPageSize() == 0) {
            page.setPageSize(20);
        }

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        request.filterEqual(QHousekeepingShop.companyId, context.getCompanyId());

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingShop.id + "|" + QSort.Order.DESC.name()});
        }

        ShareListShopUseCase.OutputData outputData = shareListShopUseCase.execute(ShareListShopUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return ShareLimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
