package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.service.product.ProductSkuService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductSku;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gadzs
 * @description 家政企业产品SKU
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductSkuListUseCase extends AbstractUseCase<ProductSkuListUseCase.InputData, ProductSkuListUseCase.OutputData> {
    private final ProductSkuService productSkuService;

    @Override
    protected OutputData doAction(InputData inputData) {
        return OutputData.builder()
                .productSkuList(productSkuService.findSkuByProductId(inputData.getProductId()))
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "产品id不能为空"
        )
        private Long productId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private List<ProductSku> productSkuList;
    }

}
