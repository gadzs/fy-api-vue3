package cn.turboinfo.fuyang.api.gateway.admin.fo.company;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class HousekeepingCompanyAuthLabelSearchFO {

    @QField(
            placeholder = "名称",
            operator = Operator.LIKE
    )
    private String name;

}
