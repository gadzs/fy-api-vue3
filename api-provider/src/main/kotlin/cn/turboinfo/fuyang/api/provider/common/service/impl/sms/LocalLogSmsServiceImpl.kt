package cn.turboinfo.fuyang.api.provider.common.service.impl.sms

import cn.turboinfo.fuyang.api.entity.common.enumeration.sms.SmsType
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Service

@Service
class LocalLogSmsServiceImpl(
    redisTemplate: StringRedisTemplate,
) : AbstractSmsServiceImpl(redisTemplate) {

    override fun invokeSendCode(type: SmsType, phone: String, code: String) {
        logger.error("发送短信验证码, type=${type}, phone=$phone, code=$code")
    }

    override fun getRateLimitSeconds(): Long {
        return 10
    }

    override fun verifyCode(type: SmsType, phone: String, code: String) {
        if (code == "888888") {
            // 万能验证码
            return
        }
        super.verifyCode(type, phone, code)
    }
}
