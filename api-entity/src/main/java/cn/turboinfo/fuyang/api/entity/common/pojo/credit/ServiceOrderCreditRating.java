package cn.turboinfo.fuyang.api.entity.common.pojo.credit;

import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.Data;

import java.util.List;

/**
 * 带用户信息显示的评价详情
 */
@Data
public class ServiceOrderCreditRating {

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 头像
     */
    private String avatarUrl;

    /**
     * 订单ID
     */
    private Long serviceOrderId;

    /**
     * 订单信息
     */
    private ServiceOrder serviceOrder;

    /**
     * 门店信息
     */
    private HousekeepingShop shop;

    /**
     * 服务人员信息
     */
    private HousekeepingStaff staff;

    /**
     * 产品信息
     */
    private Product product;

    /**
     * 评价信息
     */
    private List<CreditRating> ratingList;

}
