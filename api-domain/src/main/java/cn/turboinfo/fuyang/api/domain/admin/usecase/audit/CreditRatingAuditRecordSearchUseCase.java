package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.handler.credit.CreditRatingDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.credit.CreditRatingService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class CreditRatingAuditRecordSearchUseCase extends AbstractUseCase<CreditRatingAuditRecordSearchUseCase.InputData, CreditRatingAuditRecordSearchUseCase.OutputData> {
    private final CreditRatingService creditRatingService;

    private final CreditRatingDataFactory creditRatingDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<CreditRating> response = creditRatingService.findAll(inputData.getRequest(), inputData.getRequestPage());

        List<CreditRating> creditRatingList = new ArrayList<>(response.getPagedData());

        creditRatingDataFactory.assembleAppraiser(creditRatingList);

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户ID不能为空")
        private Long userId;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<CreditRating> qResponse;
    }
}
