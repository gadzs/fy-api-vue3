package cn.turboinfo.fuyang.api.gateway.mini.contract.address

import cn.turboinfo.fuyang.api.domain.mini.usecase.address.MiniCreateUserAddressUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.address.MiniUpdateUserAddressUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.address.MiniViewUserAddressUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/address"])
interface MiniConsumerAddressContract {

    // 添加新地址
    @AutoMappingContract(
        beanType = MiniCreateUserAddressUseCase::class,
    )
    fun create()

    // 编辑地址
    @AutoMappingContract(
        beanType = MiniUpdateUserAddressUseCase::class,
    )
    fun update()

    @AutoMappingContract(
        beanType = MiniViewUserAddressUseCase::class,
        paths = ["/view/{addressId}"],
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun viewById()

}
