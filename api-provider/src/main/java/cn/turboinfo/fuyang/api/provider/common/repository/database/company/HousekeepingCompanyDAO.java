package cn.turboinfo.fuyang.api.provider.common.repository.database.company;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface HousekeepingCompanyDAO extends BaseRepository<HousekeepingCompanyPO, Long> {
    List<HousekeepingCompanyPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    Optional<HousekeepingCompanyPO> findByUscc(String uscc);

    @Query(value = "select t from HousekeepingCompanyPO t where t.name like %?1%")
    List<HousekeepingCompanyPO> findByName(String name);

    Optional<HousekeepingCompanyPO> findByContactNumber(String contactNumber);

    List<HousekeepingCompanyPO> findByAreaCode(Long areaCode);
}
