package cn.turboinfo.fuyang.api.gateway.admin.contract.audit

import cn.turboinfo.fuyang.api.domain.admin.usecase.audit.*
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/audit"])
@ResponseBodyWrapper
interface AuditContract {

    // 门店信息审核通过
    @PermissionScope(value = ["adminAudit:shopAudit"])
    @AutoMappingContract(
        paths = ["/shop/reviewed"],
        beanType = HousekeepingShopReviewedUseCase::class

    )
    fun shopReviewed()

    // 门店信息审核不通过
    @PermissionScope(value = ["adminAudit:shopAudit"])
    @AutoMappingContract(
        paths = ["/shop/notpass"],
        beanType = HousekeepingShopNotPassUseCase::class
    )
    fun shopNotpass()

    // 家政员信息审核通过 productList
    @PermissionScope(value = ["adminAudit:staffAudit"])
    @AutoMappingContract(
        paths = ["/staff/reviewed"],
        beanType = HousekeepingStaffReviewedUseCase::class

    )
    fun staffReviewed()

    // 家政员信息审核不通过
    @PermissionScope(value = ["adminAudit:staffAudit"])
    @AutoMappingContract(
        paths = ["/staff/notpass"],
        beanType = HousekeepingStaffNotPassUseCase::class
    )
    fun staffNotpass()

    // 产品信息审核通过
    @PermissionScope(value = ["adminAudit:productAudit"])
    @AutoMappingContract(
        paths = ["/product/reviewed"],
        beanType = ProductReviewedUseCase::class

    )
    fun productReviewed()

    // 产品信息审核不通过
    @PermissionScope(value = ["adminAudit:productAudit"])
    @AutoMappingContract(
        paths = ["/product/notpass"],
        beanType = ProductNotPassUseCase::class
    )
    fun productNotpass()

    // 评价审核通过
    @PermissionScope(value = ["adminAudit:creditAudit"])
    @AutoMappingContract(
        paths = ["/credit/reviewed"],
        beanType = CreditRatingReviewedUseCase::class

    )
    fun creditReviewed()

    // 评价审核不通过
    @PermissionScope(value = ["adminAudit:creditAudit"])
    @AutoMappingContract(
        paths = ["/credit/notpass"],
        beanType = CreditRatingNotPassUseCase::class
    )
    fun creditNotpass()
}
