package cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.IDCardType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class IDCardTypeConverter extends BaseEnumConverter<IDCardType> {
}
