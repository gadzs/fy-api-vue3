package cn.turboinfo.fuyang.api.domain.common.service.category;

import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;

/**
 * 分类服务
 */
public interface CategoryService extends BaseQService<Category, Long> {

    /**
     * @return 返回所有的带层级和排序的分类列表
     */
    List<Category> findAllSortedWithHierarchy();

    List<Category> findWithHierarchy(Collection<Long> categoryIdCollection);

    /**
     * @param parentId 父级编码
     * @param name     名称
     */
    List<Category> findByParentIdAndName(Long parentId, String name);

    /**
     * @return 返回顶级分类列表
     */
    List<Category> findTop();

    /**
     * 检查是否可用
     *
     * @param parentId 父级编码
     * @param name     名称
     * @return
     */
    boolean checkAvailable(Long parentId, String name);

    /**
     * 更新检查是否可用
     *
     * @param categoryId 自身编码
     * @param parentId   父级编码
     * @param name       名称
     * @return
     */
    boolean checkAvailable(Long categoryId, Long parentId, String name);

    List<Category> findByCodeWithChildren(String code);

    List<Category> findAllChildren();

}
