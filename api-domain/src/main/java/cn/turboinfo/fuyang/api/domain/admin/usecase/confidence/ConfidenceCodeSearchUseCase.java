package cn.turboinfo.fuyang.api.domain.admin.usecase.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ConfidenceCodeSearchUseCase extends AbstractUseCase<ConfidenceCodeSearchUseCase.InputData, ConfidenceCodeSearchUseCase.OutputData> {
    private final ConfidenceCodeService confidenceCodeService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<ConfidenceCode> response = confidenceCodeService.findAll(inputData.getRequest(), inputData.getRequestPage());

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<ConfidenceCode> qResponse;
    }
}
