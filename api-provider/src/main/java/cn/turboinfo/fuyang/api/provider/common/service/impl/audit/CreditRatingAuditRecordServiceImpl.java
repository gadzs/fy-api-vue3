package cn.turboinfo.fuyang.api.provider.common.service.impl.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.CreditRatingAuditRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.CreditRatingAuditRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.CreditRatingAuditRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.CreditRatingAuditRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class CreditRatingAuditRecordServiceImpl extends DefaultQServiceImpl<CreditRatingAuditRecord, Long, CreditRatingAuditRecordPO, CreditRatingAuditRecordDAO> implements CreditRatingAuditRecordService {
    @Override
    public List<CreditRatingAuditRecord> findByCreditRatingId(Long creditRatingId) {
        return convertStreamQBeanToList(dao.findByCreditRatingIdOrderByIdDesc(creditRatingId).stream());
    }
}
