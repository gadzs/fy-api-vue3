package cn.turboinfo.fuyang.api.provider.common.repository.database.confidence;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

public interface ConfidenceCodeDAO extends FYRepository<ConfidenceCodePO, Long> {
    
}
