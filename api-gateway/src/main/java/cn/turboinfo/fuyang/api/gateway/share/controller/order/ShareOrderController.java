package cn.turboinfo.fuyang.api.gateway.share.controller.order;

import cn.turboinfo.fuyang.api.domain.share.usecase.order.ShareListServiceOrderUseCase;
import cn.turboinfo.fuyang.api.domain.util.DateTimeFormatHelper;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.QHousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.share.fo.order.ShareServiceOrderFO;
import cn.turboinfo.fuyang.api.gateway.share.fo.order.ShareServiceOrderSearchFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.context.ShareRequestContext;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitDataFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareLimitFO;
import cn.turboinfo.fuyang.api.gateway.share.framework.http.fo.ShareSortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * 服务订单列表
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ShareResponseBodyWrapper
@RequestMapping("/share/order")
public class ShareOrderController {

    private final ShareListServiceOrderUseCase shareListServiceOrderUseCase;

    @RequestMapping("/list")
    public ShareLimitDataFO<ShareServiceOrderFO> list(@Valid ShareServiceOrderSearchFO search, ShareLimitFO page, ShareSortFO sort, ShareRequestContext context) {

        if (page.getPageSize() == 0) {
            page.setPageSize(20);
        }

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        request.filterEqual(QServiceOrder.companyId, context.getCompanyId());

        if (StringUtils.hasLength(search.getStatus())) {
            String[] statusArray = search.getStatus().split(",");
            val orderStatusList = Arrays.stream(statusArray)
                    .toList()
                    .stream()
                    .map(status -> (Object) ServiceOrderStatus.get(Integer.parseInt(status)))
                    .toList();
            request.filterIn("orderStatus", orderStatusList);
        }

        LocalDateTime stateTime = DateTimeFormatHelper.getStartTime(search.getStartDate());
        LocalDateTime endTime = DateTimeFormatHelper.getEndTime(search.getEndDate());
        if (stateTime != null && endTime != null) {
            request.filterBetween(QServiceOrder.createdTime, stateTime, endTime);
        } else if (stateTime != null) {
            request.filterGreaterEqual(QServiceOrder.createdTime, stateTime);
        } else if (endTime != null) {
            request.filterLessEqual(QServiceOrder.createdTime, endTime);
        }

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QHousekeepingStaff.id + "|" + QSort.Order.DESC.name()});
        }

        ShareListServiceOrderUseCase.OutputData outputData = shareListServiceOrderUseCase.execute(ShareListServiceOrderUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return ShareLimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
