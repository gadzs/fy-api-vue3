package cn.turboinfo.fuyang.api.domain.admin.usecase.spec;

import cn.turboinfo.fuyang.api.domain.common.service.spec.SpecService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecUpdateUseCase extends AbstractUseCase<SpecUpdateUseCase.InputData, SpecUpdateUseCase.OutputData> {
    private final SpecService specService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();
        Long parentId = inputData.getParentId();
        String name = inputData.getName();
        String description = inputData.getDescription();
        String displayName = inputData.getDisplayName();
        if (parentId != null && parentId != 0) {
            // 判断父级是否存在
            specService.getByIdEnsure(parentId);
        }

        // 检查分类名称是否存在
        if (!specService.checkAvailable(id, parentId, name)) {
            throw new RuntimeException(String.format("%s 已存在", name));
        }

        SpecUpdater.Builder builder = SpecUpdater.builder(id);

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, SpecUpdater.class, inputData);

        if (displayName == null) {
            builder.withDisplayName(StringUtils.EMPTY);
        }
        if (description == null) {
            builder.withDescription(StringUtils.EMPTY);
        }
        Spec spec = specService.update(builder.build());

        return OutputData.builder()
                .spec(spec)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        private String displayName;

        private String description;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;

        /**
         * 规格组编码
         */
        private Long specSetId;

        private Integer sortValue;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Spec spec;
    }
}
