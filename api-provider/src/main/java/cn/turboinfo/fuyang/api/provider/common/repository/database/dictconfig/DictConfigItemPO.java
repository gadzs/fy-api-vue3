package cn.turboinfo.fuyang.api.provider.common.repository.database.dictconfig;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 数据字典条目
 */
@Table(
        name = "dict_config_item"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
public class DictConfigItemPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 字典ID
     */
    @Column(
            name = "dict_config_id"
    )
    private Long dictConfigId;

    /**
     * 字典Key
     */
    @Column(
            name = "dict_config_key"
    )
    private String dictConfigKey;

    /**
     * 条目值
     */
    @Column(
            name = "item_value"
    )
    private String itemValue;

    /**
     * 条目名称
     */
    @Column(
            name = "item_name"
    )
    private String itemName;

    /**
     * 条目描述
     */
    private String description;

    /**
     * 条目顺序
     */
    @Column(
            name = "item_order"
    )
    private Integer itemOrder;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
