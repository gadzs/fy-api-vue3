package cn.turboinfo.fuyang.api.provider.common.repository.database.shop.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * @author hai.
 */
@Converter(autoApply = true)
public class ShopStatusConverter extends BaseEnumConverter<ShopStatus> {
}
