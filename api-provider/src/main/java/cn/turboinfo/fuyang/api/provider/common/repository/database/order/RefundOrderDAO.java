package cn.turboinfo.fuyang.api.provider.common.repository.database.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface RefundOrderDAO extends FYRepository<RefundOrderPO, Long> {

    List<RefundOrderPO> findByObjectIdAndObjectType(Long objectId, EntityObjectType objectType);
}
