package cn.turboinfo.fuyang.api.provider.common.repository.database.role;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface RolePermissionDAO extends BaseRepository<RolePermissionPO, Long> {

    List<RolePermissionPO> findByRoleId(Long roleId);

    List<RolePermissionPO> findByRoleIdIn(Collection<Long> roleIdCollection);

    List<RolePermissionPO> findByPermissionId(Long permissionId);
}
