package cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: sunshow.
 */
@Converter(autoApply = true)
public class EntityObjectTypeConverter extends BaseEnumConverter<EntityObjectType> {
}
