package cn.turboinfo.fuyang.api.domain.common.service.activity;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityApply;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;
import java.util.Optional;

/**
 * 活动报名服务
 * author: hai
 */
public interface ActivityApplyService extends BaseQService<ActivityApply, Long> {

    Optional<ActivityApply> findByActivityIdAndUserId(Long activityId, Long userId, UserType userType);

    List<ActivityApply> findByActivityId(Long activityId);
}
