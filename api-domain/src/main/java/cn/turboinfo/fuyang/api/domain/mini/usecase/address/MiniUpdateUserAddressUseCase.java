package cn.turboinfo.fuyang.api.domain.mini.usecase.address;

import cn.turboinfo.fuyang.api.domain.common.service.address.AddressService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.PhoneUtils;
import cn.turboinfo.fuyang.api.domain.util.SecurityUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.Address;
import cn.turboinfo.fuyang.api.entity.common.pojo.address.AddressUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * 更新用户地址
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniUpdateUserAddressUseCase extends AbstractUseCase<MiniUpdateUserAddressUseCase.InputData, MiniUpdateUserAddressUseCase.OutputData> {

    private final AddressService addressService;

    @Value("${kit.security.rsa.private-key:}")
    private String base64RSAPrivateKey;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long addressId = inputData.getAddressId();
        String mobile = new String(SecurityUtils.decryptRSA(SecurityUtils.decodeHex(inputData.getMobile()), SecurityUtils.decodeBase64(base64RSAPrivateKey)), StandardCharsets.UTF_8);
        String contact = new String(SecurityUtils.decryptRSA(SecurityUtils.decodeHex(inputData.getContact()), SecurityUtils.decodeBase64(base64RSAPrivateKey)), StandardCharsets.UTF_8);

        Address original = addressService.getByIdEnsure(addressId);

        if (!Objects.equals(original.getUserId(), inputData.getUserId())) {
            throw new IllegalArgumentException("地址 ID 与用户 ID 不匹配");
        }

        AddressUpdater.Builder builder = AddressUpdater.builder(addressId);
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, AddressUpdater.class, inputData);

        builder.withContact(contact)
                .withMobile(mobile);

        if (!mobile.contains("*")) {
            if (PhoneUtils.isValidPhoneNum(mobile)) {
                builder.withMobile(mobile);
            } else {
                throw new IllegalArgumentException("手机号格式不正确");
            }
        }

        // 先把经纬度设置为 0
        if (inputData.latitude == null) {
            builder.withLatitude(BigDecimal.ZERO);
        }

        if (inputData.longitude == null) {
            builder.withLongitude(BigDecimal.ZERO);
        }

        addressService.update(builder.build());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 地址 ID
         */
        @NotNull(message = "地址 ID 不能为空")
        @Positive
        private Long addressId;

        /**
         * 所属用户 ID
         */
        @NotNull(message = "用户 ID 不能为空")
        @Positive
        private Long userId;

        /**
         * 联系人
         */
        @NotBlank(message = "联系人不能为空")
        private String contact;

        /**
         * 性别
         */
        private GenderType genderType;

        /**
         * 手机号
         */
        @NotBlank(message = "手机号不能为空")
        private String mobile;

        /**
         * 区域编码
         */
        @NotNull(message = "区域编码不能为空")
        @Positive
        private Long divisionId;

        /**
         * 兴趣点地址
         */
        @NotBlank(message = "位置不能为空")
        private String poiName;

        /**
         * 详细地址
         */
        @NotBlank(message = "详细地址不能为空")
        private String detail;

        /**
         * 经度
         */
        private BigDecimal longitude;

        /**
         * 纬度
         */
        private BigDecimal latitude;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
