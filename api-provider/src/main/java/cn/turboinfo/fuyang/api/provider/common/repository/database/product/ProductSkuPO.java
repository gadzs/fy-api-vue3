package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSkuStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import io.hypersistence.utils.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;

/**
 * 产品SKU
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "product_sku"
)
@Where(clause = "deleted = 0")
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Entity
@DynamicUpdate
@DynamicInsert
public class ProductSkuPO extends SoftDeleteFYEntity {

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 原始价
     */
    @Column(name = "original_price")
    private BigDecimal originalPrice;

    /**
     * 当前售价
     */
    private BigDecimal price;

    // 冗余 SKU 对应规格信息 (树型层级结构)
    @Type(type = "json")
    @Column(name = "spec_list", columnDefinition = "json")
    private List<Spec> specList;

    /**
     * 产品SKU状态
     */
    private ProductSkuStatus status;

}
