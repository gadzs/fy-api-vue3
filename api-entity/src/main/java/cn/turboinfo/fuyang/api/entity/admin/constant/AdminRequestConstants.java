package cn.turboinfo.fuyang.api.entity.admin.constant;

/**
 * author: sunshow.
 */
public class AdminRequestConstants {

    public static final String AttrSession = "ADMIN_SESSION";

    public static final String AttrSearch = "qsearch";

    public static final String AttrSort = "qsort";

    public static final String AttrQResponse = "qresponse";

    public static final String AttrSearchableDefList = "searchableDefList";

    public static final String AttrSortableDefList = "sortableDefList";

}
