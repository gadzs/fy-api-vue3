package cn.turboinfo.fuyang.api.entity.common.fo.staff;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author hai.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HousekeepingStaffImportError {

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 展示名称
     */
    private String displayName;

    /*
     * 身份证
     */
    private String idCard;

    /*
     * 身份证
     */
    private Long idCardFile;

    /**
     * 个人照片
     */
    private Long photoFile;

    /**
     * 联系电话
     */
    private String contactMobile;

    /**
     * 介绍
     */
    private String introduction;


    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 家政员类型
     */
    private String staffType;

    private String error;
}
