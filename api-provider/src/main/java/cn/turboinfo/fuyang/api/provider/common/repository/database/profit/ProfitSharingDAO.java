package cn.turboinfo.fuyang.api.provider.common.repository.database.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface ProfitSharingDAO extends FYRepository<ProfitSharingPO, Long> {

    List<ProfitSharingPO> findByObjectIdAndObjectType(Long orderId, EntityObjectType objectType);

}
