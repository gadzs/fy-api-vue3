package cn.turboinfo.fuyang.api.provider.common.repository.database.division;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 地区
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "division"
)
@Where(clause = "deleted = 0")
@Entity
public class DivisionPO extends SoftDeleteFYEntity {

    /**
     * 区域编码
     */
    private Long areaCode;

    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 父级ID
     */
    private Long parentId;

    /**
     * 父级编码
     */
    private Long parentCode;

    /**
     * 省编码
     */
    private Long provinceCode;

    /**
     * 市编码
     */
    private Long cityCode;

}
