package cn.turboinfo.fuyang.api.gateway.mini.contract.order

import cn.turboinfo.fuyang.api.domain.mini.usecase.order.*
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@ConsumerScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/order"])
interface MiniConsumerServiceOrderContract {

    // 服务预约下单
    @AutoMappingContract(
        beanType = MiniConsumerPlaceServiceOrderUseCase::class,
    )
    fun placeServiceOrder()

    // 服务订单预览
    @AutoMappingContract(
        beanType = MiniConsumerPreviewServiceOrderUseCase::class,
    )
    fun previewServiceOrder()

    // 可用支付类型
    @AutoMappingContract(
        beanType = MiniConsumerSubmitOrderAvailablePayTypeUseCase::class,
    )
    fun submitOrderAvailablePayType()

    // 下单时的提交支付请求
    @AutoMappingContract(
        beanType = MiniConsumerSubmitOrderPayUseCase::class,
    )
    fun submitOrderPay()

    // 扫码开始服务
    @AutoMappingContract(
        beanType = MiniConsumerScanStartServiceUseCase::class,
    )
    fun scanStartService()

    // 取消订单
    @AutoMappingContract(
        beanType = MiniConsumerCancelServiceOrderUseCase::class,
    )
    fun cancel()

    // 可用支付类型
    @AutoMappingContract(
        beanType = MiniConsumerPayRemainingUnpaidAvailablePayTypeUseCase::class,
    )
    fun payRemainingUnpaidAvailablePayType()

    // 支付剩余金额
    @AutoMappingContract(
        beanType = MiniConsumerPayRemainingUnpaidUseCase::class,
    )
    fun payRemainingUnpaid()

    // 微信支付成功
    @AutoMappingContract(
        beanType = MiniConsumerWechatPaySuccessUseCase::class,
    )
    fun paySuccess()

    // 微信支付失败
    @AutoMappingContract(
        beanType = MiniConsumerWechatPayFailureUseCase::class,
    )
    fun payFailure()

    // 微信支付成功
    @AutoMappingContract(
        beanType = MiniConsumerRemainingUnpaidPaySuccessUseCase::class,
    )
    fun remainingUnpaidPaySuccess()

    // 微信支付失败
    @AutoMappingContract(
        beanType = MiniConsumerRemainingUnpaidPayFailureUseCase::class,
    )
    fun remainingUnpaidPayFailure()

}
