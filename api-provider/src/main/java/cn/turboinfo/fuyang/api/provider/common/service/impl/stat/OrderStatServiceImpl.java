package cn.turboinfo.fuyang.api.provider.common.service.impl.stat;

import cn.turboinfo.fuyang.api.domain.common.service.stat.OrderStatService;
import cn.turboinfo.fuyang.api.entity.common.pojo.stat.OrderStat;
import cn.turboinfo.fuyang.api.provider.common.repository.database.stat.OrderStatDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.stat.OrderStatPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class OrderStatServiceImpl extends DefaultQServiceImpl<OrderStat, Long, OrderStatPO, OrderStatDAO> implements OrderStatService {
    @Override
    public Optional<OrderStat> getByCompanyIdAndCategory(Long companyId, Long categoryId, Integer year, Integer month) {
        return dao.findByCompanyIdAndCategoryIdAndYearAndMonth(companyId, categoryId, year, month).map(this::convertQBean);
    }

    @Override
    public List<OrderStat> findByCompanyIdCollection(Collection<Long> companyIdCollection) {
        return convertQBeanToList(dao.findByCompanyIdIn(companyIdCollection));
    }

    @Override
    public List<OrderStat> findByCategoryIdCollection(Collection<Long> categoryIdCollection) {
        return convertQBeanToList(dao.findByCategoryIdIn(categoryIdCollection));
    }
}
