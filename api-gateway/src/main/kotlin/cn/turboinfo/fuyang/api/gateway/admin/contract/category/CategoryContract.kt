package cn.turboinfo.fuyang.api.gateway.admin.contract.category

import cn.turboinfo.fuyang.api.domain.admin.usecase.category.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/category"])
interface CategoryContract {

    // 添加分类
    @PermissionScope("category:create")
    @AutoMappingContract(
        beanType = CategoryCreateUseCase::class,
    )
    fun create()

    // 修改分类
    @PermissionScope("category:update")
    @AutoMappingContract(
        beanType = CategoryUpdateUseCase::class,
    )
    fun update()

    // 查询上级分类
    @PermissionScope("category:list")
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = CategoryAncestorsUpdateUseCase::class,
        consumes = [],
    )
    fun ancestors()

    // 检查名称是否可用
    @PermissionScope("category:list")
    @CompanyScope
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = CategoryCheckAvailableUseCase::class,
        consumes = [],
    )
    fun checkAvailable()

    @PermissionScope("category:delete")
    @AutoMappingContract(
        beanType = CategoryDeleteUseCase::class,
        consumes = [],
    )
    fun delete()

    // 返回tree列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = CategoryTreeUseCase::class,
        consumes = [],
    )
    fun tree()
}