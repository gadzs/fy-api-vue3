package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 服务订单状态
 */
@Slf4j
public enum ServiceOrderStatus implements BaseEnum {

    INIT(0, "未支付"),

    PENDING_DISPATCH(10, "待派单"),

    WAITING_STAFF_CONFIRM(20, "待服务人员确认"),

    PENDING_REASSIGNMENT(30, "待重新派单"),

    DISPATCHED_WAITING_SERVICE(40, "已派单待服务"),

    CANCELLED_UNPAID(50, "未支付取消"),

    CANCELLED_BY_CUSTOMER(60, "客户取消"),

    CANCELLED_BY_STAFF(65, "服务人员取消"),

    CANCELLED_BY_COMPANY(70, "家政公司取消"),

    IN_SERVICE(80, "正在服务"),

    SERVICE_COMPLETED(90, "服务完成"),

    COMPLETED(100, "订单完成"),

    ;

    private int value;
    private String name;

    ServiceOrderStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ServiceOrderStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ServiceOrderStatus> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ServiceOrderStatus> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
