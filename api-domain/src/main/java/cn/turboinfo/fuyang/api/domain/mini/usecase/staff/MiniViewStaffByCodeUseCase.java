package cn.turboinfo.fuyang.api.domain.mini.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.handler.order.ServiceOrderDataFactory;
import cn.turboinfo.fuyang.api.domain.common.handler.product.ProductDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.service.division.DivisionService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.division.Division;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 查看家政人员详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewStaffByCodeUseCase extends AbstractUseCase<MiniViewStaffByCodeUseCase.InputData, MiniViewStaffByCodeUseCase.OutputData> {

    private final HousekeepingStaffService housekeepingStaffService;

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final ServiceOrderService serviceOrderService;

    private final ProductService productService;

    private final ServiceOrderDataFactory serviceOrderDataFactory;

    private final ProductDataFactory productDataFactory;

    private final DivisionService divisionService;

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        String code = inputData.getCode();

        // 根据放心码查询家政人员
        HousekeepingStaff staff = housekeepingStaffService.findByCode(code).orElseThrow(() -> new RuntimeException("放心码错误"));

        // 根据家政人员ID查询家政公司
        HousekeepingCompany company = housekeepingCompanyService.getByIdEnsure(staff.getCompanyId());

        // 根据用户ID和家政人员ID查询已派单待服务的订单
        List<ServiceOrder> serviceOrderList = serviceOrderService.findByUserIdAndStaffId(userId, staff.getId(), ServiceOrderStatus.DISPATCHED_WAITING_SERVICE);

        Division division = divisionService.getByIdEnsure(staff.getProvinceCode());

        staff.setProvinceName(division.getAreaName());
        staff.setAge(IdCardUtils.countAge(staff.getIdCard()));

        serviceOrderDataFactory.assembleCompany(serviceOrderList);

        // 产品信息
        Set<Long> productIdSet = serviceOrderList.stream()
                .map(ServiceOrder::getProductId)
                .collect(Collectors.toSet());

        List<Product> productList = productService.findByIdCollection(productIdSet);

        productDataFactory.assembleAttachment(productList);

        // 定制服务信息
        List<ServiceCustom> serviceCustomList = serviceCustomService.findByUserIdAndStaffId(userId, staff.getId(), ServiceCustomStatus.DISPATCHED_WAITING_SERVICE);

        return OutputData.builder()
                .staff(staff)
                .company(company)
                .serviceOrderList(serviceOrderList)
                .productList(productList)
                .serviceCustomList(serviceCustomList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        /**
         * 放心码
         */
        @NotBlank(
                message = "放心码不能为空"
        )
        private String code;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingStaff staff;

        private HousekeepingCompany company;

        private List<ServiceOrder> serviceOrderList;

        private List<Product> productList;

        private List<ServiceCustom> serviceCustomList;

    }
}
