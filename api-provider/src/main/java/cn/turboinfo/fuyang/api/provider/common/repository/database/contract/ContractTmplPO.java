package cn.turboinfo.fuyang.api.provider.common.repository.database.contract;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 合同管理
 * author: hai
 */
@Table(
        name = "contract_tmpl"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Where(clause = "deleted = 0")
@Entity
public class ContractTmplPO extends SoftDeleteFYEntity {
    /**
     * 合同名称
     */
    private String name;

    /**
     * 合同类别
     */
    @Column(
            name = "category_id"
    )
    private Long categoryId;

    /**
     * 企业编码
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;
}
