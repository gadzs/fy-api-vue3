package cn.turboinfo.fuyang.api.entity.common.exception.staff;

public class HousekeepingStaffException extends RuntimeException {
    public HousekeepingStaffException() {
        this("操作 Shop 出错");
    }

    public HousekeepingStaffException(String message) {
        super(message);
    }

    public HousekeepingStaffException(Throwable cause) {
        super(cause);
    }

    public HousekeepingStaffException(String message, Throwable cause) {
        super(message, cause);
    }
}
