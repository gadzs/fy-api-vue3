package cn.turboinfo.fuyang.api.entity.common.pojo.sensitive;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 敏感词管理
 *
 * @author hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class SensitiveWord extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    private String word;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
