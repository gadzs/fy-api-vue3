package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.staff.HousekeepingStaffException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 家政人员审核usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffReviewedUseCase extends AbstractUseCase<HousekeepingStaffReviewedUseCase.InputData, HousekeepingStaffReviewedUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;

    private final SysUserService sysUserService;

    private final HousekeepingStaffAuditRecordService housekeepingStaffAuditRecordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();
        val auditorId = inputData.getUserId();
        val staff = housekeepingStaffService.getByIdEnsure(id);
        if (staff.getStatus() != StaffStatus.DEFAULT) {
            throw new HousekeepingStaffException("数据状态不是待审核，操作失败");
        }

        // 更新状态
        HousekeepingStaffUpdater.Builder builder = HousekeepingStaffUpdater.builder(id);

        builder
                .withStatus(StaffStatus.PUBLISHED);

        HousekeepingStaff housekeepingStaff = housekeepingStaffService.update(builder.build());

        val auditorUser = sysUserService.getByIdEnsure(auditorId);

        // 更新审核记录
        HousekeepingStaffAuditRecordCreator housekeepingStaffAuditRecord = HousekeepingStaffAuditRecordCreator.builder()
                .withStaffId(housekeepingStaff.getId())
                .withCompanyId(housekeepingStaff.getCompanyId())
                .withAuditStatus(AuditStatus.REVIEWED)
                .withUserId(auditorId)
                .withUserName(auditorUser.getUsername())
                .withRemark("审核通过")
                .build();
        housekeepingStaffAuditRecordService.save(housekeepingStaffAuditRecord);


        return OutputData.builder()
                .id(housekeepingStaff.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(message = "审核用户ID不能为空")
        private Long userId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
