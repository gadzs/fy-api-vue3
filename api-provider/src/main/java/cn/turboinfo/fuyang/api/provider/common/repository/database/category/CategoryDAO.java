package cn.turboinfo.fuyang.api.provider.common.repository.database.category;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;

public interface CategoryDAO extends FYRepository<CategoryPO, Long> {

    List<CategoryPO> findByParentIdAndNameOrderByIdDesc(Long parentId, String name);

    List<CategoryPO> findByParentIdOrderBySortValueAscIdAsc(Long parentId);

    List<CategoryPO> findByParentIdIn(Collection<Long> parentIdCollection);

    List<CategoryPO> findByCode(String code);

    List<CategoryPO> findAllByParentIdNot(Long parentId);

}
