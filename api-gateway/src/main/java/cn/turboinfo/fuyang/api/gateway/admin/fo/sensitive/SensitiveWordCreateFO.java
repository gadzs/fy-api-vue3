package cn.turboinfo.fuyang.api.gateway.admin.fo.sensitive;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SensitiveWordCreateFO {
    @NotBlank(
            message = "word不能为空"
    )
    private String word;
}
