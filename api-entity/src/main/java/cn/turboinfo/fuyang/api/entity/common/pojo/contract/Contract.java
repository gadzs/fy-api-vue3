package cn.turboinfo.fuyang.api.entity.common.pojo.contract;

import cn.turboinfo.fuyang.api.entity.common.enumeration.contract.ContractStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 合同管理
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Contract extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 合同编码
     */
    private String contractNo;

    /**
     * 订单编码
     */
    private Long orderId;

    /**
     * 企业编码
     */
    private Long companyId;

    /**
     * 合同模板编码
     */
    private Long templateId;

    /**
     * 合同状态
     */
    private ContractStatus status;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
