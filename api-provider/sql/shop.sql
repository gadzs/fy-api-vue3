DROP TABLE IF EXISTS `housekeeping_shop`;
CREATE TABLE `housekeeping_shop`
(
    `id`                    BIGINT        NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`            BIGINT        NOT NULL DEFAULT 0 COMMENT '所属企业',
    `name`                  VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '门店名称',
    `found_date`            TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '成立日期',
    `province_code`         BIGINT        NOT NULL DEFAULT 0 COMMENT '省编码',
    `city_code`             BIGINT        NOT NULL DEFAULT 0 COMMENT '市编码',
    `area_code`             BIGINT        NOT NULL DEFAULT 0 COMMENT '区编码',
    `address`               VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '地址',
    `longitude`             decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '经度',
    `latitude`              decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '纬度',
    `contact_person`        VARCHAR(30)   NOT NULL DEFAULT '' COMMENT '联系人',
    `contact_mobile`        VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '联系人电话',
    `business_time`         VARCHAR(128)  NOT NULL DEFAULT '' COMMENT '营业时间',
    `business_license_file` bigint(20)    NOT NULL DEFAULT 0 COMMENT '营业执照',
    `status`                TINYINT       NOT NULL DEFAULT 0 COMMENT '状态',
    `credit_score`          decimal(5, 2) NOT NULL DEFAULT 0 COMMENT '信用分',
    `order_num`             BIGINT        NOT NULL DEFAULT 0 COMMENT '订单数',
    `deleted`               BIGINT        NOT NULL DEFAULT 0 COMMENT '删除时间（大于 0 为已删除）',
    `created_time`          TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time`          TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_company_name` (`company_id`, `name`, `deleted`),
    KEY `idx_creditscore_createdtime` (`credit_score`, `created_time`),
    KEY `idx_citycode` (`city_code`),
    KEY `idx_createdtime` (`created_time`),
    KEY `idx_updatetime` (`updated_time`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic
  AUTO_INCREMENT = 100000 COMMENT ='家政门店';
