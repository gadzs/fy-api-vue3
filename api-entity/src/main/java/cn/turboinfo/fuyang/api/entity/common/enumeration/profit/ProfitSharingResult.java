package cn.turboinfo.fuyang.api.entity.common.enumeration.profit;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 分账结果
 */
@Slf4j
public enum ProfitSharingResult implements BaseEnum {

    PENDING(0, "待分账", "PENDING"),

    SUCCESS(10, "分账成功", "SUCCESS"),

    CLOSED(90, "已关闭", "CLOSED"),

    ;

    private int value;
    private String name;
    private String code;

    ProfitSharingResult(int value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProfitSharingResult get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ProfitSharingResult> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ProfitSharingResult> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
