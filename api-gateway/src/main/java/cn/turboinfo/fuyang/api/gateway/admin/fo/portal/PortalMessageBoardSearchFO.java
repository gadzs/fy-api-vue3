package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

/**
 * 留言板FO
 */
@Data
@QField
public class PortalMessageBoardSearchFO {
    /**
     * 类型
     */
    @QField
    private CmsMessageBoardType type;

    @QField(placeholder = "留言内容", operator = Operator.LIKE)
    private String content;
}
