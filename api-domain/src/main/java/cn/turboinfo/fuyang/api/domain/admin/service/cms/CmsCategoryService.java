package cn.turboinfo.fuyang.api.domain.admin.service.cms;


import cn.turboinfo.fuyang.api.entity.admin.exception.cms.CmsCategoryException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategory;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategoryCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategoryUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CmsCategoryService {
    Optional<CmsCategory> getById(Long id);

    CmsCategory getByIdEnsure(Long id);

    List<CmsCategory> findByIdCollection(Collection<Long> idCollection);

    CmsCategory save(CmsCategoryCreator creator) throws CmsCategoryException;

    CmsCategory update(CmsCategoryUpdater updater) throws CmsCategoryException;

    QResponse<CmsCategory> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws CmsCategoryException;

    List<CmsCategory> listByParent(Long parentId);

    List<CmsCategory> listAllByParent(Long parentId, List<CmsCategory> all);

    /**
     * @return 以指定编码为顶点的树
     */
    List<CmsCategory> findTopTree(Long id);

    /**
     * @return 以顶层机构为顶点的树
     */
    List<CmsCategory> findTopTree();

    CmsCategory getCategoryByCode(String code);
}
