package cn.turboinfo.fuyang.api.provider.common.repository.database.policy.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: sunshow.
 */
@Converter(autoApply = true)
public class PolicyTypeConverter extends BaseEnumConverter<PolicyType> {
}
