package cn.turboinfo.fuyang.api.provider.common.repository.database.contract;

import cn.turboinfo.fuyang.api.entity.common.enumeration.contract.ContractStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 合同管理
 * author: hai
 */
@Table(
        name = "contract"
)
@EqualsAndHashCode(
        callSuper = true
)
@Where(clause = "deleted = 0")
@Data
@Entity
public class ContractPO extends SoftDeleteFYEntity {

    /**
     * 合同编码
     */
    @Column(
            name = "contract_no"
    )
    private String contractNo;

    /**
     * 订单编码
     */
    @Column(
            name = "order_id"
    )
    private Long orderId;

    /**
     * 企业编码
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 合同模板编码
     */
    @Column(
            name = "template_id"
    )
    private Long templateId;

    /**
     * 合同状态
     */

    @Column(
            name = "status"
    )
    private ContractStatus status;
}
