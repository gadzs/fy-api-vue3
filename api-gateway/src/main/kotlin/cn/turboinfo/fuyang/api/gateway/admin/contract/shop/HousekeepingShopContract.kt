package cn.turboinfo.fuyang.api.gateway.admin.contract.shop

import cn.turboinfo.fuyang.api.domain.admin.usecase.shop.*
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

/**
 * @author gadzs
 * @description 企业门店自动映射
 * @date 2023/1/29 16:21
 */
@AutoMappingContract(paths = ["/admin/shop"])
@ResponseBodyWrapper
interface HousekeepingShopContract {

    /**
     * 根据企业name获取列表
     */
    @CompanyScope
    @PermissionScope(value = ["shop:list"])
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = HousekeepingShopListUseCase::class,
        consumes = []
    )
    fun listByCompany()

    // 家政门店添加
    @CompanyScope
    @PermissionScope(value = ["shop:create"])
    @AutoMappingContract(
        beanType = HousekeepingShopCreateUseCase::class
    )
    fun create()

    // 家政门店信息修改
    @CompanyScope
    @PermissionScope(value = ["shop:update"])
    @AutoMappingContract(
        beanType = HousekeepingShopUpdateUseCase::class
    )
    fun update()

    /**
     * 家政门店删除
     */
    @CompanyScope
    @PermissionScope(value = ["shop:delete"])
    @AutoMappingContract(
        beanType = HousekeepingShopDeleteUseCase::class
    )
    fun delete()

    /**
     * 家政门店详情
     */
    @CompanyScope
    @PermissionScope(value = ["shop:list", "adminAudit:shopList"])
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = HousekeepingShopViewUseCase::class,
        consumes = [],
        paths = ["/view/{id}"]
    )
    fun view()
}
