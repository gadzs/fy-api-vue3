package cn.turboinfo.fuyang.api.domain.admin.service.cms;


import cn.turboinfo.fuyang.api.entity.admin.exception.cms.CmsMessageBoardException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoard;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsMessageBoardUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CmsMessageBoardService {
    Optional<CmsMessageBoard> getById(Long id);

    CmsMessageBoard getByIdEnsure(Long id);

    List<CmsMessageBoard> findByIdCollection(Collection<Long> idCollection);

    CmsMessageBoard save(CmsMessageBoardCreator creator) throws CmsMessageBoardException;

    CmsMessageBoard update(CmsMessageBoardUpdater updater) throws CmsMessageBoardException;

    QResponse<CmsMessageBoard> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws CmsMessageBoardException;
}
