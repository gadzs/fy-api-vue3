package cn.turboinfo.fuyang.api.domain.mini.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.component.credit.CreditRatingAssembleHelper;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.ServiceOrderCreditRating;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Collection;
import java.util.List;

/**
 * 查看公司详情
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniViewCompanyUseCase extends AbstractUseCase<MiniViewCompanyUseCase.InputData, MiniViewCompanyUseCase.OutputData> {

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final CreditRatingAssembleHelper creditRatingAssembleHelper;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long companyId = inputData.getCompanyId();
        val company = housekeepingCompanyService.getByIdEnsure(companyId);

        // 查询最近的五条评价
        Collection<ServiceOrderCreditRating> ratingCollection = creditRatingAssembleHelper.findLatestPageable(
                EntityObjectType.COMPANY, companyId, 0, 5, CreditRatingAssembleHelper.ASSEMBLE_USER).getPagedData();

        return OutputData.builder()
                .company(company)
                .creditRatingList(List.copyOf(ratingCollection))
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "公司ID不能为空"
        )
        @Positive
        private Long companyId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingCompany company;

        private List<ServiceOrderCreditRating> creditRatingList;

    }
}
