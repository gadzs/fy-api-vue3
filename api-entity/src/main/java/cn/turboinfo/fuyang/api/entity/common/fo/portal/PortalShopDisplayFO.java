package cn.turboinfo.fuyang.api.entity.common.fo.portal;

import com.google.common.collect.Lists;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author gadzs
 * @description
 * @date 2023/3/8 15:59
 */
@Data
public class PortalShopDisplayFO {

    private String name;
    private String address;
    private String companyName;
    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 成立日期
     */
    private LocalDate foundDate;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 店铺图片
     */
    private List<Long> imgList;

    public List<Long> getImgList() {
        if (imgList == null) {
            imgList = Lists.newArrayList();
        }
        return imgList;
    }

}
