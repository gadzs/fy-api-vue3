package cn.turboinfo.fuyang.api.provider.common.service.impl.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityApplyService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityApply;
import cn.turboinfo.fuyang.api.provider.common.repository.database.activity.ActivityApplyDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.activity.ActivityApplyPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ActivityApplyServiceImpl extends DefaultQServiceImpl<ActivityApply, Long, ActivityApplyPO, ActivityApplyDAO> implements ActivityApplyService {
    @Override
    public Optional<ActivityApply> findByActivityIdAndUserId(Long activityId, Long userId, UserType userType) {
        return dao.findByActivityIdAndUserIdAndUserType(activityId, userId, userType).map(this::convertQBean);
    }

    @Override
    public List<ActivityApply> findByActivityId(Long activityId) {
        return convertQBeanToList(dao.findByActivityIdOrderByCreatedTimeDesc(activityId));
    }
}
