package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class SysUserException extends RuntimeException {
    public SysUserException() {
        this("操作 SysUser 出错");
    }

    public SysUserException(String message) {
        super(message);
    }

    public SysUserException(Throwable cause) {
        super(cause);
    }

    public SysUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
