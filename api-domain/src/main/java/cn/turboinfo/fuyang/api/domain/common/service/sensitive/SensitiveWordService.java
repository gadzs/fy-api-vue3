package cn.turboinfo.fuyang.api.domain.common.service.sensitive;

import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 敏感词管理服务
 *
 * @author hai
 */
public interface SensitiveWordService extends BaseQService<SensitiveWord, Long> {

    List<SensitiveWord> findAll();

    /**
     * 是否包含敏感词
     *
     * @param text 文本
     * @return 是否包含
     */
    boolean containsDenyWords(String text);

    /**
     * 刷新
     */
    void refresh();
}
