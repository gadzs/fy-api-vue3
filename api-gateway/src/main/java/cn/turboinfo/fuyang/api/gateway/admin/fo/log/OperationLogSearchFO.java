package cn.turboinfo.fuyang.api.gateway.admin.fo.log;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class OperationLogSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "系统用户ID"
    )
    private Long sysUserId;

    @QField(
            placeholder = "访问地址",
            operator = Operator.LIKE
    )
    private String url;

    @QField(
            placeholder = "请求参数",
            operator = Operator.LIKE
    )
    private String params;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
