package cn.turboinfo.fuyang.api.provider.common.repository.database.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.WithoutIdFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 分账管理
 * author: hai
 */
@Table(
        name = "profit_sharing"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
public class ProfitSharingPO extends WithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 订单编码
     */
    private Long objectId;

    private EntityObjectType objectType;

    /**
     * 企业ID
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 微信支付订单号
     */
    @Column(
            name = "wx_transaction_id"
    )
    private String wxTransactionId;

    /**
     * 微信分账单号
     */
    @Column(
            name = "wx_order_id"
    )
    private String wxOrderId;

    /**
     * 分账状态
     */
    private ProfitSharingStatus status;
}
