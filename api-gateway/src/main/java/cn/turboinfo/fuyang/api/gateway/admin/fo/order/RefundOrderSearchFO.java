package cn.turboinfo.fuyang.api.gateway.admin.fo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.FundsAccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundChannelType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class RefundOrderSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "微信支付订单号",
            operator = Operator.LIKE
    )
    private String wxTransactionId;

    @QField(
            placeholder = "微信退款单号",
            operator = Operator.LIKE
    )
    private String wxRefundId;

    @QField(
            placeholder = "订单编码"
    )
    private Long OrderId;

    @QField(
            placeholder = "支付订单编码"
    )
    private Long payOrderId;

    @QField(
            placeholder = "退款原因",
            operator = Operator.LIKE
    )
    private String reason;

    @QField(
            placeholder = "退款金额（单位分）"
    )
    private Long refundAmount;

    @QField(
            placeholder = "订单金额（单位分）"
    )
    private Long orderAmount;

    @QField(
            placeholder = "用户支付金额"
    )
    private Long userPayAmount;

    @QField(
            placeholder = "用户退款金额"
    )
    private Long userRefundAmount;

    @QField(
            placeholder = "退款渠道",
            control = Control.SELECT,
            ref = "refundChannelTypeList",
            template = "enum"
    )
    private RefundChannelType refundChannel;

    @QField(
            placeholder = "退款入账账户",
            operator = Operator.LIKE
    )
    private String userReceivedAccount;

    @QField(
            placeholder = "退款成功时间",
            operator = Operator.LIKE
    )
    private String successTime;

    @QField(
            placeholder = "退款状态",
            control = Control.SELECT,
            ref = "refundStatusList",
            template = "enum"
    )
    private RefundStatus status;

    @QField(
            placeholder = "退款资金账户",
            control = Control.SELECT,
            ref = "fundsAccountTypeList",
            template = "enum"
    )
    private FundsAccountType fundsAccount;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
    
    @QField(
            searchable = false
    )
    private Long companyId;

    @QField(
            searchable = false
    )
    private String companyName;
}
