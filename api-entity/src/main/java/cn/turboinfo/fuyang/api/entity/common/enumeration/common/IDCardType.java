package cn.turboinfo.fuyang.api.entity.common.enumeration.common;

import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

public enum IDCardType implements BaseEnum {
    ALL(-1, "全部"),

    DEFAULT(0, "身份证"),

    PASSPORT(30, "护照"),

    ;

    private int value;
    private String name;

    IDCardType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static IDCardType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<IDCardType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<IDCardType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
