package cn.turboinfo.fuyang.api.gateway.admin.fo.contract;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;

import java.time.LocalDateTime;

@Data
@QField
public class ContractSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "订单编码"
    )
    private Long orderId;

    @QField(
            placeholder = "企业编码"
    )
    private Long companyId;

    @QField(
            placeholder = "合同模板编码"
    )
    private Long templateId;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
