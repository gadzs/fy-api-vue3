package cn.turboinfo.fuyang.api.entity.common.event.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.event.AbstractEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 服务订单状态变更事件
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceOrderStatusChangedEvent extends AbstractEvent {

    private Long serviceOrderId;

    private ServiceOrderStatus status;

    private ServiceOrderStatus fromStatus;
    
}
