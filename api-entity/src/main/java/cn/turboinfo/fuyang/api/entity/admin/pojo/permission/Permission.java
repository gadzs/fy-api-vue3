package cn.turboinfo.fuyang.api.entity.admin.pojo.permission;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class Permission extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    private String name;

    private String description;

    private String resource;

    private String url;

    private String component;

    private String icon;

    private YesNoStatus visibleStatus;

    /**
     * 特定角色可分配
     */
    private Long roleId;

    private Long parentId;

    private Integer sortValue;

    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
