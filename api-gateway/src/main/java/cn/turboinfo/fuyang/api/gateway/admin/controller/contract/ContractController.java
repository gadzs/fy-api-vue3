package cn.turboinfo.fuyang.api.gateway.admin.controller.contract;

import cn.turboinfo.fuyang.api.domain.admin.usecase.contract.ContractSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.fo.contract.ContractFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.QContract;
import cn.turboinfo.fuyang.api.gateway.admin.fo.contract.ContractSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/contract")
public class ContractController {
    private final ContractSearchUseCase contractSearchUseCase;

    @RequiresPermissions("contract:list")
    @CompanyScope
    @RequestMapping("/search")
    public LimitDataFO<ContractFO> list(@Valid ContractSearchFO search, LimitFO page, SortFO sort, AdminRequestContext context) {

        if (search.getCompanyId() == null) {
            search.setCompanyId(context.getCompanyId());
        }

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QContract.id + "|" + QSort.Order.DESC.name()});
        }

        ContractSearchUseCase.OutputData outputData = contractSearchUseCase.execute(ContractSearchUseCase.InputData
                .builder()
                .userId(context.getUserId())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }
}
