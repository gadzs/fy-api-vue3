package cn.turboinfo.fuyang.api.gateway.admin.fo.category;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class CategorySearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "名称",
            operator = Operator.LIKE
    )
    private String name;

    @QField(
            placeholder = "显示名称",
            operator = Operator.LIKE
    )
    private String displayName;

    @QField(
            placeholder = "描述信息",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            placeholder = "父级 ID"
    )
    private Long parentId;

    @QField(
            placeholder = "排序值"
    )
    private Integer sortValue;

    @QField(
            placeholder = "Children"
    )
    private List children;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
