package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 留言板
 */
@Table(
        name = "cms_message_board"
)
@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
public class CmsMessageBoardPO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 留言用户
     */
    @Column(
            name = "user_id"
    )
    private Long userId;

    private String username;

    /**
     * 标题
     */
    private String title;

    /**
     * 留言内容
     */
    private String content;

    private String mobile;

    private String email;


    /**
     * 类型
     */
    private CmsMessageBoardType type;

    /**
     * 业务子类型
     */
    @Column(
            name = "busi_type"
    )
    private String busiType;

    /**
     * 是否展示
     */
    @Column(
            name = "is_show"
    )
    private YesNoStatus isShow;

    /**
     * 回复用户
     */
    @Column(
            name = "reply_user_id"
    )
    private Long replyUserId;

    @Column(
            name = "reply_username"
    )
    private String replyUsername;

    /**
     * 回复内容
     */
    @Column(
            name = "reply_content"
    )
    private String replyContent;

    /**
     * 回复时间
     */
    @Column(
            name = "reply_time"
    )
    private LocalDateTime replyTime;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 意见征集id
     */
    @Column(
            name = "advice_id"
    )
    private Long adviceId;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
