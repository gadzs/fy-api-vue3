package cn.turboinfo.fuyang.api.gateway.admin.controller.upload;

import cn.turboinfo.fuyang.api.domain.web.usecase.file.UploadTinymceFileAttachmentUseCase;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.provider.admin.component.config.AdminKitConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/upload")
public class AdminUploadController {
    private final UploadTinymceFileAttachmentUseCase uploadTinymceFileAttachmentUseCase;

    /**
     * 允许上传的附件类型
     */
    private final AdminKitConfig adminKitConfig;

    @RequiresPermissions("upload:tinymce")
    @PostMapping("/tinymce/image")
    public RestResponseFO list(MultipartFile file) {

        UploadTinymceFileAttachmentUseCase.OutputData outputData = uploadTinymceFileAttachmentUseCase.execute(UploadTinymceFileAttachmentUseCase.InputData
                .builder()
                .allowExts(StringUtils.split(adminKitConfig.getAttachmentAllowExt(), "|"))
                .file(file)
                .relativePath("/tinymce/image")
                .refType("tinymceImg")
                .build());

        return RestResponseFO.ok(outputData.getAttachment());
    }
}
