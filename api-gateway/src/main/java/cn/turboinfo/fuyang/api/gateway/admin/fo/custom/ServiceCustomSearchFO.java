package cn.turboinfo.fuyang.api.gateway.admin.fo.custom;

import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@QField
public class ServiceCustomSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "用户编码"
    )
    private Long userId;

    @QField(
            placeholder = "服务类型"
    )
    private Long categoryId;

    @QField(
            placeholder = "服务地址编码"
    )
    private Long addressId;

    @QField(
            placeholder = "人数"
    )
    private Integer peopleNum;

    @QField(
            placeholder = "预算"
    )
    private BigDecimal budget;

    @QField(
            placeholder = "描述",
            operator = Operator.LIKE
    )
    private String description;

    @QField(
            searchable = false
    )
    private String status;

    @QField(
            placeholder = "公司编码"
    )
    private Long companyId;

    @QField(
            placeholder = "计划开始时间"
    )
    private LocalDateTime scheduledStartTime;

    @QField(
            placeholder = "计划结束时间"
    )
    private LocalDateTime scheduledEndTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
