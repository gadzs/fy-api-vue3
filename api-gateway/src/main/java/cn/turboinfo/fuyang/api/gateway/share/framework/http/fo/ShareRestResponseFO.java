package cn.turboinfo.fuyang.api.gateway.share.framework.http.fo;

import cn.turboinfo.fuyang.api.gateway.share.constant.ShareRestConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShareRestResponseFO {

    private int code;

    private String message;

    private Object data;

    public static ShareRestResponseFO ok() {
        return ok("操作成功", null);
    }

    public static <T> ShareRestResponseFO ok(T data) {
        return ok("操作成功", data);
    }

    public static <T> ShareRestResponseFO ok(String message, T data) {
        return new ShareRestResponseFO(ShareRestConstants.RC_SUCCESS, message, data);
    }

    public static ShareRestResponseFO error(String message) {
        return error(ShareRestConstants.RC_ERROR_DEFAULT, message);
    }

    public static ShareRestResponseFO error(int code, String message) {
        return new ShareRestResponseFO(code, message, null);
    }

    public static ShareRestResponseFO error(int code, String message, Object data) {
        return new ShareRestResponseFO(code, message, data);
    }

}
