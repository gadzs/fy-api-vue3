package cn.turboinfo.fuyang.api.provider.common.component.custom.listener

import cn.turboinfo.fuyang.api.entity.common.event.WrapperApplicationEvent
import cn.turboinfo.fuyang.api.entity.common.event.custom.NewServiceCustomEvent
import cn.turboinfo.fuyang.api.provider.common.repository.database.custom.ServiceCustomPO
import nxcloud.foundation.core.data.support.listener.EntityLifecycleListener
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronization
import org.springframework.transaction.support.TransactionSynchronizationManager

@Component
class ServiceCustomPublishEventListener(
    private val applicationContext: ApplicationContext,
) : EntityLifecycleListener {

    override fun onPostPersist(entity: Any) {
        (entity as ServiceCustomPO)
            .let {
                TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronization {
                    override fun afterCommit() {
                        applicationContext.publishEvent(
                            WrapperApplicationEvent(
                                NewServiceCustomEvent.builder()
                                    .serviceCustomId(entity.id)
                                    .build()
                            )
                        )
                    }
                })
            }
    }

}
