package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;

public interface ProductDAO extends FYRepository<ProductPO, Long> {

    List<ProductPO> findByShopIdInAndStatus(Collection<Long> shopIdCollection, ProductStatus productStatus);

    List<ProductPO> findByCompanyIdAndStatusOrderByUpdatedTimeDesc(Long companyId, ProductStatus productStatus);

    List<ProductPO> findByCategoryIdAndStatusOrderByUpdatedTimeDesc(Long categoryId, ProductStatus productStatus);

    long countByCompanyIdAndStatus(Long company, ProductStatus status);

    List<ProductPO> findByNameContaining(String name);

}
