package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class UserTypeRelException extends RuntimeException {
    public UserTypeRelException() {
        this("操作用户类型关联出错");
    }

    public UserTypeRelException(String message) {
        super(message);
    }

    public UserTypeRelException(Throwable cause) {
        super(cause);
    }

    public UserTypeRelException(String message, Throwable cause) {
        super(message, cause);
    }
}
