package cn.turboinfo.fuyang.api.gateway.mini.constant;

public final class MiniRestConstants {

    public static final String HEADER_SESSION_TOKEN = "X-MINI-SESSION-TOKEN";

    public static final String HEADER_USER_TYPE = "X-MINI-USER-TYPE";

    public static final String HEADER_USER_TYPE_OBJECT = "X-MINI-USER-TYPE-OBJECT";

    public static final String HEADER_USER_TYPE_REFERENCE = "X-MINI-USER-TYPE-REFERENCE";

    public static final int RC_SUCCESS = 0;

    public static final int RC_ERROR_DEFAULT = -1;

    public static final int RC_ERROR_BAD_REQUEST = 40000;

    public static final int RC_ERROR_UNAUTHENTICATED = 40100;

    public static final int RC_ERROR_UNAUTHORIZED = 40300;

}
