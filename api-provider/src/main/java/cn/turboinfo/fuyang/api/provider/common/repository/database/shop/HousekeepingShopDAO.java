package cn.turboinfo.fuyang.api.provider.common.repository.database.shop;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;
import java.util.Optional;

public interface HousekeepingShopDAO extends FYRepository<HousekeepingShopPO, Long> {

    Optional<HousekeepingShopPO> findByCompanyIdAndName(Long companyId, String name);

    List<HousekeepingShopPO> findByCompanyId(Long companyId);

    List<HousekeepingShopPO> findByCityCode(Long cityCode);

    List<HousekeepingShopPO> findByAreaCode(Long areaCode);

    List<HousekeepingShopPO> findByNameContaining(String name);

    long countByCompanyIdAndStatus(Long companyId, ShopStatus status);

}
