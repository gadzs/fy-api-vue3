package cn.turboinfo.fuyang.api.gateway.admin.fo.knowledge;

import java.time.LocalDateTime;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

@Data
@QField
public class KnowledgeBaseSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "知识类型",
            operator = Operator.LIKE
    )
    private String type;

    @QField(
            placeholder = "问题",
            operator = Operator.LIKE
    )
    private String question;

    @QField(
            placeholder = "答案",
            operator = Operator.LIKE
    )
    private String answer;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
