package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CmsCategoryDisplayFO {

    private CmsCategory category;

    private CmsCategory parent;

}
