package cn.turboinfo.fuyang.api.provider.common.repository.database.knowledge;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface KnowledgeBaseDAO extends FYRepository<KnowledgeBasePO, Long> {

    List<KnowledgeBasePO> findByTypeOrderByIdDesc(String type);

}
