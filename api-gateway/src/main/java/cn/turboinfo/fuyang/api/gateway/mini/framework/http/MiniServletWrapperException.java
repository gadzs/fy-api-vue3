package cn.turboinfo.fuyang.api.gateway.mini.framework.http;

public class MiniServletWrapperException extends RuntimeException {

    public MiniServletWrapperException(Throwable cause) {
        this("小程序请求异常", cause);
    }

    public MiniServletWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

}
