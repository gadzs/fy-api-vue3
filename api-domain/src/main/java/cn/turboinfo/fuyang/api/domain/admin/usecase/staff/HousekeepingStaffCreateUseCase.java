package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.IdCardUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.staff.StaffStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.staff.HousekeepingStaffException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecordCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaffCreator;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政企业创建usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffCreateUseCase extends AbstractUseCase<HousekeepingStaffCreateUseCase.InputData, HousekeepingStaffCreateUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final FileAttachmentService fileAttachmentService;
    private final HousekeepingStaffAuditRecordService housekeepingStaffAuditRecordService;
    private final ConfidenceCodeService confidenceCodeService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getName())) {
            throw new HousekeepingStaffException("姓名包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getDisplayName())) {
            throw new HousekeepingStaffException("展示名称包含敏感词汇，请修改后重新提交！");
        }

        // 检查家政员介绍是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getIntroduction())) {
            throw new HousekeepingStaffException("家政员介绍包含敏感词汇，请修改后重新提交");
        }

        String contactMobile = inputData.getContactMobile();

        Optional<HousekeepingStaff> housekeepingStaffOptional1 = housekeepingStaffService.findByIdCard(inputData.getIdCard());
        if (housekeepingStaffOptional1.isPresent()) {
            throw new HousekeepingStaffException("家政员身份证号已存在");
        }
        Optional<HousekeepingStaff> housekeepingStaffOptional2 = housekeepingStaffService.findByMobile(contactMobile);
        if (housekeepingStaffOptional2.isPresent()) {
            throw new HousekeepingStaffException("家政员手机号已存在");
        }

        inputData.setGender(IdCardUtils.judgeGender(inputData.getIdCard()));
        inputData.setProvinceCode(IdCardUtils.getProvinceCode(inputData.getIdCard()));
        HousekeepingStaffCreator.Builder builder = HousekeepingStaffCreator.builder();
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, HousekeepingStaffCreator.class, inputData);

        // 生成放心码
        ConfidenceCode confidenceCode = confidenceCodeService.tagging();

        HousekeepingStaff housekeepingStaff = housekeepingStaffService.save(builder
                .withCode(confidenceCode.getId().toString())
                .withStatus(StaffStatus.DEFAULT)
                .build());

        HousekeepingStaffAuditRecordCreator housekeepingStaffAuditRecordCreator = HousekeepingStaffAuditRecordCreator.builder()
                .withCompanyId(housekeepingStaff.getCompanyId())
                .withStaffId(housekeepingStaff.getId())
                .withAuditStatus(AuditStatus.INIT)
                .withUserId(0L)
                .withUserName(StringUtils.EMPTY)
                .withRemark("新增家政员")
                .build();
        housekeepingStaffAuditRecordService.save(housekeepingStaffAuditRecordCreator);

        //保存附件关系
        fileAttachmentService.updateRefId(Lists.newArrayList(inputData.getIdCardFile(), inputData.getPhotoFile()), housekeepingStaff.getId());

        return OutputData.builder()
                .housekeepingStaff(housekeepingStaff)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "公司编码不能为空"
        )
        private Long companyId;

        @NotBlank(
                message = "家政员名称不能为空"
        )
        private String name;

        @NotBlank(
                message = "展示名称不能为空"
        )
        private String displayName;

        @NotBlank(
                message = "身份证号不能为空"
        )
        private String idCard;

        @NotNull(
                message = "身份证附件不能为空"
        )
        private Long idCardFile;

        @NotNull(
                message = "个人照片不能为空"
        )
        private Long photoFile;

        @NotBlank(
                message = "联系电话不能为空"
        )
        private String contactMobile;

        @NotBlank(
                message = "个人介绍不能为空"
        )
        private String introduction;

        /**
         * 性别
         */
        private GenderType gender;

        @NotNull(
                message = "参加工作时间不能为空"
        )
        private LocalDate employmentDate;

        @NotNull(
                message = "参加工作时间不能为空"
        )
        private Integer seniority;

        /**
         * 家政员籍贯
         */
        private Long provinceCode;

        @NotBlank(
                message = "家政员类型不能为空"
        )
        private String staffType;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingStaff housekeepingStaff;
    }

}
