package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;

public interface SpecDAO extends FYRepository<SpecPO, Long> {

    List<SpecPO> findByParentId(Long parentId);

    List<SpecPO> findByParentIdAndNameOrderByIdDesc(Long parentId, String name);

    List<SpecPO> findAllBySpecSetIdInOrderBySortValueDescIdAsc(Collection<Long> setIdCollection);

}
