package cn.turboinfo.fuyang.api.gateway.mini.contract.credit

import cn.turboinfo.fuyang.api.domain.mini.usecase.credit.MiniConsumerSubmitCreditRatingUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.ConsumerScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@ConsumerScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/consumer/credit"])
interface MiniConsumerCreditRatingContract {

    // 提交订单评价
    @AutoMappingContract(
        beanType = MiniConsumerSubmitCreditRatingUseCase::class,
    )
    fun submit()

}