package cn.turboinfo.fuyang.api.gateway.admin.fo.portal;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 文章列表展示FO
 */
@Data
public class PortalArticleDisplayFO {
    private Long id;

    /**
     * 分类
     */
    private Long categoryId;
    private String categoryName;

    /**
     * 文章类型
     */
    private CmsArticleType type;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subtitle;

    /**
     * 作者
     */
    private String author;

    /**
     * 公司
     */
    private String company;

    /**
     * 链接
     */
    private String linkedUrl;

    /**
     * 标题图片
     */
    private Long titleImageId;

    /**
     * 附件id
     */
    private Long attachId;

    /**
     * 摘要
     */
    private String abstractContent;

    /**
     * 发布时间
     */
    private LocalDateTime publishedTime;

    /**
     * 访问次数
     */
    private Long visitCount;
}
