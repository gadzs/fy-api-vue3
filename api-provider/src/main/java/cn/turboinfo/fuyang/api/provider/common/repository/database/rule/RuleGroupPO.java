package cn.turboinfo.fuyang.api.provider.common.repository.database.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.RuleControlType;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleItem;
import io.hypersistence.utils.hibernate.type.json.JsonStringType;
import lombok.Data;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 规则组
 * author: sunshow.
 */
@Table(
        name = "rule_group"
)
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Entity
@Where(clause = "deleted_time = 0")
@DynamicInsert
@DynamicUpdate
@Data
public class RuleGroupPO implements BaseEntity {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 规则组名称
     */
    private String name;

    /**
     * 规则组描述
     */
    private String description;

    /**
     * 规则编码
     */
    @Column(name = "rule_key")
    private String ruleKey;

    /**
     * 规则控制类型
     */
    @Column(name = "control_type")
    private RuleControlType controlType;

    private EnableStatus status;

    /**
     * 规则明细项
     */
    @Type(type = "json")
    @Column(name = "item_list", columnDefinition = "json")
    private List<RuleItem> itemList;

    @Column(name = "deleted_time")
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }

}
