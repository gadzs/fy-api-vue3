package cn.turboinfo.fuyang.api.provider.rabbit.constant

object RabbitConstants {

    private const val ExchangePrefix = "event."

    const val ExchangeEvent = ExchangePrefix + "exchange"

    const val RoutingKeyEventPrefix = "event."

    const val QueueEventPrefix = "event."

}