package cn.turboinfo.fuyang.api.gateway.admin.fo.role;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleAssignPermissionFO {

    @NotNull(message = "角色不能为空")
    private Long roleId;

    private Long[] checkedPermissionIds;

    private Long[] halfCheckedPermissionIds;

}
