package cn.turboinfo.fuyang.api.provider.common.repository.database.stat;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 订单统计
 * author: hai
 */
@Table(
        name = "order_stat"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
public class OrderStatPO extends FYEntity {
    /**
     * 企业编码
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 分类编码
     */
    @Column(
            name = "category_id"
    )
    private Long categoryId;

    /**
     * 年份
     */
    private Integer year;

    /**
     * 月份
     */
    private Integer month;

    /**
     * 订单数量
     */
    @Column(
            name = "order_count"
    )
    private Long orderCount;
}
