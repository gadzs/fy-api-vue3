package cn.turboinfo.fuyang.api.domain.common.service.user;

import cn.turboinfo.fuyang.api.entity.common.exception.user.SysUserException;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUserCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUserUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface SysUserService extends BaseQService<SysUser, Long> {
    Optional<SysUser> getById(Long id);

    SysUser getByIdEnsure(Long id);

    Optional<SysUser> getByUsername(String username);

    Optional<SysUser> getByMobile(String mobile);

    SysUser save(SysUserCreator creator, String password) throws SysUserException;

    SysUser save(SysUserCreator creator, String password, Long agenciesId) throws SysUserException;

    SysUser update(SysUserUpdater updater) throws SysUserException;

    SysUser update(SysUserUpdater updater, Long agenciesId) throws SysUserException;

    QResponse<SysUser> findAll(QRequest request, QPage requestPage);

    void modifyPassword(Long userId, String originalPassword, String newPassword) throws SysUserException;

    void resetPassword(Long userId, String newPassword) throws SysUserException;

    void enable(Long userId) throws SysUserException;

    void disable(Long userId) throws SysUserException;

    void deleteById(Long userId) throws SysUserException;

    void deleteDisabled(Long userId) throws SysUserException;

    void enableAndAssignRole(Long userId, String roleCode) throws SysUserException;

    List<SysUser> findByIdCollection(Collection<Long> idCollection);
}
