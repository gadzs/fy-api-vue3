package cn.turboinfo.fuyang.api.entity.common.pojo.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 服务订单状态记录
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceCustomStatusLog {

    /**
     * 服务订单状态
     */
    private ServiceCustomStatus currentStatus;

    /**
     * 变更之前的服务订单状态
     */
    private ServiceCustomStatus previousStatus;

    /**
     * 变更之前的公司ID
     */
    private Long companyId;

    /**
     * 变更之前的服务人员ID
     */
    private Long staffId;

    /**
     * 服务订单状态变更时间
     */
    private LocalDateTime updatedTime;

}
