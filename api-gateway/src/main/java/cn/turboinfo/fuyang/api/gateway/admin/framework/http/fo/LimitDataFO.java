package cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * author: sunshow.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LimitDataFO<T> {

    private Collection<T> items;

    private long total;

    private int totalPage;

    public static <E> LimitDataFO<E> fromQResponse(QResponse<E> qresponse) {
        // 下标从1开始 所以要+1
        return new LimitDataFO<>(qresponse.getPagedData(), qresponse.getTotal(), qresponse.getPageTotal());
    }

    public static <E, D> LimitDataFO<D> fromQResponse(QResponse<E> qresponse, Function<E, D> mapper) {
        // 下标从1开始 所以要+1
        return new LimitDataFO<>(qresponse.getPagedData().stream().map(mapper).collect(Collectors.toList()), qresponse.getTotal(), qresponse.getPageTotal());
    }
}
