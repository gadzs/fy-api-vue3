package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author gadzs
 * @description 家政企业获取usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyViewUseCase extends AbstractUseCase<HousekeepingCompanyViewUseCase.InputData, HousekeepingCompanyViewUseCase.OutputData> {
    private final HousekeepingCompanyService housekeepingCompanyService;
    private final FileAttachmentService fileAttachmentService;
    private final CompanyAccountService companyAccountService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Optional<HousekeepingCompany> housekeepingCompanyOptional = housekeepingCompanyService.getById(inputData.getCompanyId());
        HousekeepingCompany housekeepingCompany = housekeepingCompanyOptional.orElse(null);
        List<CompanyAccount> companyAccountList = new ArrayList<>();
        if (housekeepingCompany != null) {
            // 不返回 appId、 appSecret
//            housekeepingCompany.setAppId(null);
            housekeepingCompany.setAppSecret(null);
            val companyAttach = fileAttachmentService.findByRefIdAndRefType(housekeepingCompany.getId(), FileRefTypeConstant.COMPANY_ATTACH);
            companyAttach.forEach(fileAttachment -> {
                if (fileAttachment.getId().equals(housekeepingCompany.getBusinessLicenseFile())) {
                    housekeepingCompany.setBusinessLicenseFileName(fileAttachment.getDisplayName());
                } else if (fileAttachment.getId().equals(housekeepingCompany.getLegalPersonIdCardFile())) {
                    housekeepingCompany.setLegalPersonIdCardFileName(fileAttachment.getDisplayName());
                } else if (fileAttachment.getId().equals(housekeepingCompany.getBankAccountCertificateFile())) {
                    housekeepingCompany.setBankAccountCertificateFileName(fileAttachment.getDisplayName());
                }
            });
            companyAccountList = companyAccountService.findByCompanyId(housekeepingCompany.getId());
        }

        return OutputData.builder()
                .housekeepingCompany(housekeepingCompany)
                .companyAccountList(companyAccountList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "companyId不能为空"
        )
        private Long companyId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingCompany housekeepingCompany;

        private List<CompanyAccount> companyAccountList;
    }

}
