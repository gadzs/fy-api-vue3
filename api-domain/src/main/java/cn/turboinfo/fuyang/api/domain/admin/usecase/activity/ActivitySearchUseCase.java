package cn.turboinfo.fuyang.api.domain.admin.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileRefTypeConstant;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.QActivity;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class ActivitySearchUseCase extends AbstractUseCase<ActivitySearchUseCase.InputData, ActivitySearchUseCase.OutputData> {
    private final ActivityService activityService;
    private final FileAttachmentService fileAttachmentService;
    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long companyId = inputData.getCompanyId();
        QRequest request = inputData.getRequest();
        if (companyId != null) {
            request.filterEqual(QActivity.companyId, companyId);
        }

        QResponse<Activity> response = activityService.findAll(request, inputData.getRequestPage());

        Set<Long> activityIdSet = response.getPagedData().stream()
                .map(Activity::getId)
                .collect(Collectors.toSet());

        Map<Long, List<FileAttachment>> fileMap = fileAttachmentService.findByRefIdInAndRefType(activityIdSet, FileRefTypeConstant.ACTIVITY_IMG)
                .stream()
                .collect(Collectors.groupingBy(FileAttachment::getRefId));

        // 查询企业
        Set<Long> companyIdSet = response.getPagedData().stream()
                .filter(it -> it.getCompanyId() != null && it.getCompanyId() > 0)
                .map(Activity::getCompanyId)
                .collect(Collectors.toSet());

        Map<Long, HousekeepingCompany> companyMap = housekeepingCompanyService.findByIdCollection(companyIdSet)
                .stream()
                .collect(Collectors.toMap(HousekeepingCompany::getId, it -> it));

        response.getPagedData().forEach(it -> {
            if (it.getCompanyId() != null && it.getCompanyId() > 0) {
                if (companyMap.containsKey(it.getCompanyId())) {
                    it.setCompanyName(companyMap.get(it.getCompanyId()).getName());
                }
            } else {
                it.setCompanyName("阜阳市商务局");
            }

            if (fileMap.get(it.getId()) == null) {
                return;
            }
            it.setImageIdList(
                    fileMap.get(it.getId())
                            .stream()
                            .map(FileAttachment::getId)
                            .collect(Collectors.toList())
            );
        });

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private Long companyId;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<Activity> qResponse;
    }
}
