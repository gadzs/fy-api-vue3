package cn.turboinfo.fuyang.api.entity.common.pojo.shop;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 家政门店
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class HousekeepingShop extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 公司编码
     */
    private Long companyId;

    private String companyName;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 成立日期
     */
    private LocalDate foundDate;

    /**
     * 省编码
     */
    private Long provinceCode;

    /**
     * 市编码
     */
    private Long cityCode;

    /**
     * 区编码
     */
    private Long areaCode;

    private String areaName;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系人电话
     */
    private String contactMobile;

    /**
     * 加密联系电话
     */
    private String contactMobileEncrypt;

    /**
     * 营业时间
     */
    private String businessTime;

    /**
     * 状态
     */
    private ShopStatus status;

    /**
     * 营业执照
     */
    private Long businessLicenseFile;

    private String businessLicenseFileName;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 店铺图片
     */
    private List<Long> imgList;

    public List<Long> getImgList() {
        if (imgList == null) {
            imgList = Lists.newArrayList();
        }
        return imgList;
    }

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
