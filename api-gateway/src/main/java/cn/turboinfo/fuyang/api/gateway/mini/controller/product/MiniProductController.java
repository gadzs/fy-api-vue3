package cn.turboinfo.fuyang.api.gateway.mini.controller.product;

import cn.turboinfo.fuyang.api.domain.mini.usecase.product.MiniListProductUserCase;
import cn.turboinfo.fuyang.api.domain.mini.usecase.product.MiniProductListCreditRatingUserCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSortType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.ServiceOrderCreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.QProduct;
import cn.turboinfo.fuyang.api.entity.mini.fo.product.MiniProduct;
import cn.turboinfo.fuyang.api.gateway.admin.fo.product.ProductSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @author gadzs
 * @description 产品列表
 * @date 2023/2/13 17:08
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/mini/product")
public class MiniProductController {

    private final MiniListProductUserCase miniListProductUserCase;

    private final MiniProductListCreditRatingUserCase miniProductListCreditRatingUserCase;

    @RequestMapping("/list")
    public LimitDataFO<MiniProduct> list(@Valid ProductSearchFO search, LimitFO page, SortFO sort) {
        search.setStatus(ProductStatus.PUBLISHED);
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (search.getSortType() == null) {
            sort.setSortFields(new String[]{QProduct.id + "|" + QSort.Order.DESC.name()});
        } else if (search.getSortType() == ProductSortType.CREDIT_SCORE_DESC) { // 信用分排序
            sort.setSortFields(new String[]{QProduct.creditScore + "|" + QSort.Order.DESC.name()});
        }
        MiniListProductUserCase.OutputData outputData = miniListProductUserCase.execute(MiniListProductUserCase.InputData
                .builder()
                .areaCode(search.getAreaCode())
                .categoryCode(search.getCategoryCode())
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequestMapping("/listCreditRating")
    public LimitDataFO<ServiceOrderCreditRating> listCreditRating(Long productId, LimitFO page) {
        MiniProductListCreditRatingUserCase.OutputData outputData = miniProductListCreditRatingUserCase.execute(MiniProductListCreditRatingUserCase.InputData
                .builder()
                .productId(productId)
                .requestPage(page.toQPage())
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }
}
