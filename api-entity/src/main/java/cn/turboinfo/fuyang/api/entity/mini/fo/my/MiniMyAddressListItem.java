package cn.turboinfo.fuyang.api.entity.mini.fo.my;

import cn.turboinfo.fuyang.api.entity.common.fo.address.ViewAddressFO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MiniMyAddressListItem {

    private ViewAddressFO address;

}
