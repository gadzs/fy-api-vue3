package cn.turboinfo.fuyang.api.provider.admin.framework.shiro.filter;

import cn.turboinfo.fuyang.api.entity.admin.exception.user.SysUserNotLoginException;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class RestAuthenticationFilter extends FormAuthenticationFilter {

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        throw new SysUserNotLoginException();
    }
}
