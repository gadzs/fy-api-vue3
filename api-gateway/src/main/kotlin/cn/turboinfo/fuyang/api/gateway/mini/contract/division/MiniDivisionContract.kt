package cn.turboinfo.fuyang.api.gateway.mini.contract.division

import cn.turboinfo.fuyang.api.domain.mini.usecase.division.MiniListDefaultDivisionUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/division"])
interface MiniDivisionContract {

    // 获取默认地区列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniListDefaultDivisionUseCase::class,
        consumes = []
    )
    fun defaultArea()
}
