package cn.turboinfo.fuyang.api.gateway.mini.contract.category

import cn.turboinfo.fuyang.api.domain.mini.usecase.category.MiniCategoryListUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/category"])
interface MiniCategoryContract {

    // 首页推荐服务分类列表
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniCategoryListUseCase::class,
        consumes = [],
    )
    fun list()

}