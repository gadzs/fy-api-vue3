package test.kotlin.lib

import org.junit.jupiter.api.Test

class BitwiseFlagTest {

    @Test
    fun testBitwise() {
        println(1 shl 0)
        println(1 shl 1)
        println(1 shl 2)
        println(1 shl 3)
    }

    @Test
    fun testFlag() {
        val flg1 = 1 shl 0
        val flg2 = 1 shl 1
        val flg3 = 1 shl 2
        val flg4 = 1 shl 3

        println(7 and flg1)
        println(7 and flg2)
        println(8 and flg3)
        println(7 and flg4)
    }
}