package cn.turboinfo.fuyang.api.domain.common.handler.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.RuleControlType;

public interface IRuleGroupFilter {

    RuleControlType getType();

}
