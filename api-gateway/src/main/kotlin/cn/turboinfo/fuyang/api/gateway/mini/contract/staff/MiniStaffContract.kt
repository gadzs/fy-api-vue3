package cn.turboinfo.fuyang.api.gateway.mini.contract.staff

import cn.turboinfo.fuyang.api.domain.mini.usecase.staff.MiniViewStaffByCodeUseCase
import cn.turboinfo.fuyang.api.domain.mini.usecase.staff.MiniViewStaffUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/staff"])
interface MiniStaffContract {

    // 家政服务人员详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewStaffUseCase::class,
        consumes = [],
        paths = ["/view/{staffId}"]
    )
    fun view()

    // 家政服务人员详情
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = MiniViewStaffByCodeUseCase::class,
        consumes = [],
        paths = ["/viewByCode/{code}"]
    )
    fun viewByCode()

}
