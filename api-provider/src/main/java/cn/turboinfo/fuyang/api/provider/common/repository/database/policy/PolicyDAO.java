package cn.turboinfo.fuyang.api.provider.common.repository.database.policy;

import cn.turboinfo.fuyang.api.entity.common.enumeration.policy.PolicyType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.List;

public interface PolicyDAO extends BaseRepository<PolicyPO, Long> {

    List<PolicyPO> findByPolicyTypeOrderByIdAsc(PolicyType policyType);

}
