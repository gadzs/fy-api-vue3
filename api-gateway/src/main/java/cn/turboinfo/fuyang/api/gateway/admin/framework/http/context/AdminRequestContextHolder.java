package cn.turboinfo.fuyang.api.gateway.admin.framework.http.context;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public final class AdminRequestContextHolder {

    private final static ThreadLocal<AdminRequestContext> contextThreadLocal = new ThreadLocal<>();

    public static AdminRequestContext current() {
        AdminRequestContext context = contextThreadLocal.get();
        if (context == null) {
            context = new AdminRequestContext();
            contextThreadLocal.set(context);
        }
        return context;
    }

    public static boolean exists() {
        return contextThreadLocal.get() != null;
    }

    public static void reset() {
        contextThreadLocal.remove();
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
    }

}
