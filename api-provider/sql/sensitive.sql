DROP TABLE IF EXISTS `sensitive_word`;
CREATE TABLE `sensitive_word`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `word`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '',
    `deleted`      BIGINT       NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_word` (`word`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='敏感词管理';
