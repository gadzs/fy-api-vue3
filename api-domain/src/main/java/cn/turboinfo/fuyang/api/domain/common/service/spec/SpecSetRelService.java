package cn.turboinfo.fuyang.api.domain.common.service.spec;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSetRel;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;
import java.util.Optional;

/**
 * @author gadzs
 * @description 规格组关联
 * @date 2023/2/6 11:48
 */
public interface SpecSetRelService extends BaseQService<SpecSetRel, Long> {

    List<SpecSetRel> findBySpecSetId(Long specSetId);

    /**
     * 根据关联id和关联类型查找
     *
     * @param objectId
     * @param objectType
     * @return
     */
    Optional<SpecSetRel> getByRelObject(Long objectId, EntityObjectType objectType);

}
