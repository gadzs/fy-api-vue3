package cn.turboinfo.fuyang.api.entity.common.enumeration.shop;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 门店排序方式
 */
@Slf4j
public enum ShopSortType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "不限排序"),

    CREDIT_SCORE_DESC(10, "按信用分从高到低"),

    DISTANCE_NEAREST(20, "距离最近"),

    STAFF_COUNT_DESC(30, "家政员数量从高到低"),

    ;

    private int value;
    private String name;

    ShopSortType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ShopSortType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ShopSortType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<ShopSortType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
