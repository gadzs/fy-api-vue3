package cn.turboinfo.fuyang.api.gateway.admin.fo.profit;

import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@Data
@QField
public class ProfitSharingSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(
            placeholder = "订单编码"
    )
    private Long objectId;

    @QField(
            placeholder = "企业ID"
    )
    private Long companyId;

    @QField(
            searchable = false,
            placeholder = "企业名称",
            operator = Operator.LIKE
    )
    private String companyName;

    @QField(
            placeholder = "微信支付订单号",
            operator = Operator.LIKE
    )
    private String wxTransactionId;

    @QField(
            placeholder = "微信分账单号",
            operator = Operator.LIKE
    )
    private String wxOrderId;

    @QField(
            placeholder = "分账状态",
            control = Control.SELECT,
            ref = "profitSharingStatusList",
            template = "enum"
    )
    private ProfitSharingStatus status;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
