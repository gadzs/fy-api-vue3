package cn.turboinfo.fuyang.api.entity.share.fo.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sunshow.toolkit.core.base.enums.EnableStatus;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShareShopFO {

    private Long id;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 成立日期
     */
    private LocalDate foundDate;

    /**
     * 省编码
     */
    private Long provinceCode;
    private String provinceName;

    /**
     * 市编码
     */
    private Long cityCode;
    private String cityName;

    /**
     * 区编码
     */
    private Long areaCode;
    private String areaName;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系人电话
     */
    private String contactMobile;

    /**
     * 营业时间
     */
    private String businessTime;

    /**
     * 状态
     */
    private EnableStatus status;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

}
