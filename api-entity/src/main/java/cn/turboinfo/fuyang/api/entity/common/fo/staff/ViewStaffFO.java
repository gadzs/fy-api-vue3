package cn.turboinfo.fuyang.api.entity.common.fo.staff;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author gadzs
 * @description 家政员详情
 * @date 2023/2/13 13:48
 */
@Data
public class ViewStaffFO {

    private Long id;

    /**
     * 展示名称
     */
    private String displayName;

    /**
     * 联系电话
     */
    private String contactMobile;

    private String contactMobileEncrypt;

    /**
     * 公司id
     */
    private String companyName;

    /**
     * 性别
     */
    private GenderType gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 个人照片
     */
    private Long photoFile;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 工作经验年数
     */
    private Integer seniority;

    /**
     * 家政员籍贯
     */
    private String provinceName;
}
