package cn.turboinfo.fuyang.api.scheduler.scheduler.profit

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.profit.SchedulerServiceCustomProfitSharingUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 定制服务订单分账定时任务
 */
@Component
class ServiceCustomProfitSharingScheduler(
    private val schedulerServiceCustomProfitSharingUseCase: SchedulerServiceCustomProfitSharingUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 3600000)
    fun run() {
        runCatching {
            schedulerServiceCustomProfitSharingUseCase.execute(
                SchedulerServiceCustomProfitSharingUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "定制服务订单分账处理失败"
            }
        }
    }

}
