package cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo;

import cn.turboinfo.fuyang.api.entity.admin.constant.AdminRestConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestResponseFO {

    private int code;

    private String message;

    private Object data;

    public static RestResponseFO ok() {
        return ok("操作成功", null);
    }

    public static <T> RestResponseFO ok(T data) {
        return ok("操作成功", data);
    }

    public static <T> RestResponseFO ok(String message, T data) {
        return new RestResponseFO(AdminRestConstants.RC_SUCCESS, message, data);
    }

    public static RestResponseFO error(String message) {
        return error(AdminRestConstants.RC_ERROR_DEFAULT, message);
    }

    public static RestResponseFO error(int code, String message) {
        return new RestResponseFO(code, message, null);
    }

    public static RestResponseFO error(int code, String message, Object data) {
        return new RestResponseFO(code, message, data);
    }

}
