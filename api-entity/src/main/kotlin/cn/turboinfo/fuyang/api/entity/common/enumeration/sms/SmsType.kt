package cn.turboinfo.fuyang.api.entity.common.enumeration.sms

enum class SmsType {

    RegisterOrLogin,

    ResetPassword,

}