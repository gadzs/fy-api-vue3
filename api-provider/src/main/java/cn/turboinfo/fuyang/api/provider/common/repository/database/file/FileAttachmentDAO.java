package cn.turboinfo.fuyang.api.provider.common.repository.database.file;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

public interface FileAttachmentDAO extends BaseRepository<FileAttachmentPO, Long> {
    List<FileAttachmentPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<FileAttachmentPO> findByRefIdOrderById(Long refId);

    List<FileAttachmentPO> findByRefIdAndRefTypeInOrderById(Long refId, Collection<String> refTypeCollection);

    List<FileAttachmentPO> findByRefIdAndRefTypeOrderById(Long refId, String refType);

    List<FileAttachmentPO> findByRefIdInAndRefTypeOrderById(Collection<Long> refIdCollection, String refType);

    List<FileAttachmentPO> findByRefIdInOrderById(Collection<Long> refIdCollection);
}
