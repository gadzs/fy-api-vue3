-- 2023-07-11 企业账户增加状态字段
ALTER TABLE `company_account`
    ADD `status` TINYINT NOT NULL DEFAULT 0 COMMENT '状态' AFTER `custom_relation`;

SET
    @accountPermissionParentId = (select id
                                  from sys_permission
                                  where resource = 'adminAccount:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('导出分账账户', 'adminAccount:export', '', '', '', 0, @accountPermissionParentId);
