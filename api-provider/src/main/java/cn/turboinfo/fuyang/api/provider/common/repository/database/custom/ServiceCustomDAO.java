package cn.turboinfo.fuyang.api.provider.common.repository.database.custom;

import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.Collection;
import java.util.List;

public interface ServiceCustomDAO extends FYRepository<ServiceCustomPO, Long> {


    List<ServiceCustomPO> findByStaffIdAndCustomStatusOrderByCreatedTimeDesc(Long staffId, ServiceCustomStatus status);

    List<ServiceCustomPO> findByCompanyIdAndCustomStatusOrderByCreatedTimeDesc(Long companyId, ServiceCustomStatus status);

    List<ServiceCustomPO> findByCompanyIdAndCustomStatusInOrderByCreatedTimeDesc(Long companyId, Collection<ServiceCustomStatus> statusCollection);

    List<ServiceCustomPO> findByShopIdAndCustomStatusOrderByCreatedTimeDesc(Long shopId, ServiceCustomStatus status);

    List<ServiceCustomPO> findByUserIdAndStaffIdAndCustomStatus(Long userId, Long staffId, ServiceCustomStatus status);
}
