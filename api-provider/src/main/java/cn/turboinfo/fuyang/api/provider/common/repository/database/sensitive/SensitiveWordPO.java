package cn.turboinfo.fuyang.api.provider.common.repository.database.sensitive;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 敏感词管理
 *
 * @author hai
 */
@Table(
        name = "sensitive_word"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Entity
@Where(clause = "deleted = 0")
public class SensitiveWordPO extends SoftDeleteFYEntity {
    private String word;
}
