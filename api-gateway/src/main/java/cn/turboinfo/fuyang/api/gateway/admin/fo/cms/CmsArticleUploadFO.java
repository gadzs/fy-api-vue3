package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CmsArticleUploadFO {

    private String location;

    private Long attachId;

}
