package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecordCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 产品服务审核usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductReviewedUseCase extends AbstractUseCase<ProductReviewedUseCase.InputData, ProductReviewedUseCase.OutputData> {
    private final ProductService productService;

    private final SysUserService sysUserService;

    private final ProductAuditRecordService productAuditRecordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();
        val auditorId = inputData.getUserId();
        val product = productService.getByIdEnsure(id);
        if (product.getStatus() != ProductStatus.DEFAULT) {
            throw new ProductException("数据状态不是待审核，操作失败");
        }

        // 更新状态
        productService.updateStatus(id, ProductStatus.PASSED);

        val auditorUser = sysUserService.getByIdEnsure(auditorId);

        // 更新审核记录
        ProductAuditRecordCreator productAuditRecord = ProductAuditRecordCreator.builder()
                .withProductId(id)
                .withCompanyId(product.getCompanyId())
                .withShopId(product.getShopId())
                .withAuditStatus(AuditStatus.REVIEWED)
                .withUserId(auditorId)
                .withUserName(auditorUser.getUsername())
                .withRemark("审核通过")
                .build();
        productAuditRecordService.save(productAuditRecord);


        return OutputData.builder()
                .id(product.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(message = "审核用户ID不能为空")
        private Long userId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
