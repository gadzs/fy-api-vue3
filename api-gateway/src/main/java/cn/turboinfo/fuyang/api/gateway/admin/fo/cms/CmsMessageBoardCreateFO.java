package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsMessageBoardType;
import lombok.Data;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class CmsMessageBoardCreateFO {

    private Long id;

    private Long userId;

    private String username;

    private String title;

    private String content;

    private String mobile;

    private String email;

    @NotNull(
            message = "类型不能为空"
    )
    private CmsMessageBoardType type;


    private String busiType;

    private YesNoStatus isShow;

    private Long replyUserId;

    private String replyUsername;

    private String replyContent;

    private String captcha;

    /**
     * 意见征集id
     */
    private Long adviceId;

    private LocalDateTime replyTime;

    private Integer sort;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
