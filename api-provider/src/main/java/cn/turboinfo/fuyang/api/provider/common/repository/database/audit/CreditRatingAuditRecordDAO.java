package cn.turboinfo.fuyang.api.provider.common.repository.database.audit;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface CreditRatingAuditRecordDAO extends FYRepository<CreditRatingAuditRecordPO, Long> {

    List<CreditRatingAuditRecordPO> findByCreditRatingIdOrderByIdDesc(Long creditRatingId);

}
