package cn.turboinfo.fuyang.api.provider.common.repository.database.custom.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class ServiceCustomStatusConverter extends BaseEnumConverter<ServiceCustomStatus> {
}
