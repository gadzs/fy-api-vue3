DROP TABLE IF EXISTS `confidence_code`;
CREATE TABLE `confidence_code`
(
    `id`           BIGINT(1) NOT NULL,
    `status`       TINYINT   NOT NULL DEFAULT 0 COMMENT '状态',
    `deleted`      BIGINT(1) NOT NULL DEFAULT 0,
    `created_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '放心码管理';
