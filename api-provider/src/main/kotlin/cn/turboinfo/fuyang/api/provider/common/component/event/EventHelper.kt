package cn.turboinfo.fuyang.api.provider.common.component.event

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class EventHelper {

    @Value("\${deploy.redis.pubsub.key-prefix:__pubsub__}")
    private lateinit var keyPrefix: String

    fun topicKey(topic: String) = "$keyPrefix:$topic"

}