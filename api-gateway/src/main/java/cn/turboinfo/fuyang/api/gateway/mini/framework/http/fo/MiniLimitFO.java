package cn.turboinfo.fuyang.api.gateway.mini.framework.http.fo;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QSort;

import java.util.List;

/**
 * author: sunshow.
 */
@Getter
@Setter
public class MiniLimitFO {

    private int page;

    private int limit;

    private long total;

    public QPage toQPage() {
        return toQPage(null);
    }

    public QPage toQPage(List<QSort> sortList) {
        // 前端分页从1开始 要-1转成内部从0开始
        QPage page = QPage.newInstance().paging(this.page - 1, limit).withoutCountQuery(false);
        page.setSortList(sortList);
        return page;
    }

}
