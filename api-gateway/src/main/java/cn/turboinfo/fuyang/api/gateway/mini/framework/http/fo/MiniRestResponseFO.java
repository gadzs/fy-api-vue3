package cn.turboinfo.fuyang.api.gateway.mini.framework.http.fo;

import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MiniRestResponseFO {

    private int code;

    private String message;

    private Object data;

    public static MiniRestResponseFO ok() {
        return ok("操作成功", null);
    }

    public static <T> MiniRestResponseFO ok(T data) {
        return ok("操作成功", data);
    }

    public static <T> MiniRestResponseFO ok(String message, T data) {
        return new MiniRestResponseFO(MiniRestConstants.RC_SUCCESS, message, data);
    }

    public static MiniRestResponseFO error(String message) {
        return error(MiniRestConstants.RC_ERROR_DEFAULT, message);
    }

    public static MiniRestResponseFO error(int code, String message) {
        return new MiniRestResponseFO(code, message, null);
    }

    public static MiniRestResponseFO error(int code, String message, Object data) {
        return new MiniRestResponseFO(code, message, data);
    }

}
