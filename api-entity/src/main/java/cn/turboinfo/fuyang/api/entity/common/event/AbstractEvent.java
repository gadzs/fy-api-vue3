package cn.turboinfo.fuyang.api.entity.common.event;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@SuperBuilder
public abstract class AbstractEvent implements Serializable {

    @Builder.Default
    private LocalDateTime instant = LocalDateTime.now();

}
