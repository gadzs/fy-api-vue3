package cn.turboinfo.fuyang.api.entity.common.enumeration.user;

import cn.turboinfo.fuyang.api.entity.admin.constant.AdminRoleConstants;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public enum UserType implements BaseEnum {

    Default(0, "默认"),

    Platform(10, "平台", AdminRoleConstants.ROLE_PLATFORM),

    Company(20, "公司", AdminRoleConstants.ROLE_COMPANY),

    Staff(30, "家政员", AdminRoleConstants.ROLE_STAFF),

    Consumer(40, "消费者", AdminRoleConstants.ROLE_CONSUMER),

    ;

    private int value;
    private String name;
    private String roleCode;

    UserType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    UserType(int value, String name, String roleCode) {
        this.value = value;
        this.name = name;
        this.roleCode = roleCode;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getRoleCode() {
        return roleCode;
    }

    @Override
    public String toString() {
        return name;
    }

    public static UserType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<UserType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<UserType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }

    public static List<String> listRoleCode() {
        return list().stream().map(UserType::getRoleCode).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
