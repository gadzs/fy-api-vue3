package cn.turboinfo.fuyang.api.scheduler.scheduler.order

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.order.SchedulerUnpaidPayOrderByWechatUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 支付状态定时任务
 */
@Component
class PayOrderStatusScheduler(
    private val schedulerUnpaidPayOrderByWechatUseCase: SchedulerUnpaidPayOrderByWechatUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 5000)
    fun run() {
        runCatching {
            schedulerUnpaidPayOrderByWechatUseCase.execute(
                SchedulerUnpaidPayOrderByWechatUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "未支付支付订单定时任务执行失败"
            }
        }
    }

}
