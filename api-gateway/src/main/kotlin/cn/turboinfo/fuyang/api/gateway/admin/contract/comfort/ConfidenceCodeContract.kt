package cn.turboinfo.fuyang.api.gateway.admin.contract.comfort

import cn.turboinfo.fuyang.api.domain.admin.usecase.confidence.ConfidenceCodeDeleteUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.confidence.ConfidenceCodeGenerateUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.confidence.ConfidenceCodeUpdateUseCase
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@AutoMappingContract(paths = ["/admin/confidence"])
@ResponseBodyWrapper
interface ConfidenceCodeContract {

    // 放心码生成
    @PermissionScope(value = ["adminConfidence:generate"])
    @AutoMappingContract(
        beanType = ConfidenceCodeGenerateUseCase::class
    )
    fun generate()

    // 放心码信息修改
    @PermissionScope(value = ["adminConfidence:update"])
    @AutoMappingContract(
        beanType = ConfidenceCodeUpdateUseCase::class
    )
    fun update()

    /**
     * 删除
     */
    @PermissionScope(value = ["adminConfidence:delete"])
    @AutoMappingContract(
        beanType = ConfidenceCodeDeleteUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/delete/{id}"]
    )
    fun delete()
}
