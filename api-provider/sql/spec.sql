CREATE TABLE `spec`
(
    `id`           BIGINT(1)    NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(64)  NOT NULL DEFAULT '',
    `display_name` VARCHAR(64)  NOT NULL DEFAULT '',
    `description`  VARCHAR(255) NOT NULL DEFAULT '',
    `parent_id`    BIGINT(1)    NOT NULL DEFAULT 0,
    `spec_set_id`  BIGINT(1)    NOT NULL DEFAULT 0,
    `sort_value`   INT(1)       NOT NULL DEFAULT 0,
    `allow_delete` TINYINT      NOT NULL DEFAULT 0,
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE `spec_set`
(
    `id`           BIGINT(1)    NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(64)  NOT NULL DEFAULT '',
    `company_id`   BIGINT(1)    NOT NULL DEFAULT 0,
    `description`  VARCHAR(255) NOT NULL DEFAULT '',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE `spec_set_rel`
(
    `id`           BIGINT(1) NOT NULL AUTO_INCREMENT,
    `spec_set_id`  BIGINT(1) NOT NULL,
    `object_type`  INT(1)    NOT NULL DEFAULT 0,
    `object_id`    BIGINT(1) NOT NULL DEFAULT 0,
    `deleted`      BIGINT(1) NOT NULL DEFAULT 0,
    `created_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_specsetid_objectid_objecttype` (`spec_set_id`, `object_id`, `object_type`, `deleted`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
