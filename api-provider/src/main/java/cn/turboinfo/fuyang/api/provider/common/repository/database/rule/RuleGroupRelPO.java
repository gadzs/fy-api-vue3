package cn.turboinfo.fuyang.api.provider.common.repository.database.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 规则组关联关系
 * author: sunshow.
 */
@Table(
        name = "rule_group_rel"
)
@Entity
@DynamicInsert
@DynamicUpdate
@Data
public class RuleGroupRelPO implements BaseEntity {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 规则组ID
     */
    @Column(name = "rule_group_id")
    private Long ruleGroupId;

    /**
     * 对象类型
     */
    @Column(name = "object_type")
    private EntityObjectType objectType;

    /**
     * 对象ID
     */
    @Column(name = "object_id")
    private Long objectId;

    /**
     * 排序值
     */
    @Column(name = "sort_value")
    private Integer sortValue;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }

}
