package cn.turboinfo.fuyang.api.domain.mini.service.session;

import cn.turboinfo.fuyang.api.entity.mini.exception.session.MiniSessionNotFoundException;
import cn.turboinfo.fuyang.api.entity.mini.pojo.session.MiniSession;

import java.util.Optional;

public interface MiniSessionService {

    /**
     * 按 token 查找 Session 信息
     *
     * @param token 会话令牌
     * @return Session
     */
    Optional<MiniSession> findByToken(String token);

    /**
     * 按 token 保存 Session
     *
     * @param token   会话令牌
     * @param session 会话信息
     */
    void save(String token, MiniSession session);

    /**
     * 按 token 删除 Session
     *
     * @param token 会话令牌
     */
    void removeByToken(String token);

    /**
     * 按 token 刷新 Session 有效期
     *
     * @param token 会话令牌
     */
    void refreshByToken(String token);

    /**
     * 按 token 验证 Session
     *
     * @param token 会话令牌
     */
    void validateByToken(String token) throws MiniSessionNotFoundException;

    MiniSession validateAndRefreshByToken(String token) throws MiniSessionNotFoundException;
}
