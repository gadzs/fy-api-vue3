package cn.turboinfo.fuyang.api.domain.admin.service.cms;

import cn.turboinfo.fuyang.api.entity.admin.exception.cms.CmsArticleException;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticleCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticleUpdater;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CmsArticleService {
    Optional<CmsArticle> getById(Long id);

    CmsArticle getByIdEnsure(Long id);

    List<CmsArticle> findByIdCollection(Collection<Long> idCollection);

    CmsArticle save(CmsArticleCreator creator) throws CmsArticleException;

    CmsArticle update(CmsArticleUpdater updater) throws CmsArticleException;

    QResponse<CmsArticle> findAll(QRequest request, QPage requestPage);

    void deleteById(Long id) throws CmsArticleException;

    List<CmsArticle> findArticleByCategory(Long categoryId);

    List<CmsArticle> findAllArticleByCategory(List<Long> categoryCollection);
}
