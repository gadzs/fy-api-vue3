package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 退款状态
 */
@Slf4j
public enum RefundStatus implements BaseEnum {

    INIT(0, "初始化"),

    PROCESSING(10, "退款处理中"),

    NO_REFUND(20, "无需退款"),

    SUCCESS(70, "退款成功"),

    CLOSED(80, "退款关闭"),

    ABNORMAL(90, "退款异常"),
    ;

    private int value;
    private String name;

    RefundStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static RefundStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<RefundStatus> list() {
        return BaseEnumHelper.getList(values());
    }

}
