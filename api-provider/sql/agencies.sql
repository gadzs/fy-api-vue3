DROP TABLE IF EXISTS `agencies`;
CREATE TABLE `agencies`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `name`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '',
    `area_code`    BIGINT(1)    NOT NULL DEFAULT 0 COMMENT '',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='机构管理';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('机构管理', 'adminAgencies:null', '/agencies', 1, 0);
SET @agenciesPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`)
VALUES ('机构列表', 'adminAgencies:list', '/agencies/list', 1, @agenciesPermissionParentId),
       ('新增机构', 'adminAgencies:create', '', 0, @agenciesPermissionParentId),
       ('更新机构', 'adminAgencies:update', '', 0, @agenciesPermissionParentId),
       ('删除机构', 'adminAgencies:delete', '', 0, @agenciesPermissionParentId);

INSERT INTO `agencies`(`name`, `area_code`, `deleted`)
VALUES ('阜阳市', 341200, 0),
       ('阜阳市颍州区', 341202, 0),
       ('阜阳市颖东区', 341203, 0),
       ('阜阳市颍泉区', 341204, 0),
       ('阜阳市临泉县', 341221, 0),
       ('阜阳市太和县', 341222, 0),
       ('阜阳市阜南县', 341225, 0),
       ('阜阳市颖上县', 341226, 0),
       ('阜阳合肥现代产业园区', 341271, 0),
       ('阜阳经济技术开发区', 341222, 0),
       ('阜阳市界首市', 341282, 0)
;
