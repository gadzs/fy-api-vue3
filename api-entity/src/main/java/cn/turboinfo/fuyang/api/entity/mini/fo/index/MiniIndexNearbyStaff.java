package cn.turboinfo.fuyang.api.entity.mini.fo.index;

import cn.turboinfo.fuyang.api.entity.common.fo.staff.ViewStaffFO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MiniIndexNearbyStaff {

    private ViewStaffFO staff;

    /**
     * 距离
     */
    private BigDecimal distance;

}
