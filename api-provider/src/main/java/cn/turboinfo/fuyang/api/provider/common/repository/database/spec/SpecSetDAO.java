package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

public interface SpecSetDAO extends FYRepository<SpecSetPO, Long> {

}
