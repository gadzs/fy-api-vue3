package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.login.AdminLoginOperation;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Table(
        name = "sys_user"
)
@Entity
@Where(clause = "deleted = 0")
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class SysUserPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;
    
    @Convert(converter = DataEncryptConverter.class)
    private String username;

    @Convert(converter = DataEncryptConverter.class)
    private String mobile;

    private EnableStatus status;

    /**
     * 登录后的操作
     */
    @Column(name = "login_operation")
    private AdminLoginOperation loginOperation;

}
