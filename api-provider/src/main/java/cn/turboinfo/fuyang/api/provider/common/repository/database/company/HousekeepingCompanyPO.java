package cn.turboinfo.fuyang.api.provider.common.repository.database.company;

import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 家政公司
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "housekeeping_company"
)
@Where(clause = "deleted = 0")
@Entity
@DynamicUpdate
@DynamicInsert
public class HousekeepingCompanyPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司短名
     */
    private String shortName;

    /**
     * 统一社会信用代码
     */
    @Convert(converter = DataEncryptConverter.class)
    private String uscc;

    /**
     * 成立日期
     */
    @Column(
            name = "establishment_date"
    )
    private LocalDate establishmentDate;

    /**
     * 省id
     */
    @Column(
            name = "province_code"
    )
    private Long provinceCode;

    /**
     * 市id
     */
    @Column(
            name = "city_code"
    )
    private Long cityCode;

    /**
     * 区id
     */
    @Column(
            name = "area_code"
    )
    private Long areaCode;

    /**
     * 注册地址
     */
    @Column(
            name = "registered_address"
    )
    private String registeredAddress;

    /**
     * 注册资本
     */
    @Column(
            name = "registered_capital"
    )
    private BigDecimal registeredCapital;

    /**
     * 公司类型
     */
    @Column(
            name = "company_type"
    )
    private String companyType;

    /**
     * 法人
     */
    @Column(
            name = "legal_person"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String legalPerson;

    /**
     * 法人身份证
     */
    @Column(
            name = "legal_person_id_card"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String legalPersonIdCard;

    /**
     * 工商注册号
     */
    @Column(
            name = "business_registration_number"
    )
    private String businessRegistrationNumber;

    /**
     * 经营范围
     */
    @Column(
            name = "business_scope"
    )
    private String businessScope;

    /**
     * 联系人
     */
    @Column(
            name = "contact_person"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String contactPerson;

    /**
     * 联系电话
     */
    @Column(
            name = "contact_number"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String contactNumber;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 员工人数
     */
    @Column(
            name = "employees_number"
    )
    private String employeesNumber;

    /**
     * 企业状态
     */
    private CompanyStatus status;

    /**
     * 营业执照
     */
    @Column(
            name = "business_license_file"
    )
    private Long businessLicenseFile;

    /**
     * 法定代表人身份证
     */
    @Column(
            name = "legal_person_id_card_file"
    )
    private Long legalPersonIdCardFile;

    /**
     * 银行开户证明
     */
    @Column(
            name = "bank_account_certificate_file"
    )
    private Long bankAccountCertificateFile;

    /**
     * 审核时间
     */
    @Column(
            name = "review_time"
    )
    private LocalDateTime reviewTime;

    /**
     * 审核人
     */
    @Column(
            name = "review_user_id"
    )
    private Long reviewUserId;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 家政员数
     */
    private Long staffNum;

    /**
     * 产品服务数
     */
    private Long productNum;

    /**
     * 店铺数
     */
    private Long shopNum;

    /**
     * 认证标签
     */
    @Type(type = "json")
    @Column(name = "auth_label_list", columnDefinition = "json")
    private List<HousekeepingCompanyAuthLabel> authLabelList;

    /**
     * 认证标签权重
     */
    private Long authLabelWeight;

    /**
     * 家政公司类型
     */
    @Column(
            name = "house_keeping_type"
    )
    private String houseKeepingType;

    @Column(
            name = "app_id"
    )
    private String appId;

    @Column(
            name = "app_secret"
    )
    private String appSecret;

}
