package cn.turboinfo.fuyang.api.entity.mini.fo.index;

import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MiniIndexBanner {

    private CmsArticle article;

}
