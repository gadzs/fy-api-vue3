package cn.turboinfo.fuyang.api.domain.admin.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.HtmlHelper;
import cn.turboinfo.fuyang.api.entity.common.enumeration.activity.ActivityAuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class ActivityCreateUseCase extends AbstractUseCase<ActivityCreateUseCase.InputData, ActivityCreateUseCase.OutputData> {
    private final ActivityService activityService;

    private final FileAttachmentService fileAttachmentService;
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {


        // 敏感词检查
        if (sensitiveWordService.containsDenyWords(inputData.getName())) {
            throw new ProductException("活动名称包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getOrganizer())) {
            throw new ProductException("主办单位包含敏感词汇，请修改后重新提交！");
        }

        if (sensitiveWordService.containsDenyWords(inputData.getLocation())) {
            throw new ProductException("活动地点包含敏感词汇，请修改后重新提交！");
        }

        // 检查活动介绍是否包含敏感词
        if (sensitiveWordService.containsDenyWords(inputData.getDescription())) {
            throw new ProductException("活动介绍包含敏感词汇，请修改后重新提交");
        }

        ActivityCreator.Builder builder = ActivityCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, ActivityCreator.class, inputData);

        if (inputData.getDescription() == null) {
            builder.withDescription(StringUtils.EMPTY);
        } else {
            builder.withDescription(HtmlHelper.safeFilter(inputData.getDescription()));
        }

        if (inputData.getOrganizer() == null) {
            builder.withOrganizer(StringUtils.EMPTY);
        }

        if (inputData.getLocation() == null) {
            builder.withLocation(StringUtils.EMPTY);
        }

        if (inputData.getCompanyId() == null) {
            builder.withCompanyId(0L);
        }

        // 处理活动范围

        Activity activity = activityService.save(builder
                .withApplied(0)
                .withAuditStatus(ActivityAuditStatus.INIT)
                .withAuditReason(StringUtils.EMPTY)
                .build());

        fileAttachmentService.updateRefId(inputData.getImageIdList(), activity.getId());

        return OutputData.builder()
                .activity(activity)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 公司ID
         */
        private Long companyId;

        @NotBlank(
                message = "活动名称不能为空"
        )
        private String name;

        private String description;

        @NotBlank(
                message = "活动类型不能为空"
        )
        private String activityType;

        /**
         * 活动范围
         */
        private List<UserType> activityScope;

        private String organizer;

        private String location;

        @NotNull(
                message = "限制数量不能为空"
        )
        private Integer limitNum;

        @NotNull(
                message = "是否开放报名不能为空"
        )
        private EnableStatus openApply;

        private LocalDateTime startTime;

        private LocalDateTime endTime;

        @NotNull(
                message = "状态不能为空"
        )
        private EnableStatus status;

        /**
         * 图片编码列表
         */
        private List<Long> imageIdList;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Activity activity;
    }
}
