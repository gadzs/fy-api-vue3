package cn.turboinfo.fuyang.api.domain.mini.usecase.custom;

import cn.turboinfo.fuyang.api.domain.common.service.custom.ServiceCustomService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 消费者取消订单
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerCancelServiceCustomUseCase extends AbstractUseCase<MiniConsumerCancelServiceCustomUseCase.InputData, MiniConsumerCancelServiceCustomUseCase.OutputData> {

    private final ServiceCustomService serviceCustomService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long userId = inputData.getUserId();
        Long serviceCustomId = inputData.getServiceCustomId();

        ServiceCustom serviceCustom = serviceCustomService.getByIdEnsure(serviceCustomId);

        if (!Objects.equals(userId, serviceCustom.getUserId())) {
            throw new IllegalArgumentException("订单所属人员不匹配");
        }

        // 取消
        serviceCustomService.consumerCancel(serviceCustomId);

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        /**
         * 用户ID
         */
        @NotNull(message = "用户ID不能为空")
        @Positive
        private Long userId;

        /**
         * 服务订单ID
         */
        @NotNull(message = "服务订单不能为空")
        @Positive
        private Long serviceCustomId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {


    }
}
