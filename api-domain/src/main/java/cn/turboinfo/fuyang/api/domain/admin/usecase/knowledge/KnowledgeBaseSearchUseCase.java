package cn.turboinfo.fuyang.api.domain.admin.usecase.knowledge;

import cn.turboinfo.fuyang.api.domain.common.service.knowledge.KnowledgeBaseService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.knowledge.KnowledgeBase;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class KnowledgeBaseSearchUseCase extends AbstractUseCase<KnowledgeBaseSearchUseCase.InputData, KnowledgeBaseSearchUseCase.OutputData> {
    private final KnowledgeBaseService knowledgeBaseService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<KnowledgeBase> response = knowledgeBaseService.findAll(inputData.getRequest(), inputData.getRequestPage());

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<KnowledgeBase> qResponse;
    }
}
