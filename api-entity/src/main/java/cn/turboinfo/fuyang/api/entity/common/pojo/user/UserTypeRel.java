package cn.turboinfo.fuyang.api.entity.common.pojo.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 用户类型关联表
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class UserTypeRel extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户编码
     */
    private Long userId;

    /**
     * 用户类型
     */
    private UserType userType;

    /**
     * 关联实体类型
     */
    private EntityObjectType objectType;

    /**
     * 关联实体ID
     */
    private Long objectId;

    /**
     * 引用的ID, 例如家政员类型时引用ID为公司ID, 业务自行判断
     */
    private Long referenceId;

    /**
     * 关联实体名称
     */
    private String objectName;

    private String referenceName;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
