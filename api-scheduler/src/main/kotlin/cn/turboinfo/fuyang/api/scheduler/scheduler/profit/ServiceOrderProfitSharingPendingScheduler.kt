package cn.turboinfo.fuyang.api.scheduler.scheduler.profit

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.profit.SchedulerServiceOrderProfitSharingPendingUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 服务订单分账待处理定时任务
 */
@Component
class ServiceOrderProfitSharingPendingScheduler(
    private val schedulerServiceOrderProfitSharingPendingUseCase: SchedulerServiceOrderProfitSharingPendingUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 600000)
    fun run() {
        runCatching {
            schedulerServiceOrderProfitSharingPendingUseCase.execute(
                SchedulerServiceOrderProfitSharingPendingUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "服务订单分账待处理订单处理失败"
            }
        }
    }

}
