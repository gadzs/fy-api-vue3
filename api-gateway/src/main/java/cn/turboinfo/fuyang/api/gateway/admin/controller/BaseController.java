package cn.turboinfo.fuyang.api.gateway.admin.controller;

import cn.turboinfo.fuyang.api.domain.util.DateTimeFormatHelper;
import cn.turboinfo.fuyang.api.domain.util.UrlUtils;
import cn.turboinfo.fuyang.api.domain.web.component.file.FileAttachmentHelper;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author gadzs
 * @description Controlller父类
 * @date 2023/2/16 18:44
 */
@RestController
public class BaseController {

    @Autowired
    FileAttachmentHelper fileAttachmentHelper;

    /**
     * 组装日期查询
     */
    protected void makeBetweenDate(QRequest qRequest, String fieldName, LocalDate stateDate, LocalDate endDate) {
        LocalDateTime stateTime = DateTimeFormatHelper.getStartTime(stateDate);
        LocalDateTime endTime = DateTimeFormatHelper.getEndTime(endDate);
        if (stateTime != null && endTime != null) {
            qRequest.filterBetween(fieldName, stateTime, endTime);
        } else if (stateTime != null && endTime == null) {
            qRequest.filterGreaterEqual(fieldName, stateTime);
        } else if (stateTime == null && endTime != null) {
            qRequest.filterLessEqual(fieldName, endTime);
        }
    }

    /**
     * 组装日期查询
     */
    protected void makeBetweenDateTime(QRequest qRequest, String fieldName, LocalDateTime stateTime, LocalDateTime endTime) {
        if (stateTime != null && endTime != null) {
            qRequest.filterBetween(fieldName, stateTime, endTime);
        } else if (stateTime != null && endTime == null) {
            qRequest.filterGreaterEqual(fieldName, endTime);
        } else if (stateTime == null && endTime != null) {
            qRequest.filterLessEqual(fieldName, endTime);
        }
    }


    /**
     * 查看图片
     *
     * @param fileAttachment
     * @param response
     * @throws IOException
     */
    protected void viewAttachmentImage(FileAttachment fileAttachment, HttpServletResponse response) throws IOException {
        File file = fileAttachmentHelper.readFileAttachment(fileAttachment);
        try (InputStream is = new FileInputStream(file);
             OutputStream os = response.getOutputStream()) {
            response.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
            IOUtils.copy(is, os);
            os.flush();
        }
    }


    /**
     * 查看图片
     *
     * @param fileAttachmentId
     * @param response
     * @throws IOException
     */
    protected void viewAttachmentImage(Long fileAttachmentId, HttpServletResponse response) throws IOException {
        File file = fileAttachmentHelper.readFileAttachment(fileAttachmentId);
        try (InputStream is = new FileInputStream(file);
             OutputStream os = response.getOutputStream()) {
            response.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
            IOUtils.copy(is, os);
            os.flush();
        }
    }

    /**
     * 下载
     *
     * @param fileAttachment
     * @param response
     * @throws IOException
     */
    protected void downloadAttachment(FileAttachment fileAttachment, HttpServletResponse response) throws IOException {
        File file = fileAttachmentHelper.readFileAttachment(fileAttachment);
        try (InputStream is = new FileInputStream(file);
             OutputStream os = response.getOutputStream()) {
            response.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
            response.setHeader("Content-Disposition", "attachment; filename=" + UrlUtils.encodeURIComponent(fileAttachment.getDisplayName()));
            IOUtils.copy(is, os);
            os.flush();
        }
    }


    /**
     * 检查前端查询分页数过大问题
     *
     * @param limitFO
     */
    protected void checkLimit(LimitFO limitFO) {
        int page = limitFO.getPage();
        int limit = limitFO.getLimit();
        limitFO.setPage(page == 0 ? 1 : page);
        limitFO.setLimit(limit == 0 ? 10 : limit);
        if (limit > 50) {
            throw new RuntimeException("分页数过大，请重试");
        }
    }

}
