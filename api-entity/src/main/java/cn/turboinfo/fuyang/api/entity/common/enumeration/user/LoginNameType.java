package cn.turboinfo.fuyang.api.entity.common.enumeration.user;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

@Slf4j
public enum LoginNameType implements BaseEnum {

    ALL(-1, "全部"),

    DEFAULT(0, "默认"),

    PHONE_NUM(1, "手机号"),
    USERNAME(2, "用户名"),

    THIRD_PARTY(10, "三方账号"),
    ;

    private int value;
    private String name;

    LoginNameType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static LoginNameType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<LoginNameType> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<LoginNameType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
