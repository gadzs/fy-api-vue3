package cn.turboinfo.fuyang.api.entity.admin.pojo.log;

import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 登录用户操作日志
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class OperationLog extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 系统用户ID
     */
    private Long sysUserId;

    /**
     * 系统用户名
     */
    private String username;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求参数
     */
    private String params;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
