package cn.turboinfo.fuyang.api.domain.mini.usecase.index;

import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.mini.service.cms.MiniCmsArticleService;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import cn.turboinfo.fuyang.api.entity.mini.fo.index.MiniIndexBanner;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 首页 banner 列表
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniIndexListBannerUseCase extends AbstractUseCase<MiniIndexListBannerUseCase.InputData, MiniIndexListBannerUseCase.OutputData> {

    private final MiniCmsArticleService miniCmsArticleService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<MiniIndexBanner> homeArticle = miniCmsArticleService.findHomeArticle()
                .stream()
                .filter(it -> it.getType() == CmsArticleType.PICTURE)
                .map(it -> MiniIndexBanner
                        .builder()
                        .article(it)
                        .build())
                .toList();

        return OutputData.builder()
                .bannerList(homeArticle)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class InputData extends AbstractUseCase.InputData {

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        /**
         * 首页轮播
         */
        private List<MiniIndexBanner> bannerList;

    }
}
