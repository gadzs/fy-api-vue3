package cn.turboinfo.fuyang.api.provider.common.repository.database.account.converter;

import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

@Converter(
        autoApply = true
)
public class AccountStatusConverter extends BaseEnumConverter<AccountStatus> {
}
