package cn.turboinfo.fuyang.api.provider.common.service.impl.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.provider.common.repository.database.account.CompanyAccountDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.account.CompanyAccountPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class CompanyAccountServiceImpl extends DefaultQServiceImpl<CompanyAccount, Long, CompanyAccountPO, CompanyAccountDAO> implements CompanyAccountService {
    @Override
    public Optional<CompanyAccount> getByCompanyIdAndType(Long companyId, AccountType type) {
        return dao.findFirstByCompanyIdAndTypeOrderByIdDesc(companyId, type).map(this::convertQBean);
    }

    @Override
    public List<CompanyAccount> findByCompanyId(Long companyId) {
        return convertStreamQBeanToList(dao.findByCompanyIdOrderByTypeAsc(companyId).stream());
    }
}
