package cn.turboinfo.fuyang.api.entity.common.enumeration.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control.ClientRuleControl;
import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control.CollectionOrderControl;
import cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control.PagingRequestControl;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 规则控制类型
 */
@Slf4j
public enum RuleControlType implements BaseEnum {

    // ALL(-1, "全部"),

    // DEFAULT(0, "默认"),

    CLIENT(10, "客户端信息"),

    COLLECTION_ORDER(20, "集合排序"),

    PAGING_REQUEST(30, "分页请求"),

    ;

    private int value;
    private String name;

    RuleControlType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static RuleControlType get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<RuleControlType> list() {
        return BaseEnumHelper.getList(values());
    }

    /*
    public static List<RuleControlType> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
    */

    @SuppressWarnings("unchecked")
    public static <T extends BaseEnum> List<T> getControlList(RuleControlType controlType) {
        switch (controlType) {
            case CLIENT:
                return (List<T>) ClientRuleControl.list();
            case COLLECTION_ORDER:
                return (List<T>) CollectionOrderControl.list();
            case PAGING_REQUEST:
                return (List<T>) PagingRequestControl.list();
        }

        throw new RuntimeException("不支持的规则控制类型");
    }

    public <T extends BaseEnum> List<T> getControlList() {
        return getControlList(this);
    }
}
