package cn.turboinfo.fuyang.api.gateway.admin.controller.product;


import cn.turboinfo.fuyang.api.domain.admin.usecase.product.ProductSearchUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.product.ProductSkuListUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.product.ProductViewUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.QProduct;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import cn.turboinfo.fuyang.api.gateway.admin.fo.product.ProductSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.provider.admin.component.session.AdminSessionHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * 家政公司商品管理
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/product")
public class ProductController {

    private final ProductSearchUseCase productSearchUseCase;
    private final ProductViewUseCase productViewUseCase;
    private final ProductSkuListUseCase productSkuListUseCase;
    private final AdminSessionHelper sessionHelper;

    @RequiresPermissions(value = {"product:list"}, logical = Logical.OR)
    @RequestMapping("/list")
    public LimitDataFO<Product> list(@Valid ProductSearchFO search, LimitFO page, SortFO sort) {
        Long companyId = sessionHelper.getSession().getUserTypeRelList().stream()
                .filter(userTypeRel -> userTypeRel.getUserType() == UserType.Company)
                .map(UserTypeRel::getObjectId).findFirst().orElse(null);
        if (companyId != null) {
            search.setCompanyId(companyId);
        }
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QProduct.id + "|" + QSort.Order.DESC.name()});
        }
        ProductSearchUseCase.OutputData outputData = productSearchUseCase.execute(ProductSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions(value = {"product:list", "product:update", "adminAudit:productList"}, logical = Logical.OR)
    @GetMapping("/get/{id}")
    public RestResponseFO get(@PathVariable("id") Long id) {
        ProductViewUseCase.OutputData outputData = productViewUseCase.execute(ProductViewUseCase.InputData.builder().id(id).build());
        return RestResponseFO.ok(outputData);
    }

    @RequiresPermissions(value = {"product:list", "product:update"}, logical = Logical.OR)
    @GetMapping("/listSku/{productId}")
    public RestResponseFO listSku(@PathVariable("productId") Long productId) {
        ProductSkuListUseCase.OutputData outputData = productSkuListUseCase.execute(ProductSkuListUseCase.InputData.builder().productId(productId).build());
        return RestResponseFO.ok(outputData);
    }

}
