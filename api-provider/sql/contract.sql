DROP TABLE IF EXISTS `contract_tmpl`;
CREATE TABLE `contract_tmpl`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `contract_no`  VARCHAR(32)  NOT NULL DEFAULT '' COMMENT '合同编码',
    `name`         VARCHAR(128) NOT NULL DEFAULT '' COMMENT '合同名称',
    `category_id`  BIGINT       NOT NULL DEFAULT 0 COMMENT '合同类别',
    `company_id`   BIGINT       NOT NULL DEFAULT 0 COMMENT '企业编码',
    `status`       TINYINT      NOT NULL DEFAULT 0 COMMENT '状态',
    `deleted`      BIGINT(1)    NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='合同管理';

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('合同管理', 'contract:null', '/contract', 1, 0, 'el-icon-menu');
SET @agenciesPermissionParentId = LAST_INSERT_ID();

INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('合同模版列表', 'contractTmpl:list', '/contract/template/list', 1, @agenciesPermissionParentId, 'el-icon-menu'),
       ('新增合同模版', 'contractTmpl:create', '', 0, @agenciesPermissionParentId, ''),
       ('更新合同模版', 'contractTmpl:update', '', 0, @agenciesPermissionParentId, ''),
       ('删除合同模版', 'contractTmpl:delete', '', 0, @agenciesPermissionParentId, '');

DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract`
(
    `id`           BIGINT    NOT NULL AUTO_INCREMENT COMMENT '',
    `order_id`     BIGINT    NOT NULL DEFAULT 0 COMMENT '订单编码',
    `company_id`   BIGINT    NOT NULL DEFAULT 0 COMMENT '企业编码',
    `template_id`  BIGINT    NOT NULL DEFAULT 0 COMMENT '合同模板编码',
    `deleted`      BIGINT(1) NOT NULL DEFAULT 0,
    `created_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='合同管理';

SET
    @agenciesPermissionParentId = (select id
                                   from sys_permission
                                   where resource = 'contract:null');
INSERT INTO `sys_permission` (`name`, `resource`, `component`, `visible_status`, `parent_id`, `icon`)
VALUES ('合同列表', 'contract:list', '/contract/list', 1, @agenciesPermissionParentId, 'el-icon-tickets');
