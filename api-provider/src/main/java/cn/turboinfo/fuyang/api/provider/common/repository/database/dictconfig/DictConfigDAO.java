package cn.turboinfo.fuyang.api.provider.common.repository.database.dictconfig;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DictConfigDAO extends BaseRepository<DictConfigPO, Long> {
    List<DictConfigPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    Optional<DictConfigPO> findByDictKey(String dictKey);
}
