package cn.turboinfo.fuyang.api.entity.common.pojo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 服务订单状态记录
 * author: sunshow.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceOrderStatusLog {

    /**
     * 服务订单状态
     */
    private ServiceOrderStatus currentStatus;

    /**
     * 变更之前的服务订单状态
     */
    private ServiceOrderStatus previousStatus;

    /**
     * 服务订单状态变更时间
     */
    private LocalDateTime updatedTime;

}
