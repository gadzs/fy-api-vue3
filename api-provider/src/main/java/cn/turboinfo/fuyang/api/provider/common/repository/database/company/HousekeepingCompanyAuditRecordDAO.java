package cn.turboinfo.fuyang.api.provider.common.repository.database.company;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

import java.util.List;

public interface HousekeepingCompanyAuditRecordDAO extends FYRepository<HousekeepingCompanyAuditRecordPO, Long> {

    List<HousekeepingCompanyAuditRecordPO> findByCompanyIdOrderByCreatedTimeDesc(Long companyId);

}
