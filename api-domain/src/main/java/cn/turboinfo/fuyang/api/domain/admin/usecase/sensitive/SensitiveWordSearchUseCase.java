package cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive;

import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.sensitive.SensitiveWord;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SensitiveWordSearchUseCase extends AbstractUseCase<SensitiveWordSearchUseCase.InputData, SensitiveWordSearchUseCase.OutputData> {
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        QResponse<SensitiveWord> response = sensitiveWordService.findAll(inputData.request, inputData.getRequestPage());

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<SensitiveWord> qResponse;
    }
}
