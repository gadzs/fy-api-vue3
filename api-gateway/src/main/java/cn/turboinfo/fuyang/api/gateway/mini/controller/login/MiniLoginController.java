package cn.turboinfo.fuyang.api.gateway.mini.controller.login;

import cn.turboinfo.fuyang.api.gateway.mini.fo.login.MiniValidateLoginResponse;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@RequestMapping("/mini/login")
public class MiniLoginController {

    @SessionScope(throwException = false)
    @PostMapping("/validateLogin")
    public MiniValidateLoginResponse validateLogin(MiniRequestContext requestContext) {
        return MiniValidateLoginResponse
                .builder()
                .login(requestContext.isLogin())
                .build();
    }

}
