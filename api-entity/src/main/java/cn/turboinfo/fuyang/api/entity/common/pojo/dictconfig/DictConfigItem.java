package cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 数据字典条目
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class DictConfigItem extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 字典ID
     */
    private Long dictConfigId;

    /**
     * 字典Key
     */
    private String dictConfigKey;

    /**
     * 条目值
     */
    private String itemValue;

    /**
     * 条目名称
     */
    private String itemName;

    /**
     * 条目描述
     */
    private String description;

    /**
     * 条目顺序
     */
    private Integer itemOrder;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

    public IntegerViewDictConfigItem toIntegerView() {
        return IntegerViewDictConfigItem
                .builder()
                .dictConfigKey(dictConfigKey)
                .itemName(itemName)
                .itemValue(Integer.valueOf(itemValue))
                .build();
    }
}
