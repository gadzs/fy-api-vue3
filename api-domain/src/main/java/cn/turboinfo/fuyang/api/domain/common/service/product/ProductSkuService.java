package cn.turboinfo.fuyang.api.domain.common.service.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSkuStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductSku;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 产品SKU服务
 */
public interface ProductSkuService extends BaseQService<ProductSku, Long> {

    /**
     * 单个SKU最好不要更新SPEC信息， 走删除重建的逻辑
     * 如果一定要更新，使用这个方法并传入完整的带层级的规格信息更新
     */
    void updateSpecList(Long productSkuId, List<Spec> specList);

    /**
     * 根据产品获取SKU列表
     * @param productId
     * @return
     */
    List<ProductSku> findSkuByProductId(Long productId);

    /**
     * 更新状态
     * @param id
     * @param productSkuStatus
     * @throws ProductException
     */
    void updateStatus(Long id, ProductSkuStatus productSkuStatus) throws ProductException;

    /**
     * 更新状态
     * @param productId
     * @param productSkuStatus
     * @throws ProductException
     */
    void updateStatusByProduct(Long productId, ProductSkuStatus productSkuStatus) throws ProductException;
}
