package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyUpdater;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author gadzs
 * @description 家政企业修改usecase
 * @date 2023/1/29 16:26
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyUpdateUseCase extends AbstractUseCase<HousekeepingCompanyUpdateUseCase.InputData, HousekeepingCompanyUpdateUseCase.OutputData> {
    private final HousekeepingCompanyService housekeepingCompanyService;
    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {
        HousekeepingCompanyUpdater.Builder builder = HousekeepingCompanyUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, HousekeepingCompanyUpdater.class, inputData);
        HousekeepingCompany housekeepingCompany = housekeepingCompanyService.update(builder.build());
        List<Long> fileIdList = Lists.newArrayList(housekeepingCompany.getBusinessLicenseFile(), housekeepingCompany.getLegalPersonIdCardFile(), housekeepingCompany.getBankAccountCertificateFile());
        fileAttachmentService.updateRefId(fileIdList, housekeepingCompany.getId());
        return OutputData.builder()
                .housekeepingCompany(housekeepingCompany)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        private Long companyId;

        @NotBlank(
                message = "企业名称"
        )
        private String name;

        @NotBlank(
                message = "统一社会信用代码不能为空"
        )
        private String uscc;

        @NotNull(
                message = "成立日期不能为空"
        )
        private LocalDate establishmentDate;

        @NotNull(
                message = "省id不能为空"
        )
        private Long provinceCode;

        @NotNull(
                message = "市id不能为空"
        )
        private Long cityCode;

        @NotNull(
                message = "区id不能为空"
        )
        private Long areaCode;

        @NotBlank(
                message = "注册地址不能为空"
        )
        private String registeredAddress;

        @NotNull(
                message = "注册资本不能为空"
        )
        private BigDecimal registeredCapital;

        @NotBlank(
                message = "公司类型不能为空"
        )
        private String companyType;

        @NotBlank(
                message = "法人不能为空"
        )
        private String legalPerson;

        @NotBlank(
                message = "法人身份证不能为空"
        )
        private String legalPersonIdCard;

        private String businessRegistrationNumber;

        private String businessScope;

        @NotBlank(
                message = "联系人不能为空"
        )
        private String contactPerson;

        @NotBlank(
                message = "联系电话不能为空"
        )
        private String contactNumber;

        private String email;

        @NotBlank(
                message = "员工人数不能为空"
        )
        private String employeesNumber;

        @NotNull(
                message = "营业执照"
        )
        private Long businessLicenseFile;

        @NotNull(
                message = "法定代表人身份证"
        )
        private Long legalPersonIdCardFile;

        @NotNull(
                message = "银行开户证明"
        )
        private Long bankAccountCertificateFile;

        @NotNull(
                message = "家政公司类型不能为空"
        )
        private String houseKeepingType;

        @NotNull(
                message = "公司短名不能为空"
        )
        private String shortName;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HousekeepingCompany housekeepingCompany;
    }

}
