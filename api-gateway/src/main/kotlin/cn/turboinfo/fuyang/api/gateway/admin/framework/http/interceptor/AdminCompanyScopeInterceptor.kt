package cn.turboinfo.fuyang.api.gateway.admin.framework.http.interceptor

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContextHolder
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.interceptor.BaseHandlerInterceptorAdapter
import mu.KotlinLogging
import nxcloud.ext.springmvc.automapping.spring.AutoMappingRequestParameterTypeBinding
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.jvm.optionals.getOrNull

/**
 * 公司用户拦截器
 */
@Component
class AdminCompanyScopeInterceptor(
    private val autoMappingRequestParameterTypeBinding: AutoMappingRequestParameterTypeBinding,
) : BaseHandlerInterceptorAdapter() {
    private val logger = KotlinLogging.logger {}

    @OptIn(ExperimentalStdlibApi::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        if (handler is HandlerMethod) {
            val requestContext = AdminRequestContextHolder.current()

            if (requestContext.isLogin) {

                getAnnotation(
                    handler,
                    CompanyScope::class.java,
                    true
                ).getOrNull()
                    ?: autoMappingRequestParameterTypeBinding.getAnnotation(
                        handler.method,
                        CompanyScope::class.java,
                        true
                    )
                    ?: return true

//                if (!StringUtils.equals(
//                        request.getHeader(MiniRestConstants.HEADER_USER_TYPE),
//                        UserType.Company.value.toString()
//                    )
//                ) {
//                    throw RuntimeException("当前请求用户类型不正确")
//                }

                try {

//                    if (requestContext.session.userTypeRelList
//                        .stream()
//                        .noneMatch { userTypeRel -> userTypeRel.userType === UserType.Company }) {
//                        throw RuntimeException("当前请求用户不是家政企业管理用户")
//                    }

                    // 设置会话中的家政企业ID
                    var companyId = requestContext.session.userTypeRelList.stream()
                        .filter { userTypeRel -> userTypeRel.userType === UserType.Company }
                        .map(UserTypeRel::getObjectId)
                        .findFirst().orElse(null)
                    if(companyId != null) {
                        requestContext.companyId = companyId
                    }
                } catch (e: NumberFormatException) {
                    throw RuntimeException("请求的家政企业编码不正确")
                }
            }
        }

        return true
    }
}