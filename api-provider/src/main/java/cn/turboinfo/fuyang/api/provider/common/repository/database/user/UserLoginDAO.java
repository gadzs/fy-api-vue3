package cn.turboinfo.fuyang.api.provider.common.repository.database.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginCheckType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.LoginNameType;
import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserLoginDAO extends BaseRepository<UserLoginPO, Long> {
    List<UserLoginPO> findByIdInOrderByIdDesc(Collection<Long> idCollection);

    List<UserLoginPO> findByUserId(Long userId);

    Optional<UserLoginPO> getByLoginNameAndLoginNameTypeAndLoginCheckTypeAndLoginCheckId(String loginName, LoginNameType loginNameType, LoginCheckType loginCheckType, Long loginCheckId);

    List<UserLoginPO> findByLoginNameAndLoginNameTypeAndLoginCheckType(String loginName, LoginNameType loginNameType, LoginCheckType loginCheckType);

    List<UserLoginPO> findByLoginNameAndLoginNameType(String loginName, LoginNameType loginNameType);
}
