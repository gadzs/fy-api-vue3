package cn.turboinfo.fuyang.api.gateway.mini.framework.http;

import cn.turboinfo.fuyang.api.entity.mini.exception.session.MiniSessionNotFoundException;
import cn.turboinfo.fuyang.api.gateway.mini.constant.MiniRestConstants;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.fo.MiniRestResponseFO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

/**
 * author: sunshow.
 */
@Slf4j
@RestControllerAdvice(basePackages = {
        "cn.turboinfo.fuyang.api.gateway.mini.controller",
})
public class MiniExceptionAdvice {

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<MiniRestResponseFO> exceptionHandler(Throwable e) {
        return doInExceptionHandler(e);
    }

    private static ResponseEntity<MiniRestResponseFO> doInExceptionHandler(Throwable e) {
        log.error(e.getMessage(), e);

        String message = e.getMessage();
        int code = MiniRestConstants.RC_ERROR_DEFAULT;

        if (e instanceof MiniResponseBodyWrapperException) {
            MiniResponseBodyWrapperException exception = (MiniResponseBodyWrapperException) e;
            code = exception.getCode();
            message = exception.getMessage();

            return new ResponseEntity<>(MiniRestResponseFO.error(code, message, exception.getData()), HttpStatus.INTERNAL_SERVER_ERROR);
        } else if (e instanceof UnauthorizedException) {
            message = "没有权限执行当前的操作";
            code = MiniRestConstants.RC_ERROR_UNAUTHORIZED;
            return new ResponseEntity<>(MiniRestResponseFO.error(code, message), HttpStatus.FORBIDDEN);
        } else if (e instanceof MiniSessionNotFoundException) {
            message = "未登录";
            code = MiniRestConstants.RC_ERROR_UNAUTHENTICATED;
            return new ResponseEntity<>(MiniRestResponseFO.error(code, message), HttpStatus.UNAUTHORIZED);
        } else if (e instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException) e).getBindingResult();
            List<String> errorList = new ArrayList<>();
            bindingResult.getAllErrors().forEach(err -> errorList.add(err.getDefaultMessage()));
            message = "参数验证出错: " + StringUtils.join(errorList, "; ");
            code = MiniRestConstants.RC_ERROR_BAD_REQUEST;
            return new ResponseEntity<>(MiniRestResponseFO.error(code, message), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(MiniRestResponseFO.error(code, message), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Slf4j
    @RestControllerAdvice(basePackages = {
            "cn.turboinfo.fuyang.api.gateway.web.framework.http",
    })
    public static class MiniErrorHandler {

        @ExceptionHandler(MiniServletWrapperException.class)
        public ResponseEntity<MiniRestResponseFO> miniServletWrapperExceptionHandler(MiniServletWrapperException e) {
            return doInExceptionHandler(e.getCause());
        }

    }

}
