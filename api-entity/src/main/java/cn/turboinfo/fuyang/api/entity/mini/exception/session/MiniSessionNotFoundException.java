package cn.turboinfo.fuyang.api.entity.mini.exception.session;

public class MiniSessionNotFoundException extends RuntimeException {
    public MiniSessionNotFoundException() {
        this("小程序用户会话未找到");
    }

    public MiniSessionNotFoundException(String message) {
        super(message);
    }

    public MiniSessionNotFoundException(Throwable cause) {
        super(cause);
    }

    public MiniSessionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
