SET NAMES `utf8mb4`;

CREATE TABLE `sys_permission`
(
    `id`             BIGINT       NOT NULL AUTO_INCREMENT,
    `name`           VARCHAR(64)  NOT NULL DEFAULT '',
    `description`    VARCHAR(256) NOT NULL DEFAULT '',
    `resource`       VARCHAR(256) NOT NULL DEFAULT '',
    `url`            VARCHAR(256) NOT NULL DEFAULT '',
    `icon`           VARCHAR(256) NOT NULL DEFAULT '',
    `component`      VARCHAR(256) NOT NULL DEFAULT '',
    `component_name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '组件名称',
    `visible_status` TINYINT      NOT NULL DEFAULT 0,
    `role_id`        BIGINT       NOT NULL DEFAULT 0,
    `parent_id`      BIGINT       NOT NULL DEFAULT 0,
    `sort_value`     INTEGER      NOT NULL DEFAULT 0,
    `created_time`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_resource` (`resource`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `sys_role`
(
    `id`           BIGINT       NOT NULL AUTO_INCREMENT,
    `code`         VARCHAR(64)  NOT NULL,
    `name`         VARCHAR(64)  NOT NULL DEFAULT '',
    `description`  VARCHAR(256) NOT NULL DEFAULT '',
    `is_reserved`  TINYINT      NOT NULL DEFAULT 0,
    `created_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `uk_code` (`code`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `sys_role_permission`
(
    `id`            BIGINT   NOT NULL AUTO_INCREMENT,
    `role_id`       BIGINT   NOT NULL,
    `permission_id` BIGINT   NOT NULL,
    `created_time`  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `uk_roleid_permissionid` (`role_id`, `permission_id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `sys_user`
(
    `id`              BIGINT       NOT NULL AUTO_INCREMENT,
    `username`        VARCHAR(64)  NOT NULL,
    `mobile`          VARCHAR(32)  NOT NULL DEFAULT '',
    `credential`      VARCHAR(512) NOT NULL DEFAULT '',
    `salt`            VARCHAR(512) NOT NULL DEFAULT '',
    `status`          TINYINT      NOT NULL DEFAULT 0,
    `login_operation` TINYINT      NOT NULL DEFAULT 0,
    `deleted`         BIGINT       NOT NULL DEFAULT 0,
    `created_time`    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time`    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `uk_username_deletedtime` (`username`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

CREATE TABLE `sys_user_role`
(
    `id`           BIGINT   NOT NULL AUTO_INCREMENT,
    `sys_user_id`  BIGINT   NOT NULL,
    `role_id`      BIGINT   NOT NULL,
    `created_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `uk_sysuserid_roleid` (`sys_user_id`, `role_id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

INSERT INTO `sys_role` (`id`, `code`, `name`, `description`, `is_reserved`)
VALUES (1, 'admin', '管理员', '', 1),
       (2, 'platform', '平台运营', '', 1),
       (3, 'area', '区域管理', '', 1),
       (4, 'company', '家政公司', '', 1),
       (5, 'staff', '家政服务人员', '', 1),
       (6, 'consumer', '消费者', '', 1);
