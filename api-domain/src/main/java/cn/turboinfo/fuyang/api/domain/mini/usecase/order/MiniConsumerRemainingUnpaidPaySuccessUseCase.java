package cn.turboinfo.fuyang.api.domain.mini.usecase.order;

import cn.turboinfo.fuyang.api.domain.common.service.order.PayOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.order.ServiceOrderService;
import cn.turboinfo.fuyang.api.domain.common.service.wechat.WechatPayService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.PayOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.PayOrder;
import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryV3Result;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

/**
 * 剩余未支付支付调用成功接口
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MiniConsumerRemainingUnpaidPaySuccessUseCase extends AbstractUseCase<MiniConsumerRemainingUnpaidPaySuccessUseCase.InputData, MiniConsumerRemainingUnpaidPaySuccessUseCase.OutputData> {

    private final PayOrderService payOrderService;

    private final WechatPayService wechatPayService;

    private final ServiceOrderService serviceOrderService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long userId = inputData.getUserId();
        Long payOrderId = inputData.getPayOrderId();

        PayOrder payOrder = payOrderService.getByIdEnsure(payOrderId);

        if (!Objects.equals(userId, payOrder.getUserId())) {
            throw new IllegalArgumentException("用户不匹配");
        }

        if (payOrder.getPayOrderStatus().equals(PayOrderStatus.UNPAID)) {
            // 查询微信支付状态
            WxPayOrderQueryV3Result result = wechatPayService.findByPayOrderId(payOrderId);

            if (result != null && "SUCCESS".equals(result.getTradeState())) {
                // 更新支付订单状态
                serviceOrderService.payRemainingUnpaid(payOrder.getObjectId(), payOrderId);
            }
        }

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        @Positive
        private Long userId;

        /**
         * 支付订单号
         */
        @NotNull(message = "支付订单号不能为空")
        @Positive
        private Long payOrderId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
