package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.handler.staff.StaffDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingStaffAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingStaffAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.staff.HousekeepingStaff;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffViewUseCase extends AbstractUseCase<HousekeepingStaffViewUseCase.InputData, HousekeepingStaffViewUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final StaffDataFactory staffDataFactory;
    private final HousekeepingStaffAuditRecordService housekeepingStaffAuditRecordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        HousekeepingStaff housekeepingStaff = housekeepingStaffService.getById(inputData.getId()).orElseThrow(() -> new RuntimeException("门店不存在"));

        // 公司名称
        staffDataFactory.assembleCompany(housekeepingStaff);

        // 城市名称
        staffDataFactory.assembleCityName(housekeepingStaff);

        // 身份证照片名称
        staffDataFactory.assembleAttachment(housekeepingStaff);

        // 查询审核记录
        List<HousekeepingStaffAuditRecord> auditRecordList = housekeepingStaffAuditRecordService.findByStaffId(housekeepingStaff.getId());

        return OutputData.builder()
                .staff(housekeepingStaff)
                .auditRecordList(auditRecordList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingStaff staff;

        private List<HousekeepingStaffAuditRecord> auditRecordList;
    }
}
