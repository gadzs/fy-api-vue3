package cn.turboinfo.fuyang.api.domain.admin.usecase.account;

import cn.turboinfo.fuyang.api.domain.common.service.account.CompanyAccountService;
import cn.turboinfo.fuyang.api.domain.common.service.wechat.WechatPayService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.AccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.account.RelationType;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccountCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Slf4j
@RequiredArgsConstructor
@Component
public class CompanyAccountCreateUseCase extends AbstractUseCase<CompanyAccountCreateUseCase.InputData, CompanyAccountCreateUseCase.OutputData> {
    private final CompanyAccountService companyAccountService;

    private final WechatPayService wechatPayService;

    @Override
    protected OutputData doAction(InputData inputData) {

        CompanyAccountCreator.Builder builder = CompanyAccountCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, CompanyAccountCreator.class, inputData);

        CompanyAccount companyAccount = companyAccountService.save(builder
                .withType(AccountType.MERCHANT_ID)
                .withRelationType(RelationType.BRAND)
                .withCustomRelation(StringUtils.EMPTY)
                .withStatus(AccountStatus.INIT)
                .build());

        // 微信支付 添加分账账户
        try {
            wechatPayService.addProfitSharingReceiver(companyAccount.getAccount(), companyAccount.getName(), companyAccount.getType().getCode(), companyAccount.getRelationType().getCode());

        } catch (RuntimeException e) {
            // 删除账户
            companyAccountService.deleteById(companyAccount.getId());
            log.error("添加分账账户失败", e);
            throw e;
        }

        return OutputData.builder()
                .companyAccount(companyAccount)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "企业ID不能为空"
        )
        @Positive
        private Long companyId;

        private AccountType type;

        @NotBlank(
                message = "账户不能为空"
        )
        private String account;

        @NotBlank(
                message = "账户名称不能为空"
        )
        private String name;

        private RelationType relationType;

        private String customRelation;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private CompanyAccount companyAccount;
    }
}
