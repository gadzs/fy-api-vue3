package cn.turboinfo.fuyang.api.gateway.admin.controller.cms;

import cn.turboinfo.fuyang.api.domain.admin.service.cms.CmsArticleService;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsArticle;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategory;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategoryCreator;
import cn.turboinfo.fuyang.api.entity.admin.pojo.cms.CmsCategoryUpdater;
import cn.turboinfo.fuyang.api.gateway.admin.controller.BaseController;
import cn.turboinfo.fuyang.api.gateway.admin.fo.cms.CmsCategoryCreateFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.cms.CmsCategoryUpdateFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/cms/category")
@Slf4j
@RequiredArgsConstructor
public class CmsCategoryAdminController extends BaseController {

    private final cn.turboinfo.fuyang.api.domain.admin.service.cms.CmsCategoryService cmsCategoryService;
    private final CmsArticleService cmsArticleService;

    @RequiresPermissions("cms:category:list")
    @GetMapping("/list")
    public RestResponseFO list() {
        return RestResponseFO.ok(cmsCategoryService.findTopTree());
    }

    @RequiresPermissions("cms:category:list")
    @GetMapping("/categoryTypes")
    public RestResponseFO CmsCategoryTypes() {
        return RestResponseFO.ok(CmsCategoryType.getList());
    }

    @RequiresPermissions("cms:category:list")
    @GetMapping("/get/{id}")
    public RestResponseFO get(@PathVariable("id") Long id) {
        CmsCategory cmsCategory = cmsCategoryService.getByIdEnsure(id);
        Optional<CmsCategory> parent = cmsCategoryService.getById(cmsCategory.getParentId());
        parent.ifPresent(value -> cmsCategory.setParentName(value.getName()));
        return RestResponseFO.ok(cmsCategory);
    }

    @RequiresPermissions("cms:category:create")
    @PostMapping("/create")
    public RestResponseFO create(@Valid CmsCategoryCreateFO fo) {
        CmsCategory cmsCategoryData = cmsCategoryService.getCategoryByCode(fo.getCode());
        if (cmsCategoryData != null) {
            return RestResponseFO.error("栏目编码重复，请重新输入");
        }
        CmsCategoryCreator.Builder builder = CmsCategoryCreator.builder();
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, CmsCategoryCreator.class, fo);
        CmsCategory cmsCategory = cmsCategoryService.save(builder.build());
        return RestResponseFO.ok(cmsCategory);
    }

    @RequiresPermissions("cms:category:update")
    @PostMapping("/update")
    public RestResponseFO update(@Valid CmsCategoryUpdateFO fo) {
        CmsCategory cmsCategoryData = cmsCategoryService.getCategoryByCode(fo.getCode());
        if (cmsCategoryData != null && !cmsCategoryData.getId().equals(fo.getId())) {
            return RestResponseFO.error("栏目编码重复，请重新输入");
        }
        CmsCategoryUpdater.Builder builder = CmsCategoryUpdater.builder(fo.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CmsCategoryUpdater.class, fo);
        if (fo.getParentId() == null) {
            builder.withParentId(0L);
        }
        CmsCategory cmsCategory = cmsCategoryService.update(builder.build());
        return RestResponseFO.ok(cmsCategory);
    }

    @RequiresPermissions("cms:category:delete")
    @PostMapping("/delete/{id}")
    @ResponseBody
    public RestResponseFO delete(@PathVariable("id") Long id) {

        List<CmsCategory> children = cmsCategoryService.listByParent(id);
        List<Long> childrenIds = children.stream().map(CmsCategory::getId).collect(Collectors.toList());
        if (childrenIds.size() > 0) {
            return RestResponseFO.error("栏目下有子集，无法删除");
        }
        childrenIds.add(id);
        List<CmsArticle> articles = cmsArticleService.findAllArticleByCategory(childrenIds);
        if (articles.size() > 0) {
            return RestResponseFO.error("栏目下有文章，无法删除");
        }
        CmsCategory cmsCategory = cmsCategoryService.getByIdEnsure(id);
        cmsCategoryService.deleteById(cmsCategory.getId());
        return RestResponseFO.ok();
    }
}
