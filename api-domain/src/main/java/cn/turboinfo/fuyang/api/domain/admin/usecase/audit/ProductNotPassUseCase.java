package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecordCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author gadzs
 * 产品服务审核不通过usecase
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ProductNotPassUseCase extends AbstractUseCase<ProductNotPassUseCase.InputData, ProductNotPassUseCase.OutputData> {
    private final ProductService productService;
    private final ProductAuditRecordService productAuditRecordService;
    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val auditorId = inputData.getUserId();

        val product = productService.getByIdEnsure(inputData.getId());
        if (product.getStatus() != ProductStatus.DEFAULT) {
            throw new ProductException("数据状态不是待审核，操作失败");
        }

        productService.updateStatus(product.getId(), ProductStatus.NOT_PASS);

        // 更新审核记录
        {
            val auditorUser = sysUserService.getByIdEnsure(auditorId);

            ProductAuditRecordCreator productAuditRecord = ProductAuditRecordCreator.builder()
                    .withProductId(product.getId())
                    .withCompanyId(product.getCompanyId())
                    .withShopId(product.getShopId())
                    .withAuditStatus(AuditStatus.NOT_PASS)
                    .withUserId(auditorId)
                    .withUserName(auditorUser.getUsername())
                    .withRemark(inputData.getRemark())
                    .build();
            productAuditRecordService.save(productAuditRecord);
        }

        return OutputData.builder()
                .id(product.getId())
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotNull(
                message = "审核人不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "失败原因不能为空"
        )
        private String remark;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Long id;
    }

}
