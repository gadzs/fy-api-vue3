package cn.turboinfo.fuyang.api.domain.admin.usecase.staff;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.staff.HousekeepingStaffService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingStaffDeleteUseCase extends AbstractUseCase<HousekeepingStaffDeleteUseCase.InputData, HousekeepingStaffDeleteUseCase.OutputData> {
    private final HousekeepingStaffService housekeepingStaffService;
    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val staffOptional = housekeepingStaffService.getById(inputData.getId());
        housekeepingStaffService.deleteById(inputData.getId());
        //更新企业家政员数量
        housekeepingCompanyService.updateStaffNum(staffOptional.get().getCompanyId(), housekeepingStaffService.countByCompanyId(staffOptional.get().getCompanyId()));

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;

        private Long companyId;
    }

    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
