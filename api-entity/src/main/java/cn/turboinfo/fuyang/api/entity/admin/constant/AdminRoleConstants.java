package cn.turboinfo.fuyang.api.entity.admin.constant;

public final class AdminRoleConstants {

    public static final String ROLE_ADMIN = "admin";

    public static final String ROLE_PLATFORM = "platform";

    public static final String ROLE_COMPANY = "company";

    public static final String ROLE_STAFF = "staff";

    public static final String ROLE_CONSUMER = "consumer";

}
