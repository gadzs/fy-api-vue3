package cn.turboinfo.fuyang.api.domain.admin.usecase.company;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.audit.AuditStatus;
import cn.turboinfo.fuyang.api.entity.common.fo.company.CompanyAuditFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompany;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * 家政企业审核查询
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingCompanyAuditQueryUseCase extends AbstractUseCase<HousekeepingCompanyAuditQueryUseCase.InputData, HousekeepingCompanyAuditQueryUseCase.OutputData> {

    private final HousekeepingCompanyService housekeepingCompanyService;

    private final HousekeepingCompanyAuditRecordService housekeepingCompanyAuditRecordService;

    private final SysUserService sysUserService;

    @Override
    protected OutputData doAction(InputData inputData) {
        String keyword = inputData.getKeyword();

        HousekeepingCompany company = housekeepingCompanyService.findByUscc(keyword).orElse(null);

        if (company == null) {
            return OutputData.builder()
                    .recordList(new ArrayList<>())
                    .build();
        }

        List<CompanyAuditFO> recordList = housekeepingCompanyAuditRecordService.findByCompanyId(company.getId())
                .stream().map(record ->
                {

                    CompanyAuditFO.CompanyAuditFOBuilder builder = CompanyAuditFO.builder()
                            .auditStatus(record.getAuditStatus())
                            .auditStatusName(AuditStatus.get(record.getAuditStatus().getValue()).getName())
                            .remark(record.getRemark())
                            .auditTime(record.getCreatedTime());

                    if (record.getUserId() != null && record.getUserId() != 0L) {
                        sysUserService.getById(record.getUserId()).ifPresent(user ->
                                builder.auditUserName(user.getUsername()));
                    }
                    return builder.build();
                })
                .toList();

        return OutputData.builder()
                .recordList(recordList)
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotBlank(
                message = "统一社会信用代码不能为空"
        )
        private String keyword;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<CompanyAuditFO> recordList;
    }

}
