package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleStatus;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.helper.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 文章
 */
@Table(
        name = "cms_article"
)
@Entity
@DynamicInsert
@DynamicUpdate
@Where(
        clause = "deleted_time = 0"
)
@Getter
@Setter
public class CmsArticlePO implements BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    /**
     * 分类
     */
    @Column(name = "category_id")
    private Long categoryId;

    /**
     * 文章类型
     */
    private CmsArticleType type;

    /**
     * 文章状态
     */
    private CmsArticleStatus status;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subtitle;

    /**
     * 作者
     */
    private String author;

    /**
     * 公司
     */
    private String company;

    /**
     * 链接
     */
    @Column(
            name = "linked_url"
    )
    private String linkedUrl;

    /**
     * 标题图片
     */
    @Column(name = "title_image_id")
    private Long titleImageId;

    /**
     * 附件
     */
    @Column(name = "attach_id")
    private Long attachId;

    /**
     * 摘要
     */
    @Column(
            name = "abstract_content"
    )
    private String abstractContent;

    /**
     * 内容
     */
    private String content;

    /**
     * 发布时间
     */
    @Column(
            name = "published_time"
    )
    private LocalDateTime publishedTime;

    /**
     * 访问次数
     */
    @Column(
            name = "visit_count"
    )
    private Long visitCount;

    /**
     * 排序
     */
    private Integer sort;

    @Column(
            name = "deleted_time"
    )
    private Long deletedTime;

    @Column(
            name = "created_time",
            nullable = false,
            updatable = false
    )
    private LocalDateTime createdTime;

    @Column(
            name = "updated_time",
            nullable = false
    )
    private LocalDateTime updatedTime;

    @PrePersist
    public void onCreate() {
        if (this.getCreatedTime() == null) {
            createdTime = LocalDateTime.now();
        }
        if (this.getUpdatedTime() == null) {
            updatedTime = LocalDateTime.now();
        }
    }

    @PreUpdate
    public void onUpdate() {
        updatedTime = LocalDateTime.now();
    }
}
