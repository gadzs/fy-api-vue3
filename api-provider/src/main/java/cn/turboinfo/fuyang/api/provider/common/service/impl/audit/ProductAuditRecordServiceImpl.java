package cn.turboinfo.fuyang.api.provider.common.service.impl.audit;

import cn.turboinfo.fuyang.api.domain.common.service.audit.ProductAuditRecordService;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.ProductAuditRecord;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.ProductAuditRecordDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.audit.ProductAuditRecordPO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import nxcloud.foundation.core.data.support.annotation.EnableSoftDelete;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Service
@EnableSoftDelete
public class ProductAuditRecordServiceImpl extends DefaultQServiceImpl<ProductAuditRecord, Long, ProductAuditRecordPO, ProductAuditRecordDAO> implements ProductAuditRecordService {
    @Override
    public List<ProductAuditRecord> findByProductId(Long productId) {
        return convertStreamQBeanToList(dao.findByProductIdOrderByIdDesc(productId).stream());
    }
}
