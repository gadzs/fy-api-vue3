package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleStatus;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import lombok.Data;
import net.sunshow.toolkit.core.qbean.api.annotation.QField;
import net.sunshow.toolkit.core.qbean.api.enums.Control;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;

import java.time.LocalDateTime;

@QField
@Data
public class CmsArticleSearchFO {
    @QField(
            sortable = true,
            placeholder = "ID"
    )
    private Long id;

    @QField(placeholder = "分类ID", control = Control.SELECT, ref = "categoryList", template = "vo")
    private Long categoryId;

    @QField(searchable = false)
    private String categoryCodes;

    @QField(
            placeholder = "文章类型",
            control = Control.SELECT,
            ref = "articleTypeList",
            template = "enum"
    )
    private CmsArticleType type;

    @QField(
            placeholder = "文章状态",
            control = Control.SELECT,
            ref = "articleStatusList",
            template = "enum"
    )
    private CmsArticleStatus status;

    @QField(
            placeholder = "标题",
            operator = Operator.LIKE
    )
    private String title;

    @QField(
            placeholder = "副标题",
            operator = Operator.LIKE
    )
    private String subtitle;

    @QField(
            placeholder = "作者",
            operator = Operator.LIKE
    )
    private String author;

    @QField(
            placeholder = "公司",
            operator = Operator.LIKE
    )
    private String company;

    @QField(
            placeholder = "链接",
            operator = Operator.LIKE
    )
    private String linkedUrl;

    @QField(
            placeholder = "摘要",
            operator = Operator.LIKE
    )
    private String abstractContent;

    @QField(
            placeholder = "内容",
            operator = Operator.LIKE
    )
    private String content;

    @QField(
            placeholder = "发布时间"
    )
    private LocalDateTime publishedTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "创建时间"
    )
    private LocalDateTime createdTime;

    @QField(
            control = Control.DATETIME,
            sortable = true,
            placeholder = "更新时间"
    )
    private LocalDateTime updatedTime;
}
