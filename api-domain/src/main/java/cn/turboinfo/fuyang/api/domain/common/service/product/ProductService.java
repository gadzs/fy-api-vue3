package cn.turboinfo.fuyang.api.domain.common.service.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductUpdater;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 产品服务
 */
public interface ProductService extends BaseQService<Product, Long> {

    Product save(ProductCreator creator) throws ProductException;

    Product update(ProductUpdater updater) throws ProductException;

    void updateStatus(Long id, ProductStatus productStatus) throws ProductException;

    void updateCreditScore(Long id, BigDecimal creditScore);

    void updateOrderNum(Long id, Long orderNum);

    /**
     * 根据店铺id集合查询所有产品
     */
    List<Product> findByShopIdCollection(Collection<Long> shopIdCollection);

    List<Product> findByCompanyId(Long companyId);

    long countByCompanyId(Long companyId);

    /**
     * 计算多个产品到指定点的距离
     */
    Map<Long, BigDecimal> calculateDistance(Collection<Long> productIdCollection, BigDecimal longitude, BigDecimal latitude);

    List<Product> findByNameContaining(String name);

    List<Product> findByCategoryId(Long categoryId);
}
