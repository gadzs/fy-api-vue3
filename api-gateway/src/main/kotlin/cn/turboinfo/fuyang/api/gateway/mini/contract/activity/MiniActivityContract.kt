package cn.turboinfo.fuyang.api.gateway.mini.contract.activity

import cn.turboinfo.fuyang.api.domain.mini.usecase.activity.MiniViewActivityUseCase
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@MiniResponseBodyWrapper
@AutoMappingContract(paths = ["/mini/activity"])
interface MiniActivityContract {

    /**
     * 活动详情
     */
    @AutoMappingContract(
        beanType = MiniViewActivityUseCase::class,
        paths = ["/view/{activityId}"],
        method = AutoMappingContract.Method.GET,
        consumes = [],
    )
    fun viewById()

}
