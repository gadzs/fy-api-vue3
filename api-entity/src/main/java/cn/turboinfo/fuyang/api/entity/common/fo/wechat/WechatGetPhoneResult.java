package cn.turboinfo.fuyang.api.entity.common.fo.wechat;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WechatGetPhoneResult {


    private String unionid;

    private String errcode;

    private String errmsg;

    private WechatPhoneInfo phone_info;
}
