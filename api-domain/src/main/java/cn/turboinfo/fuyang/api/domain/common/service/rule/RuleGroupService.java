package cn.turboinfo.fuyang.api.domain.common.service.rule;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.exception.rule.RuleException;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroup;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroupCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroupUpdater;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 规则组服务
 */
public interface RuleGroupService {

    /**
     * 按规则组ID集合查找规则组列表
     *
     * @param idCollection 规则组ID集合
     * @return 规则组列表
     */
    List<RuleGroup> findByIdCollection(Collection<Long> idCollection);

    Optional<RuleGroup> getById(Long id);

    Optional<RuleGroup> getByRuleKey(String ruleKey);

    RuleGroup save(RuleGroupCreator creator) throws RuleException;

    RuleGroup update(RuleGroupUpdater updater) throws RuleException;

    void deleteById(Long id) throws RuleException;

    /**
     * 根据关联信息获取规则组
     *
     * @param objectType 关联对象类型
     * @param objectId   关联对象编码
     * @return
     */
    List<RuleGroup> findByRelObjectId(EntityObjectType objectType, Long objectId);
}
