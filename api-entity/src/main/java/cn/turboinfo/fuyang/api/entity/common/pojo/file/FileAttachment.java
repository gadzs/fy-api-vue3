package cn.turboinfo.fuyang.api.entity.common.pojo.file;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 文件附件
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class FileAttachment extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 引用业务类型
     */
    private String refType;

    /**
     * 引用业务ID
     */
    private Long refId;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 相对路径
     */
    private String relativePath;

    /**
     * 原始文件名
     */
    private String originalFilename;

    /**
     * 外部访问 url
     */
    private String externalUrl;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

    private String customData;

    public ViewAttachment toView() {
        return ViewAttachment
                .builder()
                .fileAttachmentId(getId())
                .displayName(getDisplayName())
                .build();
    }
}
