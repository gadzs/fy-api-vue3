package cn.turboinfo.fuyang.api.gateway.share.contract.staff

import cn.turboinfo.fuyang.api.domain.share.usecase.staff.ShareAddStaffUseCase
import cn.turboinfo.fuyang.api.domain.share.usecase.staff.ShareViewStaffUseCase
import cn.turboinfo.fuyang.api.gateway.share.framework.http.ShareResponseBodyWrapper
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@ShareResponseBodyWrapper
@AutoMappingContract(paths = ["/share/staff"])
interface ShareStaffContract {

    // 上传家政员
    @AutoMappingContract(
        beanType = ShareAddStaffUseCase::class,
    )
    fun add()

    // 家政员详情
    @AutoMappingContract(
        beanType = ShareViewStaffUseCase::class,
    )
    fun view()

}
