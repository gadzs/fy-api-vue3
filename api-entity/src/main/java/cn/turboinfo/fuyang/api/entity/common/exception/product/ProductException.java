package cn.turboinfo.fuyang.api.entity.common.exception.product;

public class ProductException extends RuntimeException {
    public ProductException() {
        this("操作 HousekeepingCompany 出错");
    }

    public ProductException(String message) {
        super(message);
    }

    public ProductException(Throwable cause) {
        super(cause);
    }

    public ProductException(String message, Throwable cause) {
        super(message, cause);
    }
}
