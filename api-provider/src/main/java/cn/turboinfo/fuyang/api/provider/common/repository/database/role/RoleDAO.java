package cn.turboinfo.fuyang.api.provider.common.repository.database.role;

import net.sunshow.toolkit.core.qbean.helper.repository.BaseRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RoleDAO extends BaseRepository<RolePO, Long> {

    Optional<RolePO> findByCode(String code);

    List<RolePO> findAllByOrderByIdAsc();

    List<RolePO> findByCodeInOrderByIdAsc(Collection<String> codeCollection);
}
