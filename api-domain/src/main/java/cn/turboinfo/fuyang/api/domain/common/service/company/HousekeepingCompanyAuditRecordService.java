package cn.turboinfo.fuyang.api.domain.common.service.company;

import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuditRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 企业审核记录服务
 * author: hai
 */
public interface HousekeepingCompanyAuditRecordService extends BaseQService<HousekeepingCompanyAuditRecord, Long> {

    List<HousekeepingCompanyAuditRecord> findByCompanyId(Long companyId);

}
