package cn.turboinfo.fuyang.api.entity.common.pojo.company;

import cn.turboinfo.fuyang.api.entity.common.enumeration.company.CompanyStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 家政公司
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class HousekeepingCompany extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司短名
     */
    private String shortName;

    /**
     * 统一社会信用代码
     */
    private String uscc;

    /**
     * 成立日期
     */
    private LocalDate establishmentDate;

    /**
     * 省id
     */
    private Long provinceCode;

    private String provinceName;

    /**
     * 市id
     */
    private Long cityCode;

    private String cityName;

    /**
     * 区id
     */
    private Long areaCode;

    private String areaName;

    /**
     * 注册地址
     */
    private String registeredAddress;

    /**
     * 注册资本
     */
    private BigDecimal registeredCapital;

    /**
     * 公司类型
     */
    private String companyType;

    /**
     * 法人
     */
    private String legalPerson;

    /**
     * 法人身份证
     */
    private String legalPersonIdCard;

    /**
     * 工商注册号
     */
    private String businessRegistrationNumber;

    /**
     * 经营范围
     */
    private String businessScope;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 加密联系电话
     */
    private String contactMobileEncrypt;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 员工人数
     */
    private String employeesNumber;

    /**
     * 企业状态
     */
    private CompanyStatus status;

    /**
     * 营业执照
     */
    private Long businessLicenseFile;

    private String businessLicenseFileName;

    /**
     * 法定代表人身份证
     */
    private Long legalPersonIdCardFile;

    private String legalPersonIdCardFileName;

    /**
     * 银行开户证明
     */
    private Long bankAccountCertificateFile;

    private String bankAccountCertificateFileName;

    /**
     * 审核时间
     */
    private LocalDateTime reviewTime;

    /**
     * 审核人
     */
    private Long reviewUserId;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 家政员数
     */
    private Long staffNum;

    /**
     * 产品服务数
     */
    private Long productNum;

    /**
     * 门店数
     */
    private Long shopNum;

    /**
     * 认证标签
     */
    private List<HousekeepingCompanyAuthLabel> authLabelList;

    /**
     * 认证标签权重
     */
    private Long authLabelWeight;

    /**
     * 家政公司类型
     */
    private String houseKeepingType;

    private String appId;

    private String appSecret;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
