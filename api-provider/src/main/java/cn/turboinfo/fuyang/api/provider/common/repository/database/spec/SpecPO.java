package cn.turboinfo.fuyang.api.provider.common.repository.database.spec;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 规格
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(
        name = "spec"
)
@Entity
@Where(clause = "deleted = 0")
public class SpecPO extends SoftDeleteFYEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 父级 ID
     */
    private Long parentId;

    /**
     * 规格组编码
     */
    private Long specSetId;

    /**
     * 排序值
     */
    private Integer sortValue;

    /**
     * 是否允许删除
     */
    private YesNoStatus allowDelete;
}
