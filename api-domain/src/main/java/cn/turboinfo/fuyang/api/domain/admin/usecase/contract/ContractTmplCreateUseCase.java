package cn.turboinfo.fuyang.api.domain.admin.usecase.contract;

import cn.turboinfo.fuyang.api.domain.common.service.contract.ContractTmplService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.service.user.UserTypeRelService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmpl;
import cn.turboinfo.fuyang.api.entity.common.pojo.contract.ContractTmplCreator;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class ContractTmplCreateUseCase extends AbstractUseCase<ContractTmplCreateUseCase.InputData, ContractTmplCreateUseCase.OutputData> {
    private final ContractTmplService contractTmplService;

    private final FileAttachmentService fileAttachmentService;

    private final UserTypeRelService userTypeRelService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long userId = inputData.getUserId();

        ContractTmplCreator.Builder builder = ContractTmplCreator.builder();

        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, ContractTmplCreator.class, inputData);

        userTypeRelService.findByUserId(userId).stream()
                .filter(userTypeRel -> userTypeRel.getUserType() == UserType.Company)
                .map(UserTypeRel::getObjectId)
                .findFirst()
                .ifPresentOrElse(builder::withCompanyId, () -> builder.withCompanyId(0L));

        ContractTmpl contractTmpl = contractTmplService.save(builder.build());

        List<Long> fileIds = inputData.getFileIds();
        if (fileIds != null && fileIds.size() > 0) {
            fileAttachmentService.updateRefId(fileIds, contractTmpl.getId());
        }

        return OutputData.builder()
                .contractTmpl(contractTmpl)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "用户编码不能为空"
        )
        private Long userId;

        @NotBlank(
                message = "合同名称不能为空"
        )
        private String name;

        @NotNull(
                message = "合同类别不能为空"
        )
        private Long categoryId;

        private List<Long> fileIds;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private ContractTmpl contractTmpl;
    }
}
