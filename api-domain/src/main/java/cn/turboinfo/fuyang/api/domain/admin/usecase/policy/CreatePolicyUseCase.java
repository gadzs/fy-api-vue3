package cn.turboinfo.fuyang.api.domain.admin.usecase.policy;

import cn.turboinfo.fuyang.api.domain.common.service.policy.PolicyService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.fo.policy.PolicyCreateFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.Policy;
import cn.turboinfo.fuyang.api.entity.common.pojo.policy.PolicyCreator;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 创建策略
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class CreatePolicyUseCase extends AbstractUseCase<CreatePolicyUseCase.InputData, CreatePolicyUseCase.OutputData> {

    private final PolicyService policyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        PolicyCreateFO createFO = inputData.getCreateFO();

        // 保存数据生成记录
        PolicyCreator.Builder builder = PolicyCreator.builder();
        QBeanCreatorHelper.copyPropertiesToCreatorBuilder(builder, PolicyCreator.class, createFO);
        Policy policy = policyService.save(builder.build());

        return OutputData.builder()
                .policy(policy)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "创建对象不能为空")
        private PolicyCreateFO createFO;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private Policy policy;

    }
}
