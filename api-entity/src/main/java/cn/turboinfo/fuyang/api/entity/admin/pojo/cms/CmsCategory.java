package cn.turboinfo.fuyang.api.entity.admin.pojo.cms;


import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 分类
 */
@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class CmsCategory extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    private String label;

    /**
     * 描述
     */
    private String description;

    /**
     * 类型
     */
    private CmsCategoryType type;

    /**
     * 是否首页展示
     */
    private YesNoStatus indexShow;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 英文简写路径
     */
    private String slugName;

    /**
     * 链接
     */
    private String linkedUrl;

    /**
     * 父级分类ID
     */
    private Long parentId;

    /**
     * 父级name
     */
    private String parentName;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

    private List<CmsCategory> children;


    public static CmsCategory toTree(Collection<CmsCategory> CmsCategoryList, Long parentId) {
        Map<Long, List<CmsCategory>> collect = CmsCategoryList.stream().collect(Collectors.groupingBy(CmsCategory::getParentId));
        CmsCategory CmsCategory = new CmsCategory();
        CmsCategory.setId(parentId);
        forEach(collect, CmsCategory);
        return CmsCategory;
    }

    public static CmsCategory toTree(Collection<CmsCategory> CmsCategoryList, Long parentId, Long CmsCategoryId) {
        Map<Long, List<CmsCategory>> collect = CmsCategoryList.stream().collect(Collectors.groupingBy(CmsCategory::getParentId));
        List<CmsCategory> list = collect.get(parentId);
        CmsCategory CmsCategory = list.stream().filter(it -> it.getId().equals(CmsCategoryId)).findAny().orElseThrow(() -> new RuntimeException("未找到对应分类"));
        forEach(collect, CmsCategory);
        return CmsCategory;
    }


    private static void forEach(Map<Long, List<CmsCategory>> collect, CmsCategory treeMenuNode) {
        treeMenuNode.setLabel(treeMenuNode.getName());
        List<CmsCategory> treeMenuNodeList = collect.get(treeMenuNode.getId());
        if (treeMenuNodeList != null) {
            //排序
            treeMenuNodeList.sort(
                    (u1, u2) -> {
                        if (u1.getSort().equals(u2.getSort())) {
                            return u2.getId().compareTo(u1.getId());
                        }
                        return u2.getSort().compareTo(u1.getSort());
                    }
            );
            treeMenuNode.setChildren(treeMenuNodeList);
            treeMenuNode.getChildren().forEach(t -> {
                t.setParentName(treeMenuNode.getName());
                forEach(collect, t);
            });
        }
    }

    /**
     * 获取指定菜单的所有子节点
     *
     * @param childTrees   子节点收集器
     * @param trees        当前菜单
     * @param parentTreeId 父菜单ID
     */
    public void treeOrgChildren(List<CmsCategory> childTrees, List<CmsCategory> trees, Long parentTreeId) {
        for (CmsCategory tree : trees) {
            if (tree.getParentId() != null && tree.getParentId().equals(parentTreeId)) {
                treeOrgChildren(childTrees, trees, tree.getId());
                childTrees.add(tree);
            }
        }
    }
}
