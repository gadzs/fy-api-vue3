-- 2023-01-29 创建分类管理权限
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('分类管理', 'category:null', '', '/category', '', 1, 0);
SET @categoryPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('分类列表', 'category:list', '', '/category/list', '', 1, @categoryPermissionParentId),
       ('新增分类', 'category:create', '', '', '', 0, @categoryPermissionParentId),
       ('更新分类', 'category:update', '', '', '', 0, @categoryPermissionParentId),
       ('删除分类', 'category:delete', '', '', '', 0, @categoryPermissionParentId);

-- 2023-01-31 创建家政企业管理权限
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('家政企业管理', 'company:null', '', '/company', '', 1, 0);
SET @companyPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('企业信息查询', 'company:list', '', '/company/list', '', 1, @companyPermissionParentId),
       ('企业注册审核', 'company:review', '', '/company/review', '', 1, 0),
       ('企业基础信息', 'company:update', '', '/company/edit', '', 1, 0),
       ('查看企业', 'company:view', '', '', '', 0, @companyPermissionParentId),
       ('删除企业', 'company:delete', '', '', '', 0, @companyPermissionParentId);
DROP TABLE IF EXISTS `housekeeping_company`;
CREATE TABLE `housekeeping_company`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT '',
    `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '公司名称',
    `uscc` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '统一社会信用代码',
    `establishment_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '成立日期',
    `province_code` BIGINT NOT NULL DEFAULT 0 COMMENT '省id',
    `city_code` BIGINT NOT NULL DEFAULT 0 COMMENT '市id',
    `area_code` BIGINT NOT NULL DEFAULT 0 COMMENT '区id',
    `registered_address` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '注册地址',
    `registered_capital` DECIMAL(15, 4)  NOT NULL DEFAULT 0 COMMENT '注册资本',
    `company_type` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '公司类型',
    `legal_person` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '法人',
    `legal_person_id_card` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '法人身份证',
    `business_registration_number` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '工商注册号',
    `business_scope` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '经营范围',
    `contact_person` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '联系人',
    `contact_number` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '联系电话',
    `email` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '邮箱',
    `employees_number` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '员工人数',
    `status` TINYINT NOT NULL DEFAULT 0 COMMENT '企业状态',
    `business_license_file` bigint(20) NOT NULL DEFAULT 0 COMMENT '营业执照',
    `legal_person_id_card_file` bigint(20) NOT NULL DEFAULT 0 COMMENT '法定代表人身份证',
    `bank_account_certificate_file` bigint(20) NOT NULL DEFAULT 0 COMMENT '银行开户证明',
    `deleted` BIGINT NOT NULL DEFAULT 0 COMMENT '',
    `review_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '审核用户',
    `review_time` TIMESTAMP NULL DEFAULT NULL COMMENT '审核时间',
    `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_uscc` (`uscc`,`deleted`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic COMMENT ='家政公司';
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('企业门店管理', 'shop:null', '', '/shop', '', 1, 0);
SET @shopPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('门店管理', 'shop:list', '', '/shop/list', '', 1, @shopPermissionParentId);
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('企业门店管理', 'shop:admin:list', '', '/shop/adminList', '', 1, @shopPermissionParentId);

INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES    ('修改门店', 'shop:update', '', '', '', 0, @shopPermissionParentId),
       ('查看门店', 'shop:view', '', '', '', 0, @shopPermissionParentId),
       ('删除门店', 'shop:delete', '', '', '', 0, @shopPermissionParentId);
DROP TABLE IF EXISTS `housekeeping_shop`;
CREATE TABLE `housekeeping_shop`
(
    `id`             BIGINT       NOT NULL AUTO_INCREMENT COMMENT '',
    `company_id`     BIGINT       NOT NULL DEFAULT 0 COMMENT '所属企业',
    `name`           VARCHAR(128) NOT NULL DEFAULT '' COMMENT '门店名称',
    `found_date`     TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '成立日期',
    `province_code`  BIGINT       NOT NULL DEFAULT 0 COMMENT '省编码',
    `city_code`      BIGINT       NOT NULL DEFAULT 0 COMMENT '市编码',
    `area_code`      BIGINT       NOT NULL DEFAULT 0 COMMENT '区编码',
    `address`        VARCHAR(128) NOT NULL DEFAULT '' COMMENT '地址',
    `longitude`      decimal(9, 6) NOT NULL DEFAULT 0 COMMENT '经度',
    `latitude`       decimal(9, 6)  NOT NULL DEFAULT 0 COMMENT '纬度',
    `contact_person` VARCHAR(30)   NOT NULL DEFAULT '' COMMENT '联系人',
    `contact_mobile` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '联系人电话',
    `business_time`  VARCHAR(128) NOT NULL DEFAULT '' COMMENT '营业时间',
    `status`         TINYINT      NOT NULL DEFAULT 0 COMMENT '状态',
    `deleted`        BIGINT       NOT NULL DEFAULT 0,
    `created_time`   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
    `updated_time`   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_company_name` (`company_id`, `name`, `deleted`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT ='家政门店';