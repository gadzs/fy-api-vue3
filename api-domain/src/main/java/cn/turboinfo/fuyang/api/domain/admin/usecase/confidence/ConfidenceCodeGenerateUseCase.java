package cn.turboinfo.fuyang.api.domain.admin.usecase.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCodeCreator;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ConfidenceCodeGenerateUseCase extends AbstractUseCase<ConfidenceCodeGenerateUseCase.InputData, ConfidenceCodeGenerateUseCase.OutputData> {
    private final ConfidenceCodeService confidenceCodeService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Integer count = inputData.getCount();

        if (count == null) {
            count = 1;
        }

        for (int i = 0; i < count; i++) {
            confidenceCodeService.save(ConfidenceCodeCreator.builder()
                    .withStatus(ConfidenceCodeStatus.DEFAULT)
                    .build());
        }

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        private Integer count;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
