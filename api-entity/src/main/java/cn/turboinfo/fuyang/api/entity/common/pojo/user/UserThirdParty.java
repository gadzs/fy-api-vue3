package cn.turboinfo.fuyang.api.entity.common.pojo.user;

import cn.turboinfo.fuyang.api.entity.common.enumeration.user.ThirdPartyType;
import lombok.Getter;
import lombok.Setter;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

@Getter
@Setter
@QBean
@QBeanCreator
@QBeanUpdater
public class UserThirdParty extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 用户编码
     */
    private Long userId;

    /**
     * 第三方类型
     */
    private ThirdPartyType thirdPartyType;

    /**
     * 第三方账户名
     */
    private String thirdPartyAccount;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
