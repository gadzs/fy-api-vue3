package cn.turboinfo.fuyang.api.entity.common.constant;

/**
 * 微信附加信息常量
 */
public class WechatAttachConstants {

    public final static String WECHAT_ATTACH_PREPAY = "WECHAT_ATTACH_PREPAY";

    public final static String WECHAT_ATTACH_REMAINING_UNPAID = "WECHAT_ATTACH_REMAINING_UNPAID";

}
