package cn.turboinfo.fuyang.api.gateway.admin.contract.agencies

import cn.turboinfo.fuyang.api.domain.admin.usecase.agencies.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/agencies"])
interface AgenciesContract {

    // 添加分类
    @PermissionScope("adminAgencies:create")
    @AutoMappingContract(
        beanType = AgenciesCreateUseCase::class,
    )
    fun create()

    // 修改分类
    @PermissionScope("adminAgencies:update")
    @AutoMappingContract(
        beanType = AgenciesUpdateUseCase::class,
    )
    fun update()


    @PermissionScope("adminAgencies:delete")
    @AutoMappingContract(
        beanType = AgenciesDeleteUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/delete/{id}"]
    )
    fun delete()

    // 检查名称是否可用
    @PermissionScope("adminAgencies:list")
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = AgenciesCheckAvailableUseCase::class,
        consumes = [],
    )
    fun checkAvailable()

    // 返回所有列表
    @PermissionScope("adminAgencies:list")
    @AutoMappingContract(
        method = AutoMappingContract.Method.GET,
        beanType = AgenciesListUseCase::class,
        consumes = [],
    )
    fun findAll()

}
