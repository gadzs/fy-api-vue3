package cn.turboinfo.fuyang.api.domain.admin.usecase.product;

import cn.turboinfo.fuyang.api.domain.common.service.company.HousekeepingCompanyService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductService;
import cn.turboinfo.fuyang.api.domain.common.service.product.ProductSkuService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductSkuStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import cn.turboinfo.fuyang.api.entity.common.exception.product.ProductException;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.ProductUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProductReviewUseCase extends AbstractUseCase<ProductReviewUseCase.InputData, ProductReviewUseCase.OutputData> {
    private final ProductService productService;
    private final ProductSkuService productSkuService;
    private final HousekeepingCompanyService housekeepingCompanyService;

    @Override
    protected OutputData doAction(InputData inputData) {
        val product = productService.getById(inputData.id).orElse(new Product());
        if (!inputData.getCompanyId().equals(product.getCompanyId())) {
            throw new ProductException("没有操作权限");
        }
        val status = inputData.getStatus();
        if (status.equals(ProductStatus.DEFAULT)) {
            throw new ProductException("产品正在审核无法操作！");
        }
        if (status.equals(ProductStatus.NOT_PASS)) {
            throw new ProductException("产品审核失败，无法操作！");
        }

        productSkuService.updateStatusByProduct(product.getId(), status.equals(ProductStatus.PUBLISHED) ? ProductSkuStatus.PUBLISHED : ProductSkuStatus.OFF_SHELF);
        ProductUpdater.Builder builder = ProductUpdater.builder(inputData.getId());
        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, ProductUpdater.class, inputData);
        productService.updateStatus(inputData.getId(), status);

        //更新企业产品数量
        housekeepingCompanyService.updateProductNum(product.getCompanyId(), productService.countByCompanyId(product.getCompanyId()));

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;

        @NotNull(
                message = "公司id不能为空"
        )
        private Long companyId;

        @NotNull(
                message = "状态不能为空"
        )
        private ProductStatus status;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
