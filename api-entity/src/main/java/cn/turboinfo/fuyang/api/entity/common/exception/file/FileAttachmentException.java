package cn.turboinfo.fuyang.api.entity.common.exception.file;

public class FileAttachmentException extends RuntimeException {
    public FileAttachmentException() {
        this("操作 FileAttachment 出错");
    }

    public FileAttachmentException(String message) {
        super(message);
    }

    public FileAttachmentException(Throwable cause) {
        super(cause);
    }

    public FileAttachmentException(String message, Throwable cause) {
        super(message, cause);
    }
}
