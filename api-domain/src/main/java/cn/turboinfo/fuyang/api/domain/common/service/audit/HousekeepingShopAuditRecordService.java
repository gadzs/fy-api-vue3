package cn.turboinfo.fuyang.api.domain.common.service.audit;

import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecord;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;

/**
 * 企业门店审核记录服务
 *
 * @author: hai
 */
public interface HousekeepingShopAuditRecordService extends BaseQService<HousekeepingShopAuditRecord, Long> {


    List<HousekeepingShopAuditRecord> findByCompanyId(Long companyId);

    List<HousekeepingShopAuditRecord> findByShopId(Long shopId);
}
