package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;


import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigItem;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 根据 dictConfigId 查询dict config item
 *
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigItemByDictConfigKeyUseCase extends AbstractUseCase<DictConfigItemByDictConfigKeyUseCase.InputData, DictConfigItemByDictConfigKeyUseCase.OutputData> {

    private final DictConfigItemService dictConfigItemService;

    @Override
    protected OutputData doAction(InputData inputData) {

        List<DictConfigItem> dictConfigItemList = dictConfigItemService.findAllDictConfigItem(inputData.dictConfigKey);

        return OutputData.builder()
                .dictConfigItemList(dictConfigItemList)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "字段key不能为空")
        private String dictConfigKey;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private List<DictConfigItem> dictConfigItemList;
    }
}
