package cn.turboinfo.fuyang.api.entity.mini.fo.my;

import cn.turboinfo.fuyang.api.entity.common.fo.order.ViewServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.pojo.credit.CreditRating;
import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MiniMySubmittedCreditRatingListItem {

    private ViewServiceOrderFO serviceOrder;

    private List<CreditRating> creditRatingList;

    private Product product;

}
