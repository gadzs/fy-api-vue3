package cn.turboinfo.fuyang.api.entity.common.fo.portal;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 门户家政员工展示
 *
 * @author hai
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PortalStaffDisplayFO {


    /**
     * 员工姓名
     */
    private String name;

    /**
     * 展示名称
     */
    private String displayName;
    private String qrCodeUrl;

    /**
     * 公司id
     */
    private String companyName;

    /*
     * 身份证
     */
    private String idCard;

    /**
     * 性别
     */
    private GenderType gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 个人照片
     */
    private Long photoFile;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 参加工作时间
     */
    private LocalDate employmentDate;

    /**
     * 工作经验年数
     */
    private Integer seniority;

    /**
     * 家政员籍贯
     */
    private String provinceName;

    /**
     * 家政员类型
     */
    private String staffType;
}
