package cn.turboinfo.fuyang.api.domain.common.handler.rule;

import cn.turboinfo.fuyang.api.entity.common.pojo.rule.RuleGroup;

public interface IRuleGroupObjectFilter extends IRuleGroupFilter {

    /**
     * 按照规则组对单个对象进行过滤
     *
     * @param ruleGroup     规则组定义
     * @param controlObject 规则组对应的控制对象
     */
    Object filterObject(Object element, RuleGroup ruleGroup, Object controlObject);

}
