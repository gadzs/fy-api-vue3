package cn.turboinfo.fuyang.api.domain.admin.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Stack;

@Slf4j
@RequiredArgsConstructor
@Component
public class CategoryAncestorsUpdateUseCase extends AbstractUseCase<CategoryAncestorsUpdateUseCase.InputData, CategoryAncestorsUpdateUseCase.OutputData> {
    private final CategoryService categoryService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Stack<Category> stack = new Stack<>();

        Long id = inputData.getId();

        if (id != null && id != 0) {
            Long ancestor = id;
            while (true) {
                Category category = categoryService.getByIdEnsure(ancestor);
                stack.push(category);
                if (category.getParentId() == 0 || category.getParentId().equals(category.getId())) {
                    break;
                }
                ancestor = category.getParentId();
            }
            Collections.reverse(stack);
        }
        return OutputData.builder()
                .list(stack)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {

        private Long id;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private Collection<Category> list;
    }
}
