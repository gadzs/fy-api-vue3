package cn.turboinfo.fuyang.api.scheduler.scheduler.profit

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.profit.SchedulerServiceCustomProfitSharingPendingUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 定制服务订单分账待处理定时任务
 */
@Component
class ServiceCustomProfitSharingPendingScheduler(
    private val schedulerServiceCustomProfitSharingPendingUseCase: SchedulerServiceCustomProfitSharingPendingUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 600000)
    fun run() {
        runCatching {
            schedulerServiceCustomProfitSharingPendingUseCase.execute(
                SchedulerServiceCustomProfitSharingPendingUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "定制服务订单分账待处理订单处理失败"
            }
        }
    }

}
