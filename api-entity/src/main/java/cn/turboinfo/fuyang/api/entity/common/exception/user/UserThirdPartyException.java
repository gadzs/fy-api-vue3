package cn.turboinfo.fuyang.api.entity.common.exception.user;

public class UserThirdPartyException extends RuntimeException {
    public UserThirdPartyException() {
        this("操作 UserThirdParty 出错");
    }

    public UserThirdPartyException(String message) {
        super(message);
    }

    public UserThirdPartyException(Throwable cause) {
        super(cause);
    }

    public UserThirdPartyException(String message, Throwable cause) {
        super(message, cause);
    }
}
