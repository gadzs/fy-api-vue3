package cn.turboinfo.fuyang.api.domain.admin.usecase.user;

import cn.turboinfo.fuyang.api.domain.common.service.role.RoleService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserRoleService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRole;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.SysUser;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 后台分配角色用例
 *
 * @author sunshow
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class AdminReassignUserRoleUseCase extends AbstractUseCase<AdminReassignUserRoleUseCase.InputData, AdminReassignUserRoleUseCase.OutputData> {

    private final RoleService roleService;

    private final SysUserService sysUserService;

    private final SysUserRoleService sysUserRoleService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long sysUserId = inputData.getSysUserId();
        Long[] checkedRoleIds = MoreObjects.firstNonNull(inputData.getCheckedRoleIds(), new Long[0]);

        // 验证用户是否存在
        SysUser sysUser = sysUserService.getByIdEnsure(sysUserId);

        // 限制保留角色分配问题
        // 涉及用户类型的保留角色不允许自由分配
        Map<Long, Role> userTypeRoleIdMap = roleService.findByCodeCollection(UserType.listRoleCode()
                        // 过滤掉平台运营角色
                        .stream().filter(it -> !Objects.equals(it, UserType.Platform.getRoleCode()))
                        .collect(Collectors.toList())
                )
                .stream()
                .collect(Collectors.toMap(Role::getId, Function.identity()));

        // 不允许分配用户类型角色
        Optional<Long> shouldNotAssignRoleIdOptional = Arrays.stream(checkedRoleIds).filter(userTypeRoleIdMap::containsKey).findFirst();
        if (shouldNotAssignRoleIdOptional.isPresent()) {
            throw new RuntimeException("不允许分配的角色: " + userTypeRoleIdMap.get(shouldNotAssignRoleIdOptional.get()).getName());
        }

        // 不允许去除分配的用户类型角色
        Set<Long> checkedRoleIdSet = Sets.newHashSet(checkedRoleIds);
        // 获取已分配的角色进行判断
        Optional<Long> shouldNotRemoveRoleIdOptional = sysUserRoleService.findBySysUserId(sysUser.getId())
                .stream()
                .map(SysUserRole::getRoleId)
                .filter(userTypeRoleIdMap::containsKey)
                .filter(roleId -> !checkedRoleIdSet.contains(roleId))
                .findFirst();
        if (shouldNotRemoveRoleIdOptional.isPresent()) {
            throw new RuntimeException("不允许去除的角色: " + userTypeRoleIdMap.get(shouldNotRemoveRoleIdOptional.get()).getName());
        }

        // 实际分配角色
        sysUserRoleService.reassign(sysUser.getId(), checkedRoleIdSet);

        return OutputData.builder()
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户不能为空")
        private Long sysUserId;

        private Long[] checkedRoleIds;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

    }
}
