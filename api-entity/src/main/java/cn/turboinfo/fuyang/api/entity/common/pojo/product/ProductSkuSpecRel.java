package cn.turboinfo.fuyang.api.entity.common.pojo.product;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * sku与规格关联关系
 * author: gadzs.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class ProductSkuSpecRel extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 规格id
     */
    private Long specId;

    /**
     * skuid
     */
    private Long productSkuId;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
