package cn.turboinfo.fuyang.api.provider.common.repository.database.agencies;

import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteFYEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 机构管理
 * author: hai
 */
@Table(
        name = "agencies"
)
@EqualsAndHashCode(
        callSuper = true
)
@Data
@Where(clause = "deleted = 0")
@Entity
public class AgenciesPO extends SoftDeleteFYEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 管辖地区
     */
    private Long areaCode;
}
