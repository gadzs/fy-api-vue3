package cn.turboinfo.fuyang.api.provider.common.repository.database.product;

import cn.turboinfo.fuyang.api.provider.common.repository.database.FYRepository;

public interface ProductSkuSpecRelDAO extends FYRepository<ProductSkuSpecRelPO, Long> {

    void deleteByProductSkuId(Long productSkuId);

}
