package cn.turboinfo.fuyang.api.gateway.admin.fo.cms;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleStatus;
import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsArticleType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class CmsArticleUpdateFO {
    @NotNull(
            message = "id不能为空"
    )
    private Long id;

    @NotNull(message = "分类不能为空")
    private Long categoryId;

    @NotNull(
            message = "文章类型不能为空"
    )
    private CmsArticleType type;

    @NotBlank(
            message = "标题不能为空"
    )
    private String title;

    private CmsArticleStatus status;

    private Integer sort;

    private Integer visitCount;

    private String subtitle;

    private String author;

    private String company;

    private String linkedUrl;

    private MultipartFile titleImageFile;

    private Long titleImageId;

    private Long attachId;

    private String abstractContent;

    private String content;

    /**
     * el-date-picker控件使用value-format="yyyy-MM-dd HH:mm:ss"，传入字符串，需要加如下连个格式转换
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime publishedTime;

    private MultipartFile[] attachmentFiles;

    private Long[] deleteAttachmentIds;
}
