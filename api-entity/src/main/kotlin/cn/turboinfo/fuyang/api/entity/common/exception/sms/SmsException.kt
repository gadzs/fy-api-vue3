package cn.turboinfo.fuyang.api.entity.common.exception.sms

open class SmsException(message: String = "操作短信出错", cause: Throwable? = null) : RuntimeException(message, cause)

class SmsRateLimitedException(message: String = "发送短信超出频率限制", cause: Throwable? = null) :
    SmsException(message, cause)

class SmsVerifyUnmatchedException(message: String = "短信验证码不一致", cause: Throwable? = null) :
    SmsException(message, cause)