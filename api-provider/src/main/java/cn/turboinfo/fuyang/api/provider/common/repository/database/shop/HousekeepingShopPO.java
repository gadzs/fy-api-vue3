package cn.turboinfo.fuyang.api.provider.common.repository.database.shop;

import cn.turboinfo.fuyang.api.entity.common.enumeration.shop.ShopStatus;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 家政门店
 * author: sunshow.
 */
@Table(
        name = "housekeeping_shop"
)
@EqualsAndHashCode(
        callSuper = true
)
@Where(clause = "deleted = 0")
@Data
@Entity
@DynamicUpdate
@DynamicInsert
public class HousekeepingShopPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 企业id
     */
    @Column(
            name = "company_id"
    )
    private Long companyId;

    /**
     * 成立日期
     */
    @Column(
            name = "found_date"
    )
    private LocalDate foundDate;

    /**
     * 省编码
     */
    @Column(
            name = "province_code"
    )
    private Long provinceCode;

    /**
     * 市编码
     */
    @Column(
            name = "city_code"
    )
    private Long cityCode;

    /**
     * 区编码
     */
    @Column(
            name = "area_code"
    )
    private Long areaCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 联系人
     */
    @Column(
            name = "contact_person"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String contactPerson;

    /**
     * 联系人电话
     */
    @Column(
            name = "contact_mobile"
    )
    @Convert(converter = DataEncryptConverter.class)
    private String contactMobile;

    /**
     * 营业时间
     */
    @Column(
            name = "business_time"
    )
    private String businessTime;

    /**
     * 营业执照
     */
    @Column(
            name = "business_license_file"
    )
    private Long businessLicenseFile;

    /**
     * 状态
     */
    private ShopStatus status;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;
}
