package cn.turboinfo.fuyang.api.entity.common.pojo.product;

import cn.turboinfo.fuyang.api.entity.common.enumeration.product.ProductStatus;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.base.enums.YesNoStatus;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 产品
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class Product extends AbstractQBean {

    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    private String companyName;

    /**
     * 店铺ID
     */
    private Long shopId;

    private String shopName;

    /**
     * 分类id
     */
    private Long categoryId;

    private String categoryName;

    /**
     * 名称
     */
    private String name;

    /**
     * 产品状态
     */
    private ProductStatus status;

    /**
     * 介绍
     */
    private String description;

    /**
     * 图片id
     */
    @NotNull(
            message = "图片不能为空"
    )
    private List<Long> imgList;

    /**
     * 视频id
     */
    private List<Long> videoList;

    private List<String> videoNameList;

    /**
     * 规格组编码
     */
    private Long specSetId;
    private String specSetName;

    /**
     * sku列表
     */
    private List<ProductSku> productSkuList;

    /**
     * 移动端内容详情
     */
    private String mobileContent;

    /**
     * 信用分
     */
    private BigDecimal creditScore;

    /**
     * 订单数
     */
    private Long orderNum;

    /**
     * 当前售价(最小)
     */
    private BigDecimal minPrice;

    /**
     * 当前售价（最大）
     */
    private BigDecimal maxPrice;

    /**
     * 是否需要合同
     */
    private YesNoStatus needContract;

    /**
     * 合同模板id
     */
    private Long contractTmplId;

    public List<String> getVideoNameList() {
        if (videoNameList == null) {
            videoNameList = Lists.newArrayList();
        }
        return videoNameList;
    }

    public List<Long> getImgList() {
        if (imgList == null) {
            imgList = Lists.newArrayList();
        }
        return imgList;
    }

    public List<Long> getVideoList() {
        if (videoList == null) {
            videoList = Lists.newArrayList();
        }
        return videoList;
    }

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;

}
