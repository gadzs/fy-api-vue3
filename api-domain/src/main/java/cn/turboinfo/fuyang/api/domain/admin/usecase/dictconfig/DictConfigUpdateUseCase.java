package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfig;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.DictConfigUpdater;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigUpdateUseCase extends AbstractUseCase<DictConfigUpdateUseCase.InputData, DictConfigUpdateUseCase.OutputData> {
    private final DictConfigService dictConfigService;

    @Override
    protected OutputData doAction(InputData inputData) {

        DictConfigUpdater.Builder builder = DictConfigUpdater.builder(inputData.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, DictConfigUpdater.class, inputData);

        DictConfig dictConfig = dictConfigService.update(builder.build());

        return OutputData.builder()
                .dictConfig(dictConfig)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "字典名称不能为空"
        )
        private String dictName;

        @NotBlank(
                message = "字典 Key不能为空"
        )
        private String dictKey;

        private String description;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private DictConfig dictConfig;
    }
}
