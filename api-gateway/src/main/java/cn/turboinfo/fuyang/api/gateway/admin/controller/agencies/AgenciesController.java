package cn.turboinfo.fuyang.api.gateway.admin.controller.agencies;

import cn.turboinfo.fuyang.api.domain.admin.usecase.agencies.AgenciesSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.entity.common.pojo.dictconfig.QDictConfig;
import cn.turboinfo.fuyang.api.gateway.admin.fo.agencies.AgenciesSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/agencies")
public class AgenciesController {

    private final AgenciesSearchUseCase agenciesSearchUseCase;

    @RequiresPermissions("adminAgencies:list")
    @RequestMapping("/search")
    public LimitDataFO<Agencies> list(@Valid AgenciesSearchFO search, LimitFO page,
                                      SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QDictConfig.id + "|" + QSort.Order.ASC.name()});
        }

        AgenciesSearchUseCase.OutputData outputData = agenciesSearchUseCase.execute(AgenciesSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
