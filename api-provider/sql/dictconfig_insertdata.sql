-- 2023-01-29 企业类型
INSERT INTO `dict_config`(`dict_name`, `dict_key`, `description`)
VALUES ('企业类型', 'companyType', '');
SET
    @dictId = LAST_INSERT_ID();
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '10', '有限责任公司', '', 10);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '20', '股份有限公司', '', 20);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '30', '有限合伙企业', '', 30);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '40', '外商独资公司', '', 40);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '50', '个人独资企业', '', 50);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '60', '国有独资公司', '', 60);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictId, 'companyType', '70', '其他', '', 70);

INSERT INTO `dict_config`(`dict_name`, `dict_key`, `description`)
VALUES ('企业员工规模', 'employeeSize', '');
SET
    @employeeId = LAST_INSERT_ID();
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '10', '0-50人', '', 10);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '20', '50-100人', '', 20);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '30', '100-500人', '', 30);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '40', '500-1000人', '', 40);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '50', '1000人-5000人', '', 50);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@employeeId, 'employeeSize', '60', '5000人以上', '', 60);


-- 2023.3.8 家政员类型
INSERT INTO `dict_config`(`dict_name`, `dict_key`, `description`)
VALUES ('家政员类型', 'staffType', '');
SET
    @dictParentId = LAST_INSERT_ID();
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '10', '保姆', '', 10);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '20', '育婴师', '', 20);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '30', '月嫂', '', 30);

INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '40', '养老护理', '', 40);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '50', '保洁', '', 50);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '60', '维修', '', 60);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '70', '催乳师', '', 70);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'staffType', '80', '钟点工', '', 80);

INSERT INTO `dict_config`(`dict_name`, `dict_key`, `description`)
VALUES ('家政企业类型', 'housekeepingCompanyType', '');
SET
    @dictParentId = LAST_INSERT_ID();
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'housekeepingCompanyType', '10', '家政服务企业', '', 10);
INSERT INTO `dict_config_item`(`dict_config_id`, `dict_config_key`, `item_value`, `item_name`, `description`,
                               `item_order`)
VALUES (@dictParentId, 'housekeepingCompanyType', '20', '家政培训机构', '', 20);
