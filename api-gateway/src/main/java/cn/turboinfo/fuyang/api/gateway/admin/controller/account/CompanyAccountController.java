package cn.turboinfo.fuyang.api.gateway.admin.controller.account;

import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountExportUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.account.CompanyAccountSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.CompanyAccount;
import cn.turboinfo.fuyang.api.entity.common.pojo.account.QCompanyAccount;
import cn.turboinfo.fuyang.api.gateway.admin.fo.account.CompanyAccountExportFO;
import cn.turboinfo.fuyang.api.gateway.admin.fo.account.CompanyAccountSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/account")
public class CompanyAccountController {
    private final CompanyAccountSearchUseCase companyAccountSearchUseCase;

    private final CompanyAccountExportUseCase companyAccountExportUseCase;

    @RequiresPermissions("adminAccount:list")
    @RequestMapping("/search")
    public LimitDataFO<CompanyAccount> search(@Valid CompanyAccountSearchFO search, String companyName, LimitFO page,
                                              SortFO sort) {
        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QCompanyAccount.id + "|" + QSort.Order.DESC.name()});
        }

        CompanyAccountSearchUseCase.OutputData outputData = companyAccountSearchUseCase.execute(CompanyAccountSearchUseCase.InputData
                .builder()
                .companyName(companyName)
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());

        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }


    @RequiresPermissions("adminAccount:export")
    @RequestMapping("/exportExcel")
    public void exportExcel(@RequestBody CompanyAccountExportFO fo, HttpServletResponse response) {

        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");// 设置字符编码
        response.setHeader("Content-disposition", "attachment;filename*=UTF-8''" + URLEncoder.encode("企业账户列表" + ".xlsx", StandardCharsets.UTF_8));

        companyAccountExportUseCase.execute(CompanyAccountExportUseCase.InputData
                .builder()
                .idList(fo.getIdList())
                .response(response)
                .build());

    }

}
