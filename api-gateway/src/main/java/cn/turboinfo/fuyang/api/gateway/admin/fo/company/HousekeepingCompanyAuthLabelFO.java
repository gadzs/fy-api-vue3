package cn.turboinfo.fuyang.api.gateway.admin.fo.company;

import lombok.Builder;
import lombok.Data;

/**
 * @author gadzs
 * @description
 * @date 2023/3/6 13:26
 */
@Data
@Builder
public class HousekeepingCompanyAuthLabelFO {

    private String name;
    private Long id;
}
