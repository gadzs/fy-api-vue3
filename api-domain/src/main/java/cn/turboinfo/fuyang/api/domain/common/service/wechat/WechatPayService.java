package cn.turboinfo.fuyang.api.domain.common.service.wechat;

import com.github.binarywang.wxpay.bean.profitsharingV3.ProfitSharingResult;
import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryV3Result;
import com.github.binarywang.wxpay.bean.result.WxPayRefundQueryV3Result;
import com.github.binarywang.wxpay.bean.result.WxPayRefundV3Result;
import com.github.binarywang.wxpay.config.WxPayConfig;

public interface WechatPayService {

    /**
     * 生成预支付信息
     *
     * @param productName 商品名称
     * @param payOrderId  支付订单ID
     * @param amount      金额
     * @param payerOpenId 支付者openId
     * @param attach      附加信息
     * @return 预支付编码
     */
    String generatePrepayment(String productName, Long payOrderId, Long amount, String payerOpenId, String attach);

    /**
     * 根据订单ID查询交易信息
     *
     * @param payOrderId 订单ID
     * @return 交易信息
     */
    WxPayOrderQueryV3Result findByPayOrderId(Long payOrderId);

    /**
     * 根据交易ID查询交易信息
     *
     * @param transactionId 交易ID
     * @return 交易信息
     */
    WxPayOrderQueryV3Result findByPayOrderId(String transactionId);

    /**
     * 取消交易
     *
     * @param payOrderId 订单ID
     */
    void cancelTransaction(Long payOrderId);

    /**
     * 申请退款
     *
     * @param payOrderId   订单ID
     * @param amount       交易金额
     * @param refundAmount 退款金额
     * @param refundId     退款ID
     * @param reason       退款原因
     * @return 退款信息
     */
    WxPayRefundV3Result applyRefund(Long payOrderId, Long amount, Long refundAmount, Long refundId, String reason);

    /**
     * 根据退款ID查询退款信息
     *
     * @param refundId 退款ID
     * @return 退款信息
     */
    WxPayRefundQueryV3Result findByRefundId(Long refundId);

    /**
     * 添加分账接收者
     *
     * @param account      账号
     * @param name         名称
     * @param type         类型
     * @param relationType 关系类型
     * @return 分账接收者
     */
    void addProfitSharingReceiver(String account, String name, String type, String relationType);

    /**
     * 删除分账接收者
     *
     * @param account 账号
     * @param type    类型
     */
    void deleteProfitSharingReceiver(String account, String type);

    /**
     * 分账
     *
     * @param transactionId     交易ID
     * @param profitId          分账ID
     * @param amount            分账金额
     * @param receiverAccount   分账接收者openId
     * @param receiverName      分账接收者姓名
     * @param receiverType      分账接收者类型
     * @param profitDescription 分账描述
     * @return 分账结果
     */
    ProfitSharingResult applyProfitsharing(String transactionId, Long profitId, Long amount, String receiverAccount, String receiverName, String receiverType, String profitDescription);

    /**
     * 根据交易ID查询分账信息
     *
     * @param transactionId 交易ID
     * @param profitId      分账ID
     * @return 分账信息
     */
    ProfitSharingResult findProfitsharing(String transactionId, Long profitId);

    /**
     * 获取配置
     *
     * @return 配置
     */
    WxPayConfig getConfig();
}
