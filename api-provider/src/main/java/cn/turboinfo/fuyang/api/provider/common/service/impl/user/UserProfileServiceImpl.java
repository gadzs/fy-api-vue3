package cn.turboinfo.fuyang.api.provider.common.service.impl.user;

import cn.turboinfo.fuyang.api.domain.common.service.user.UserProfileService;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfile;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfileCreator;
import cn.turboinfo.fuyang.api.provider.common.repository.database.user.UserProfileDAO;
import cn.turboinfo.fuyang.api.provider.common.repository.database.user.UserProfilePO;
import lombok.RequiredArgsConstructor;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanCreatorHelper;
import net.sunshow.toolkit.core.qbean.helper.service.impl.DefaultQServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class UserProfileServiceImpl extends DefaultQServiceImpl<UserProfile, Long, UserProfilePO, UserProfileDAO> implements UserProfileService {

    @Override
    @Transactional
    public UserProfile save(UserProfileCreator creator) {

        UserProfilePO userProfilePO = new UserProfilePO();

        QBeanCreatorHelper.copyCreatorField(userProfilePO, creator);
        // 手动生成ID
        userProfilePO.setId(creator.getId());

        // 保存系统用户信息
        userProfilePO = dao.save(userProfilePO);

        return convertQBean(userProfilePO);
    }
}
