package cn.turboinfo.fuyang.api.entity.common.enumeration.order;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 退款状态
 */
@Slf4j
public enum ServiceOrderRefundStatus implements BaseEnum {

    INIT(0, "未退款"),

    PROCESSING(10, "退款处理中"),

    REFUND_PART(20, "部分退款"),

    SUCCESS(70, "退款成功"),

    FAILURE(80, "退款失败"),

    NO_REFUND(90, "无需退款"),

    ;

    private int value;
    private String name;

    ServiceOrderRefundStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ServiceOrderRefundStatus get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<ServiceOrderRefundStatus> list() {
        return BaseEnumHelper.getList(values());
    }

}
