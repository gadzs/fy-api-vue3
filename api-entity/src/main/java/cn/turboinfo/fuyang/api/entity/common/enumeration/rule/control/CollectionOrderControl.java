package cn.turboinfo.fuyang.api.entity.common.enumeration.rule.control;

import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.BaseEnum;
import net.sunshow.toolkit.core.base.enums.BaseEnumHelper;

import java.util.List;

/**
 * 集合排序规则控制
 */
@Slf4j
public enum CollectionOrderControl implements BaseEnum {

    // ALL(-1, "全部"),

    // DEFAULT(0, "默认"),

    Limit(10, "限制数量"),

    OrderStrategy(15, "排序策略"),

    ;

    private int value;
    private String property;
    private String name;

    CollectionOrderControl(int value, String name) {
        this.value = value;
        this.name = name;
    }

    CollectionOrderControl(int value, String property, String name) {
        this.value = value;
        this.property = property;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static CollectionOrderControl get(int value) {
        return BaseEnumHelper.getByValue(value, values());
    }

    public static List<CollectionOrderControl> list() {
        return BaseEnumHelper.getList(values());
    }

    public static List<CollectionOrderControl> listAll() {
        return BaseEnumHelper.getAllList(values());
    }
}
