package cn.turboinfo.fuyang.api.domain.admin.usecase.audit;

import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.agencies.AgenciesService;
import cn.turboinfo.fuyang.api.domain.common.service.role.RoleService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.service.user.SysUserRoleService;
import cn.turboinfo.fuyang.api.domain.common.service.user.UserProfileService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.admin.pojo.role.Role;
import cn.turboinfo.fuyang.api.entity.admin.pojo.user.SysUserRole;
import cn.turboinfo.fuyang.api.entity.common.pojo.agencies.Agencies;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.QHousekeepingShop;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserProfile;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.api.enums.Operator;
import net.sunshow.toolkit.core.qbean.api.request.QFilter;
import net.sunshow.toolkit.core.qbean.api.request.QPage;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.response.QResponse;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopAuditRecordSearchUseCase extends AbstractUseCase<HousekeepingShopAuditRecordSearchUseCase.InputData, HousekeepingShopAuditRecordSearchUseCase.OutputData> {

    private final HousekeepingShopService housekeepingShopService;
    private final UserProfileService userProfileService;
    private final AgenciesService agenciesService;
    private final SysUserRoleService sysUserRoleService;
    private final RoleService roleService;
    private final ShopDataFactory shopDataFactory;

    @Override
    protected OutputData doAction(InputData inputData) {

        QRequest request = inputData.getRequest();

        Long userId = inputData.getUserId();

        UserProfile userProfile = userProfileService.getByIdEnsure(userId);

        Long agenciesId = userProfile.getAgenciesId();

        Set<Long> roleIdSet = sysUserRoleService.findBySysUserId(userId)
                .stream()
                .map(SysUserRole::getRoleId)
                .collect(Collectors.toSet());

        Optional<Role> roleOptional = roleService.findByIdCollection(roleIdSet).stream()
                .filter(it -> "admin".equals(it.getCode()) || "platform".equals(it.getCode()))
                .findAny();

        // 如果是管理员或者平台运营 则不需要过滤
        if (roleOptional.isEmpty() && agenciesId != null && agenciesId > 0) {
            Agencies agencies = agenciesService.getByIdEnsure(agenciesId);
            List<QFilter> filterList = new ArrayList<>();

            {
                QFilter qFilter = new QFilter();
                qFilter.setValue(agencies.getAreaCode());
                qFilter.setOperator(Operator.EQUAL);
                qFilter.setField(QHousekeepingShop.provinceCode);
                filterList.add(qFilter);
            }
            {
                QFilter qFilter = new QFilter();
                qFilter.setValue(agencies.getAreaCode());
                qFilter.setOperator(Operator.EQUAL);
                qFilter.setField(QHousekeepingShop.cityCode);
                filterList.add(qFilter);
            }
            {
                QFilter qFilter = new QFilter();
                qFilter.setValue(agencies.getAreaCode());
                qFilter.setOperator(Operator.EQUAL);
                qFilter.setField(QHousekeepingShop.areaCode);
                filterList.add(qFilter);
            }
            request.filterOr(filterList.toArray(new QFilter[filterList.size()]));
        }

        QResponse<HousekeepingShop> response = housekeepingShopService.findAll(request, inputData.getRequestPage());

        List<HousekeepingShop> shopList = new ArrayList<>(response.getPagedData());

        // 拼装城市名称
        shopDataFactory.assembleAreaName(shopList);

        // 拼装图片
        shopDataFactory.assembleAttachment(shopList);

        // 拼装企业名称
        shopDataFactory.assembleCompany(shopList);

        return OutputData.builder()
                .qResponse(response)
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {

        @NotNull(message = "用户ID不能为空")
        private Long userId;

        private QRequest request;

        private QPage requestPage;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private QResponse<HousekeepingShop> qResponse;
    }
}
