package cn.turboinfo.fuyang.api.gateway.admin.controller.order;

import cn.turboinfo.fuyang.api.domain.admin.usecase.order.ServiceOrderDetailUseCase;
import cn.turboinfo.fuyang.api.domain.admin.usecase.order.ServiceOrderSearchUseCase;
import cn.turboinfo.fuyang.api.entity.admin.fo.order.ServiceOrderFO;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.user.UserType;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.QServiceOrder;
import cn.turboinfo.fuyang.api.entity.common.pojo.user.UserTypeRel;
import cn.turboinfo.fuyang.api.gateway.admin.controller.BaseController;
import cn.turboinfo.fuyang.api.gateway.admin.fo.order.ServiceOrderSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.RestResponseFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.provider.admin.component.session.AdminSessionHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author gadzs
 * @description 订单查询
 * @date 2023/2/16 13:48
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/serviceOrder")
public class ServiceOrderController extends BaseController {

    private final ServiceOrderSearchUseCase serviceOrderSearchUseCase;
    private final ServiceOrderDetailUseCase serviceOrderDetailUseCase;
    private final AdminSessionHelper sessionHelper;

    @RequiresPermissions(value = {"serviceOrder:list"}, logical = Logical.OR)
    @RequestMapping("/list")
    public LimitDataFO<ServiceOrderFO> list(@Valid ServiceOrderSearchFO search, LimitFO page, SortFO sort) {
        Long companyId = sessionHelper.getSession().getUserTypeRelList().stream()
                .filter(userTypeRel -> userTypeRel.getUserType() == UserType.Company)
                .map(UserTypeRel::getObjectId).findFirst().orElse(null);
        if (companyId != null) {
            search.setCompanyId(companyId);
        }
        QRequest request = QBeanSearchHelper.convertQRequest(search);
        if (StringUtils.hasLength(search.getStatus())) {
            String[] statusArray = search.getStatus().split(",");
            val orderStatusList = Arrays.stream(statusArray).toList().stream().map(status -> {
                return (Object) ServiceOrderStatus.get(Integer.valueOf(status));
            }).collect(Collectors.toList());
            request.filterIn("orderStatus", orderStatusList);
        }
        makeBetweenDate(request, QServiceOrder.createdTime, search.getStartDate(), search.getEndDate());

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QServiceOrder.updatedTime + "|" + QSort.Order.DESC.name()});
        }
        ServiceOrderSearchUseCase.OutputData outputData = serviceOrderSearchUseCase.execute(ServiceOrderSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

    @RequiresPermissions("serviceOrder:list")
    @GetMapping("/get/{id}")
    public RestResponseFO get(@PathVariable("id") Long id) {
        Long companyId = sessionHelper.getSession().getUserTypeRelList().stream()
                .filter(userTypeRel -> userTypeRel.getUserType() == UserType.Company)
                .map(UserTypeRel::getObjectId).findFirst().orElse(null);

        val outputData = serviceOrderDetailUseCase.execute(ServiceOrderDetailUseCase.InputData.builder().companyId(companyId).id(id).build());
        return RestResponseFO.ok(outputData.getServiceOrder());
    }
}
