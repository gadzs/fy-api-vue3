package cn.turboinfo.fuyang.api.domain.admin.usecase.category;

import cn.turboinfo.fuyang.api.domain.common.service.category.CategoryService;
import cn.turboinfo.fuyang.api.domain.common.service.file.FileAttachmentService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.Category;
import cn.turboinfo.fuyang.api.entity.common.pojo.category.CategoryUpdater;
import cn.turboinfo.fuyang.api.entity.common.pojo.file.FileAttachment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class CategoryUpdateUseCase extends AbstractUseCase<CategoryUpdateUseCase.InputData, CategoryUpdateUseCase.OutputData> {
    private final CategoryService categoryService;

    private final FileAttachmentService fileAttachmentService;

    @Override
    protected OutputData doAction(InputData inputData) {
        Long id = inputData.getId();
        Long parentId = inputData.getParentId();
        String name = inputData.getName();
        String description = inputData.getDescription();
        String displayName = inputData.getDisplayName();
        Long iconId = inputData.getIconId();
        if (parentId != null && parentId != 0) {
            // 判断父级是否存在
            categoryService.getByIdEnsure(parentId);
        }

        // 检查分类名称是否存在
        if (!categoryService.checkAvailable(id, parentId, name)) {
            throw new RuntimeException(String.format("%s 已存在", name));
        }

        CategoryUpdater.Builder builder = CategoryUpdater.builder(id);

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, CategoryUpdater.class, inputData);

        if (displayName == null) {
            builder.withDisplayName(StringUtils.EMPTY);
        }
        if (description == null) {
            builder.withDescription(StringUtils.EMPTY);
        }
        Category category = categoryService.update(builder.build());

        // 更新图标关联
        // 查询原始编码
        List<FileAttachment> origFileList = fileAttachmentService.findByRefId(category.getId());

        // 遍历需要删除的附件
        if (origFileList != null && !origFileList.isEmpty()) {
            for (FileAttachment fileAttachment : origFileList) {
                if (!fileAttachment.getId().equals(iconId)) {
                    fileAttachmentService.deleteById(fileAttachment.getId());
                }
            }
        }

        fileAttachmentService.updateRefId(iconId, category.getId());

        return OutputData.builder()
                .category(category)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        @NotBlank(
                message = "名称不能为空"
        )
        private String name;

        @NotBlank(
                message = "名称不能为空"
        )
        private String displayName;

        @NotBlank(
                message = "编码不能为空"
        )
        private String code;

        private String description;

        @NotNull(
                message = "父级 ID不能为空"
        )
        private Long parentId;

        @NotNull(
                message = "排序值不能为空"
        )
        private Integer sortValue;

        @NotNull(
                message = "图标不能为空"
        )
        private Long iconId;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private Category category;
    }
}
