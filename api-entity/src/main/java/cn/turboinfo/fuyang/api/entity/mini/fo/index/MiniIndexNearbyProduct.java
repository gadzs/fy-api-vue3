package cn.turboinfo.fuyang.api.entity.mini.fo.index;

import cn.turboinfo.fuyang.api.entity.common.pojo.product.Product;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MiniIndexNearbyProduct {

    private Product product;

    /**
     * 距离
     */
    private BigDecimal distance;

}
