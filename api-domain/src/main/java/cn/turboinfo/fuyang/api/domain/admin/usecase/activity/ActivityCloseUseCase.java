package cn.turboinfo.fuyang.api.domain.admin.usecase.activity;

import cn.turboinfo.fuyang.api.domain.common.service.activity.ActivityService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.activity.ActivityAuditStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.Activity;
import cn.turboinfo.fuyang.api.entity.common.pojo.activity.ActivityUpdater;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.sunshow.toolkit.core.base.enums.EnableStatus;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanUpdaterHelper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 关闭活动
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ActivityCloseUseCase extends AbstractUseCase<ActivityCloseUseCase.InputData, ActivityCloseUseCase.OutputData> {
    private final ActivityService activityService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Activity activity = activityService.getByIdEnsure(inputData.getId());

        if (!activity.getAuditStatus().equals(ActivityAuditStatus.PASS)) {
            throw new IllegalArgumentException("活动审核状态不正确，不能关闭活动");
        }

        ActivityUpdater.Builder builder = ActivityUpdater.builder(activity.getId());

        QBeanUpdaterHelper.copyPropertiesToUpdateBuilder(builder, ActivityUpdater.class, inputData);

        builder.withStatus(EnableStatus.DISABLED);

        activityService.update(builder.build());

        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;

    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
