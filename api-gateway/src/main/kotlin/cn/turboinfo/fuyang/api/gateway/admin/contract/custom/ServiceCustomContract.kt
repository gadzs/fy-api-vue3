package cn.turboinfo.fuyang.api.gateway.admin.contract.custom

import cn.turboinfo.fuyang.api.domain.admin.usecase.company.HousekeepingCompanyCancelServiceCustomUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.custom.ServiceCustomAssignStaffUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.custom.ServiceCustomAvailableAssignStaffListUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.custom.ServiceCustomCompanyReceiveUseCase
import cn.turboinfo.fuyang.api.domain.admin.usecase.custom.ServiceCustomDetailUseCase
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.SessionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@SessionScope
@AutoMappingContract(paths = ["/admin/company/custom"])
interface ServiceCustomContract {

    // 定制服务详情
    @PermissionScope(value = ["serviceCustom:view"])
    @CompanyScope
    @AutoMappingContract(
        beanType = ServiceCustomDetailUseCase::class,
        method = AutoMappingContract.Method.GET,
        paths = ["/view/{id}"],
        consumes = []
    )
    fun view()


    // 企业接单
    @PermissionScope(value = ["serviceCustom:view"])
    @CompanyScope
    @AutoMappingContract(
        beanType = ServiceCustomCompanyReceiveUseCase::class,
    )
    fun receive()

    // 可分配服务人员列表
    @PermissionScope(value = ["staff:list", "serviceCustom:view"])
    @CompanyScope
    @AutoMappingContract(
        beanType = ServiceCustomAvailableAssignStaffListUseCase::class,
    )
    fun availableAssignStaffList()

    // 分配服务人员
    @PermissionScope(value = ["serviceCustom:view", "serviceCustom:assign"])
    @CompanyScope
    @AutoMappingContract(
        beanType = ServiceCustomAssignStaffUseCase::class,
    )
    fun assignStaff()

    // 取消订单
    @PermissionScope(value = ["serviceCustom:view", "serviceCustom:cancel"])
    @CompanyScope
    @AutoMappingContract(
        beanType = HousekeepingCompanyCancelServiceCustomUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = []
    )
    fun cancel()
}
