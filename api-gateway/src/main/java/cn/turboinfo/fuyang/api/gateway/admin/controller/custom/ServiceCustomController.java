package cn.turboinfo.fuyang.api.gateway.admin.controller.custom;

import cn.turboinfo.fuyang.api.domain.admin.usecase.custom.ServiceCustomSearchUseCase;
import cn.turboinfo.fuyang.api.entity.common.enumeration.custom.ServiceCustomStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.QServiceCustom;
import cn.turboinfo.fuyang.api.entity.common.pojo.custom.ServiceCustom;
import cn.turboinfo.fuyang.api.gateway.admin.controller.BaseController;
import cn.turboinfo.fuyang.api.gateway.admin.fo.custom.ServiceCustomSearchFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.ResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.context.AdminRequestContext;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.SortFO;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.sunshow.toolkit.core.qbean.api.request.QRequest;
import net.sunshow.toolkit.core.qbean.api.request.QSort;
import net.sunshow.toolkit.core.qbean.helper.component.request.QBeanSearchHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Controller
@ResponseBodyWrapper
@RequestMapping("/admin/serviceCustom")
public class ServiceCustomController extends BaseController {

    private final ServiceCustomSearchUseCase serviceCustomSearchUseCase;

    @RequiresPermissions(value = {"serviceCustom:list"}, logical = Logical.OR)
    @RequestMapping("/list")
    @CompanyScope
    public LimitDataFO<ServiceCustom> list(@Valid ServiceCustomSearchFO search, LimitFO page, SortFO sort, AdminRequestContext context) {

        QRequest request = QBeanSearchHelper.convertQRequest(search);

        if (StringUtils.hasLength(search.getStatus())) {
            String[] statusArray = search.getStatus().split(",");
            val orderStatusList = Arrays.stream(statusArray)
                    .toList()
                    .stream()
                    .map(status -> (Object) ServiceCustomStatus.get(Integer.parseInt(status))).collect(Collectors.toList());
            request.filterIn(QServiceCustom.customStatus, orderStatusList);
        }

        Long companyId = context.getCompanyId();
        if (companyId != null && search.getCompanyId() == null) {
            request.filterIn(QServiceCustom.companyId, Arrays.asList(new Long[]{0L, companyId}));
        }

        if (sort.getSortFields() == null) {
            sort.setSortFields(new String[]{QServiceCustom.updatedTime + "|" + QSort.Order.DESC.name()});
        }
        ServiceCustomSearchUseCase.OutputData outputData = serviceCustomSearchUseCase.execute(ServiceCustomSearchUseCase.InputData
                .builder()
                .request(request)
                .requestPage(page.toQPage(sort.toQSortList()))
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
