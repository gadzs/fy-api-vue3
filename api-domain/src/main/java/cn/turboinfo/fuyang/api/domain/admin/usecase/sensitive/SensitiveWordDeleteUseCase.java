package cn.turboinfo.fuyang.api.domain.admin.usecase.sensitive;

import cn.turboinfo.fuyang.api.domain.common.service.sensitive.SensitiveWordService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SensitiveWordDeleteUseCase extends AbstractUseCase<SensitiveWordDeleteUseCase.InputData, SensitiveWordDeleteUseCase.OutputData> {
    private final SensitiveWordService sensitiveWordService;

    @Override
    protected OutputData doAction(InputData inputData) {

        sensitiveWordService.deleteById(inputData.getId());

        // 刷新
        sensitiveWordService.refresh();
        
        return OutputData.builder()
                .build();
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
