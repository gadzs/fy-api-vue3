package cn.turboinfo.fuyang.api.domain.admin.usecase.shop;

import cn.turboinfo.fuyang.api.domain.common.handler.shop.ShopDataFactory;
import cn.turboinfo.fuyang.api.domain.common.service.audit.HousekeepingShopAuditRecordService;
import cn.turboinfo.fuyang.api.domain.common.service.shop.HousekeepingShopService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.entity.common.pojo.audit.HousekeepingShopAuditRecord;
import cn.turboinfo.fuyang.api.entity.common.pojo.shop.HousekeepingShop;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author hai
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class HousekeepingShopViewUseCase extends AbstractUseCase<HousekeepingShopViewUseCase.InputData, HousekeepingShopViewUseCase.OutputData> {
    private final HousekeepingShopService housekeepingShopService;
    private final ShopDataFactory shopDataFactory;
    private final HousekeepingShopAuditRecordService housekeepingShopAuditRecordService;

    @Override
    protected OutputData doAction(InputData inputData) {
        HousekeepingShop housekeepingShop = housekeepingShopService.getById(inputData.getId()).orElseThrow(() -> new RuntimeException("门店不存在"));

        // 拼装城市名称
        shopDataFactory.assembleAreaName(housekeepingShop);

        // 拼装图片
        shopDataFactory.assembleAttachment(housekeepingShop);

        // 拼装企业名称
        shopDataFactory.assembleCompany(housekeepingShop);

        // 查询审核记录
        List<HousekeepingShopAuditRecord> auditRecordList = housekeepingShopAuditRecordService.findByShopId(housekeepingShop.getId());

        return OutputData.builder()
                .shop(housekeepingShop)
                .auditRecordList(auditRecordList)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long id;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {

        private HousekeepingShop shop;

        private List<HousekeepingShopAuditRecord> auditRecordList;
    }
}
