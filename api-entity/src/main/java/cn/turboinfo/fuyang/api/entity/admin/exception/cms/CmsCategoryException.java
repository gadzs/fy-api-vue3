package cn.turboinfo.fuyang.api.entity.admin.exception.cms;

public class CmsCategoryException extends RuntimeException {
    public CmsCategoryException() {
        this("操作 Category 出错");
    }

    public CmsCategoryException(String message) {
        super(message);
    }

    public CmsCategoryException(Throwable cause) {
        super(cause);
    }

    public CmsCategoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
