dependencies {
    api(project(":api-domain"))
    api(libs.toolkit.core.base.enums.converter)

    compileOnly(libs.nxcloud.starter.jpa)
    compileOnly("org.springframework.boot:spring-boot-starter-data-jpa")
    compileOnly(libs.shiro.spring)
    compileOnly("org.springframework.boot:spring-boot-starter-web")
    compileOnly("org.springframework.boot:spring-boot-starter-data-redis")
    compileOnly("org.springframework.session:spring-session-data-redis")
    compileOnly("org.springframework.boot:spring-boot-starter-amqp")

    implementation(libs.hypersistence.utils.hibernate55)
    implementation("com.squareup.okhttp3:logging-interceptor")
    implementation(libs.sensitive.word)
    
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("com.h2database:h2")

}
