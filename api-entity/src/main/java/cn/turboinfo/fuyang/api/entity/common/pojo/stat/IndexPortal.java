package cn.turboinfo.fuyang.api.entity.common.pojo.stat;

import lombok.Data;

import java.util.Map;

/**
 * @author gadzs
 * @description 门户页统计
 * @date 2023/3/8 14:28
 */
@Data
public class IndexPortal {

    private Map<String, Long> companyStat;

    private Map<String, Long> shopAreaStat;

    private int staffNum;

    private Map<String, String> staffTypeStat;
}

