package cn.turboinfo.fuyang.api.scheduler.scheduler.profit

import cn.turboinfo.fuyang.api.domain.scheduler.usecase.profit.SchedulerServiceOrderProfitSharingUseCase
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * 服务订单分账定时任务
 */
@Component
class ServiceOrderProfitSharingScheduler(
    private val schedulerServiceOrderProfitSharingUseCase: SchedulerServiceOrderProfitSharingUseCase,
) {
    private val logger = KotlinLogging.logger {}

    @Scheduled(initialDelay = 60000, fixedDelay = 3600000)
    fun run() {
        runCatching {
            schedulerServiceOrderProfitSharingUseCase.execute(
                SchedulerServiceOrderProfitSharingUseCase.InputData()
            )
        }.onFailure {
            logger.error(it) {
                "服务订单分账处理失败"
            }
        }
    }

}
