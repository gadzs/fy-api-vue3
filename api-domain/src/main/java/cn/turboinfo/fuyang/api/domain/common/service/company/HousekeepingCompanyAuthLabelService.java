package cn.turboinfo.fuyang.api.domain.common.service.company;


import cn.turboinfo.fuyang.api.entity.common.pojo.company.HousekeepingCompanyAuthLabel;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.List;
import java.util.Optional;

public interface HousekeepingCompanyAuthLabelService extends BaseQService<HousekeepingCompanyAuthLabel, Long> {

    Optional<HousekeepingCompanyAuthLabel> findByName(String name);

    List<HousekeepingCompanyAuthLabel> findList();
}
