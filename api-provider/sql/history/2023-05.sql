-- 2023-05-9 订单管理增加派单权限
SET
    @orderPermissionParentId = (select id
                                from sys_permission
                                where resource = 'order:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('订单派单', 'order:assign', '', '', '', 0, @orderPermissionParentId),
       ('订单取消', 'order:cancel', '', '', '', 0, @orderPermissionParentId);

-- 定制服务订单增加权限
SET
    @orderPermissionParentId = (select id
                                from sys_permission
                                where resource = 'serviceCustom:null');
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`)
VALUES ('订单查看', 'serviceCustom:view', '', '/custom/view', '', 0, @orderPermissionParentId),
       ('接受订单', 'serviceCustom:receive', '', '', '', 0, @orderPermissionParentId),
       ('订单派单', 'serviceCustom:assign', '', '', '', 0, @orderPermissionParentId),
       ('订单取消', 'serviceCustom:cancel', '', '', '', 0, @orderPermissionParentId);

ALTER table `housekeeping_company`
    ADD COLUMN `app_id` VARCHAR(32) NOT NULL DEFAULT '' COMMENT 'appId' AFTER `auth_label_weight`;
ALTER TABLE `housekeeping_company`
    ADD COLUMN `app_secret` VARCHAR(32) NOT NULL DEFAULT '' COMMENT 'appSecret' AFTER `app_id`;

-- 2023-05-12 门店自增id
ALTER TABLE `housekeeping_shop`
    AUTO_INCREMENT = 100000;

-- 2023-05-12 产品增加是否需要合同字段
ALTER TABLE `product`
    ADD COLUMN `need_contract` TINYINT NOT NULL DEFAULT 0 COMMENT '是否需要合同' AFTER `max_price`;
-- 2023-05-15 产品增加合同模版编码字段
ALTER TABLE `product`
    ADD COLUMN `contract_tmpl_id` BIGINT NOT NULL DEFAULT 0 COMMENT '合同模版编码' AFTER `need_contract`;
-- 合同增加状态字段
ALTER TABLE `contract`
    ADD COLUMN `status` TINYINT NOT NULL DEFAULT 0 COMMENT '状态' AFTER `company_id`;
-- 2023-05-16 合同增加合同编码字段
ALTER TABLE `contract`
    ADD COLUMN `contract_no` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '合同编码' AFTER `id`;

-- 2023-05-30 增加退款管理权限
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('退款管理', 'adminRefund:null', '', '/refund', '', 1, 0, 'el-icon-refresh-left', 46);
SET
    @refundPermissionParentId = LAST_INSERT_ID();
INSERT INTO `sys_permission` (`name`, `resource`, `url`, `component`, `component_name`, `visible_status`, `parent_id`,
                              `icon`, `sort_value`)
VALUES ('退款列表', 'adminRefund:list', '', '/refund/list', '', 1, @refundPermissionParentId, 'el-icon-refresh-left',
        0),
       ('退款查看', 'adminRefund:view', '', '/refund/view', '', 0, @refundPermissionParentId, '', 0);
