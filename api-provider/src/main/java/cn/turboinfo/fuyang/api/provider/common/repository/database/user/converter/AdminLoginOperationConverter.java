package cn.turboinfo.fuyang.api.provider.common.repository.database.user.converter;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.login.AdminLoginOperation;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: sunshow.
 */
@Converter(autoApply = true)
public class AdminLoginOperationConverter extends BaseEnumConverter<AdminLoginOperation> {
}
