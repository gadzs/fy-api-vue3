package cn.turboinfo.fuyang.api.provider.common.repository.database.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.GenderType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderCreditRatingStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderPayStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderRefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.ServiceOrderStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.profit.ProfitSharingStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.order.ServiceOrderStatusLog;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.provider.common.repository.database.SoftDeleteWithoutIdFYEntity;
import cn.turboinfo.fuyang.api.provider.common.repository.database.common.converter.DataEncryptConverter;
import io.hypersistence.utils.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 服务订单
 * author: sunshow.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(
        name = "service_order"
)
@Where(clause = "deleted = 0")
@Entity
public class ServiceOrderPO extends SoftDeleteWithoutIdFYEntity {

    @Id
    private Long id;

    /**
     * 下单人ID
     */
    private Long userId;

    /**
     * 产品分类ID
     */
    private Long categoryId;

    /**
     * 产品SKU
     */
    private Long productSkuId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 服务人员ID
     */
    private Long staffId;

    // 冗余 SKU 对应规格信息 (树型层级结构)
    @Type(type = "json")
    @Column(name = "spec_list", columnDefinition = "json")
    private List<Spec> specList;

    /**
     * 下单备注信息
     */
    private String comment;

    /**
     * 计划开始时间
     */
    private LocalDateTime scheduledStartTime;

    /**
     * 计划结束时间
     */
    private LocalDateTime scheduledEndTime;

    /**
     * 服务开始时间
     */
    private LocalDateTime serviceStartTime;

    /**
     * 服务结束时间
     */
    private LocalDateTime serviceEndTime;

    /**
     * 服务订单状态
     */
    private ServiceOrderStatus orderStatus;

    /**
     * 服务订单状态历史
     */
    @Type(type = "json")
    @Column(name = "order_status_history", columnDefinition = "json")
    private List<ServiceOrderStatusLog> orderStatusHistory;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 预付金额
     */
    private BigDecimal prepaid;

    /**
     * 额外费用 (由服务人员确认完成时添加)
     */
    private BigDecimal additionalFee;

    /**
     * 减免费用 (由服务人员确认完成时添加)
     */
    private BigDecimal discountFee;

    /**
     * 支付订单状态
     */
    private ServiceOrderPayStatus payStatus;

    /**
     * 退款订单状态
     */
    private ServiceOrderRefundStatus refundStatus;

    /**
     * 评价状态
     */
    private ServiceOrderCreditRatingStatus creditRatingStatus;

    /**
     * 分润状态
     */
    private ProfitSharingStatus profitSharingStatus;

    /**
     * 服务完成时间
     */
    private LocalDateTime completedTime;

    /**
     * 选择的地址ID
     */
    private Long addressId;

    /**
     * 联系人
     */
    @Convert(converter = DataEncryptConverter.class)
    private String contact;

    /**
     * 性别
     */
    private GenderType genderType;

    /**
     * 手机号
     */
    @Convert(converter = DataEncryptConverter.class)
    private String mobile;

    /**
     * 区域编码
     */
    private Long divisionId;

    /**
     * 兴趣点地址
     */
    private String poiName;

    /**
     * 详细地址
     */
    @Convert(converter = DataEncryptConverter.class)
    private String detail;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

}
