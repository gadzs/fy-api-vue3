package cn.turboinfo.fuyang.api.gateway.mini.controller.company;

import cn.turboinfo.fuyang.api.domain.mini.usecase.company.MiniCompanyListCreditRatingUserCase;
import cn.turboinfo.fuyang.api.entity.common.fo.credit.ViewCreditRatingFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitDataFO;
import cn.turboinfo.fuyang.api.gateway.admin.framework.http.fo.LimitFO;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.MiniResponseBodyWrapper;
import cn.turboinfo.fuyang.api.gateway.mini.framework.http.context.MiniRequestContext;
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequiredArgsConstructor
@Controller
@MiniResponseBodyWrapper
@CompanyScope
@RequestMapping("/mini/company")
public class MiniCompanyController {

    private final MiniCompanyListCreditRatingUserCase miniCompanyListCreditRatingUserCase;

    @RequestMapping("/listCreditRating")
    public LimitDataFO<ViewCreditRatingFO> listCreditRating(MiniRequestContext context, LimitFO page) {
        MiniCompanyListCreditRatingUserCase.OutputData outputData = miniCompanyListCreditRatingUserCase.execute(MiniCompanyListCreditRatingUserCase.InputData
                .builder()
                .companyId(context.getCompanyId())
                .requestPage(page.toQPage())
                .build());
        return LimitDataFO.fromQResponse(outputData.getQResponse());
    }

}
