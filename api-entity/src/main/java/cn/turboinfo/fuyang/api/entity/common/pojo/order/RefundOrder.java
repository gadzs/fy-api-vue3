package cn.turboinfo.fuyang.api.entity.common.pojo.order;

import cn.turboinfo.fuyang.api.entity.common.enumeration.common.EntityObjectType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.common.FundsAccountType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundChannelType;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundStatus;
import cn.turboinfo.fuyang.api.entity.common.enumeration.order.RefundType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sunshow.toolkit.core.qbean.api.annotation.*;
import net.sunshow.toolkit.core.qbean.api.bean.AbstractQBean;

import java.time.LocalDateTime;

/**
 * 退款管理
 * author: hai
 */
@EqualsAndHashCode(
        callSuper = true
)
@Data
@QBean
@QBeanCreator
@QBeanUpdater
public class RefundOrder extends AbstractQBean {
    @QBeanID
    @QBeanCreatorIgnore
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;
    private String companyName;

    /**
     * 微信支付订单号
     */
    private String wxTransactionId;

    /**
     * 微信退款单号
     */
    private String wxRefundId;

    /**
     * 退款对象类型
     */
    private EntityObjectType objectType;

    /**
     * 退款对象编码
     */
    private Long objectId;

    /**
     * 支付订单编码
     */
    private Long payOrderId;

    /**
     * 退款原因
     */
    private String reason;

    /**
     * 退款金额（单位分）
     */
    private Long refundAmount;

    /**
     * 订单金额（单位分）
     */
    private Long orderAmount;

    /**
     * 用户支付金额
     */
    private Long userPayAmount;

    /**
     * 用户退款金额
     */
    private Long userRefundAmount;

    /**
     * 退款渠道
     */
    private RefundChannelType refundChannel;

    /**
     * 退款入账账户
     */
    private String userReceivedAccount;

    /**
     * 退款成功时间
     */
    private LocalDateTime successTime;

    /**
     * 退款状态
     */
    private RefundStatus status;

    /**
     * 退款方式
     */
    private RefundType refundType;

    /**
     * 退款资金账户
     */
    private FundsAccountType fundsAccount;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime createdTime;

    @QBeanCreatorIgnore
    @QBeanUpdaterIgnore
    private LocalDateTime updatedTime;
}
