package cn.turboinfo.fuyang.api.domain.admin.usecase.confidence;

import cn.turboinfo.fuyang.api.domain.common.service.confidence.ConfidenceCodeService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import cn.turboinfo.fuyang.api.domain.util.QRCodeUtils;
import cn.turboinfo.fuyang.api.entity.common.enumeration.confidence.ConfidenceCodeStatus;
import cn.turboinfo.fuyang.api.entity.common.pojo.confidence.ConfidenceCode;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 查看二维码
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ConfidenceCodeQRViewUseCase extends AbstractUseCase<ConfidenceCodeQRViewUseCase.InputData, ConfidenceCodeQRViewUseCase.OutputData> {
    private final ConfidenceCodeService confidenceCodeService;

    @Override
    protected OutputData doAction(InputData inputData) {

        Long id = inputData.getId();
        HttpServletResponse response = inputData.getResponse();

        ConfidenceCode confidenceCode = confidenceCodeService.getByIdEnsure(id);
        if (confidenceCode.getStatus().equals(ConfidenceCodeStatus.DISCARD)) {
            throw new RuntimeException("放心码已作废");
        }

        try {
            QRCodeUtils.qrCodeGenerate(id.toString(), 500, inputData.getImageType(), response.getOutputStream());

        } catch (Exception e) {

            throw new RuntimeException(e);
        }

        return OutputData.builder()
                .response(response)
                .build();
    }

    @EqualsAndHashCode(
            callSuper = true
    )
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "id不能为空"
        )
        private Long id;

        private String imageType;

        @NotNull()
        private HttpServletResponse response;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
        private HttpServletResponse response;
    }
}
