package cn.turboinfo.fuyang.api.domain.admin.usecase.dictconfig;

import cn.turboinfo.fuyang.api.domain.common.service.dictconfig.DictConfigItemService;
import cn.turboinfo.fuyang.api.domain.common.usecase.AbstractUseCase;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
@Component
public class DictConfigItemDeleteUseCase extends AbstractUseCase<DictConfigItemDeleteUseCase.InputData, DictConfigItemDeleteUseCase.OutputData> {
    private final DictConfigItemService dictConfigItemService;

    @Override
    protected OutputData doAction(InputData inputData) {

        dictConfigItemService.deleteById(inputData.getDictConfigItemId());

        return OutputData.builder()
                .build();
    }

    @Getter
    @Setter
    @Builder
    public static class InputData extends AbstractUseCase.InputData {
        @NotNull(
                message = "编码不能为空"
        )
        private Long dictConfigItemId;
    }

    @Getter
    @Setter
    @Builder
    public static class OutputData extends AbstractUseCase.OutputData {
    }
}
