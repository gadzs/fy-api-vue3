package cn.turboinfo.fuyang.api.domain.common.service.spec;

import cn.turboinfo.fuyang.api.entity.common.pojo.spec.Spec;
import cn.turboinfo.fuyang.api.entity.common.pojo.spec.SpecSet;
import net.sunshow.toolkit.core.qbean.api.service.BaseQService;

import java.util.Collection;
import java.util.List;

/**
 * 规格组合服务
 */
public interface SpecSetService extends BaseQService<SpecSet, Long> {

    /**
     * @return 返回所有的带层级和排序的规格列表
     */
    List<Spec> findWithHierarchy(Long specSetId);

    /**
     * @return 返回所有的带层级和排序的规格列表
     */
    List<Spec> findWithHierarchy(Collection<Long> specSetIdCollection);

}
