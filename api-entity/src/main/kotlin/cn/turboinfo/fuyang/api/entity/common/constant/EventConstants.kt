package cn.turboinfo.fuyang.api.entity.common.constant

import cn.turboinfo.fuyang.api.entity.common.event.credit.NewCreditRatingEvent
import cn.turboinfo.fuyang.api.entity.common.event.custom.NewServiceCustomEvent
import cn.turboinfo.fuyang.api.entity.common.event.custom.ServiceCustomStatusChangedEvent
import cn.turboinfo.fuyang.api.entity.common.event.order.NewServiceOrderEvent
import cn.turboinfo.fuyang.api.entity.common.event.order.ServiceOrderStatusChangedEvent
import cn.turboinfo.fuyang.api.entity.common.event.product.ProductStatusChangedEvent
import cn.turboinfo.fuyang.api.entity.common.event.shop.ShopGeoChangedEvent

object EventConstants {

    /**
     * 有新的评价事件
     */
    const val TopicNewCreditRatingEvent = "TopicNewCreditRatingEvent"

    /**
     * 有新的服务订单事件
     */
    const val TopicNewServiceOrderEvent = "TopicNewServiceOrderEvent"

    /**
     * 服务订单状态变更事件
     */
    const val TopicServiceOrderStatusChangedEvent = "TopicServiceOrderStatusChangedEvent"

    /**
     * 门店位置变更事件
     */
    const val TopicShopGeoChangedEvent = "TopicShopGeoChangedEvent"

    /**
     * 产品状态变更事件
     */
    const val TopicProductStatusChangedEvent = "TopicProductStatusChangedEvent"

    /**
     * 有新的定制服务订单事件
     */
    const val TopicNewServiceCustomEvent = "TopicNewServiceCustomEvent"

    /**
     * 定制服务订单状态变更事件
     */
    const val TopicServiceCustomStatusChangedEvent = "TopicServiceCustomStatusChangedEvent"

    // 按照事件类型获取订阅名称
    fun getTopicName(event: Class<*>): String {
        return when (event) {
            NewCreditRatingEvent::class.java -> TopicNewCreditRatingEvent
            NewServiceOrderEvent::class.java -> TopicNewServiceOrderEvent
            ServiceOrderStatusChangedEvent::class.java -> TopicServiceOrderStatusChangedEvent
            ShopGeoChangedEvent::class.java -> TopicShopGeoChangedEvent
            ProductStatusChangedEvent::class.java -> TopicProductStatusChangedEvent
            NewServiceCustomEvent::class.java -> TopicNewServiceCustomEvent
            ServiceCustomStatusChangedEvent::class.java -> TopicServiceCustomStatusChangedEvent

            else -> throw IllegalArgumentException("未知的事件类型")
        }
    }
}
