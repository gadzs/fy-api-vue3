package cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation;

import java.lang.annotation.*;

/**
 * 需要验证消费者登录
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@SessionScope
public @interface ConsumerScope {
}
