package cn.turboinfo.fuyang.api.provider.rabbit.config

import cn.turboinfo.fuyang.api.provider.rabbit.constant.RabbitConstants
import org.springframework.amqp.core.TopicExchange
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ProviderRabbitConfig {

    @Bean
    protected fun eventExchange(): TopicExchange {
        return TopicExchange(RabbitConstants.ExchangeEvent)
    }

}