package cn.turboinfo.fuyang.api.provider.admin.repository.database.cms.converter;

import cn.turboinfo.fuyang.api.entity.admin.enumeration.cms.CmsCategoryType;
import net.sunshow.toolkit.core.base.enums.converter.BaseEnumConverter;

import javax.persistence.Converter;

/**
 * author: gadzs.
 */
@Converter(autoApply = true)
public class CategoryTypeConverter extends BaseEnumConverter<CmsCategoryType> {
}
