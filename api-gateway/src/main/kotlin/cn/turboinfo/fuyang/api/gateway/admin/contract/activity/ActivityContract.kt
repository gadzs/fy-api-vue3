package cn.turboinfo.fuyang.api.gateway.admin.contract.activity

import cn.turboinfo.fuyang.api.domain.admin.usecase.activity.*
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.CompanyScope
import cn.turboinfo.fuyang.api.gateway.web.framework.http.annotation.PermissionScope
import nxcloud.ext.springmvc.automapping.base.annotation.AutoMappingContract

@AutoMappingContract(paths = ["/admin/activity"])
interface ActivityContract {

    // 添加分类
    @PermissionScope("activity:create")
    @CompanyScope
    @AutoMappingContract(
        beanType = ActivityCreateUseCase::class,
    )
    fun create()

    // 修改分类
    @PermissionScope("activity:update")
    @CompanyScope
    @AutoMappingContract(
        beanType = ActivityUpdateUseCase::class,
    )
    fun update()


    @PermissionScope("activity:delete")
    @AutoMappingContract(
        beanType = ActivityDeleteUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/delete/{id}"]
    )
    fun delete()

    @PermissionScope(value = ["activity:view", "activity:list"])
    @AutoMappingContract(
        beanType = ActivityViewUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/view/{id}"]
    )
    fun view()

    // 审核成功
    @PermissionScope("activity:audit")
    @AutoMappingContract(
        beanType = ActivityAuditPassUseCase::class,
    )
    fun auditPass()


    // 审核失败
    @PermissionScope("activity:audit")
    @AutoMappingContract(
        beanType = ActivityAuditFailureUseCase::class,
    )
    fun auditFailure()

    // 开启活动
    @PermissionScope("activity:update")
    @AutoMappingContract(
        beanType = ActivityOpenUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/open/{id}"]
    )
    fun open()


    // 关闭活动
    @PermissionScope("activity:update")
    @AutoMappingContract(
        beanType = ActivityCloseUseCase::class,
        method = AutoMappingContract.Method.GET,
        consumes = [],
        paths = ["/close/{id}"]
    )
    fun close()

}
